﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using log4net.Core;
using Quartz;
using Unisys.AML.Common;
using Unisys.AML.DirectoryWatcher;

namespace Unisys.AML.ImportService
{
    [DisallowConcurrentExecution]
    public class CommandLineJob : QuartzJobBase
    {
        public string ProgramPath { get; set; }
        public string ProgramArguments { get; set; }
        public string FilePath { get; set; }

        Fortify fortifyService = new Fortify();

        public CommandLineJob()
        {
        }

        protected override void InternalExecute(IJobExecutionContext context)
        {
            Log.WriteToLog("Info", "CommandLineJob ProgramPath:{0} ProgramArguments:{1} FilePath:{2}", ProgramPath, ProgramArguments, FilePath);

            try
            {
                if (ProgramPath != string.Empty && ProgramPath.ToUpper().Contains("EXE"))
                {
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.StartInfo.FileName = fortifyService.PathManipulation(ProgramPath);
                    p.StartInfo.WorkingDirectory = @"D:\";
                    p.StartInfo.Arguments = fortifyService.PathManipulation(ProgramArguments + FilePath);
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardInput = true;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.RedirectStandardError = true;
                    p.StartInfo.CreateNoWindow = true;

                    //Log.WriteToLog("Info", "filename=" + p.StartInfo.FileName + ", Arguments=" + p.StartInfo.Arguments);
                    Log.WriteToLog("Info", "Added program to execute \"{0}\" argument \"{1}\".", p.StartInfo.FileName, p.StartInfo.Arguments);
                    p.Start();

                    using (StreamReader myStreamReader = p.StandardOutput)
                    {
                        string result = "";
                        while((result = ReadLineSafe(myStreamReader, int.MaxValue)) == null)
                        {
                            break;
                        }
                        p.WaitForExit();
                        p.Close();
                        Log.WriteToLog("Info", "Command finished!");
                    }
                    
                    File.Delete(FilePath);
                }
                else if (ProgramPath != string.Empty)
                {
                    Thread executionThread = new Thread(new ParameterizedThreadStart(RunPrograms));
                    ProcessStartInfo startInfo = new ProcessStartInfo(fortifyService.PathManipulation(ProgramPath));
                    startInfo.Arguments = fortifyService.PathManipulation(ProgramArguments);
                    startInfo.UseShellExecute = false;
                    startInfo.RedirectStandardInput = false;
                    ExecutionInstance executionInstance = new ExecutionInstance(startInfo, FilePath);
                    executionThread.Start(executionInstance);
                    Log.WriteToLog("Info", "Added program to execute \"{0}\" argument \"{1}\".", ProgramPath, ProgramArguments);
                }
            }
            catch (Exception ex)
            {
                LogException(ex, "CommandLineJob");
            }
        }

        public void RunPrograms(object source)
        {
            try
            {
                bool success = false;
                int retries = 100;

                Fortify fortifyService = new Fortify();

                ExecutionInstance executionInstance = (ExecutionInstance)source;
                FileStream fileStream = null;

                while (!success)
                {
                    try
                    {
                        fileStream = File.Open(executionInstance.FilePath, FileMode.Open, FileAccess.Read, FileShare.None);
                        success = true;

                        fileStream.Close();
                        fileStream.Dispose();
                    }
                    catch (Exception ex)
                    {
                        LogException(ex, string.Format("Unhandled exception occurred while waiting for \"{0}\" to become available", executionInstance.FilePath));
                        Thread.Sleep(1000);
                        if (--retries == 0)
                        {
                            LogException(ex, string.Format("Exception occurred while opening the file. File: {0}", executionInstance.FilePath));
                            break;
                        }
                    }
                }

                try
                {
                    Log.WriteToLog("Info", "Running program in response to event for {2}: {0}{1}.", executionInstance.StartInfo.FileName, (executionInstance.StartInfo.Arguments != "" ? " " + executionInstance.StartInfo.Arguments : ""), executionInstance.FilePath);
                    Process executionProcess = Process.Start(executionInstance.StartInfo);
                    executionProcess.WaitForExit();
                    executionProcess.Close();

                    string path = Path.GetDirectoryName(executionInstance.FilePath);
                    string[] filePaths = Directory.GetFiles(path);
                    foreach (string filePath in filePaths)
                    {

                        if (!filePath.ToUpper().Contains("ACTIVITY"))
                        {
                            File.Delete(filePath);
                            Log.WriteToLog("Info", "Delete {0}", filePath);
                        }
                        else
                        {
                            FileInfo finfo = new FileInfo(filePath);

                            if (finfo.Length == 0)
                            {
                                File.Delete(filePath);
                                Log.WriteToLog("Info", "empty file Deleted:{0}", filePath);
                            }
                        }
                        
                    }
                    string file = Path.GetFileName(executionInstance.FilePath);
                    if (file.ToLower().Contains("sancparty"))
                    {
                        string tmp = path + @"\..\..\Logs\OFAC\ImportSancParty.tmp";
                        if (File.Exists(tmp))
                        {
                            File.Delete(fortifyService.PathManipulation(tmp));
                            Log.WriteToLog("Info", "Delete {0}", tmp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogException(ex, string.Format("Exception occurred while running {0}", (executionInstance.StartInfo != null ? "\"" + executionInstance.StartInfo.FileName + "\"" : "")));
                }
            }
            catch (Exception ex)
            {
                LogException(ex, "Exception occurred");
            }
        }

        /// <summary>
        /// add Jul.11,2024 readline for fortify
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public string ReadLineSafe(TextReader reader, int maxLength)
        {
            var sb = new StringBuilder();
            bool bol_end = false;
            while (true)
            {
                int ch = reader.Read();
                if (ch == -1) break;
                if (ch == '\r' || ch == '\n')
                {
                    if (ch == '\r' && reader.Peek() == '\n')
                    {
                        bol_end = true;
                    }
                    else if (ch == '\r')
                    {
                        return sb.ToString();
                    }
                    if (ch == '\n')
                    {
                        return sb.ToString();
                    }
                }

                if (!bol_end)
                {
                    sb.Append((char)ch);
                }

                // Safety net here
                if (sb.Length > maxLength)
                {
                    return null;
                }
            }
            if (sb.Length > 0) return sb.ToString();
            return null;
        }

        private void Logerr(string log)
        {
            Log.WriteToLog("Error", log);
        }

        public bool LogException(Exception e, string str_FunctionName)
        {
            Logerr(string.Format("Error at Function {0}", str_FunctionName));
            Logerr(string.Format("In the log routine. Caught {0}", e.GetType()));
            Logerr(string.Format("Message: {0}", e.Message));
            return true;
        }
    }
}
