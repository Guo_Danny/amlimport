﻿using System;
using System.Diagnostics;
using System.IO;

namespace Unisys.AML.DirectoryWatcher
{
    public struct ExecutionInstance
    {
        public ProcessStartInfo StartInfo;
        public string FilePath;

        public ExecutionInstance(ProcessStartInfo startInfo, string filePath)
        {
            StartInfo = startInfo;
            FilePath = filePath;
        }
    }
}
