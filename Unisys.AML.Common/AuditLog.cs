﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace Unisys.AML.Common
{
    public class AuditLog
    {
        private static readonly ILog log = LogManager.GetLogger("Audit.Logging");

        public static void WriteToLog(string objectName, string actionDetail, string actionResult,string returnCount)
        {
            log4net.ThreadContext.Properties["dateTimeProperty"] = DateTime.Now.ToString("yyyy:MM:dd:hh:mm:ss");
            log4net.ThreadContext.Properties["objectNameProperty"] = objectName;
            log4net.ThreadContext.Properties["actionDetailProperty"] = actionDetail;
            log4net.ThreadContext.Properties["actionResultProperty"] = actionResult;
            log4net.ThreadContext.Properties["returnCountProperty"] = returnCount;
            if (log.IsInfoEnabled)
                log.Info("");
        }
    }
}
