﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using Unisys.AML.Domain;
using log4net;

namespace Unisys.AML.DataAccess
{
    public class CustomerRepository : ICustomerRepository 
    {     
        private Database database;

        public CustomerRepository()     
        {
            database = EnterpriseLibraryContainer.Current.GetInstance<Database>("PBSA"); 
        }

        public Customer GetByID(string id)     
        {
            string sql = @"SELECT * FROM [dbo].[Customer] WHERE Id=@Id";
            using (DbCommand dBCommand = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(dBCommand, "@Id", DbType.String, id);
                using (IDataReader reader = database.ExecuteReader(dBCommand))
                {
                    if (reader.Read())
                    {
                        IRowMapper<Customer> rowMapper = MapBuilder<Customer>.BuildAllProperties();
                        Customer customer = rowMapper.MapRow(reader);
                        return customer;             
                    }
                }
            }
            return null;
        }

        public DataSet CheckCustID(string id)
        {
            string sql = @"SELECT * FROM [dbo].[Customer] WHERE Id=@Id and closed = 0";
            DataSet ds = new DataSet();
            using (DbCommand dBCommand = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(dBCommand, "@Id", DbType.String, id);
                ds = database.ExecuteDataSet(dBCommand);

            }
            return ds;
        }

        public String GetAccount(string custid, string str_ref)
        {
            string AccountID = "";
            //inner join currentarchactivity c on replace(a.ref,' ','') =  replace(c.ref,' ','') 
            //inner join currentarchactivity c on (right(rtrim(a.ref),7) = right(rtrim(c.ref),7) and substring(ltrim(a.ref),1,5) = substring(ltrim(c.ref),1,5))
            string sql = @"select top 1 a.Account from currentarchactivity a
                inner join activitymap b  on a.type=b.primecode
                inner join currentarchactivity c on replace(a.ref,' ','') =  replace(c.ref,' ','')
                where a.type not in (select primecode from activitymap where origactivity like '%I%')
                  and c.type  in (select primecode from activitymap where origactivity like '%I%') 
                and a.account != c.account
                and c.Cust = @Cust
                and c.Ref = @ref";
            DataSet ds = new DataSet();
            using (DbCommand dBCommand = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(dBCommand, "@Cust", DbType.String, custid);
                database.AddInParameter(dBCommand, "@ref", DbType.String, str_ref);
                ds = database.ExecuteDataSet(dBCommand);
                
                if(ds.Tables[0].Rows.Count > 0)
                {
                    AccountID = ds.Tables[0].Rows[0]["Account"].ToString();
                }
            }

            return AccountID;
        }

        public List<Customer> GetByName(string name, string filter = null, string tin = null)
        {
            var customers = new List<Customer>();
            string sql = string.Empty;
            string sql_noncust = "";
            string str_chk_Tin = "";
            //JUL-26-2021 for the relationimport search the noncust id
            if (!string.IsNullOrEmpty(tin))
            {
                sql_noncust = "and id like 'NONCUST%' order by lastmodify desc ";
            }
            
            if (string.IsNullOrEmpty(filter))
            {
                sql = "SELECT * FROM [dbo].[Customer] WHERE name = @Name and Closed = 0 ";
            }
            else
            {
                sql = string.Format("SELECT * FROM [dbo].[Customer] WHERE {0} = {1} and Closed = 0 ",
                                    SQLReplace("name", filter),
                                    SQLReplace("@Name", filter));
            }

            sql = sql + sql_noncust;

            using (DbCommand dBCommand = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(dBCommand, "@Name", DbType.String, name);
                using (IDataReader reader = database.ExecuteReader(dBCommand))
                {
                    while (reader.Read())
                    {
                        IRowMapper<Customer> rowMapper = MapBuilder<Customer>.BuildAllProperties();
                        Customer customer = rowMapper.MapRow(reader);
                        

                        //log4net.ILog log = LogManager.GetLogger("App.Logging");
                        //log.Debug("getname= " + Convert.ToString(reader["TIN"]));
                        if (reader.GetName(12).Equals("TIN", StringComparison.InvariantCultureIgnoreCase))
                        {
                            str_chk_Tin = Convert.ToString(reader["TIN"]);
                        }

                        if (!string.IsNullOrEmpty(tin) && !string.IsNullOrEmpty(str_chk_Tin))
                        {
                            
                        }
                        else
                        {
                            customers.Add(customer);
                        }

                    }
                }

                if (customers != null && customers.Count > 0 && !string.IsNullOrEmpty(tin) && string.IsNullOrEmpty(str_chk_Tin))
                {
                    sql = "update dbo.Customer set tin = @tin, lastoper = 'primeadmin' where id = @id;";
                    dBCommand.CommandText = sql;
                    database.AddInParameter(dBCommand, "@tin", DbType.String, tin);
                    database.AddInParameter(dBCommand, "@id", DbType.String, customers[0].Id);
                    database.ExecuteScalar(dBCommand);
                }
            }

            return customers;
        }

        public List<Customer> GetByNameLike(string name)
        {
            var customers = new List<Customer>();
            string sql = @"SELECT * FROM [dbo].[Customer] WHERE Name like @Name and Closed = 0;";
            using (DbCommand dBCommand = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(dBCommand, "@Name", DbType.String, string.Format("{0}%", name));
                using (IDataReader reader = database.ExecuteReader(dBCommand))
                {
                    while (reader.Read())
                    {
                        IRowMapper<Customer> rowMapper = MapBuilder<Customer>.BuildAllProperties();
                        Customer customer = rowMapper.MapRow(reader);
                        customers.Add(customer);             
                    }
                    return customers;
                }
            }
        }

        public string GetSequence(string objectType)
        {
            string sql = @"BSA_GetNextSequence";
            using (DbCommand dBCommand = database.GetStoredProcCommand(sql))
            {
                database.AddInParameter(dBCommand, "@ObjectType", DbType.String, objectType);
                database.AddOutParameter(dBCommand, "@NextSequence", DbType.String, 35);
                database.ExecuteScalar(dBCommand);
                return database.GetParameterValue(dBCommand, "NextSequence").ToString();
            }
        }

        public void Insert(string id, string parent, string name, string ownerBranch, string ownerDept, string riskClass, 
                           string createOper, string type, int status, string indOrBusType, string address, string country, string unification)
        {
            DateTime lastActReset;
            bool result = DateTime.TryParseExact("1899/12/30", "yyyy/MM/dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastActReset);

            string sql = @"BSA_InsCustomer";
            using (DbCommand dBCommand = database.GetStoredProcCommand(sql))
            {
                database.AddInParameter(dBCommand, "@id", DbType.String, id);
                database.AddInParameter(dBCommand, "@parent", DbType.String, parent);
                database.AddInParameter(dBCommand, "@name", DbType.String, name);
                database.AddInParameter(dBCommand, "@dBA", DbType.String, null);
                database.AddInParameter(dBCommand, "@secCode", DbType.String, null);
                database.AddInParameter(dBCommand, "@address", DbType.String, string.IsNullOrEmpty(address) ? null : address);
                database.AddInParameter(dBCommand, "@city", DbType.String, null);
                database.AddInParameter(dBCommand, "@state", DbType.String, null);
                database.AddInParameter(dBCommand, "@zip", DbType.String, null);
                database.AddInParameter(dBCommand, "@country", DbType.String, country);
                database.AddInParameter(dBCommand, "@telephone", DbType.String, null);
                database.AddInParameter(dBCommand, "@email", DbType.String, null);
                database.AddInParameter(dBCommand, "@tIN", DbType.String, string.IsNullOrEmpty(unification) ? null : unification);
                database.AddInParameter(dBCommand, "@LicenseNo", DbType.String, null);
                database.AddInParameter(dBCommand, "@LicenseState", DbType.String, null);
                database.AddInParameter(dBCommand, "@passportNo", DbType.String, null);
                database.AddInParameter(dBCommand, "@dOB", DbType.Int32, null);
                database.AddInParameter(dBCommand, "@typeOfBusiness", DbType.String, null);
                database.AddInParameter(dBCommand, "@sourceOfFunds", DbType.String, null);
                database.AddInParameter(dBCommand, "@accountOfficer", DbType.String, null);
                database.AddInParameter(dBCommand, "@acctOffTel", DbType.String, null);
                database.AddInParameter(dBCommand, "@acctOffEmail", DbType.String, null);
                database.AddInParameter(dBCommand, "@compOfficer", DbType.String, null);
                database.AddInParameter(dBCommand, "@compOffTel", DbType.String, null);
                database.AddInParameter(dBCommand, "@compOffEmail", DbType.String, null);
                database.AddInParameter(dBCommand, "@idList", DbType.String, null);
                database.AddInParameter(dBCommand, "@notes", DbType.String, null);
                database.AddInParameter(dBCommand, "@exemptionStatus", DbType.String, null);
                database.AddInParameter(dBCommand, "@lastActReset", DbType.DateTime, lastActReset);
                database.AddInParameter(dBCommand, "@lastReview", DbType.DateTime, null);
                database.AddInParameter(dBCommand, "@lastReviewOper", DbType.String, null);
                database.AddInParameter(dBCommand, "@ownerBranch", DbType.String, ownerBranch);
                database.AddInParameter(dBCommand, "@ownerDept", DbType.String, ownerDept);
                database.AddInParameter(dBCommand, "@ownerOper", DbType.String, null);
                database.AddInParameter(dBCommand, "@riskClass", DbType.String, riskClass);
                database.AddInParameter(dBCommand, "@createOper", DbType.String, createOper);
                database.AddInParameter(dBCommand, "@countryOfOrigin", DbType.String, null);
                database.AddInParameter(dBCommand, "@countryOfIncorp", DbType.String, null);
                database.AddInParameter(dBCommand, "@OffshoreCorp", DbType.Int32, null);
                database.AddInParameter(dBCommand, "@BearerShares", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@NomeDePlume", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@Type", DbType.String, type);
                database.AddInParameter(dBCommand, "@Resident", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@BusRelationNature", DbType.String, null);
                database.AddInParameter(dBCommand, "@PrevRelations", DbType.String, null);
                database.AddInParameter(dBCommand, "@PEP", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@PoliticalPos", DbType.String, null);
                database.AddInParameter(dBCommand, "@FEP", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@MSB", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@CorrBankRelation", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@Sex", DbType.String, null);
                database.AddInParameter(dBCommand, "@ShellBank", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@HighRiskRespBank", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@OffshoreBank", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@PayThroughAC", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@RegulatedAffiliate", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@USPerson", DbType.String, null);
                database.AddInParameter(dBCommand, "@RFCalculate", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@Status", DbType.Int32, status);
                database.AddInParameter(dBCommand, "@ReviewDate", DbType.DateTime, null);
                database.AddInParameter(dBCommand, "@CTRAmt", DbType.Int32, 0);
                database.AddInParameter(dBCommand, "@User1", DbType.String, null);
                database.AddInParameter(dBCommand, "@User2", DbType.String, null);
                database.AddInParameter(dBCommand, "@User3", DbType.String, null);
                database.AddInParameter(dBCommand, "@User4", DbType.String, null);
                database.AddInParameter(dBCommand, "@User5", DbType.String, null);
                database.AddInParameter(dBCommand, "@SwiftTID", DbType.String, null);
                database.AddInParameter(dBCommand, "@embassy", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@ForeignGovt", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@charityOrg", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@DoCIP", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@Prospect", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@AssetSize", DbType.Int32, null);
                database.AddInParameter(dBCommand, "@Income", DbType.Int32, null);
                database.AddInParameter(dBCommand, "@IndOrBusType", DbType.String, indOrBusType);
                database.AddInParameter(dBCommand, "@ProfiledCountries", DbType.String, null);
                database.AddInParameter(dBCommand, "@OnProbation", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@ProbationReason", DbType.String, null);
                database.AddInParameter(dBCommand, "@ProbationStartDate", DbType.Int32, false);
                database.AddInParameter(dBCommand, "@ProbationEndDate", DbType.Int32, false); 
                database.AddInParameter(dBCommand, "@Closed", DbType.Boolean, false);
                database.AddInParameter(dBCommand, "@ClosedDate", DbType.Int32, null);
                database.AddInParameter(dBCommand, "@ClosedReason", DbType.String, null);
                database.AddInParameter(dBCommand, "@OpenDate", DbType.Int32, null);
                database.AddInParameter(dBCommand, "@CountryofResidence", DbType.String, null);
                database.AddInParameter(dBCommand, "@CountryofCitizenship", DbType.String, null);
                database.AddInParameter(dBCommand, "@KYCStatus", DbType.Int32, 0);
                database.AddInParameter(dBCommand, "@KYCOperator", DbType.String, null);
                database.AddInParameter(dBCommand, "@KYCDataCreateDate", DbType.DateTime, null);
                database.ExecuteScalar(dBCommand);
            }
        }

        private string SQLReplace(string sqlFeild, string regex)
        {
            foreach (char c in regex)
                sqlFeild = string.Format("replace({0},'{1}','')", sqlFeild, c);

            return sqlFeild;
        }

        public void insertlog(string str_id,string str_logtext)
        {
            string sql = "";

            sql = "insert into Event(TrnTime,Oper,Type,ObjectType,ObjectId,LogText,EvtDetail) ";
            sql += "values(getdate(), 'PRIMEADMIN', 'Imp', 'Activity', @objID, @logtxt, @evtDetail)";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@objID", str_id));
                cmd.Parameters.Add(new SqlParameter("@logtxt", str_logtext));
                cmd.Parameters.Add(new SqlParameter("@evtDetail", str_logtext));
                database.ExecuteDataSet(cmd);
            }
        }

        public void Insertlog2(string str_type, string str_objType, string str_id, string str_logtext)
        {
            string sql = "";

            sql = "insert into Event(TrnTime,Oper,Type,ObjectType,ObjectId,LogText,EvtDetail) ";
            sql += "values(getdate(), 'PRIMEADMIN', @type, @objType, @objID, @logtxt, @evtDetail)";

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@type", str_type));
                cmd.Parameters.Add(new SqlParameter("@objType", str_objType));
                cmd.Parameters.Add(new SqlParameter("@objID", str_id));
                cmd.Parameters.Add(new SqlParameter("@logtxt", str_logtext));
                cmd.Parameters.Add(new SqlParameter("@evtDetail", str_logtext));
                database.ExecuteDataSet(cmd);
            }
        }

        public void RefreshMode(string strMode, string strType)
        {
            if (strMode == "R")
            {
                string sql = string.Empty;


                if (strType.ToUpper() == "RELATION")
                {
                    sql = @" truncate table PartyRelation; ";
                }
                else if (strType.ToUpper() == "PARTYID")
                {
                    sql = @" delete PartyID where CreateOper = 'PRIMEADMIN'; ";
                }
                using (DbCommand cmd = database.GetSqlStringCommand(sql))
                {
                    database.ExecuteDataSet(cmd);
                }
            }
        }

        public string GetCustIDByUnificationNo(string strUnificationNo, string strName)
        {
            string sql = @"SELECT * FROM [dbo].[Customer] WHERE TIN=@unification and Closed = 0;";
            DataSet ds = new DataSet();
            string return_ID = "";
            using (DbCommand dBCommand = database.GetSqlStringCommand(sql))
            {
                database.AddInParameter(dBCommand, "@unification", DbType.String, strUnificationNo);
                ds = database.ExecuteDataSet(dBCommand);

            }

            if(ds.Tables[0].Rows.Count > 0)
            {
                return_ID = ds.Tables[0].Rows[0]["ID"].ToString();
            }

            if (ds.Tables[0].Rows.Count > 1)
            {
                //May. 06, 2022 if find more than one records, check name
                sql = @"SELECT * FROM [dbo].[Customer] WHERE TIN=@unification and Closed = 0 and name = @name order by name;";
                using (DbCommand dBCommand = database.GetSqlStringCommand(sql))
                {
                    database.AddInParameter(dBCommand, "@unification", DbType.String, strUnificationNo);
                    database.AddInParameter(dBCommand, "@name", DbType.String, strName);
                    ds = database.ExecuteDataSet(dBCommand);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return_ID = ds.Tables[0].Rows[0]["ID"].ToString();
                    }
                }

                for (int i = 0; i < ds.Tables[0].Rows.Count; i ++)
                {
                    insertlog(strUnificationNo, "Unification No. found more than 1 customer Customerid=" + ds.Tables[0].Rows[i]["ID"].ToString());
                }
                
            }
                

            return return_ID;
        }

        public DataSet CheckRelation(string strPartyId, string strRelation, string strRelatedParty, string strRelatedSource)
        {
            DataSet ds_return;
            string sql = @"
select PartyId,Relationship,RelatedParty,RelationSource 
    from PartyRelation
where PartyId = @PartyId and Relationship = @Relationship 
and RelatedParty = @RelatedParty and isnull(RelationSource,'') = @RelationSource

";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@PartyId", strPartyId));
                cmd.Parameters.Add(new SqlParameter("@Relationship", strRelation));
                cmd.Parameters.Add(new SqlParameter("@RelatedParty", strRelatedParty));
                cmd.Parameters.Add(new SqlParameter("@RelationSource", strRelatedSource));

                ds_return = database.ExecuteDataSet(cmd);
            }

            /* Mar. 22, 2022 Marked. BOT: do not change the original import data from core banking system 
            //Mar 10, 2022 handle deleted record.
            if (ds_return.Tables[0].Rows.Count == 0)
            {

                sql = @"
select PartyId,Relationship,RelatedParty,RelationSource 
    from PartyRelationhist
where PartyId = @PartyId and Relationship = @Relationship 
and RelatedParty = @RelatedParty and isnull(RelationSource,'') = @RelationSource

";
                using (DbCommand cmd = database.GetSqlStringCommand(sql))
                {
                    cmd.Parameters.Add(new SqlParameter("@PartyId", strPartyId));
                    cmd.Parameters.Add(new SqlParameter("@Relationship", strRelation));
                    cmd.Parameters.Add(new SqlParameter("@RelatedParty", strRelatedParty));
                    cmd.Parameters.Add(new SqlParameter("@RelationSource", strRelatedSource));

                    ds_return = database.ExecuteDataSet(cmd);
                }
            }
            */
            return ds_return;
        }

        public void InsertRelation(string strPartyId, string strRelation, string strRelatedParty, string strRelatedSource)
        {
            string sql = @"
insert into PartyRelation (PartyId,Relationship,RelatedParty,RelationSource,CreateOper)
values (@PartyId,@Relationship,@RelatedParty, @RelationSource, @CreateOper)

";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@PartyId", strPartyId));
                cmd.Parameters.Add(new SqlParameter("@Relationship", strRelation));
                cmd.Parameters.Add(new SqlParameter("@RelatedParty", strRelatedParty));
                cmd.Parameters.Add(new SqlParameter("@RelationSource", strRelatedSource));
                cmd.Parameters.Add(new SqlParameter("@CreateOper", "PRIMEADMIN"));

                database.ExecuteDataSet(cmd);
            }
        }

        public int CheckPartyID(string strCustomerId, string strIDType)
        {
            string sql = @"
select count(*) cnt from PartyID where CustomerId = @CustomerId and IDType = @IDType;
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@CustomerId", strCustomerId));
                cmd.Parameters.Add(new SqlParameter("@IDType", strIDType));

                return int.Parse(database.ExecuteDataSet(cmd).Tables[0].Rows[0]["cnt"].ToString());
            }
        }

        public void InsertPartyID(string strCustomerId, string strIDType, string strIDNumber, string strIssueAgency,string strIssuePlace, string strIssueCountry, string strIssueDate, string strExpiryDate)
        {
            string sql = @"
insert into PartyID(CustomerId, IDType,IDNumber, IssueAgency, IssuePlace,IssueCountry,IssueDate,ExpiryDate,CreateOper,CreateDate)
values (@CustomerId,@IDType,@IDNumber, @IssueAgency,@IssuePlace,@IssueCountry,@IssueDate,@ExpiryDate, @CreateOper,getdate())

";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@CustomerId", strCustomerId));
                cmd.Parameters.Add(new SqlParameter("@IDType", strIDType));
                cmd.Parameters.Add(new SqlParameter("@IDNumber", strIDNumber));
                cmd.Parameters.Add(new SqlParameter("@IssueAgency", strIssueAgency));
                cmd.Parameters.Add(new SqlParameter("@IssuePlace", strIssuePlace));
                cmd.Parameters.Add(new SqlParameter("@IssueCountry", strIssueCountry));
                cmd.Parameters.Add(new SqlParameter("@IssueDate", strIssueDate));
                cmd.Parameters.Add(new SqlParameter("@ExpiryDate", strExpiryDate));
                cmd.Parameters.Add(new SqlParameter("@CreateOper", "PRIMEADMIN"));

                database.ExecuteDataSet(cmd);
                cmd.Dispose();
            }
        }
    }
}
