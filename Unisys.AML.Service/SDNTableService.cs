﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Unisys.AML.DataAccess;

namespace Unisys.AML.Service
{
    public class SDNTableService
    {
        public string UserListType { get; set; }
        public string UserProgram { get; set; }
        public List<string> Countries { get; set; }

        private SDNTableRepository sDNTableRepository;

        public DateTime UpdateDateTime { get { return sDNTableRepository.UpdateDateTime; } }

        public SDNTableService(string userListType, string userProgram, List<string> countries)
        {
            sDNTableRepository = new SDNTableRepository();
            this.UserListType = userListType;
            this.UserProgram = userProgram;
            this.Countries = countries;
        }

        public int InsertSDNTable()
        {
            return sDNTableRepository.InsertSDNTable(UserListType, UserProgram, Countries);
        }

        public int UpdateSDNTable()
        {
            return sDNTableRepository.UpdateSDNTable();
        }

        public int InsertAddr()
        {
            return sDNTableRepository.InsertAddr();
        }

        public int UpdateAddr()
        {
            return sDNTableRepository.UpdateAddr();
        }

        public int GetSDNTableUpdCnt()
        {
            return sDNTableRepository.GetSDNTableUpdCnt();
        }

        public int GetAddrUpdCnt()
        {
            return sDNTableRepository.GetAddrUpdCnt();
        }

        public int InsertAlt()
        {
            return sDNTableRepository.InsertAlt();
        }

        public int GetAltUpdCnt()
        {
            return sDNTableRepository.GetAltUpdCnt();
        }

        public int UpdateAlt()
        {
            return sDNTableRepository.UpdateAlt();
        }

        public int InsertRelationship()
        {
            return sDNTableRepository.InsertRelationship();
        }

        public int GetRelationshipUpdCnt()
        {
            return sDNTableRepository.GetRelationshipUpdCnt();
        }

        public int UpdateRelationship()
        {
            return sDNTableRepository.UpdateRelationship();
        }

        public int InsertDOBs()
        {
            return sDNTableRepository.InsertDOBs();
        }

        public int GetDOBsUpdCnt()
        {
            return sDNTableRepository.GetDOBsUpdCnt();
        }

        public int UpdateDOBs()
        {
            return sDNTableRepository.UpdateDOBs();
        }

        public int InsertKeywords()
        {
            return sDNTableRepository.InsertKeywords();
        }

        public int GetKeywordsUpdCnt()
        {
            return sDNTableRepository.GetKeywordsUpdCnt();
        }

        public int UpdateKeywords()
        {
            return sDNTableRepository.UpdateKeywords();
        }

        public int InsertKeywordsAlt()
        {
            return sDNTableRepository.InsertKeywordsAlt();
        }

        public int GetKeywordsAltUpdCnt()
        {
            return sDNTableRepository.GetKeywordsAltUpdCnt();
        }

        public int UpdateKeywordsAlt()
        {
            return sDNTableRepository.UpdateKeywordsAlt();
        }

        public int InsertUrls()
        {
            return sDNTableRepository.InsertUrls();
        }

        public int GetUrlsUpdCnt()
        {
            return sDNTableRepository.GetUrlsUpdCnt();
        }

        public int UpdateUrls()
        {
            return sDNTableRepository.UpdateUrls();
        }

        public int InsertNotes()
        {
            return sDNTableRepository.InsertNotes();
        }

        public int GetNotesUpdCnt()
        {
            return sDNTableRepository.GetNotesUpdCnt();
        }

        public int UpdateNotes()
        {
            return sDNTableRepository.UpdateNotes();
        }

        public StringBuilder GetSummary()
        {
            string time = sDNTableRepository.UpdateDateTime.ToString("MM/dd/yyyy HH:mm:ss");
            DataSet ds = sDNTableRepository.GetSummary(UserListType);
            StringBuilder s = new StringBuilder();

            s.AppendLine();
            s.AppendLine("========== PEP Customization List Update Summary ==========");

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                s.AppendLine(string.Format("{0} | {1} List Update Detail :", time, UserListType))
                 .AppendLine();

                int rows = 0;
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    s.AppendLine(string.Format("{0} | {1} => Update {2} Record(zs).", time, r["Country"].ToString(), r["Cnt"].ToString()))
                     .AppendLine();
                    rows += (int)r["Cnt"];
                }

                s.AppendLine()
                 .AppendLine(string.Format("{0} | Total Update {1} Record(s).", time, rows.ToString()))
                 .AppendLine();
            }
            else
            {
                s.AppendLine(string.Format("{0} | No Change for {1} List.", time, UserListType));
            }

            return s;
        }

        public void UpdateProgram(List<string> programCountry, string programName)
        {
            sDNTableRepository.UpdateProgram(programCountry, programName);
        }

        public void OFACEventLog(string log)
        {
            sDNTableRepository.OFACEventLog(log);
        }

        public string GetUpdateLog()
        {
            return sDNTableRepository.GetUpdateLog().ToString();
        }
    }
}