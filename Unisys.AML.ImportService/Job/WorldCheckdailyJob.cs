﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic.FileIO;
using System.Data;
using System.Data.SqlClient;
using Unisys.AML.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Data.Common;
using System.Xml;
using System.Xml.Serialization;
using log4net.DateFormatter;
using Quartz.Util;
using log4net.Core;

namespace Unisys.AML.ImportService
{
    [DisallowConcurrentExecution]
    public class WorldCheckdailyJob : IJob
    {
        private StringBuilder logs;

        public WorldCheckdailyJob()
        {
        }

        public string connectionString = ConfigurationManager.ConnectionStrings["OFAC"].ConnectionString;
        public string str_wc_imp_order = "";
        Fortify fortifyService = new Fortify();
        List<GetWCPara> getWC_para = new List<GetWCPara>();
        protected const int _batchSize = 50000;
        protected const int retries = 3;
        public string str_WCHistEnable = "";
        public string str_WCHistTimeOut = "";
        //APR.24, 2023 modify throw exception for each functions
        public virtual void Execute(IJobExecutionContext context)
        {
            string ExcuteBatchPath = "";

            bool success = false;
            for (int iretry = 0; iretry < retries; iretry ++)
            {
                try
                {
                    logs = new StringBuilder();
                    LogAdd(string.Format("***** Start WorldCheck Job [{0}] *****", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                    JobDataMap jobDataMap = context.MergedJobDataMap;

                    //wc
                    string str_day_path = jobDataMap.GetString("world-check-day");
                    string str_daydel_path = jobDataMap.GetString("world-check-deleted-day");
                    string str_wckeywordlist_path = jobDataMap.GetString("world-check-keyword-list");
                    string str_wc_entnum = jobDataMap.GetString("BaseNum");
                    string str_wc_listtype = jobDataMap.GetString("wc_listtype");
                    string str_UPDDate = jobDataMap.GetString("wc_UPDDate");
                    string str_buildtime = jobDataMap.GetString("buildtime");
                    string str_wc_excludecountry = jobDataMap.GetString("wc_excludecountry");
                    string str_wc_yearlimit = jobDataMap.GetString("wc_yearlimit");
                    string str_wc_fixedcountry = jobDataMap.GetString("wc_fixedcountry");
                    string str_wc_divisionimg = jobDataMap.GetString("wc_divisionimg");
                    string str_wc_subcate_space = jobDataMap.GetString("wc_subcate_space");
                    string str_wc_import_un = jobDataMap.GetString("wc_import_UN");
                    string str_wc_import_SQL = jobDataMap.GetString("wc_import_SQL");
                    string str_servicedelay = jobDataMap.GetString("OFACservicerestartDelay");
                    string str_servicerestart = jobDataMap.GetString("OFACservicerestart");
                    string str_updateDB = jobDataMap.GetString("UpdateDBStatus");
                    string str_wc_import_crime = jobDataMap.GetString("wc_import_CRIME");
                    string str_wc_import_crime_SQL = jobDataMap.GetString("wc_import_CRIME_SQL");

                    int int_noofsql = int.Parse(jobDataMap.GetString("No_Of_SQL"));

                    string str_weakALT = jobDataMap.GetString("wc_lowaliases");
                    string str_weakALT_listtype = jobDataMap.GetString("wc_lowaliases_listtype");
                    string str_ALTSPELL = jobDataMap.GetString("wc_AlterSpell");
                    string str_ALTPASS = jobDataMap.GetString("wc_AltPass");
                    string str_ALTBIC = jobDataMap.GetString("wc_AltBic");
                    string str_ALTIMO = jobDataMap.GetString("wc_AltImo");
                    string str_ALTARN = jobDataMap.GetString("wc_AltARN");
                    string str_ALTMSN = jobDataMap.GetString("wc_AltMSN");

                    string str_alterday = jobDataMap.GetString("altertableday");
                    string str_relatedcolumn = jobDataMap.GetString("RelatedColumn");

                    //Nov.29 2022 add worldcheckhist enable and sql time out config
                    str_WCHistEnable = jobDataMap.GetString("WCHistEnable");
                    str_WCHistTimeOut = jobDataMap.GetString("WCHistTimeOut");

                    str_wc_imp_order = jobDataMap.GetString("wc_import_order");

                    getWC_para.Clear();

                    //20181022 先給10組 不夠用就要再改程式
                    for (int i = 0; i < int_noofsql; i++)
                    {
                        GetWCPara WCparaSet = new GetWCPara();

                        WCparaSet.WCSQL = jobDataMap.GetString("WCSQL" + (i + 1));
                        WCparaSet.WCListtype = jobDataMap.GetString("WCListtype" + (i + 1));
                        WCparaSet.WCProgram = jobDataMap.GetString("WCprogram" + (i + 1));

                        getWC_para.Add(WCparaSet);

                        LogAdd("WCSQL:" + getWC_para[i].WCSQL);
                        LogAdd("WCListtype:" + getWC_para[i].WCListtype);
                        LogAdd("WCprogram:" + getWC_para[i].WCProgram);
                        LogAdd("WCPara_setCount:" + getWC_para.Count().ToString());
                    }

                    //relation
                    string currentDir = Environment.CurrentDirectory;
                    DirectoryInfo directory = new DirectoryInfo(currentDir);
                    string BCPFile_TempRelationship = jobDataMap.GetString("BCPFile_TempRelationship");
                    string BCPFile_Relationship = jobDataMap.GetString("BCPFile_Relationship");
                    string BCPFmt_TempRelationship = jobDataMap.GetString("BCPFmt_TempRelationship");
                    string BCPFmt_Relationship = jobDataMap.GetString("BCPFmt_Relationship");

                    string BCP_SDNAltTable_Enable = jobDataMap.GetString("BCPEnb_SDNAltTable");
                    string BCPFile_TmpSDNAltTable = jobDataMap.GetString("BCPFile_TmpSDNAltTable");
                    string BCPFile_SDNAltTable = jobDataMap.GetString("BCPFile_SDNAltTable");
                    string BCPFmt_TmpSDNAltTable = jobDataMap.GetString("BCPFmt_TmpSDNAltTable");
                    string BCPFmt_SDNAltTable = jobDataMap.GetString("BCPFmt_SDNAltTable");

                    string BCPFile_TempURLs = jobDataMap.GetString("BCPFile_TempURLs");
                    string BCPFile_URLs = jobDataMap.GetString("BCPFile_URLs");
                    string BCPFmt_TempURLs = jobDataMap.GetString("BCPFmt_TempURLs");
                    string BCPFmt_URLs = jobDataMap.GetString("BCPFmt_URLs");

                    string BCPFile_Error = jobDataMap.GetString("BCPFile_Error");
                    string BCPServer = jobDataMap.GetString("BCPServer");
                    string BCPUser = jobDataMap.GetString("BCPUser");
                    string BCPPwd = jobDataMap.GetString("BCPPwd");
                    string BaseNum = jobDataMap.GetString("BaseNum");

                    //Native
                    string NativePath = jobDataMap.GetString("Native_FilePath");
                    string NativeALT = jobDataMap.GetString("ALT_NATIVE");
                    string NativeNOTE = jobDataMap.GetString("NOTE_NATIVE");
                    string NativeNOTE_Enable = jobDataMap.GetString("NOTE_NATIVE_ENABLE");
                    string NativeALT_Enable = jobDataMap.GetString("ALT_NATIVE_ENABLE");

                    //Notes
                    string NoteTypeList = jobDataMap.GetString("Notes_TypeList");
                    
                    ExcuteBatchPath = jobDataMap.GetString("EXCBAT");

                    ClearConnection();

                    //晚上把status != 1 的都改回1
                    for(int i = 0; i < str_buildtime.Split(',').Length; i++)
                    {
                        if (DateTime.Now.Hour > int.Parse(str_buildtime.Split(',')[i]))
                        {
                            RefreshWCStatus();
                        }
                    }
                        
                    //Jan. 17, 2023 update each section start and complete log.
                    ImportNative(NativePath);

                    UpdateWCKeywordList(str_wckeywordlist_path);

                    UpdCsvDataIntoSqlServer(str_day_path, str_wc_entnum, str_alterday);

                    DelCsvDataIntoSqlServer(str_daydel_path, str_wc_entnum);

                    UpdateKeyWord(str_UPDDate, int.Parse(str_wc_entnum));

                    UpdateSDN(str_wc_listtype, str_UPDDate, str_wc_yearlimit, str_wc_excludecountry, str_wc_entnum, str_wc_fixedcountry, str_wc_divisionimg, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);

                    for (int i = 0; i < str_relatedcolumn.Split('|').Length; i++)
                    {
                        UpdRelationshipDailyDataIntoSqlServer(str_relatedcolumn.Split('|')[i], BCPFile_TempRelationship, BCPFile_Relationship, BCPFmt_TempRelationship, BCPFile_Error,
                        BCPFmt_Relationship, BCPServer, BCPUser, BCPPwd, BaseNum, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);
                    }

                    if (BCP_SDNAltTable_Enable == "Y")
                    {
                        UpdSDNAltTableDailyDataIntoSqlServer("ALT", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, BCPUser, BCPPwd, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);
                    }

                    if (str_weakALT == "Y")
                    {
                        UpdSDNAltTableDailyDataIntoSqlServer("ALTWEAK", str_weakALT_listtype, BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, BCPUser, BCPPwd, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);
                    }

                    if (str_ALTSPELL == "Y")
                    {
                        UpdSDNAltTableDailyDataIntoSqlServer("ALTSPELL", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, BCPUser, BCPPwd, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);
                    }

                    if (str_ALTPASS == "Y")
                    {
                        UpdSDNAltTableDailyDataIntoSqlServer("ALTPASS", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, BCPUser, BCPPwd, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);
                    }

                    if (str_ALTBIC == "Y")
                    {
                        UpdSDNAltTableDailyDataIntoSqlServer("ALTBIC", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, BCPUser, BCPPwd, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);
                    }

                    if (str_ALTIMO == "Y")
                    {
                        UpdSDNAltTableDailyDataIntoSqlServer("ALTIMO", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, BCPUser, BCPPwd, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);
                    }

                    if (str_ALTARN == "Y")
                    {
                        UpdSDNAltTableDailyDataIntoSqlServer("ALTARN", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, BCPUser, BCPPwd, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);
                    }

                    if (str_ALTMSN == "Y")
                    {
                        UpdSDNAltTableDailyDataIntoSqlServer("ALTMSN", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, BCPUser, BCPPwd, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);
                    }

                    UpdURLsDailyDataIntoSqlServer(str_UPDDate,BCPFile_TempURLs, BCPFile_URLs, BCPFmt_TempURLs, BCPFile_Error, BCPFmt_URLs, BCPServer, BCPUser, BCPPwd, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);

                    insertDobs(str_UPDDate, str_wc_subcate_space, str_wc_import_un, str_wc_import_SQL, str_wc_import_crime, str_wc_import_crime_SQL);

                    InsertAddr(str_UPDDate, str_wc_import_crime);

                    UpdateAttribute(str_UPDDate, str_wc_import_crime, str_wc_import_crime_SQL, NoteTypeList);

                    UpdateNative(str_UPDDate, NativeALT_Enable, NativeALT, NativeNOTE_Enable, NativeNOTE, BaseNum);

                    LogAdd(string.Format("***** Complete WorldCheck Job [{0}] *****", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                    OFACEventLog(string.Format("***** Complete WorldCheck Job [{0}] *****", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                    System.Threading.Thread.Sleep(5000);

                    if (!string.IsNullOrEmpty(ExcuteBatchPath))
                    {
                        Run_command(ExcuteBatchPath);
                    }

                    //2018.05 update DB status
                    if (str_updateDB == "Y")
                    {
                        //set MemImgBuildReq = 0
                        UpdDBStatus();
                    }

                    //build img 一天build一次
                    LogAdd("hour=" + DateTime.Now.Hour + ",cong=" + str_buildtime);
                    foreach (string t in str_buildtime.Split(','))
                    {
                        if (t != "" && DateTime.Now.Hour == int.Parse(t))
                        {
                            string b = "";

                            b = BuildFileImg();
                            LogAdd(b);

                            //restart the OFAC service
                            if (str_servicerestart == "Y")
                            {
                                StopService();
                                System.Threading.Thread.Sleep(int.Parse(str_servicedelay) * (1000));
                                StartService();
                            }
                        }
                    }

                    success = true;
                    break;
                }
                catch (Exception ex)
                {
                    if (!success)
                    {
                        LogException(ex, string.Format("Retry Fail Count:{0}", iretry));
                        System.Threading.Thread.Sleep(10000);

                        //Oct.24, 2022 add when exception happen
                        if (!string.IsNullOrEmpty(ExcuteBatchPath))
                        {
                            Run_command(ExcuteBatchPath);
                        }
                    }
                    else
                    {
                        Log.WriteToLog("Error", "Exception occurred Retry End. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
                        System.Threading.Thread.Sleep(10000);
                        break;
                    }

                    ClearConnection();
                }
            }
        }

        public DataTable CheckTableColumn(string TableName, string fileName, string str_AlterDay)
        {
            var dataTable = new DataTable(TableName);

            string[] List_header = null;
            var createdCount = 0;
            SqlDataAdapter da;
            DataSet ds;
            try
            {
                // Get CSV Header.
                using (var textFieldParser = new TextFieldParser(fileName, System.Text.Encoding.GetEncoding(28605)))
                {
                    textFieldParser.TextFieldType = FieldType.Delimited;
                    textFieldParser.Delimiters = new[] { "\t" };

                    while (!textFieldParser.EndOfData)
                    {
                        createdCount++;
                        if (createdCount == 1)
                        {
                            List_header = textFieldParser.ReadFields();
                            foreach (string strHeader in List_header)
                            {
                                dataTable.Columns.Add(strHeader);
                            }
                        }
                        break;
                    }
                }

                //compare columns count
                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        sqlCommand.CommandText = @"
                        SELECT COUNT(COLUMN_NAME) coun
                        FROM INFORMATION_SCHEMA.COLUMNS
                            WHERE
                                TABLE_CATALOG = 'OFAC'
                                AND TABLE_SCHEMA = 'dbo'
                                AND TABLE_NAME = '" + TableName + "'";
                        da = new SqlDataAdapter(sqlCommand);
                        ds = new DataSet();

                        da.Fill(ds);
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            LogAdd("[Daily] " + TableName + " Column count:" + ds.Tables[0].Rows[0]["coun"].ToString());
                            if (dataTable.Columns.Count > int.Parse(ds.Tables[0].Rows[0]["coun"].ToString()))
                            {
                                LogAdd("[Daily] Check " + TableName + " column MisMatch:CSV header counts=" + dataTable.Columns.Count + ",DB colums count=" + int.Parse(ds.Tables[0].Rows[0]["coun"].ToString()));
                            }
                        }
                        sqlCommand.Dispose();
                    }
                    sqlConnection.Close();
                    sqlConnection.Dispose();
                }

            }
            catch (Exception ex)
            {
                LogException(ex, string.Format("CheckTableColumn Check {0} column", TableName));
            }

            return dataTable;
        }

        public void UpdCsvDataIntoSqlServer(string fileName, string ent_num, string str_AlterDay)
        {
            // 日更新檔
            var createdCount = 0;

            var dt = new DateTime();

            dt = DateTime.Now;

            try
            {
                LogAdd(string.Format("[Daily][WorldCheckday] worldcheckday file import -- Start."));

                using (var textFieldParser = new TextFieldParser(fileName, System.Text.Encoding.GetEncoding(28605)))
                {
                    textFieldParser.TextFieldType = FieldType.Delimited;
                    textFieldParser.Delimiters = new[] { "\t" };
                    textFieldParser.HasFieldsEnclosedInQuotes = false;
                    var dataTable = CheckTableColumn("TempWorldCheckDay", fileName, str_AlterDay);
                    int i_WCHistTimeOut = 0;
                    if (string.IsNullOrEmpty(str_WCHistEnable) || str_WCHistEnable == "Y")
                    {
                        str_WCHistEnable = "Y";

                        if(string.IsNullOrEmpty(str_WCHistTimeOut))
                        {
                            i_WCHistTimeOut = 300;
                        }
                        else
                        {
                            i_WCHistTimeOut = Convert.ToInt32(str_WCHistTimeOut);
                        }
                    }

                    LogAdd("[Daily] WorldCheckHist:" + str_WCHistEnable + ",sqltimeout:" + i_WCHistTimeOut.ToString());

                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        sqlConnection.Open();

                        using (var cmd = new SqlCommand("truncate table TempWorldCheckDay;", sqlConnection))
                        {
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                        }

                        // Create the bulk copy object
                        var sqlBulkCopy = new SqlBulkCopy(sqlConnection)
                        {
                            DestinationTableName = "TempWorldCheckDay"
                        };

                        // bulk column mapping.
                        for (int i = 0; i < dataTable.Columns.Count; i++)
                        {
                            string strHeader = dataTable.Columns[i].ColumnName;

                            if (strHeader.ToUpper().Equals("LAST NAME"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("LAST NAME", "LASTNAME");

                            }
                            else if (strHeader.ToUpper().Equals("FIRST NAME"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("FIRST NAME", "FIRSTNAME");

                            }
                            else if (strHeader.ToUpper().Equals("LOW QUALITY ALIASES"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("LOW QUALITY ALIASES", "LOWQALIASES");

                            }
                            else if (strHeader.ToUpper().Equals("ALTERNATIVE SPELLING"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("ALTERNATIVE SPELLING", "ALTERSPELL");

                            }
                            else if (strHeader.ToUpper().Equals("SUB-CATEGORY"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("SUB-CATEGORY", "SUBCATEGORY");

                            }
                            else if (strHeader.ToUpper().Equals("PLACE OF BIRTH"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("PLACE OF BIRTH", "PLACEOFBIRTH");

                            }
                            else if (strHeader.ToUpper().Equals("IDENTIFICATION NUMBERS"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("IDENTIFICATION NUMBERS", "IDENTIFICATION_NUMBERS");

                            }
                            else if (strHeader.ToUpper().Equals("LOCATIONS"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("LOCATIONS", "LOCATION");

                            }
                            else if (strHeader.ToUpper().Equals("COUNTRIES"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("COUNTRIES", "COUNTRY");

                            }
                            else if (strHeader.ToUpper().Equals("COMPANIES"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("COMPANIES", "COMPANY");

                            }
                            else if (strHeader.ToUpper().Equals("E/I"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("E/I", "TYPE");

                            }
                            else if (strHeader.ToUpper().Equals("LINKED TO"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("LINKED TO", "LINKEDTO");

                            }
                            else if (strHeader.ToUpper().Equals("FURTHER INFORMATION"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("FURTHER INFORMATION", "FURTHERINFO");

                            }
                            else if (strHeader.ToUpper().Equals("EXTERNAL SOURCES"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("EXTERNAL SOURCES", "EXSOURCES");

                            }
                            else if (strHeader.ToUpper().Equals("UPDATE CATEGORY"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("UPDATE CATEGORY", "UPDCATEGORY");

                            }
                            else if (strHeader.ToUpper().Equals("UID"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("UID", "UID");

                            }
                            else if (strHeader.ToUpper().Equals("ALIASES"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("ALIASES", "ALIASES");

                            }
                            else if (strHeader.ToUpper().Equals("CATEGORY"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("CATEGORY", "CATEGORY");

                            }
                            else if (strHeader.ToUpper().Equals("TITLE"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("TITLE", "TITLE");

                            }
                            else if (strHeader.ToUpper().Equals("POSITION"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("POSITION", "POSITION");

                            }
                            else if (strHeader.ToUpper().Equals("AGE"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("AGE", "AGE");

                            }
                            else if (strHeader.ToUpper().Equals("DOB"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("DOB", "DOB");

                            }
                            else if (strHeader.ToUpper().Equals("DECEASED"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("DECEASED", "DECEASED");

                            }
                            else if (strHeader.ToUpper().Equals("PASSPORTS"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("PASSPORTS", "PASSPORTS");

                            }
                            else if (strHeader.ToUpper().Equals("SSN"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("SSN", "SSN");

                            }
                            else if (strHeader.ToUpper().Equals("KEYWORDS"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("KEYWORDS", "KEYWORDS");

                            }
                            else if (strHeader.ToUpper().Equals("ENTERED"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("ENTERED", "ENTERED");

                            }
                            else if (strHeader.ToUpper().Equals("UPDATED"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("UPDATED", "UPDATED");

                            }
                            else if (strHeader.ToUpper().Equals("EDITOR"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("EDITOR", "EDITOR");

                            }
                            else if (strHeader.ToUpper().Equals("AGE DATE (AS OF DATE)"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("AGE DATE (AS OF DATE)", "AGEDATE");

                            }
                            else if (strHeader.ToUpper().Equals("CITIZENSHIP"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("CITIZENSHIP", "CITIZENSHIP");
                                //APR.18,2022 remove alter function for CITIZENSHIP
                                //AlterWC("WorldCheck", strHeader.ToUpper().Replace("-", "").Replace("/", "").Replace(" ", "").Replace("_", ""));

                            }
                            else if (strHeader.ToUpper().Equals("DOBS"))
                            {
                                sqlBulkCopy.ColumnMappings.Add("DOBS", "DOBS");
                                //APR.18,2022 remove alter function for DOBS
                                //AlterWC("WorldCheck", strHeader.ToUpper().Replace("-", "").Replace("/", "").Replace(" ", "").Replace("_", ""));

                            }

                            else
                            {
                                //sqlBulkCopy.ColumnMappings.Add(strHeader, strHeader);
                                LogAdd("[Daily] No Match Header:" + strHeader);
                                /* Feb 16,2022 mark. BOT request do not add column automatically
                                AlterWC("TempWorldCheckDay", strHeader.ToUpper().Replace("-", "").Replace("/", "").Replace(" ", "").Replace("_", ""));

                                AlterWC("WorldCheck", strHeader.ToUpper().Replace("-", "").Replace("/", "").Replace(" ", "").Replace("_", ""));

                                sqlBulkCopy.ColumnMappings.Add(strHeader.ToUpper(), strHeader.ToUpper().Replace("-", "").Replace("/", "").Replace(" ", "").Replace("_", ""));
                            */
                            }
                        }

                        // Loop through the CSV and load each set of 100,000 records into a DataTable
                        // Then send it to the LiveTable
                        while (!textFieldParser.EndOfData)
                        {
                            createdCount++;

                            if (createdCount == 1)
                            {
                                textFieldParser.ReadFields();
                            }

                            try
                            {
                                dataTable.Rows.Add(textFieldParser.ReadFields());
                                //dr = dataTable.Rows[createdCount - 1];
                            }
                            catch (Exception ex)
                            {
                                LogException(ex, "[Daily] CSV File load");
                                throw ex;
                            }

                            if (createdCount % _batchSize == 0)
                            {
                                InsertDataTable(sqlBulkCopy, sqlConnection, dataTable);
                            }
                        }
                        // Don't forget to send the last batch under _batchSize
                        if (dataTable.Rows.Count > 0)
                            InsertDataTable(sqlBulkCopy, sqlConnection, dataTable);



                        string strWCColumns = GetWorldCheckSchema("WorldCheck");
                        //2019.12 create worldcheckhist table and insert every wc data to hist table.
                        //write the original data status = 0
                        int int_insertoldlog = 0;
                        int int_updatelog = 0;
                        int int_insertnewlog = 0;
                        //Nov.29 2022 check worldcheck hist enable flag
                        if(str_WCHistEnable == "Y")
                        {
                            using (var sqlCommand = new SqlCommand("", sqlConnection))
                            {
                                //LogAdd("Test WC WCcolumns:" + strWCColumns + "''");
                                /*sqlCommand.CommandText = @"
                            insert into WorldCheckHist
                            select UID,UID+@entnum," + strWCColumns + @",@date,0
                            from WorldCheck where uid in (select uid from TempWorldCheckDay);
                              ";*/
                                sqlCommand.CommandText = @"insert into WorldCheckHist
select UID, UID+@entnum,CAST([UID] AS nvarchar(20))+'|' + isnull([LastName],'')+'|' + isnull([FirstName],'')+'|' + isnull([Aliases],'')+'|' + isnull([LowQAliases],'')+'|' + isnull([AlterSpell],'')+'|' + isnull([Category],'')+'|' + isnull([Title],'')
+'|' + isnull([SubCategory],'')+'|' + isnull([Position],'')+'|' + CAST(isnull([Age], '') AS nvarchar(20))
+'|' + CAST(isnull([DOB], '') AS nvarchar(20))+'|' + isnull([PlaceOfBirth],'')+'|' + isnull([Deceased],'')+'|' + isnull([Passports],'')+'|' + isnull([SSN],'')+'|' + isnull([IDENTIFICATION_NUMBERS],'') +'|' + isnull([Location],'')+'|' + isnull([Country],'')+'|' + isnull([Company],'')+'|' + isnull([Type],'')
+'|' + isnull([LinkedTo],'')+'|' + isnull([FurtherInfo],'')
+'|' + isnull([Keywords],'')+'|' + isnull([ExSources],'')+'|' + isnull([UpdCategory],'')+'|' + isnull([Entered],'')+'|' + isnull([Updated],'')+'|' + isnull([Editor],'')+'|' + isnull([AgeDate],''),@date,0
from WorldCheck where uid in (select uid from TempWorldCheckDay)";
                                sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                                sqlCommand.Parameters.AddWithValue("@entnum", int.Parse(ent_num));
                                sqlCommand.CommandTimeout = i_WCHistTimeOut;
                                int_insertoldlog = sqlCommand.ExecuteNonQuery();
                            }

                            strWCColumns = GetWorldCheckSchema("TempWorldCheckDay");
                            //write the updated data status = 1
                            using (var sqlCommand = new SqlCommand("", sqlConnection))
                            {
                                //LogAdd("Test TWC WCcolumns:" + strWCColumns + "''");
                                /*sqlCommand.CommandText = @"
                            insert into WorldCheckHist
                            select UID,UID+@entnum," + strWCColumns + @",@date,1
                            from TempWorldCheckDay where uid in (select uid from WorldCheck);
                              ";*/
                                sqlCommand.CommandText = @"insert into WorldCheckHist
select UID, UID+@entnum,CAST([UID] AS nvarchar(20))+'|' + isnull([LastName],'')+'|' + isnull([FirstName],'')+'|' + isnull([Aliases],'')+'|' + isnull([LowQAliases],'')+'|' + isnull([AlterSpell],'')+'|' + isnull([Category],'')+'|' + isnull([Title],'')
+'|' + isnull([SubCategory],'')+'|' + isnull([Position],'')+'|' + CAST(isnull([Age], '') AS nvarchar(20))
+'|' + CAST(isnull([DOB], '') AS nvarchar(20))+'|' + isnull([PlaceOfBirth],'')+'|' + isnull([Deceased],'')+'|' + isnull([Passports],'')+'|' + isnull([SSN],'')+'|' + isnull([IDENTIFICATION_NUMBERS],'') +'|' + isnull([Location],'')+'|' + isnull([Country],'')+'|' + isnull([Company],'')+'|' + isnull([Type],'')
+'|' + isnull([LinkedTo],'')+'|' + isnull([FurtherInfo],'')
+'|' + isnull([Keywords],'')+'|' + isnull([ExSources],'')+'|' + isnull([UpdCategory],'')+'|' + isnull([Entered],'')+'|' + isnull([Updated],'')+'|' + isnull([Editor],'')+'|' + isnull([AgeDate],''),@date,1
from TempWorldCheckDay where uid in (select uid from WorldCheck)";
                                sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                                sqlCommand.Parameters.AddWithValue("@entnum", int.Parse(ent_num));
                                sqlCommand.CommandTimeout = i_WCHistTimeOut;
                                int_updatelog = sqlCommand.ExecuteNonQuery();
                            }

                            //write the new insert data status = 2
                            using (var sqlCommand = new SqlCommand("", sqlConnection))
                            {
                                /*sqlCommand.CommandText = @"
                            insert into WorldCheckHist
                            select UID,UID+@entnum," + strWCColumns + @",@date,2
                            from TempWorldCheckDay where uid not in (select uid from WorldCheck);
                              ";*/
                                sqlCommand.CommandText = @"insert into WorldCheckHist
select UID, UID+@entnum,CAST([UID] AS nvarchar(20))+'|' + isnull([LastName],'')+'|' + isnull([FirstName],'')+'|' + isnull([Aliases],'')+'|' + isnull([LowQAliases],'')+'|' + isnull([AlterSpell],'')+'|' + isnull([Category],'')+'|' + isnull([Title],'')
+'|' + isnull([SubCategory],'')+'|' + isnull([Position],'')+'|' + CAST(isnull([Age], '') AS nvarchar(20))
+'|' + CAST(isnull([DOB], '') AS nvarchar(20))+'|' + isnull([PlaceOfBirth],'')+'|' + isnull([Deceased],'')+'|' + isnull([Passports],'')+'|' + isnull([SSN],'')+'|' + isnull([IDENTIFICATION_NUMBERS],'') +'|' + isnull([Location],'')+'|' + isnull([Country],'')+'|' + isnull([Company],'')+'|' + isnull([Type],'')
+'|' + isnull([LinkedTo],'')+'|' + isnull([FurtherInfo],'')
+'|' + isnull([Keywords],'')+'|' + isnull([ExSources],'')+'|' + isnull([UpdCategory],'')+'|' + isnull([Entered],'')+'|' + isnull([Updated],'')+'|' + isnull([Editor],'')+'|' + isnull([AgeDate],''),@date,2
from TempWorldCheckDay where uid not in (select uid from WorldCheck)";
                                sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                                sqlCommand.Parameters.AddWithValue("@entnum", int.Parse(ent_num));
                                sqlCommand.CommandTimeout = i_WCHistTimeOut;
                                int_insertnewlog = sqlCommand.ExecuteNonQuery();

                            }

                            LogAdd(string.Format("[WCToHist] Write to World Check Hist table old data count:{0}, updated data count:{1}, new insert data count:{2}", int_insertoldlog, int_updatelog, int_insertnewlog));

                        }

                        using (var sqlCommand = new SqlCommand("", sqlConnection))
                        {
                            sqlCommand.CommandText = @"
                        UPDATE WorldCheck 
                        SET LastName = B.LastName,
                            FirstName = B.FirstName,
                            Aliases = B.Aliases,
                            LowQAliases = B.LowQAliases,
                            AlterSpell = B.AlterSpell,
                            Category = B.Category,
                            Title = B.Title,
                            SubCategory = B.SubCategory,
                            Position = B.Position,
                            Age = B.Age,
                            DOB = B.DOB,
                            DOBs= B.DOBs,
                            PlaceOfBirth = B.PlaceOfBirth,
                            Deceased = B.Deceased,
                            Passports = B.Passports,
                            SSN = B.SSN,
                            IDENTIFICATION_NUMBERS = B.IDENTIFICATION_NUMBERS,
                            Location = B.Location,
                            Country = B.Country,
                            CITIZENSHIP = B.CITIZENSHIP,
                            Company = B.Company,
                            Type = B.Type,
                            LinkedTo = B.LinkedTo,
                            FurtherInfo = B.FurtherInfo,
                            Keywords = B.Keywords,
                            ExSources = B.ExSources,
                            UpdCategory = B.UpdCategory,
                            Entered = B.Entered,
                            Updated = B.Updated,
                            Editor = B.Editor,
                            AgeDate = B.AgeDate,
                            DELFlag = 0,
                            UPDDate = @date
                                FROM WorldCheck wc JOIN TempWorldCheckday B
                                ON wc.UID = B.UID;
                          ";
                            sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                            int_updatelog = sqlCommand.ExecuteNonQuery();
                        }

                        using (var sqlCommand = new SqlCommand("", sqlConnection))
                        {
                            sqlCommand.CommandText = @"
                        insert into WorldCheck
([UID],[LastName],[FirstName],[Aliases],[LowQAliases],[AlterSpell],[Category],[Title],[SubCategory],[Position],[Age]
,[DOB],[DOBs],[PlaceOfBirth],[Deceased],[Passports],[SSN],[IDENTIFICATION_NUMBERS],[Location],[Country],[CITIZENSHIP],[Company],[Type],[LinkedTo],[FurtherInfo]
,[Keywords],[ExSources],[UpdCategory],[Entered],[Updated],[Editor],[AgeDate],[Entnum],[UPDDate],[DELFlag])
select [UID],[LastName],[FirstName],[Aliases],[LowQAliases],[AlterSpell],[Category],[Title],[SubCategory],[Position],[Age]
,[DOB],[DOBs],[PlaceOfBirth],[Deceased],[Passports],[SSN],[IDENTIFICATION_NUMBERS],[Location],[Country],[CITIZENSHIP],[Company],[Type],[LinkedTo],[FurtherInfo]
,[Keywords],[ExSources],[UpdCategory],[Entered],[Updated],[Editor],[AgeDate],[UID]+@entnum,@date,0
from TempWorldCheckDay tempwc
where not EXISTS (select [UID] from WorldCheck where uid = tempwc.uid);
                          ";
                            sqlCommand.Parameters.AddWithValue("@entnum", int.Parse(ent_num));
                            sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                            int_insertnewlog = sqlCommand.ExecuteNonQuery();
                        }
                        LogAdd(string.Format("[Daily][WorldCheckday] worldcheckday update:{0}, insert:{1}", int_updatelog, int_insertnewlog));
                        OFACEventLog(string.Format("worldcheckday update:{0}, insert:{1}", int_updatelog, int_insertnewlog));

                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }
                }
                LogAdd(string.Format("[Daily][WorldCheckday] worldcheckday file import -- Completion."));
            }
            catch (Exception ex)
            {
                LogException(ex, "UpdCsvDataIntoSqlServer");
                throw ex;
            }
        }

        public void DelCsvDataIntoSqlServer(string fileName, string ent_num)
        {
            // 日刪除檔

            var createdCount = 0;

            var dt = new DateTime();
            dt = DateTime.Now;

            LogAdd(string.Format("[Daily][WorldCheckday] worldcheckday del file import -- Start."));

            using (var textFieldParser = new TextFieldParser(fileName, System.Text.Encoding.GetEncoding(28605)))
            {
                textFieldParser.TextFieldType = FieldType.Delimited;
                textFieldParser.Delimiters = new[] { "\t" };

                var dataTable = new DataTable("TempWorldCheckDel");

                // Add the columns in the temp table
                dataTable.Columns.Add("UID");
                dataTable.Columns.Add("DATE");

                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    using (var cmd = new SqlCommand("truncate table TempWorldCheckDel;", sqlConnection))
                    {
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }

                    var sqlBulkCopy = new SqlBulkCopy(sqlConnection)
                    {
                        DestinationTableName = "TempWorldCheckDel"
                    };

                    while (!textFieldParser.EndOfData)
                    {
                        createdCount++;

                        if (createdCount == 1)
                            textFieldParser.ReadFields();

                        try
                        {
                            dataTable.Rows.Add(textFieldParser.ReadFields());
                        }
                        catch (Exception ex)
                        {
                            LogException(ex, "[Daily] Del CSV File load");
                        }

                        if (createdCount % _batchSize == 0)
                        {
                            InsertDataTable(sqlBulkCopy, sqlConnection, dataTable);
                        }
                    }
                    // Don't forget to send the last batch under _batchSize
                    if (dataTable.Rows.Count > 0)
                        InsertDataTable(sqlBulkCopy, sqlConnection, dataTable);

                    //Nov.07, 2022 modify updated format from yyyy-MM-dd to yyyy/MM/dd
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        sqlCommand.CommandText = @"
                        UPDATE WorldCheck
                        SET WorldCheck.Updated = convert(varchar(10),convert(date,TempWorldCheckDel.Updated),111),
                            WorldCheck.DelFlag = 1,
                            WorldCheck.UPDDate = @date
                                FROM TempWorldCheckDel
                                where WorldCheck.UID = TempWorldCheckDel.UID;
                          ";
                        sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        //Console.WriteLine("Update:{0}", UpdateCount);
                        LogAdd("[WorldCheckDeleteDay] Update DelFlag count:" + UpdateCount);
                        OFACEventLog("[WorldCheckDeleteDay] Update DelFlag count:" + UpdateCount);
                    }

                    //write the delete data status = 3
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        sqlCommand.CommandText = @"
                        insert into WorldCheckHist
select UID,UID+@entnum,CAST([UID] AS nvarchar(20))+ '|' +isnull([LastName],'')+ '|' +isnull([FirstName],'')+ '|' +isnull([Aliases],'')+ '|' +isnull([LowQAliases],'')+ '|' +isnull([AlterSpell],'')+ '|' +isnull([Category],'')+ '|' +isnull([Title],'')
+ '|' +isnull([SubCategory],'')+ '|' +isnull([Position],'')+ '|' +CAST(isnull([Age],'') AS nvarchar(20))
+ '|' +CAST(isnull([DOB],'') AS nvarchar(20))+ '|' +isnull([PlaceOfBirth],'')+ '|' +isnull([Deceased],'')+ '|' +isnull([Passports],'')+ '|' +isnull([SSN],'')+ '|' + isnull([IDENTIFICATION_NUMBERS],'') + '|' +isnull([Location],'')+ '|' +isnull([Country],'')+ '|' +isnull([Company],'')+ '|' +isnull([Type],'')
+ '|' +isnull([LinkedTo],'')+ '|' +isnull([FurtherInfo],'')
+ '|' +isnull([Keywords],'')+ '|' +isnull([ExSources],'')+ '|' +isnull([UpdCategory],'')+ '|' +isnull([Entered],'')+ '|' +isnull([Updated],'')+ '|' +isnull([Editor],'')+ '|' +isnull([AgeDate],''),@date,3
from WorldCheck where uid in (select uid from TempWorldCheckDel);
                          ";
                        sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                        sqlCommand.Parameters.AddWithValue("@entnum", int.Parse(ent_num));
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        LogAdd("[WCToHist] Write to World Check Hist table deleted data count:" + UpdateCount);
                    }

                    sqlConnection.Close();
                    sqlConnection.Dispose();
                }
            }
            LogAdd(string.Format("[Daily][WorldCheckday] worldcheckday del file import -- Completion."));
        }

        public void UpdateSDN(string wc_listtype, string str_UPDDate, string str_wc_yearlimit, string wc_excludecountry, string entnum,
            string wc_fixedcountry, string wc_div, string wc_subcate_space, string wc_import_un, string wc_import_SQL, string wc_import_crime, string wc_import_crime_SQL)
        {

            //2019.01 轉檔順序 ADVERSE NEWS -->CAATSA---> DPL --> 311 --> UN (*依序轉入 UN 最大)
            int wc_yearlimit = 6;
            if (!int.TryParse(str_wc_yearlimit, out wc_yearlimit))
                wc_yearlimit = 6;
            wc_yearlimit = -wc_yearlimit;
            LogAdd("[Daily] Update SDNTable -- Start.");

            // update sdn data
            var dt = new DateTime();
            dt = DateTime.Now;

            if (string.IsNullOrEmpty(str_UPDDate))
            {
                str_UPDDate = "30";
            }

            string query = "";

            int int_insertlog = 0;
            int int_updatelog = 0;
            int int_dellog = 0;

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                //已經存在SDN的做更新
                //2021.01 only the individuals set the first name and last name.
                LogAdd("[Daily] Update SDNTable with WorldCheck --  Start");

                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                        update SDNTable
                          set Name = ltrim(case when isnull(wc.FirstName,'') = '-' then '' else isnull(wc.FirstName,'') end + ' ' +isnull(wc.LastName,'')),
                          FirstName = case when isnull(wc.FirstName,'') = '-' then '' 
                                      when isnull(wc.[type],'') in ('M','F','I','U') then isnull(wc.FirstName,'') else '' end,
                          LastName = case when isnull(wc.[type],'') in ('M','F','I','U') then isnull(wc.LastName,'') else '' end,
                          ListType = 'WPEP',
                          ListID = wc.UID,
                          Program = case when wc.SubCategory = '' then 'PEP' else wc.SubCategory end,
                          Title = SUBSTRING(isnull(wc.Position,''),0,200),
                          [type] = case when isnull(wc.[type],'') in ('M','F','I','U') then 'individual' 
                                        when wc.[type] = 'E' and wc.category like '%vessel%' then 'Vessel'
                                        when wc.[type] = 'E' and wc.category not like '%vessel%' then 'Other' 
                                        else 'other' end,
                          Dob = wc.dob,
                          PrimeAdded = '0',
                          Country = wc.Country,
                          Remarks = wc.FurtherInfo + ',Keywords:' + isnull(wc.Keywords,'')+',ExSources:'+isnull(wc.ExSources,''),
                          Remarks1 = SUBSTRING(isnull(wc.Keywords,''),0,255),
                          Remarks2 = SUBSTRING(isnull(Location,''),0,255),
                          sdntype =
                          case 
                          when wc.[type] = 'E' and wc.category not like '%vessel%' then '4'
                          when wc.[type] = 'E' and wc.category like '%vessel%' then '3' 
                          when wc.[type] in ('m','f', 'i','u') then '1' end,
                          DataType = 
                          case 
                          when wc.Category in ('Country','EMBARGO') then '7' 
                          when wc.Category = 'PORT' and wc.Keywords like '%EC-PRT%' then '6'
                          when wc.Category = 'PORT' and wc.Keywords like '%EC-AIRPORT%' then '5'
                          when wc.Category = 'BANK' and wc.Keywords like '%{INT-BIC}%' then '3'
                          else '1' end,
                          UserRec = '1',Deleted = '0',DuplicRec = '0',
                          IgnoreDerived = '0',
                          ListCreateDate = wc.Entered,
                          ListModifDate = wc.Updated,
                          LastModifDate = @date,
                          LastOper = 'Prime',
                          Status = 3
                        FROM SDNTable A JOIN WorldCheck wc
	                      ON A.EntNum = wc.Entnum 
                        where wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                        and isnull(wc.DelFlag,'0') = '0';
                          ";
                    sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 360;
                    int_updatelog = sqlCommand.ExecuteNonQuery();
                    LogAdd("[Daily] Update SDNTable with WorldCheck --  Completed. Count : " + int_updatelog);
                    
                }

                //不存在的才做insert
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    LogAdd("[Daily] Insert SDNTable with WorldCheck --  Start.");

                    query = @"
                        insert into SDNTable(EntNum,
                          Name,
                          FirstName,LastName,
                          ListType,
                          ListID,
                          Program,
                          Title,
                          [type],
                          Dob,PrimeAdded,
                          Country,
                          Remarks,
                          Remarks1,
                          Remarks2,
                          SDNType,DataType,
                          UserRec,Deleted,DuplicRec,IgnoreDerived,Status,ListCreateDate,ListModifDate,CreateDate,LastModifDate,LastOper)
                        select distinct wc.Entnum, 
                            ltrim(case when isnull(FirstName,'') = '-' then '' else isnull(FirstName,'') end + ' ' +isnull(LastName,'')) name, 
                            case when isnull(wc.FirstName,'') = '-' then '' when isnull(wc.[type],'') in ('M','F','I','U') then isnull(wc.FirstName,'') else '' end FirstName, 
                            case when isnull(wc.[type],'') in ('M','F','I','U') then isnull(wc.LastName,'') else '' end  LastName, 
                            'WPEP' listtype,
                            wc.UID,
                            case when SubCategory = '' then 'PEP' else SubCategory end,
                            SUBSTRING(isnull(Position,''),0,200) Position,
                            case when isnull(wc.[type],'') in ('M','F','I','U') then 'individual' 
                                when wc.[type] = 'E' and wc.category like '%vessel%' then 'Vessel'
                                when wc.[type] = 'E' and wc.category not like '%vessel%' then 'Other' 
                                else 'other' end,
                            dob,'0' primeadded,
                            Country,
                            FurtherInfo + ',Keywords:' + isnull(Keywords,'')+',isnull(ExSources,''):'+ExSources,
                            SUBSTRING(isnull(Keywords,''),0,255) Keywords,
                            SUBSTRING(isnull(Location,''),0,255) Location,
                            case when wc.[type] = 'E' and wc.category not like '%vessel%' then '4'
                            when wc.[type] = 'E' and wc.category like '%vessel%' then '3' 
                            when wc.[type] in ('m','f', 'i','u') then '1' end,
                            case 
                            when wc.Category in ('Country','EMBARGO') then '7' 
                            when wc.Category = 'PORT' and wc.Keywords like '%EC-PRT%' then '6'
                            when wc.Category = 'PORT' and wc.Keywords like '%EC-AIRPORT%' then '5'
                            else '1' end datatype, '1' userrec,'0' deleted,'0' duplicRec,
                            '0' ignorederived,'2' [status],Entered,Updated,@date createdate,@date lastmodifdate,'Prime' lastoper
                        from [dbo].[WorldCheck] wc
                        where wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                        and isnull(DelFlag,'0') = '0'
                        AND (isnull(SubCategory,'') != '') 
                        and not EXISTS (select Entnum from SDNTable where EntNum = wc.Entnum)

                        union

                        select distinct wc.Entnum, 
                            ltrim(case when isnull(FirstName,'') = '-' then '' else isnull(FirstName,'') end + ' ' +isnull(LastName,'')) name, 
                            case when isnull(wc.FirstName,'') = '-' then '' when isnull(wc.[type],'') in ('M','F','I','U') then isnull(wc.FirstName,'') else '' end FirstName, 
                            case when isnull(wc.[type],'') in ('M','F','I','U') then isnull(wc.LastName,'') else '' end  LastName, 
                            'WPEP' listtype,
                            wc.UID,
                            case when SubCategory = '' then 'PEP' else SubCategory end,
                            SUBSTRING(isnull(Position,''),0,200) Position,
                            case when isnull(wc.[type],'') in ('M','F','I','U') then 'individual' 
                                when wc.[type] = 'E' and wc.category like '%vessel%' then 'Vessel'
                                when wc.[type] = 'E' and wc.category not like '%vessel%' then 'Other' 
                                else 'other' end,
                            dob,'0' primeadded,
                            Country,
                            FurtherInfo + ',Keywords:' + isnull(Keywords,'')+',isnull(ExSources,''):'+ExSources,
                            SUBSTRING(isnull(Keywords,''),0,255) Keywords,
                            SUBSTRING(isnull(Location,''),0,255) Location,
                            case when wc.[type] = 'E' and wc.category not like '%vessel%' then '4'
                            when wc.[type] = 'E' and wc.category like '%vessel%' then '3' 
                            when wc.[type] in ('m','f', 'i','u') then '1' end,
                            case 
                            when wc.Category in ('Country','EMBARGO') then '7' 
                            when wc.Category = 'PORT' and wc.Keywords like '%EC-PRT%' then '6'
                            when wc.Category = 'PORT' and wc.Keywords like '%EC-AIRPORT%' then '5'
                            else '1' end datatype, '1' userrec,'0' deleted,'0' duplicRec,
                            '0' ignorederived,'2' [status],Entered,Updated,@date createdate,@date lastmodifdate,'Prime' lastoper
                        from [dbo].[WorldCheck] wc
                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                        where wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                        and isnull(DelFlag,'0') = '0'
                        and (isnull(SubCategory,'') = '')
                        AND (wcl.Used = 1  ";
                    if (wc_import_crime == "Y")
                    {
                        query += "or Category like '%CRIME%' ";
                    }
                        
                    query += ")";
                    query += @"
                        and not EXISTS (select Entnum from SDNTable where EntNum = wc.Entnum)
                          ";

                    //query += "AND ((SubCategory is not null and SubCategory != '') ";

                    //2019.01 修改為抓table
                    //query += "or (wcl.Used = 1) ";

                    //if (wc_subcate_space == "N")
                    //{
                    //    query += "and SubCategory != '' ";
                    //}

                    //query += ") Or (keywords like '%USTREAS.311%') Or (furtherinfo like '%denied persons list%' and furtherinfo like '%US DEPARTMENT OF COMMERCE%') ";

                    //if (wc_import_un == "Y")
                    //{
                    //    query += wc_import_SQL;
                    //}
                    //if (wc_import_crime == "Y")
                    //{
                    //    query += wc_import_crime_SQL;
                    //}

                    //for (int i = 0; i < getWC_para.Count(); i++)
                    //{
                    //    LogAdd("WCpara_count:" + getWC_para.Count().ToString());
                    //    LogAdd("WCSQL:"+ getWC_para[i].WCSQL);
                    //    LogAdd("WCListtype:" + getWC_para[i].WCListtype);
                    //    LogAdd("WCProgram:" + getWC_para[i].WCProgram);

                    //    if (getWC_para[i].WCSQL != "")
                    //    {
                    //        query += getWC_para[i].WCSQL;
                    //    }
                    //}

                    //query += " ) and not EXISTS (select Entnum from SDNTable where EntNum = wc.Entnum) ";

                    sqlCommand.CommandText = query;
                    sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 360;
                    int_insertlog = sqlCommand.ExecuteNonQuery();
                    LogAdd("[Daily] Insert SDNTable with WorldCheck --  Completed. Count : " + int_insertlog);
                    LogAdd("Insert SDNTable SQL: " + query);
                    
                }

                //更新刪除資料
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    LogAdd("[Daily] Delete SDNTable with WorldCheck --  Start. ");

                    sqlCommand.CommandText = @"
                          update SDNTable
                            set Status = 4, LastModifDate = @date, LastOper = 'Prime'
                          from SDNTable A JOIN WorldCheck wc
                            ON A.EntNum = wc.Entnum 
                          where wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and isnull(wc.DelFlag,'0') = 1;
                          ";
                    sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 360;
                    int_dellog = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    LogAdd("[Daily] Delete SDNTable with WorldCheck @min=" + str_UPDDate + ". --  Completed. Count : " + int_dellog);

                }

                OFACEventLog(string.Format("SDN_WCday total update:{0}, insert:{1}, delete:{2}.", int_updatelog, int_insertlog, int_dellog));

                if (wc_fixedcountry != "Y")
                {
                    LogAdd("[Daily] Get Year Limit:" + wc_yearlimit);

                    //wtire exclude country
                    OFACEventLog(string.Format("Exclude WorldCheckCountry : {0} ", wc_excludecountry));

                    //更新LOG For country對應表的status set status = 1
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Get WorldCheckCountry Status 0 to 1 by Activityhist --  Start");

                        string SQL = @"select code,status from WorldCheckCountry where code in(
                                         select DISTINCT cust.Country from (select * from pbsa.dbo.Activity (nolock) union select * from pbsa.dbo.ActivityHist (nolock)) act 
                                         JOIN pbsa.dbo.Customer cust on act.Cust = cust.Id
                                         where BookDate >= convert(varchar(8),DATEADD(YEAR,@yearlimit,getdate()),112)
                                         and (cust.Id like @branch or cust.Id like 'noncust%')
                                         and Cust.country not in (@excludecountry)
                                       )
                                       and Status = 0;";
                        sqlCommand.CommandText = SQL;
                        sqlCommand.Parameters.AddWithValue("@branch", wc_listtype.Substring(0, 4) + "%");
                        sqlCommand.Parameters.AddWithValue("@yearlimit", wc_yearlimit);
                        string[] arr_excludecountry = wc_excludecountry.Split(',');
                        string strexcludecountry = string.Empty;
                        for (int i = 0; i < arr_excludecountry.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(arr_excludecountry[i]))
                            {
                                strexcludecountry += "@excludecountry" + i + ",";
                                sqlCommand.Parameters.AddWithValue("@excludecountry" + i, arr_excludecountry[i]);
                            }
                        }
                        if (strexcludecountry.Length > 0)
                            strexcludecountry = strexcludecountry.TrimEnd(',');
                        else
                            strexcludecountry = "''";
                        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@excludecountry", strexcludecountry);
                        sqlCommand.CommandTimeout = 360;
                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        string country = string.Empty;
                        try
                        {
                            while (reader.Read())
                            {
                                country += reader[0].ToString() + ",";
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                        LogAdd("[Daily] Get WorldCheckCountry Status 0 to 1 by Activityhist --  Completed. ");

                        if (country.TrimEnd(',').Trim().Length > 0)
                        {
                            OFACEventLog(string.Format("Update WorldCheckCountry Enable : {0}", country.TrimEnd(',')));
                        }
                    }

                    //更新WorldCheckCountry set status = 1
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update WorldCheckCountry Status 0 to 1 by Activityhist --  Start. ");

                        sqlCommand.CommandText = @"
                              update WorldCheckCountry set Status = 1,lastdate = @mdydate
                              where Code in (
                                select DISTINCT cust.Country from  (select * from pbsa.dbo.Activity (nolock) union select * from pbsa.dbo.ActivityHist (nolock)) act 
                                JOIN pbsa.dbo.Customer cust on act.Cust = cust.Id
                              where BookDate >= convert(varchar(8),DATEADD(YEAR,@yearlimit,getdate()),112)
                              and (cust.Id like @branch or cust.Id like 'noncust%')
                               and cust.country not in (@excludecountry)
                              )
                              and status = 0;
                              ";
                        sqlCommand.Parameters.AddWithValue("@branch", wc_listtype.Substring(0, 4) + "%");
                        sqlCommand.Parameters.AddWithValue("@yearlimit", wc_yearlimit);
                        sqlCommand.Parameters.AddWithValue("@mdydate", DateTime.Now.ToString("yyyy-MM-dd"));
                        string[] arr_excludecountry = wc_excludecountry.Split(',');
                        string strexcludecountry = string.Empty;
                        for (int i = 0; i < arr_excludecountry.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(arr_excludecountry[i]))
                            {
                                strexcludecountry += "@excludecountry" + i + ",";
                                sqlCommand.Parameters.AddWithValue("@excludecountry" + i, arr_excludecountry[i]);
                            }
                        }
                        if (strexcludecountry.Length > 0)
                            strexcludecountry = strexcludecountry.TrimEnd(',');
                        else
                            strexcludecountry = "''";
                        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@excludecountry", strexcludecountry);

                        //LogAdd("WroldcheckCountry 0 to 1 SQL : " + sqlCommand.CommandText);

                        sqlCommand.CommandTimeout = 360;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        LogAdd("[Daily] Update WorldCheckCountry Status 0 to 1 by Activityhist --  Complete. Count: " + UpdateCount);

                        OFACEventLog("Update WorldCheckCountry set status = 1 total update:" + UpdateCount);
                    }

                    //更新LOG For country對應表的status set status = 0
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Get WorldCheckCountry Status 1 to 0 by Activityhist --  Start. ");

                        string SQL = @"select code,status from WorldCheckCountry  where Code not in (
                                            select DISTINCT cust.Country from (select * from pbsa.dbo.Activity (nolock) union select * from pbsa.dbo.ActivityHist (nolock)) act 
                                            JOIN pbsa.dbo.Customer cust on act.Cust = cust.Id
                                          where BookDate >= convert(varchar(8),DATEADD(YEAR,@yearlimit,getdate()),112)
                                          and (cust.Id like @branch or cust.Id like 'noncust%')
                                           and cust.country not in (@excludecountry)
                                          )
                                          and status = 1;";
                        sqlCommand.CommandText = SQL;
                        sqlCommand.Parameters.AddWithValue("@branch", wc_listtype.Substring(0, 4) + "%");
                        sqlCommand.Parameters.AddWithValue("@yearlimit", wc_yearlimit);
                        string[] arr_excludecountry = wc_excludecountry.Split(',');
                        string strexcludecountry = string.Empty;
                        for (int i = 0; i < arr_excludecountry.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(arr_excludecountry[i]))
                            {
                                strexcludecountry += "@excludecountry" + i + ",";
                                sqlCommand.Parameters.AddWithValue("@excludecountry" + i, arr_excludecountry[i]);
                            }
                        }
                        if (strexcludecountry.Length > 0)
                            strexcludecountry = strexcludecountry.TrimEnd(',');
                        else
                            strexcludecountry = "''";
                        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@excludecountry", strexcludecountry);
                        sqlCommand.CommandTimeout = 360;

                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        string country0 = string.Empty;
                        try
                        {
                            while (reader.Read())
                            {
                                country0 += reader[0].ToString() + ",";
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                        LogAdd("[Daily] Get WorldCheckCountry Status 1 to 0 by Activityhist --  Complete.");

                        if (country0.TrimEnd(',').Trim().Length > 0)
                        {
                            OFACEventLog(string.Format("Update WorldCheckCountry Disable : {0}", country0.TrimEnd(',')));
                        }
                    }

                    //更新WorldCheckCountry set status = 0
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update WorldCheckCountry Status 1 to 0 by Activityhist --  Start. ");

                        sqlCommand.CommandText = @"
                          update WorldCheckCountry set Status = 0,lastdate = @mdydate
                          where Code not in (
                            select DISTINCT cust.Country from (select * from pbsa.dbo.Activity (nolock) union select * from pbsa.dbo.ActivityHist (nolock)) act 
                            JOIN pbsa.dbo.Customer cust on act.Cust = cust.Id
                          where BookDate >= convert(varchar(8),DATEADD(YEAR,@yearlimit,getdate()),112)
                          and (cust.Id like @branch or cust.Id like 'noncust%')
                           and cust.country not in (@excludecountry)
                          )
                          and status = 1;
                          ";
                        sqlCommand.Parameters.AddWithValue("@branch", wc_listtype.Substring(0, 4) + "%");
                        sqlCommand.Parameters.AddWithValue("@yearlimit", wc_yearlimit);
                        sqlCommand.Parameters.AddWithValue("@mdydate", DateTime.Now.ToString("yyyy-MM-dd"));
                        string[] arr_excludecountry = wc_excludecountry.Split(',');
                        string strexcludecountry = string.Empty;
                        for (int i = 0; i < arr_excludecountry.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(arr_excludecountry[i]))
                            {
                                strexcludecountry += "@excludecountry" + i + ",";
                                sqlCommand.Parameters.AddWithValue("@excludecountry" + i, arr_excludecountry[i]);
                            }
                        }
                        if (strexcludecountry.Length > 0)
                            strexcludecountry = strexcludecountry.TrimEnd(',');
                        else
                            strexcludecountry = "''";
                        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@excludecountry", strexcludecountry);

                        sqlCommand.CommandTimeout = 360;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        LogAdd("[Daily] Update WorldCheckCountry Status 1 to 0 by Activityhist --  Complete. Count: " + UpdateCount);

                        OFACEventLog("Update WorldCheckCountry set status = 0 total update:" + UpdateCount);
                    }
                }


                //更新listtype
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    LogAdd("[Daily] UPDATE SDNTABLE ListType(WPEP, xxxWPEP) from table WorldCheck of UPDdate --  Start");

                    sqlCommand.CommandText = @"
                          update SDNTable
                            set SDNTable.listtype = (select case when wcc.[status] = '1' and CHARINDEX(rtrim(a.Type),rtrim(wcc.Type)) > 0 then @wc_listtype 
                                when wcc.[status] = '1' and wcc.Type is null then @wc_listtype
                                else 'WPEP' end
                            from WorldCheckCountry wcc
                            where a.country = wcc.WorldCheck_Country),
                            LastModifDate = @date, LastOper = 'Prime'
                          from SDNTable a join WorldCheck wc
                            on a.EntNum = wc.Entnum
                          where a.country in (select WorldCheck_Country from WorldCheckCountry)
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and isnull(wc.DelFlag,'0') = 0;
                          ";
                    sqlCommand.Parameters.AddWithValue("@wc_listtype", wc_listtype);
                    sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 360;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Update:{0}", UpdateCount);
                    LogAdd("[Daily] UPDATE SDNTABLE ListType(WPEP, xxxWPEP) from table WorldCheck of UPDdate --  Completed. Count : " + UpdateCount);
                    OFACEventLog("SDN_ListType total update:" + UpdateCount);
                }


                //更新listtype BY worldcheck country
                //201912 save the parameter if there is any modified country data.
                //Nov.07,2022 add inner join and nolock
                int Imodified_country = 0;
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    LogAdd("[Daily] UPDATE SDNTABLE ListType(WPEP, xxxWPEP) from table WorldCheckCountry of lastdate --  Start.");

                    sqlCommand.CommandText = @"
                          update SDNTable
                            set SDNTable.listtype =  case when wcc.[status] = '1' and CHARINDEX(rtrim(a.Type),rtrim(wcc.Type)) > 0 then @wc_listtype 
                                when wcc.[status] = '1' and wcc.Type is null then @wc_listtype
                                else 'WPEP' end,
                            LastModifDate = @date, LastOper = 'Prime'
                               from SDNTable (nolock) a inner join WorldCheckCountry (nolock) wcc on a.Country = wcc.WorldCheck_Country
                                inner JOIN WorldCheck (nolock) wc  ON A.EntNum = wc.Entnum 
                              where wcc.lastdate >= convert(nchar(10),GETDATE(), 112)
                              and isnull(wc.DelFlag,'0') = 0;
                          ";
                    sqlCommand.Parameters.AddWithValue("@wc_listtype", wc_listtype);
                    sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                    sqlCommand.CommandTimeout = 500;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Update:{0}", UpdateCount);
                    LogAdd("[Daily] UPDATE SDNTABLE ListType(WPEP, xxxWPEP) from table WorldCheckCountry of lastdate --  Completed. Count : " + UpdateCount);
                    OFACEventLog("SDN_ListType total update by country:" + UpdateCount);
                    Imodified_country = UpdateCount;
                }

                //更新LOG For country status = 1 的所有國家
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    LogAdd("[Daily] Get WorldCheckCountry where Status = 1 --  Start. ");

                    string SQL = @"select code from WorldCheckCountry (nolock)
                                 where Status = 1;";
                    sqlCommand.CommandText = SQL;
                    sqlCommand.CommandTimeout = 360;

                    SqlDataReader reader = sqlCommand.ExecuteReader();
                    string country1 = string.Empty;
                    try
                    {
                        while (reader.Read())
                        {
                            country1 += reader[0].ToString() + ",";
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                    LogAdd("[Daily] Get WorldCheckCountry where Status = 1 --  Complete.");

                    if (country1.TrimEnd(',').Trim().Length > 0)
                    {
                        OFACEventLog(string.Format("Show WorldCheckCountry All : {0}", country1.TrimEnd(',')));
                    }
                }

                if (wc_div == "Y")
                {
                    //更新listtype For SD
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update SDNTable ListType for SD --  Start.");

                        sqlCommand.CommandText = @"
                          update SDNTable
                            set SDNTable.listtype = (select wcc.listtype
                            from WorldCheckCountry wcc
                            where a.country = wcc.WorldCheck_Country),
                            LastModifDate = @date, LastOper = 'Prime'
                          from SDNTable a join WorldCheck wc
                            on a.EntNum = wc.Entnum
                          where a.country in (select WorldCheck_Country from WorldCheckCountry)
                          and isnull(wc.SubCategory,'') != ''
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE());
                          ";
                        sqlCommand.Parameters.AddWithValue("@date", dt.ToString("yyyy-MM-dd HH:mm:ss"));
                        sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                        sqlCommand.CommandTimeout = 500;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        LogAdd("[Daily] Update SDNTable ListType for SD --  Completed. Count : " + UpdateCount);
                        OFACEventLog("SDN_ListType total update for SD:" + UpdateCount);
                    }
                }

                //2019.01 使用table來update 不同的program
                //2019.01 轉檔順序 ADVERSE NEWS -->CAATSA---> DPL --> 311 --> UN (*依序轉入 UN 最大)
                if (!string.IsNullOrEmpty(str_wc_imp_order))
                {
                    string SQL = "";
                    foreach (string str_order in str_wc_imp_order.Split('|'))
                    {
                        using (var sqlCommand = new SqlCommand("", sqlConnection))
                        {
                            LogAdd("[Daily] Update SDNTable ListType for " + str_order + " --  Start.");

                            SQL = @"
                            update SDNTable set ListType = 'WORLD-CHECK',Program = @order
                            from SDNTable a join WorldCheck wc on a.EntNum = wc.Entnum
                            left join WCKeywords wck on wc.UID = wck.uid
                            left join WCKeywordslistTable wckl on wck.Word = wckl.Abbreviation
                            where isnull(wc.DelFlag,'0') = '0'
                            and (wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())";
                            if (Imodified_country > 1)
                            {
                                //means there added new country, need to update all sanction data from the countrys.
                                SQL += @"
                            or wc.Country in (select WorldCheck_Country from WorldCheckCountry where LastDate >= convert(nchar(10),GETDATE(), 112))";
                            }

                            SQL += ")";

                            SQL += @"
                            and ((wckl.Used = 1 and wckl.SDNPROGRAM = @order ";
                            if (!string.IsNullOrEmpty(wc_import_crime_SQL) && wc_import_crime_SQL.ToUpper() == str_order.ToUpper())
                            {
                                SQL += " and (isnull(SubCategory,'') = '' or a.listtype != @wc_listtype)) or (wc.Category like '%CRIME%' and (isnull(SubCategory,'') = '' or a.listtype != @wc_listtype) ) ";
                            }
                            else
                            {
                                SQL += ") ";
                            }
                            SQL += ")";
                            sqlCommand.CommandText = SQL;
                            sqlCommand.Parameters.AddWithValue("@wc_listtype", wc_listtype);
                            sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                            sqlCommand.Parameters.AddWithValue("@order", str_order);
                            sqlCommand.CommandTimeout = 500;
                            int UpdateCount = sqlCommand.ExecuteNonQuery();
                            //顯示更新筆數
                            LogAdd("[Daily] Update SDNTable ListType for " + str_order + " SQL=" + SQL);
                            LogAdd("[Daily] Update SDNTable ListType for " + str_order + " --  Completed. Count : " + UpdateCount);
                            OFACEventLog("SDN_ListType total update for " + str_order + ": " + UpdateCount);
                        }
                    }
                }

                /*
                //更新Program For FINCEN311
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    LogAdd("[Daily] Update SDNTable ListType for FINCEN311 --  Start.");

                    sqlCommand.CommandText = @"
                          update SDNTable set ListType = 'WORLD-CHECK',Program = 'FINCEN311'
                          from SDNTable a join WorldCheck wc
                            on a.EntNum = wc.Entnum
                          where isnull(wc.DelFlag,'0') = '0'
                          and (wc.keywords like '%USTREAS.311%') 
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE());
                          ";
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 500;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Update:{0}", UpdateCount);
                    LogAdd("[Daily] Update SDNTable ListType for FINCEN311 --  Completed. Count : " + UpdateCount);
                    OFACEventLog("SDN_ListType total update for FINCEN311:" + UpdateCount);
                }

                //更新Program For DPL
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    LogAdd("[Daily] Update SDNTable ListType for DPL --  Start.");

                    sqlCommand.CommandText = @"
                          update SDNTable set ListType = 'WORLD-CHECK', Program = 'DPL'
                          from SDNTable a join WorldCheck wc
                            on a.EntNum = wc.Entnum
                          where isnull(wc.DelFlag,'0') = '0'
                          and (furtherinfo like '%denied persons list%' and furtherinfo like '%US DEPARTMENT OF COMMERCE%') 
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE());
                          ";
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 500;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                    //顯示更新筆數
                    Console.WriteLine("Update:{0}", UpdateCount);
                    LogAdd("[Daily] Update SDNTable ListType for DPL --  Completed. Count : " + UpdateCount);
                    OFACEventLog("SDN_ListType total update for DPL:" + UpdateCount);
                }

                //更新Program For Negative news. note:不會將PEP的黑名單改成負面消息的黑名單
                if (wc_import_crime == "Y")
                {
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update SDNTable ListType for Negative news --  Start.");

                        sqlCommand.CommandText = "";
                        string str_SQL = @"
                          update OFAC.dbo.SDNTable set ListType = 'WORLD-CHECK', Program = 'Negative news' 
                          from OFAC.dbo.SDNTable a join OFAC.dbo.WorldCheck wc 
                            on a.EntNum = wc.Entnum 
                          where isnull(wc.DelFlag,'0') = '0' 
                          and (1 = 0 ";
                        str_SQL += wc_import_crime_SQL;
                        str_SQL += @") and (listtype = 'WPEP' or program = 'PEP' or program is null)
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE()); 
                          ";
                        sqlCommand.CommandText = str_SQL;
                        //sqlCommand.Parameters.AddWithValue("@wc_listtype", wc_listtype);
                        sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                        sqlCommand.CommandTimeout = 500;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        LogAdd("[Daily] Update SDNTable ListType for Negative news --  Completed. Count : " + UpdateCount);
                        LogAdd("[SQL] SQL for Negative news:" + str_SQL);
                        OFACEventLog("SDN_ListType total update for Negative news:" + UpdateCount);
                    }
                }

                if (wc_import_un == "Y")
                {
                    //更新Program For UN
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[Daily] Update SDNTable ListType for UN --  Start.");
                        string str_SQL = @"update SDNTable set ListType = 'WORLD-CHECK',Program = 'UN'
                          from SDNTable a join WorldCheck wc
                            on a.EntNum = wc.Entnum
                          where isnull(wc.DelFlag,'0') = '0' 
                          and (1 = 0 ";
                        str_SQL += wc_import_SQL;
                        str_SQL += ") and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE());";

                        sqlCommand.CommandText = str_SQL;
                        sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                        sqlCommand.CommandTimeout = 500;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        LogAdd("[Daily] Update SDNTable ListType for UN --  Completed. Count : " + UpdateCount);
                        LogAdd("[SQL] SQL for UN:" + str_SQL);
                        OFACEventLog("SDN_ListType total update for UN:" + UpdateCount);
                    }
                }
                
                */
                for (int i = 0; i < getWC_para.Count(); i++)
                {
                    if (getWC_para[i].WCSQL != "" && getWC_para[i].WCListtype != "" && getWC_para[i].WCProgram != "")
                    {
                        //更新Program by parameter
                        using (var sqlCommand = new SqlCommand("", sqlConnection))
                        {
                            LogAdd("[Daily] Update SDNTable ListType for " + getWC_para[i].WCProgram + " --  Start.");
                            string str_SQL = @"update SDNTable set ListType = @listtype,Program = @program
                          from SDNTable a join WorldCheck wc
                            on a.EntNum = wc.Entnum
                          where isnull(wc.DelFlag,'0') = '0' 
                          and (1 = 0 ";
                            str_SQL += getWC_para[i].WCSQL;
                            str_SQL += ") and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE());";

                            sqlCommand.CommandText = str_SQL;
                            sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                            sqlCommand.Parameters.AddWithValue("@listtype", getWC_para[i].WCListtype);
                            sqlCommand.Parameters.AddWithValue("@program", getWC_para[i].WCProgram);
                            sqlCommand.CommandTimeout = 500;
                            int UpdateCount = sqlCommand.ExecuteNonQuery();
                            //顯示更新筆數
                            Console.WriteLine("Update:{0}", UpdateCount);
                            LogAdd("[Daily] Update SDNTable ListType for " + getWC_para[i].WCProgram + " --  Completed. Count : " + UpdateCount);
                            LogAdd("[SQL] SQL for " + getWC_para[i].WCProgram + ":" + str_SQL);
                            OFACEventLog("SDN_ListType total update for " + getWC_para[i].WCProgram + ":" + UpdateCount);
                        }
                    }
                }

                sqlConnection.Close();
                sqlConnection.Dispose();
            }

            LogAdd("[Daily] Update SDNTable -- Completion.");
        }

        public void UpdRelationshipDailyDataIntoSqlServer(string str_column_name, string BCPFile_TempRelationship, string BCPFile_Relationship, string BCPFmt_TempRelationship,
            string BCPFile_Error, string BCPFmt_Relationship, string BCPServer, string BCPUser, string BCPPwd, string BaseNum, string wc_subcate_space, string wc_import_un, string wc_import_SQL,
            string wc_import_crime, string wc_import_crime_SQL)
        {
            try
            {
                LogAdd("[Daily] Expand to Relationship ["+ str_column_name + "] -- Start");
                string query = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string LinkedTo = string.Empty;
                string RelationShipId = string.Empty;
                string EntNum = string.Empty;
                string RelatedID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                int RelationShipIdMax = 0;
                int File_TotalNum = 0;
                int File_UpdDelNum = 0;
                int File_UpdAddNum = 0;
                int File_FinalAddNum = 0;
                int finalLinkedToSplit = 0;
                int rowCount = 0;

                DateTime NowDateTime = DateTime.Now;

                if (File.Exists(BCPFile_TempRelationship)) File.Delete(BCPFile_TempRelationship);

                if (File.Exists(BCPFile_Relationship)) File.Delete(BCPFile_Relationship);

                queryMax = string.Format(@" SELECT MAX(RelationshipId)+1 AS RelationshipIdMax FROM OFAC.dbo.Relationship ");

                //query = string.Format(@" SELECT Entnum,LinkedTo FROM OFAC.dbo.WorldCheck WHERE LinkedTo IS NOT NULL AND Updated > DATEADD(Daily,-1,GETDATE()) ");
                
                //2019.01 change to table config
                query = @" SELECT distinct WC.Entnum, ";
                query += "WC." + str_column_name;
                query += @", 
                                         CASE ISNULL(WC.DelFlag, '0') 
                                         WHEN '0' THEN '1' 
                                         WHEN '1' THEN '4' END AS Status 
                                        from [dbo].[WorldCheck] wc
                                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                                         WHERE ";
                query += "WC." + str_column_name + " != '' ";
                query += @"
                                         AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) 
                                         AND (isnull(SubCategory,'') != '') 

                                        union

                                        SELECT distinct WC.Entnum, ";
                query += "WC." + str_column_name;
                query += @", 
                                         CASE ISNULL(WC.DelFlag, '0') 
                                         WHEN '0' THEN '1' 
                                         WHEN '1' THEN '4' END AS Status 
                                        from [dbo].[WorldCheck] wc
                                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                                         WHERE ";
                query += "WC." + str_column_name + " != '' ";
                query += @"
                                         AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) 
                                         and (isnull(SubCategory,'') = '')
                                         AND (wcl.Used = 1 ";
                if (wc_import_crime == "Y")
                {
                    query += @"          or Category like '%CRIME%'";
                }
                                        query += ")";

                Database db;
                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("RelationshipIdMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            RelationShipIdMax = Convert.ToInt32(dataReaderMax["RelationshipIdMax"]);
                        }
                    }
                }

                IDataReader dataReaderDT = db.ExecuteReader(dbCommand);
                DataTable dt = new DataTable();
                dt.Load(dataReaderDT);
                rowCount = dt.Rows.Count;
                dt.Dispose();
                dataReaderDT.Close();
                int intBaseNum = Convert.ToInt32(BaseNum);

                using (StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TempRelationship, false, Encoding.GetEncoding(1252)))
                {
                    using (IDataReader dataReader = db.ExecuteReader(dbCommand))
                    {
                        while (dataReader.Read())
                        {
                            EntNum = dataReader["Entnum"].ToString().Trim();
                            //LinkedTo = dataReader[str_column_name].ToString().Trim();
                            Status = dataReader["Status"].ToString().Trim();
                            string[] LinkedToSplit = dataReader[str_column_name].ToString().Trim().Split(';');
                            int i;
                            for (i = 0; i < LinkedToSplit.Length; checked(i)++)
                            {
                                int linkedNumber = 0;
                                if (LinkedToSplit[i].Trim() != "" && int.TryParse(LinkedToSplit[i].Trim(), out linkedNumber))
                                {

                                    finalLinkedToSplit = linkedNumber + intBaseNum;
                                    StringBuilder builder = new StringBuilder();
                                    builder.AppendFormat("{0}\t", RelationShipIdMax.ToString());
                                    builder.AppendFormat("{0}\t", EntNum);
                                    builder.AppendFormat("{0}\t", finalLinkedToSplit.ToString());
                                    builder.AppendFormat("{0}", Status);
                                    TempfileDWriter.WriteLine(builder.ToString());
                                    RelationShipIdMax++;
                                    File_TotalNum++;
                                }
                            }
                        }
                        dataReader.Close();
                    }

                    TempfileDWriter.Close();
                }

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TempRelationship ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                if (rowCount != 0)
                {
                    Process p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.TempRelationship in " + BCPFile_TempRelationship + "  -f " + BCPFmt_TempRelationship + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();

                    string queryUpdateDelDetail =
                        string.Format(@" SELECT * FROM OFAC.dbo.Relationship
                                     WHERE EXISTS (SELECT * FROM OFAC.dbo.TempRelationship WHERE Relationship.EntNum = TempRelationship.EntNum AND Relationship.RelatedID = TempRelationship.RelatedID AND TempRelationship.Status='4')
 AND Relationship.RelationshipPToR=@type ");

                    DbCommand dbCommandUpdateDelDetial = db.GetSqlStringCommand(queryUpdateDelDetail);
                    dbCommandUpdateDelDetial.CommandTimeout = 0;
                    var parameter_DelDetail = dbCommandUpdateDelDetial.CreateParameter();
                    parameter_DelDetail.ParameterName = "@type";
                    parameter_DelDetail.Value = str_column_name;
                    dbCommandUpdateDelDetial.Parameters.Add(parameter_DelDetail);
                    using (IDataReader drUpdateDelDetail = db.ExecuteReader(dbCommandUpdateDelDetial))
                    {
                        while (drUpdateDelDetail.Read())
                        {
                            //if (File_UpdDelNum == 0) { LogAdd("[Daily] Relationship - Set Status to 4: "); };
                            //LogAdd(
                            //    drUpdateDelDetail["RelationshipId"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["EntNum"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["RelatedID"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["RelationshipPToR"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["RelationshipRToP"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListCreateDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateOper"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastOper"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastModify"].ToString().Trim());
                            File_UpdDelNum++;
                        }
                    }
                    dbCommandUpdateDelDetial.Dispose();

                    string queryUpdateDel =
                        string.Format(@"  UPDATE OFAC.dbo.Relationship
 SET Status='4', LastModify=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempRelationship WHERE Relationship.EntNum = TempRelationship.EntNum AND Relationship.RelatedID = TempRelationship.RelatedID AND TempRelationship.Status='4')
 AND Relationship.RelationshipPToR=@type ");

                    DbCommand dbCommandUpdateDel = db.GetSqlStringCommand(queryUpdateDel);
                    dbCommandUpdateDel.CommandTimeout = 0;
                    var parameter_del = dbCommandUpdateDel.CreateParameter();
                    parameter_del.ParameterName = "@type";
                    parameter_del.Value = str_column_name;
                    dbCommandUpdateDel.Parameters.Add(parameter_del);

                    db.ExecuteReader(dbCommandUpdateDel);

                    dbCommandUpdateDel.Dispose();

                    string queryUpdateAddDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.Relationship
                                 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempRelationship WHERE Relationship.EntNum = TempRelationship.EntNum AND Relationship.RelatedID = TempRelationship.RelatedID AND TempRelationship.Status='1')
 AND Relationship.RelationshipPToR=@type ");

                    DbCommand dbCommandUpdateAddDetial = db.GetSqlStringCommand(queryUpdateAddDetail);
                    dbCommandUpdateAddDetial.CommandTimeout = 0;
                    var parameter_AddDetail = dbCommandUpdateAddDetial.CreateParameter();
                    parameter_AddDetail.ParameterName = "@type";
                    parameter_AddDetail.Value = str_column_name;
                    dbCommandUpdateAddDetial.Parameters.Add(parameter_AddDetail);

                    using (IDataReader drUpdateAddDetail = db.ExecuteReader(dbCommandUpdateAddDetial))
                    {
                        while (drUpdateAddDetail.Read())
                        {
                            //if (File_UpdAddNum == 0) { LogAdd("[Daily] Relationship - Set Status to 1: "); };
                            //LogAdd(
                            //    drUpdateAddDetail["RelationshipId"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["EntNum"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["RelatedID"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["RelationshipPToR"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["RelationshipRToP"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListCreateDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateOper"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastOper"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastModify"].ToString().Trim());
                            File_UpdAddNum++;
                        }
                    }

                    dbCommandUpdateAddDetial.Dispose();

                    string queryUpdateAdd =
                        string.Format(@"  UPDATE OFAC.dbo.Relationship
 SET Status='3', LastModify=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempRelationship WHERE Relationship.EntNum = TempRelationship.EntNum AND Relationship.RelatedID = TempRelationship.RelatedID AND TempRelationship.Status!='4')
 AND Relationship.RelationshipPToR=@type ");

                    DbCommand dbCommandUpdateAdd = db.GetSqlStringCommand(queryUpdateAdd);
                    dbCommandUpdateAdd.CommandTimeout = 0;
                    
                    var parameter_add = dbCommandUpdateAdd.CreateParameter();
                    parameter_add.ParameterName = "@type";
                    parameter_add.Value = str_column_name;
                    dbCommandUpdateAdd.Parameters.Add(parameter_add);
                    db.ExecuteReader(dbCommandUpdateAdd);

                    dbCommandUpdateAdd.Dispose();

                    DataTable data_tmp = new DataTable();
                    data_tmp.TableName = "TMPRlationship";
                    data_tmp.Columns.Add("Entnum", DbType.String.GetType());
                    data_tmp.Columns.Add("RelatedID", DbType.String.GetType());

                    using(StreamWriter fileDWriter = new StreamWriter(BCPFile_Relationship, false, Encoding.GetEncoding(1252)))
                    {
                        string queryAdd =
                        string.Format(@" SELECT DISTINCT RelationshipId, EntNum, RelatedID, '2' Status FROM OFAC.dbo.TempRelationship
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.Relationship 
                                        WHERE TempRelationship.EntNum = EntNum AND TempRelationship.RelatedID = RelatedID 
                                        and RelationshipPToR = @type) and entnum in (select entnum from sdntable(nolock))
                                        and not exists (SELECT * FROM OFAC.dbo.Relationship 
                                        where TempRelationship.EntNum = RelatedID and TempRelationship.RelatedID = EntNum)
                                     ORDER BY RelationshipId, EntNum, RelatedID ");

                        DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                        dbCommandAdd.CommandTimeout = 0;
                        var parameter_Add = dbCommandAdd.CreateParameter();
                        parameter_Add.ParameterName = "@type";
                        parameter_Add.Value = str_column_name;
                        dbCommandAdd.Parameters.Add(parameter_Add);

                        using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                        {
                            while (Tempdr.Read())
                            {
                                //remove duplicate data
                                if (data_tmp.Select("Entnum = '" + Tempdr["RelatedID"].ToString().Trim() + "' and RelatedID = '" + Tempdr["EntNum"].ToString().Trim() + "'").Count() == 0)
                                {
                                    RelationShipId = Tempdr["RelationShipId"].ToString().Trim();
                                    EntNum = Tempdr["EntNum"].ToString().Trim();
                                    RelatedID = Tempdr["RelatedID"].ToString().Trim();
                                    Status = Tempdr["Status"].ToString().Trim();
                                    StringBuilder builder = new StringBuilder();
                                    builder.AppendFormat("{0}\t", RelationShipId);
                                    builder.AppendFormat("{0}\t", EntNum);
                                    builder.AppendFormat("{0}\t", RelatedID);
                                    builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}", str_column_name, str_column_name, Status, NowDateTime.ToString(), NowDateTime.ToString(), "Prime", NowDateTime.ToString(), "Prime", NowDateTime.ToString());
                                    fileDWriter.WriteLine(builder.ToString());
                                    File_FinalAddNum++;

                                    DataRow row = data_tmp.NewRow();
                                    row["Entnum"] = EntNum;
                                    row["RelatedID"] = RelatedID;
                                    data_tmp.Rows.Add(row);
                                    data_tmp.AcceptChanges();
                                }

                            }
                            Tempdr.Close();
                        }

                        dbCommandAdd.Dispose();
                        fileDWriter.Close();
                    }

                    p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.RelationShip in " + BCPFile_Relationship + "  -f " + BCPFmt_Relationship + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();
                }

                //201904 update status where not exists
                string sqlDel = @" update OFAC..Relationship set Status = 4 , LastModify=SYSDATETIME()
                from OFAC..Relationship (nolock) rela
                where EntNum in (select EntNum from OFAC..temprelationship)
                and not exists 
                (select * from OFAC..temprelationship (nolock) trela where rela.EntNum = trela.EntNum and trela.RelatedID = rela.RelatedID) 
                and rela.RelationshipPToR = @type ";
                dbCommandDel = db.GetSqlStringCommand(sqlDel);
                dbCommandDel.CommandTimeout = 0;

                var parameter_Del = dbCommandDel.CreateParameter();
                parameter_Del.ParameterName = "@type";
                parameter_Del.Value = str_column_name;
                dbCommandDel.Parameters.Add(parameter_Del);

                int icount = db.ExecuteReader(dbCommandDel).RecordsAffected;
                
                dbCommandDel.Dispose();
                dbCommand.Dispose();
                dbCommandMax.Dispose();

                LogAdd("[Daily] update Relationship [" + str_column_name + "] set status = 4 where not exists:" + icount);

                LogAdd("[Daily] Temp" + str_column_name + " [" + str_column_name + "] - Import: [" + File_TotalNum + "]; " + str_column_name + " - Set Status to 4: [" + File_UpdDelNum + "], Set Status to 1: [" + File_UpdAddNum + "], Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[Daily] Expand to " + str_column_name + " [" + str_column_name + "] -- Completion");

                OFACEventLog(str_column_name + " [" + str_column_name + "] update: "+ (File_UpdDelNum + File_UpdAddNum).ToString() + ", insert: " + File_FinalAddNum.ToString());

                db = null;
            }
            catch (Exception ex)
            {
                LogException(ex, "UpdRelationshipDailyDataIntoSqlServer");
            }
        }

        public void UpdSDNAltTableDailyDataIntoSqlServer(string strtype, string str_sanctionlist, string BCPFile_TmpSDNAltTable, string BCPFile_SDNAltTable, string BCPFmt_TmpSDNAltTable,
            string BCPFile_Error, string BCPFmt_SDNAltTable, string BCPServer, string BCPUser, string BCPPwd, string wc_subcate_space, string wc_import_un, string wc_import_SQL,
            string wc_import_crime, string wc_import_crime_SQL)
        {
            try
            {
                LogAdd("[Daily] Expand to " + strtype + " -- Start");

                string query = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                string Aliases = string.Empty;
                string AltNum = string.Empty;
                string EntNum = string.Empty;
                string AltType = string.Empty;
                string AltName = string.Empty;
                string UID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");
                string str_type = string.Empty;
                string FirstName = string.Empty;
                string LastName = string.Empty;

                int AltNumMax = 0;
                int File_TotalNum = 0;
                int File_UpdDelNum = 0;
                int File_UpdAddNum = 0;
                int File_FinalAddNum = 0;
                int rowCount = 0;

                DateTime NowDateTime = DateTime.Now;

                

                if (File.Exists(BCPFile_TmpSDNAltTable)) File.Delete(BCPFile_TmpSDNAltTable);

                if (File.Exists(BCPFile_SDNAltTable)) File.Delete(BCPFile_SDNAltTable);

                queryMax = string.Format(@" SELECT MAX(AltNum)+1 AS AltNumMax FROM OFAC.dbo.SDNAltTable ");

                //query = string.Format(@" SELECT Entnum, Aliases FROM OFAC.dbo.WorldCheck WHERE Aliases IS NOT NULL AND Updated > DATEADD(Daily,-1,GETDATE()) ");

                //2019.01 change to table config
                query = @"SELECT WC.Entnum, ";
                if (strtype == "ALT")
                {
                    query += "WC.Aliases, ";
                }
                else if (strtype == "ALTWEAK")
                {
                    query += "WC.LowQAliases, ";
                }
                else if (strtype == "ALTSPELL")
                {
                    query += "WC.AlterSpell, ";
                }
                else if (strtype == "ALTPASS")
                {
                    query += "WC.Passports, ";
                }
                else if (strtype == "ALTBIC" || strtype == "ALTIMO" || strtype == "ALTARN" || strtype == "ALTMSN")
                {
                    query += "WC.IDENTIFICATION_NUMBERS, ";
                }

                query += @"     CASE ISNULL(WC.DelFlag, '0') 
                                        WHEN '0' THEN '1' 
                                        WHEN '1' THEN '4' END AS Status,
                                        isnull(wc.[type],'') as type
                                        FROM OFAC.dbo.WorldCheck WC
                                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation ";
                if (strtype == "ALT")
                {
                    query += "WHERE WC.Aliases != '' ";
                }
                else if (strtype == "ALTWEAK")
                {
                    query += @" left join OFAC.dbo.SDNTable sdn on sdn.EntNum = wc.Entnum 
                                WHERE WC.LowQAliases != '' ";
                    if (str_sanctionlist != "")
                    {
                        query += "  AND sdn.Program in (" + str_sanctionlist + ")";
                    }
                }
                else if (strtype == "ALTSPELL")
                {
                    query += "WHERE WC.AlterSpell != '' ";
                }
                else if (strtype == "ALTPASS")
                {
                    query += "WHERE WC.Passports != '' ";
                }
                else if (strtype == "ALTBIC" || strtype == "ALTIMO" || strtype == "ALTARN" || strtype == "ALTMSN")
                {
                    query += "WHERE WC.IDENTIFICATION_NUMBERS != '' ";
                    if (strtype == "ALTBIC")
                    {
                        query += "and WC.Category = 'BANK' ";
                    }
                }

                query += @"     AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) 
                                AND (isnull(SubCategory,'') != '') 
                        union";
                query += @" SELECT WC.Entnum, ";
                if (strtype == "ALT")
                {
                    query += "WC.Aliases, ";
                }
                else if (strtype == "ALTWEAK")
                {
                    query += "WC.LowQAliases, ";
                }
                else if (strtype == "ALTSPELL")
                {
                    query += "WC.AlterSpell, ";
                }
                else if (strtype == "ALTPASS")
                {
                    query += "WC.Passports, ";
                }
                else if (strtype == "ALTBIC" || strtype == "ALTIMO" || strtype == "ALTARN" || strtype == "ALTMSN")
                {
                    query += "WC.IDENTIFICATION_NUMBERS, ";
                }

                query += @"     CASE ISNULL(WC.DelFlag, '0') 
                                        WHEN '0' THEN '1' 
                                        WHEN '1' THEN '4' END AS Status,
                                        isnull(wc.[type],'') as type
                                        FROM OFAC.dbo.WorldCheck WC
                                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation ";
                if (strtype == "ALT")
                {
                    query += "WHERE WC.Aliases != '' ";
                }
                else if (strtype == "ALTWEAK")
                {
                    query += @" left join OFAC.dbo.SDNTable sdn on sdn.EntNum = wc.Entnum 
                                WHERE WC.LowQAliases != '' ";
                    if (str_sanctionlist != "")
                    {
                        query += "  AND sdn.Program in (" + str_sanctionlist + ")";
                    }
                }
                else if (strtype == "ALTSPELL")
                {
                    query += "WHERE WC.AlterSpell != '' ";
                }
                else if (strtype == "ALTPASS")
                {
                    query += "WHERE WC.Passports != '' ";
                }
                else if (strtype == "ALTBIC" || strtype == "ALTIMO" || strtype == "ALTARN" || strtype == "ALTMSN")
                {
                    query += "WHERE WC.IDENTIFICATION_NUMBERS != '' ";
                    if (strtype == "ALTBIC")
                    {
                        query += "and WC.Category = 'BANK' ";
                    }
                }

                query += @"     AND WC.UPDDate > DATEADD(DAY, -1, GETDATE()) 
                                    AND (isnull(SubCategory,'') = '')
                                         AND (wcl.Used = 1 ";
                if (wc_import_crime == "Y")
                {
                    query += @"          or Category like '%CRIME%'";
                }
                query += ")";
                /*
                query += "AND ((SubCategory is not null ";

                if (wc_subcate_space == "N")
                {
                    query += "and SubCategory != '' ";
                }

                query += ") Or (keywords like '%USTREAS.311%') Or (furtherinfo like '%denied persons list%' and furtherinfo like '%US DEPARTMENT OF COMMERCE%') ";

                if (wc_import_un == "Y")
                {
                    query += wc_import_SQL;
                }
                if (wc_import_crime == "Y")
                {
                    query += wc_import_crime_SQL;
                }

                //20181022. for parameter
                for (int i = 0; i < getWC_para.Count(); i++)
                {
                    if (getWC_para[i].WCSQL != "" && getWC_para[i].WCListtype != "" && getWC_para[i].WCProgram != "")
                    {
                        query += getWC_para[i].WCSQL;
                    }
                }

                query += " );";
                */

                Database db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("AltNumMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            AltNumMax = Convert.ToInt32(dataReaderMax["AltNumMax"]);
                        }
                    }
                }
                dbCommandMax.Dispose();
                IDataReader dataReaderDT = db.ExecuteReader(dbCommand);
                DataTable dt = new DataTable();
                dt.Load(dataReaderDT);
                rowCount = dt.Rows.Count;
                dt.Dispose();
                dataReaderDT.Close();
                
                using(StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TmpSDNAltTable, false, Encoding.GetEncoding(1252)))
                {
                    using (IDataReader dr = db.ExecuteReader(dbCommand))
                    {
                        while (dr.Read())
                        {
                            EntNum = dr["Entnum"].ToString().Trim();
                            Status = dr["Status"].ToString().Trim();
                            str_type = dr["type"].ToString().Trim();

                            try
                            {
                                string[] AliasesSplit = null;
                                if (strtype == "ALT")
                                {
                                    AliasesSplit = dr["Aliases"].ToString().Trim().Split(';');
                                }
                                else if (strtype == "ALTWEAK")
                                {
                                    AliasesSplit = dr["LowQAliases"].ToString().Trim().Split(';');
                                }
                                else if (strtype == "ALTSPELL")
                                {
                                    AliasesSplit = dr["AlterSpell"].ToString().Trim().Split(';');
                                }
                                else if (strtype == "ALTPASS")
                                {
                                    AliasesSplit = dr["Passports"].ToString().Trim().Split(';');
                                }
                                else if (strtype == "ALTBIC" || strtype == "ALTIMO" || strtype == "ALTARN" || strtype == "ALTMSN")
                                {
                                    AliasesSplit = dr["IDENTIFICATION_NUMBERS"].ToString().Trim().Split(';');
                                }

                                int i;

                                if (AliasesSplit != null)
                                {
                                    for (i = 0; i < AliasesSplit.Length; i++)
                                    {
                                        if (AliasesSplit[i] != "")
                                        {
                                            if ((strtype == "ALTBIC" && !AliasesSplit[i].Contains("{INT-BIC}") && !AliasesSplit[i].Contains("{INT-BIC6}")) || (strtype == "ALTIMO" && !AliasesSplit[i].Contains("{INT-IMO}")) || (strtype == "ALTARN" && !AliasesSplit[i].Contains("{INT-ARN}")) || (strtype == "ALTMSN" && !AliasesSplit[i].Contains("{INT-MSN}")))
                                                continue;
                                            StringBuilder builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", "");
                                            builder.AppendFormat("{0}\t", AliasesSplit[i].Substring(AliasesSplit[i].IndexOf("}") + 1).Replace("{INT-BIC}", "").Replace("{INT-BIC6}", "").Replace("{INT-IMO}", "").Replace("{INT-ARN}", "").Replace("{INT-MSN}", ""));
                                            //Dec.26 2022 fix for {INT-BIC6}
                                            if ((str_type == "M" || str_type == "F" || str_type == "I" || str_type == "U") && strtype != "ALTPASS")
                                            {
                                                //LogAdd("Process name=" + AliasesSplit[i]);
                                                //individual
                                                //1. 逗號前面是last name
                                                if (AliasesSplit[i].Split(',').Count() > 1)
                                                {
                                                    //LogAdd("comma count=" + (AliasesSplit[i].Split(',').Count()));
                                                    //firstname
                                                    builder.AppendFormat("{0}\t", AliasesSplit[i].Split(',')[1]);
                                                    //lastname
                                                    builder.AppendFormat("{0}\t", AliasesSplit[i].Split(',')[0]);
                                                }
                                                //2. 沒逗號>空白後面是last name
                                                else if (AliasesSplit[i].Split(' ').Count() > 1)
                                                {
                                                    //LogAdd("space count=" + (AliasesSplit[i].Split(' ').Count()));
                                                    //firstname
                                                    builder.AppendFormat("{0}\t", AliasesSplit[i].Split(' ')[0]);
                                                    //lastname
                                                    builder.AppendFormat("{0}\t", AliasesSplit[i].Split(' ')[1]);
                                                }
                                                else
                                                {
                                                    builder.AppendFormat("{0}\t", "");
                                                    builder.AppendFormat("{0}\t", AliasesSplit[i]);
                                                }

                                            }
                                            else if (str_type == "E")
                                            {
                                                //entity
                                                //全部放first name
                                                builder.AppendFormat("{0}\t", "");
                                                builder.AppendFormat("{0}\t", "");
                                            }
                                            else if (strtype == "ALTPASS")
                                            {
                                                builder.AppendFormat("{0}\t", "");
                                                builder.AppendFormat("{0}\t", "");
                                            }
                                            builder.AppendFormat("{0}", Status);
                                            TempfileDWriter.WriteLine(builder.ToString());

                                            File_TotalNum++;
                                            //LogAdd("[Daily] Log for AltName:" + builder.ToString());
                                        }
                                    }

                                }
                            }
                            catch (Exception ex)
                            {
                                LogException(ex, string.Format("[Daily] Expand to {0}", strtype));
                            }
                        }
                        dr.Close();
                    }
                    TempfileDWriter.Close();
                    dbCommand.Dispose();
                }

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TmpSDNAltTable ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);

                dbCommandDel.Dispose();

                if (rowCount != 0)
                {
                    Process p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.TmpSDNAltTable in " + BCPFile_TmpSDNAltTable + " -f " + BCPFmt_TmpSDNAltTable + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();

                    /* 20171016 danny updated : do not update the alternate name's status
                     * because HK hit lot of swift reserved word on alternate name.*/
                    string queryUpdateDelDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.SDNAltTable
                                 WHERE EXISTS (SELECT * FROM OFAC.dbo.TmpSDNAltTable WHERE SDNAltTable.EntNum = TmpSDNAltTable.EntNum AND SDNAltTable.AltName = TmpSDNAltTable.AltName AND TmpSDNAltTable.Status=4) ");

                    DbCommand dbCommandUpdateDelDetial = db.GetSqlStringCommand(queryUpdateDelDetail);
                    dbCommandUpdateDelDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateDelDetail = db.ExecuteReader(dbCommandUpdateDelDetial))
                    {
                        while (drUpdateDelDetail.Read())
                        {
                            //if (File_UpdDelNum == 0) { LogAdd("[Daily] SDNAltTable - Set Status to 4: "); };
                            //LogAdd(
                            //    drUpdateDelDetail["EntNum"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["AltNum"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["AltType"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["AltName"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["FirstName"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["MiddleName"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastName"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListID"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["Remarks"].ToString().Trim() + ", " +
                            //    drUpdateDelDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListCreateDate"].ToString().Trim() + ", " +
                            //    drUpdateDelDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastModifDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastOper"].ToString().Trim());
                            File_UpdDelNum++;
                        }
                    }

                    /*
                     * 2021, Feb. BOT asked do not update those alt status to 4 which sdn status = 4
                     
                    string queryUpdateDel =
                        string.Format(@"  UPDATE OFAC.dbo.SDNAltTable
 SET Status=4, LastModifDate=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TmpSDNAltTable WHERE SDNAltTable.EntNum = TmpSDNAltTable.EntNum AND SDNAltTable.AltName = TmpSDNAltTable.AltName AND TmpSDNAltTable.Status=4) 
 and SDNAltTable.Status != 4");

                    DbCommand dbCommandUpdateDel = db.GetSqlStringCommand(queryUpdateDel);
                    dbCommandUpdateDel.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateDel);
                    */
                    string queryUpdateAddDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.SDNAltTable
                                 WHERE EXISTS (SELECT * FROM OFAC.dbo.TmpSDNAltTable WHERE SDNAltTable.EntNum = TmpSDNAltTable.EntNum AND SDNAltTable.AltName = TmpSDNAltTable.AltName AND TmpSDNAltTable.Status=1) ");

                    DbCommand dbCommandUpdateAddDetial = db.GetSqlStringCommand(queryUpdateAddDetail);
                    dbCommandUpdateAddDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateAddDetail = db.ExecuteReader(dbCommandUpdateAddDetial))
                    {
                        while (drUpdateAddDetail.Read())
                        {
                            //if (File_UpdAddNum == 0) { LogAdd("[Daily] SDNAltTable - Set Status to 1: "); };
                            //LogAdd(
                            //    drUpdateAddDetail["EntNum"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["AltNum"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["AltType"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["AltName"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["FirstName"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["MiddleName"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastName"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListID"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["Remarks"].ToString().Trim() + ", " +
                            //    drUpdateAddDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListCreateDate"].ToString().Trim() + ", " +
                            //    drUpdateAddDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastModifDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastOper"].ToString().Trim());
                            File_UpdAddNum++;
                        }
                    }

                    //2021 Feb. add statement deleted = 0 to prevent changing user deleted data.
                    string queryUpdateAdd =
                        string.Format(@" UPDATE OFAC.dbo.SDNAltTable
 SET Status=1, LastModifDate=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TmpSDNAltTable WHERE SDNAltTable.EntNum = TmpSDNAltTable.EntNum AND SDNAltTable.AltName = TmpSDNAltTable.AltName AND TmpSDNAltTable.Status=1) 
 and SDNAltTable.Status != 1 and deleted = 0 ");

                    if (strtype == "ALTWEAK")
                    {
                        queryUpdateAdd += "and listid = 'LOWQALIASES' ";
                    }
                    else if (strtype == "ALT")
                    {
                        queryUpdateAdd += "and isnull(listid,'') in ('ALT','') ";
                    }
                    else if (strtype == "ALTSPELL")
                    {
                        queryUpdateAdd += "and listid = 'ALTSPELL' ";
                    }
                    else if (strtype == "ALTPASS")
                    {
                        queryUpdateAdd += "and listid = 'PASSPORT' ";
                    }
                    else if (strtype == "ALTBIC")
                    {
                        queryUpdateAdd += "and listid = 'BIC' ";
                    }
                    else if (strtype == "ALTIMO")
                    {
                        queryUpdateAdd += "and listid = 'IMO' ";
                    }
                    else if (strtype == "ALTARN")
                    {
                        queryUpdateAdd += "and listid = 'ARN' ";
                    }
                    else if (strtype == "ALTMSN")
                    {
                        queryUpdateAdd += "and listid = 'MSN' ";
                    }

                    DbCommand dbCommandUpdateAdd = db.GetSqlStringCommand(queryUpdateAdd);
                    dbCommandUpdateAdd.CommandTimeout = 0;
                    //2021 19, Mar. to prevent insert duplicate altname, mark this part.
                    //db.ExecuteReader(dbCommandUpdateAdd);
                    
                    using(StreamWriter fileDWriter = new StreamWriter(BCPFile_SDNAltTable, false, Encoding.GetEncoding(1252)))
                    {
                        string queryAdd =
                        string.Format(@" SELECT DISTINCT EntNum, AltName, FirstName, LastName, '2' Status FROM OFAC.dbo.TmpSDNAltTable 
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.SDNAltTable WHERE TmpSDNAltTable.EntNum = EntNum AND TmpSDNAltTable.AltName = AltName 
                                                       and (status != 4 or deleted = 1)
                                     ");
                        if (strtype == "ALTWEAK")
                        {
                            queryAdd += "and listid = 'LOWQALIASES' ";
                        }
                        else if (strtype == "ALT")
                        {
                            queryAdd += "and isnull(listid,'') in ('ALT','') ";
                        }
                        else if (strtype == "ALTSPELL")
                        {
                            queryAdd += "and listid = 'ALTSPELL' ";
                        }
                        else if (strtype == "ALTPASS")
                        {
                            queryAdd += "and listid = 'PASSPORT' ";
                        }
                        else if (strtype == "ALTBIC")
                        {
                            queryAdd += "and listid = 'BIC' ";
                        }
                        else if (strtype == "ALTIMO")
                        {
                            queryAdd += "and listid = 'IMO' ";
                        }
                        else if (strtype == "ALTARN")
                        {
                            queryAdd += "and listid = 'ARN' ";
                        }
                        else if (strtype == "ALTMSN")
                        {
                            queryAdd += "and listid = 'MSN' ";
                        }

                        queryAdd += ") ORDER BY EntNum, AltName ";

                        DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                        dbCommandAdd.CommandTimeout = 0;
                        using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                        {
                            while (Tempdr.Read())
                            {
                                EntNum = Tempdr["EntNum"].ToString().Trim();
                                AltNum = AltNumMax.ToString();
                                AltName = Tempdr["AltName"].ToString().Trim();
                                FirstName = Tempdr["FirstName"].ToString().Trim();
                                LastName = Tempdr["LastName"].ToString().Trim();
                                Status = Tempdr["Status"].ToString().Trim();
                                StringBuilder builder = new StringBuilder();
                                builder.AppendFormat("{0}\t", EntNum);
                                builder.AppendFormat("{0}\t", AltNum);
                                if (strtype == "ALTSPELL")
                                {
                                    builder.AppendFormat("{0}\t", "f.k.a.");
                                }
                                else
                                {
                                    builder.AppendFormat("{0}\t", "a.k.a.");
                                }
                                builder.AppendFormat("{0}\t", AltName);
                                builder.AppendFormat("{0}\t", FirstName);
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", LastName);

                                if (strtype == "ALTSPELL")
                                {
                                    builder.AppendFormat("{0}\t", "ALTSPELL");
                                }
                                else if (strtype == "ALTPASS")
                                {
                                    builder.AppendFormat("{0}\t", "PASSPORT");
                                }
                                else if (strtype == "ALTBIC")
                                {
                                    builder.AppendFormat("{0}\t", "BIC");
                                }
                                else if (strtype == "ALTIMO")
                                {
                                    builder.AppendFormat("{0}\t", "IMO");
                                }
                                else if (strtype == "ALTARN")
                                {
                                    builder.AppendFormat("{0}\t", "ARN");
                                }
                                else if (strtype == "ALTMSN")
                                {
                                    builder.AppendFormat("{0}\t", "MSN");
                                }
                                else if (strtype == "ALT")
                                {
                                    builder.AppendFormat("{0}\t", "ALT");
                                }
                                else if (strtype == "ALTWEAK")
                                {
                                    builder.AppendFormat("{0}\t", "LOWQALIASES");
                                }
                                else
                                {
                                    builder.AppendFormat("{0}\t", "");
                                }

                                builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}", "", "0", Status, NowDateTime.ToString(), NowDateTime.ToString(), NowDateTime.ToString(), NowDateTime.ToString(), "Prime", strtype == "ALTWEAK" ? "weak" : "");
                                fileDWriter.WriteLine(builder.ToString());
                                AltNumMax++;
                                File_FinalAddNum++;
                            }
                            Tempdr.Close();
                        }

                        fileDWriter.Close();
                        dbCommandAdd.Dispose();
                    }

                    p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.SDNAltTable in " + BCPFile_SDNAltTable + " -f " + BCPFmt_SDNAltTable + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    //if (File.Exists(BCPFile_TmpSDNAltTable)) File.Delete(BCPFile_TmpSDNAltTable);
                    p.Start();
                    p.WaitForExit();
                }

                //201904 update status where not exists
                string sqlDel = @" update SDNAltTable set Status = 4 ,LastModifDate=SYSDATETIME(), remarks = 'Expired AKAs'
                from OFAC..SDNAltTable (nolock) sdna
                where EntNum in (select EntNum from OFAC..TmpSDNAltTable)
                and not exists 
                (select * from OFAC..TmpSDNAltTable (nolock) tsdna where sdna.EntNum = tsdna.EntNum and tsdna.AltName = sdna.AltName) ";
                if (strtype == "ALTWEAK")
                {
                    sqlDel += "and sdna.listid = 'LOWQALIASES' ";
                }
                else if (strtype == "ALT")
                {
                    sqlDel += "and isnull(sdna.listid,'') in ('ALT','') ";
                }
                else if (strtype == "ALTSPELL")
                {
                    sqlDel += "and sdna.listid = 'ALTSPELL' ";
                }
                else if (strtype == "ALTPASS")
                {
                    sqlDel += "and sdna.listid = 'PASSPORT' ";
                }
                else if (strtype == "ALTBIC")
                {
                    sqlDel += "and sdna.listid = 'BIC' ";
                }
                else if (strtype == "ALTIMO")
                {
                    sqlDel += "and sdna.listid = 'IMO' ";
                }
                else if (strtype == "ALTARN")
                {
                    sqlDel += "and sdna.listid = 'ARN' ";
                }
                else if (strtype == "ALTMSN")
                {
                    sqlDel += "and sdna.listid = 'MSN' ";
                }

                dbCommandDel = db.GetSqlStringCommand(sqlDel);
                dbCommandDel.CommandTimeout = 0;
                int icount = db.ExecuteReader(dbCommandDel).RecordsAffected;
                dbCommandDel.Dispose();
                LogAdd("[Daily] update " + strtype + " set status = 4 where not exists:" + icount);

                LogAdd("[Daily] TmpSDNAltTable - Import: [" + File_TotalNum + "]; " + strtype + " - Set Status to 4: [" + File_UpdDelNum + "], Set Status to 1: [" + File_UpdAddNum + "], Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[Daily] Expand to " + strtype + " -- Completion");

                OFACEventLog(strtype + " update: "+ (File_UpdDelNum + File_UpdAddNum).ToString() + ", insert : " + File_FinalAddNum.ToString());

                //2018.07 alt type fix
                if (strtype == "ALT")
                {
                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        sqlConnection.Open();
                        //修正WC修改過catory 或subcatory等欄位，導致program = pep的名單program = Other
                        //Nov.07, 2022 fix del data's listtype and program.
                        using (var sqlCommand = new SqlCommand("", sqlConnection))
                        {
                            LogAdd("[Daily] Update SDN program = other --  Start.");

                            sqlCommand.CommandText = @"
                          update OFAC.dbo.SDNTable set listtype = 'WORLD-CHECK', program = 'OTHER' where EntNum > 20000000 and isnull(Program,'') in ('PEP','');
                          ";
                            sqlCommand.CommandTimeout = 500;
                            int UpdateCount = sqlCommand.ExecuteNonQuery();
                            //顯示更新筆數
                            Console.WriteLine("Update:{0}", UpdateCount);
                            LogAdd("[Daily] Update SDN program = other --  Completed. Count : " + UpdateCount);
                            OFACEventLog("Update SDN program = other total updated:" + UpdateCount);

                            /*2021.01 update status = 4 where worldcheck aliases field is empty.*/
                            sqlCommand.CommandText = string.Format(@"UPDATE OFAC.dbo.SDNAltTable set Status = 4, LastModifDate = getdate(), remarks = 'Expired AKAs'
                            from SDNAltTable sdna
                            join WorldCheck wc on wc.EntNum = sdna.EntNum
                            where isnull(wc.Aliases,'') = ''
                            and AltType = 'a.k.a.'
                            and isnull(listid,'') in ('ALT','')
                            and Status = 1");
                            sqlCommand.ExecuteNonQuery();

                            /*2021.03 update status = 4 where worldcheck alterspell field is empty.*/
                            sqlCommand.CommandText = string.Format(@"UPDATE OFAC.dbo.SDNAltTable set Status = 4, LastModifDate = getdate()
                            from SDNAltTable sdna
                            join WorldCheck wc on wc.EntNum = sdna.EntNum
                            where isnull(wc.alterspell,'') = ''
                            and AltType = 'f.k.a.'
                            and isnull(listid,'') in ('ALTSPELL')
                            and Status = 1");
                            sqlCommand.ExecuteNonQuery();

                            /*2021.03 update status = 4 where worldcheck lowquality field is empty.*/
                            sqlCommand.CommandText = string.Format(@"UPDATE OFAC.dbo.SDNAltTable set Status = 4, LastModifDate = getdate()
                            from SDNAltTable sdna
                            join WorldCheck wc on wc.EntNum = sdna.EntNum
                            where isnull(wc.LowQAliases,'') = ''
                            and AltType = 'a.k.a.'
                            and isnull(sdna.category,'') = 'weak'
                            and isnull(listid,'') in ('LOWQALIASES')
                            and Status = 1");
                            sqlCommand.ExecuteNonQuery();
                        }

                        sqlConnection.Close();
                        sqlConnection.Dispose();
                    }

                }
                System.Threading.Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                LogException(ex, "UpdSDNAltTableDailyDataIntoSqlServer");
            }
        }

        public void UpdURLsDailyDataIntoSqlServer(string str_UPDDate, string BCPFile_TempURLs, string BCPFile_URLs, string BCPFmt_TempURLs, string BCPFile_Error,
            string BCPFmt_URLs, string BCPServer, string BCPUser, string BCPPwd, string wc_subcate_space, string wc_import_un, string wc_import_SQL,
            string wc_import_crime, string wc_import_crime_SQL)
        {
            try
            {
                LogAdd("[Daily] Expand to URLs -- Start");

                if (string.IsNullOrEmpty(str_UPDDate))
                {
                    str_UPDDate = "30";
                }

                string query = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                string Aliases = string.Empty;
                string URLsID = string.Empty;
                string EntNum = string.Empty;
                string AltType = string.Empty;
                string URL = string.Empty;
                string Description = string.Empty;
                string UID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                int URLsIDMax = 0;
                int File_TotalNum = 0;
                int File_UpdDelNum = 0;
                int File_UpdAddNum = 0;
                int File_FinalAddNum = 0;
                int rowCount = 0;

                DateTime NowDateTime = DateTime.Now;

                Database db;

                if (File.Exists(BCPFile_TempURLs)) File.Delete(BCPFile_TempURLs);

                if (File.Exists(BCPFile_URLs)) File.Delete(BCPFile_URLs);

                queryMax = string.Format(@" SELECT MAX(URLsID)+1 AS URLsIDMax FROM OFAC.dbo.URLs ");

                //query = string.Format(@" SELECT Entnum, ExSources FROM OFAC.dbo.WorldCheck WHERE ExSources IS NOT NULL AND Updated > DATEADD(Daily,-1,GETDATE()) ");

                //2019.01 change to table config
                query = @" SELECT WC.Entnum, WC.ExSources, 
                                        CASE ISNULL(WC.DelFlag, '0') 
                                        WHEN '0' THEN '1' 
                                        WHEN '1' THEN '4' END AS Status 
                                        FROM OFAC.dbo.WorldCheck WC
                                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                                        WHERE WC.ExSources != ''
                                        AND WC.UPDDate > DATEADD(MI, @min, GETDATE()) 
                                        AND (isnull(SubCategory,'') != '') 
                                union
                                        SELECT WC.Entnum, WC.ExSources, 
                                        CASE ISNULL(WC.DelFlag, '0') 
                                        WHEN '0' THEN '1' 
                                        WHEN '1' THEN '4' END AS Status 
                                        FROM OFAC.dbo.WorldCheck WC
                                        left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                                        left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                                        WHERE WC.ExSources != ''
                                        AND WC.UPDDate > DATEADD(MI, @min, GETDATE()) 
                                        AND (isnull(SubCategory,'') = '')
                                         AND (wcl.Used = 1 ";
                if (wc_import_crime == "Y")
                {
                    query += @"          or Category like '%CRIME%'";
                }
                query += ")";

                /*
                query += "AND ((SubCategory is not null ";

                if (wc_subcate_space == "N")
                {
                    query += "and SubCategory != '' ";
                }

                query += ") Or (keywords like '%USTREAS.311%') Or (furtherinfo like '%denied persons list%' and furtherinfo like '%US DEPARTMENT OF COMMERCE%') ";

                if (wc_import_un == "Y")
                {
                    query += wc_import_SQL;
                }
                if (wc_import_crime == "Y")
                {
                    query += wc_import_crime_SQL;
                }

                //20181022. for parameter
                for (int i = 0; i < getWC_para.Count(); i++)
                {
                    if (getWC_para[i].WCSQL != "" && getWC_para[i].WCListtype != "" && getWC_para[i].WCProgram != "")
                    {
                        query += getWC_para[i].WCSQL;
                    }
                }

                query += " );";
                */

                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                
                var parameter_Add = dbCommand.CreateParameter();
                parameter_Add.ParameterName = "@min";
                parameter_Add.Value = int.Parse(str_UPDDate) * -1;
                dbCommand.Parameters.Add(parameter_Add);

                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("URLsIDMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            URLsIDMax = Convert.ToInt32(dataReaderMax["URLsIDMax"]);
                        }
                    }
                }
                dbCommandMax.Dispose();
                IDataReader dataReaderDT = db.ExecuteReader(dbCommand);
                DataTable dt = new DataTable();
                dt.Load(dataReaderDT);
                rowCount = dt.Rows.Count;
                dataReaderDT.Close();
                
                using(StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TempURLs, false, Encoding.GetEncoding(1252)))
                {
                    using (IDataReader dr = db.ExecuteReader(dbCommand))
                    {
                        while (dr.Read())
                        {
                            EntNum = dr["Entnum"].ToString().Trim();
                            Status = dr["Status"].ToString().Trim();
                            string[] ExSourcesSplit = dr["ExSources"].ToString().Trim().Split(' ');
                            int i;
                            for (i = 0; i < ExSourcesSplit.Length; i++)
                            {
                                if (ExSourcesSplit[i] != "")
                                {
                                    StringBuilder builder = new StringBuilder();
                                    builder.AppendFormat("{0}\t", URLsIDMax.ToString());
                                    builder.AppendFormat("{0}\t", EntNum);
                                    builder.AppendFormat("{0}\t", ExSourcesSplit[i].Length > 1000 ? ExSourcesSplit[i].Substring(0, 1000) : ExSourcesSplit[i]);
                                    builder.AppendFormat("{0}", Status);
                                    TempfileDWriter.WriteLine(builder.ToString());
                                    URLsIDMax++;
                                    File_TotalNum++;
                                }
                            }
                        }
                        dr.Close();
                    }
                    TempfileDWriter.Close();
                    dbCommand.Dispose();
                }

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TempURLs ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                if (rowCount != 0)
                {
                    Process p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.TempURLs in " + BCPFile_TempURLs + "  -f " + BCPFmt_TempURLs + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();

                    string queryUpdateDelDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.URLs
                                 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempURLs WHERE URLs.EntNum = TempURLs.EntNum AND URLs.URL = TempURLs.URL AND TempURLs.Status='4')
");

                    DbCommand dbCommandUpdateDelDetial = db.GetSqlStringCommand(queryUpdateDelDetail);
                    dbCommandUpdateDelDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateDelDetail = db.ExecuteReader(dbCommandUpdateDelDetial))
                    {
                        while (drUpdateDelDetail.Read())
                        {
                            //if (File_UpdDelNum == 0) { LogAdd("[Daily] URLs - Set Status to 4: "); };
                            //LogAdd(
                            //    drUpdateDelDetail["URLsID"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["EntNUm"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["URL"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["Description"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListCreateDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateOper"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["CreateDate"].ToString().Trim() + ", " +
                            //    drUpdateDelDetail["LastOper"].ToString().Trim() + "," +
                            //    drUpdateDelDetail["LastModify"].ToString().Trim());
                            File_UpdDelNum++;
                        }
                    }
                    dbCommandUpdateDelDetial.Dispose();
                    string queryUpdateDel =
                        string.Format(@" UPDATE OFAC.dbo.URLs
 SET Status='4', LastModify=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempURLs WHERE URLs.EntNum = TempURLs.EntNum AND URLs.URL = TempURLs.URL AND TempURLs.Status='4')
");

                    DbCommand dbCommandUpdateDel = db.GetSqlStringCommand(queryUpdateDel);
                    dbCommandUpdateDel.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateDel);
                    dbCommandUpdateDel.Dispose();

                    string queryUpdateAddDetail =
                    string.Format(@" SELECT * FROM OFAC.dbo.URLs
                                  WHERE EXISTS (SELECT * FROM OFAC.dbo.TempURLs WHERE URLs.EntNum = TempURLs.EntNum AND URLs.URL = TempURLs.URL AND TempURLs.Status='1') ");

                    DbCommand dbCommandUpdateAddDetial = db.GetSqlStringCommand(queryUpdateAddDetail);
                    dbCommandUpdateAddDetial.CommandTimeout = 0;
                    using (IDataReader drUpdateAddDetail = db.ExecuteReader(dbCommandUpdateAddDetial))
                    {
                        while (drUpdateAddDetail.Read())
                        {
                            //if (File_UpdAddNum == 0) { LogAdd("[Daily] URLs - Set Status to 1: "); };
                            //LogAdd(
                            //    drUpdateAddDetail["URLsID"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["EntNUm"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["URL"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["Description"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["Status"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListCreateDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["ListModifDate"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateOper"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["CreateDate"].ToString().Trim() + ", " +
                            //    drUpdateAddDetail["LastOper"].ToString().Trim() + "," +
                            //    drUpdateAddDetail["LastModify"].ToString().Trim());
                            File_UpdAddNum++;
                        }
                    }
                    dbCommandUpdateAddDetial.Dispose();

                    string queryUpdateAdd =
                        string.Format(@" UPDATE OFAC.dbo.URLs
 SET Status='3', LastModify=SYSDATETIME()
 WHERE EXISTS (SELECT * FROM OFAC.dbo.TempURLs WHERE URLs.EntNum = TempURLs.EntNum AND URLs.URL = TempURLs.URL AND TempURLs.Status='1')
 ");

                    DbCommand dbCommandUpdateAdd = db.GetSqlStringCommand(queryUpdateAdd);
                    dbCommandUpdateAdd.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandUpdateAdd);
                    dbCommandUpdateAdd.Dispose();

                    using(StreamWriter fileDWriter = new StreamWriter(BCPFile_URLs, false, Encoding.GetEncoding(1252)))
                    {
                        string queryAdd =
                        string.Format(@" SELECT DISTINCT URLsID, EntNum, URL, '2' Status FROM OFAC.dbo.TempURLs 
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.URLs WHERE TempURLs.EntNum = EntNum AND len(TempURLs.URL) = len(URL)) 
                                     ORDER BY URLsID, EntNum, URL ");

                        DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                        dbCommandAdd.CommandTimeout = 0;
                        using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                        {
                            while (Tempdr.Read())
                            {
                                URLsID = Tempdr["URLsID"].ToString().Trim();
                                EntNum = Tempdr["EntNum"].ToString().Trim();
                                URL = Tempdr["URL"].ToString().Trim();
                                Description = Tempdr["URL"].ToString().Trim();
                                Status = Tempdr["Status"].ToString().Trim();
                                StringBuilder builder = new StringBuilder();
                                builder.AppendFormat("{0}\t", URLsID);
                                builder.AppendFormat("{0}\t", EntNum);
                                builder.AppendFormat("{0}\t", URL);
                                builder.AppendFormat("{0}\t", Description);
                                builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}", Status, NowDateTime.ToString(), NowDateTime.ToString(), "Prime", NowDateTime.ToString(), "Prime", NowDateTime.ToString());
                                fileDWriter.WriteLine(builder.ToString());
                                File_FinalAddNum++;
                            }
                            Tempdr.Close();
                        }

                        fileDWriter.Close();
                        dbCommandAdd.Dispose();
                    }

                    p = new Process();
                    p.StartInfo.FileName = "bcp.exe";

                    p.StartInfo.Arguments = "OFAC.dbo.URLs in " + BCPFile_URLs + "  -f " + BCPFmt_URLs + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                    LogAdd("[Daily] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    p.WaitForExit();
                }

                //201904 update status where not exists
                string sqlDel = @" update OFAC..URLs set Status = 4, LastModify=SYSDATETIME()
                from OFAC..URLs (nolock) url
                where EntNum in (select EntNum from OFAC..tempURLs)
                and not exists 
                (select * from OFAC..tempURLs (nolock) turl where url.EntNum = turl.EntNum and turl.[url] = url.[url]) ";
                dbCommandDel = db.GetSqlStringCommand(sqlDel);
                dbCommandDel.CommandTimeout = 0;
                int icount = db.ExecuteReader(dbCommandDel).RecordsAffected;
                dbCommandDel.Dispose();
                LogAdd("[Daily] update URLs set status = 4 where not exists:" + icount);

                LogAdd("[Daily] TempURLs - Import: [" + File_TotalNum + "]; URLs - Set Status to 4: [" + File_UpdDelNum + "], Set Status to 1: [" + File_UpdAddNum + "], Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[Daily] Expand to URLs -- Completion");

                OFACEventLog("URLs update: " + (File_UpdDelNum + File_UpdAddNum).ToString() + ", insert : " + File_FinalAddNum.ToString());

                db = null;
            }
            catch (Exception ex)
            {
                LogException(ex, "UpdURLsDailyDataIntoSqlServer");
            }
        }

        public void insertDobs(string str_UPDDate, string wc_subcate_space, string wc_import_un, string wc_import_SQL, string wc_import_crime, string wc_import_crime_SQL)
        {
            // update sdn data
            var dt = new DateTime();
            dt = DateTime.Now;
            int iDOBsId = 0;
            string query = "";
            SqlDataAdapter da;
            DataSet ds;

            if (string.IsNullOrEmpty(str_UPDDate))
            {
                str_UPDDate = "30";
            }

            LogAdd("[Daily] Expand to Dobs -- Start.");

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                //清除暫存Dob
                using (var sqlCommand = new SqlCommand("truncate table TempLoadDOBs", sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }

                //get 目前最大流水號
                using (var sqlCommand = new SqlCommand("select max(DOBsId) DOBsId from DOBs", sqlConnection))
                {
                    da = new SqlDataAdapter(sqlCommand);
                    ds = new DataSet();

                    da.Fill(ds);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            if (r["DOBsId"].ToString().Trim().Length > 0)
                            {
                                iDOBsId = int.Parse(r["DOBsId"].ToString().Trim());
                            }
                        }
                    }
                }

                //insert tempdob
                //2018.07 fix ent must exist in sdntable
                //2019.01 change to table config
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    query = @"
                          select  CASE ISNULL(WC.DelFlag, '0') 
                           WHEN '0' THEN '1' 
                           WHEN '1' THEN '4' END AS delstatus,
                          wc.*
                            from worldcheck wc
                            left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                            left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                          where isnull(wc.DOBs,'') != ''
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and exists (select * from OFAC.dbo.SDNTABLE(nolock) where wc.entnum = OFAC.dbo.SDNTABLE.entnum )
                            AND (isnull(SubCategory,'') != '') 
                    union
                          select  CASE ISNULL(WC.DelFlag, '0') 
                           WHEN '0' THEN '1' 
                           WHEN '1' THEN '4' END AS delstatus,
                          wc.*
                            from worldcheck wc
                            left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                            left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                          where isnull(wc.DOBs,'') != ''
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and exists (select * from OFAC.dbo.SDNTABLE(nolock) where wc.entnum = OFAC.dbo.SDNTABLE.entnum )
                          and (isnull(SubCategory,'') = '')
                                         AND (wcl.Used = 1 ";
                    if (wc_import_crime == "Y")
                    {
                        query += @"          or Category like '%CRIME%'";
                    }
                    query += ")";

                    sqlCommand.CommandText = query;
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 300;

                    da = new SqlDataAdapter(sqlCommand);
                    ds = new DataSet();

                    int rownum = 1;
                    da.Fill(ds);

                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            
                            string entnum = r["entnum"].ToString().Trim();
                            string[] dob = r["dob"].ToString().Trim().Split(';');
                            string Entered = r["Entered"].ToString().Trim();
                            string Updated = r["Updated"].ToString().Trim();
                            string delstatus = r["delstatus"].ToString().Trim();
                            //LogAdd("log test entnum:" + entnum);
                            sqlCommand.CommandText = @"
                             insert into TempLoadDOBs(DOBsId,EntNum,dob,Status,ListCreateDate,ListModifDate,CreateOper,CreateDate,LastOper,LastModify)
                             select top 1 @dobid,@Entnum,@dob,@delstatus, @Entered,@Updated,'Prime',@date,'Prime',@date
                             from DOBs;
                            ";

                            //202008 add to check dobs
                            
                            try
                            {
                                if (r["dobs"].ToString().Trim() != "")
                                {
                                    //LogAdd("Log test dobs:" + r["dobs"]);
                                    dob = r["dobs"].ToString().Trim().Split(';');
                                }
                            }
                            catch (Exception ex)
                            {
                                LogException(ex, "Split dobs");
                                dob = r["dob"].ToString().Trim().Split(';');
                            }

                            //2020.11 dobs need to match prime's format
                            string str_dobs = "";
                            foreach (string d in dob)
                            {
                                if (d != "")
                                {
                                    //LogAdd("Log test dobs:" + d);
                                    sqlCommand.Parameters.Clear();
                                    sqlCommand.Parameters.AddWithValue("@dobid", fortifyService.PathManipulation((iDOBsId + rownum).ToString()));
                                    sqlCommand.Parameters.AddWithValue("@Entnum", fortifyService.PathManipulation(entnum));
                                    sqlCommand.Parameters.AddWithValue("@dob", fortifyService.PathManipulation(d.Substring(5, 2) + d.Substring(8, 2) + d.Substring(0, 4)));
                                    sqlCommand.Parameters.AddWithValue("@delstatus", fortifyService.PathManipulation(delstatus));
                                    sqlCommand.Parameters.AddWithValue("@Entered", fortifyService.PathManipulation(Entered));
                                    sqlCommand.Parameters.AddWithValue("@Updated", fortifyService.PathManipulation(Updated));
                                    sqlCommand.Parameters.AddWithValue("@date", fortifyService.PathManipulation((dt.ToString())));

                                    str_dobs += fortifyService.PathManipulation(d.Substring(5, 2) + d.Substring(8, 2) + d.Substring(0, 4)) + ",";

                                    sqlCommand.ExecuteNonQuery();
                                    rownum = rownum + 1;
                                }
                            }

                            //Mar.09 2023 modify the sperate character to comma
                            str_dobs = str_dobs.TrimEnd(',');
                            //LogAdd("Debug: dobs=" + str_dobs);
                            if (str_dobs.Replace(",","").Length > 0)
                            {
                                query = "update OFAC..SDNTable set Dob = @dobs where EntNum = @ent and Dob != @dobs;";
                                sqlCommand.CommandText = query;
                                sqlCommand.Parameters.Clear();
                                sqlCommand.Parameters.AddWithValue("@dobs", fortifyService.PathManipulation(str_dobs.Length > 143 ? str_dobs.Substring(0, 143) : str_dobs));
                                sqlCommand.Parameters.AddWithValue("@ent", fortifyService.PathManipulation(entnum));
                                sqlCommand.ExecuteNonQuery();
                            }

                            sqlCommand.Dispose();
                        }
                    }
                }

                int int_updatelog = 0;
                int int_insertlog = 0;
                int int_dellog = 0;

                //存在DOBS 更新狀態=1
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    update DOBs set status = '3' where entnum in (select entnum from TempLoadDOBs where status = '1');";
                    sqlCommand.CommandTimeout = 300;
                    int_updatelog = sqlCommand.ExecuteNonQuery();
                }

                //存在DOBS 更新狀態=4
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    update DOBs set status = '4' where entnum in (select entnum from TempLoadDOBs where status = '4');";
                    sqlCommand.CommandTimeout = 300;
                    int_dellog = sqlCommand.ExecuteNonQuery();
                }

                //不存在DOB insert
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    insert into DOBs(DOBsId,EntNum,dob,Status,ListCreateDate,ListModifDate,CreateOper,CreateDate,LastOper,LastModify)
                    select DOBsId,EntNum,dob,'2',ListCreateDate,ListModifDate,CreateOper,CreateDate,LastOper,LastModify
                      from TempLoadDOBs where not exists (select * from dobs where dobs.EntNum = TempLoadDOBs.EntNum and dobs.DOB = TempLoadDOBs.dob)
                    ;";
                    sqlCommand.CommandTimeout = 300;
                    int_insertlog = sqlCommand.ExecuteNonQuery();

                }
                OFACEventLog(string.Format("DOBs daily Update:{0}, Insert:{1}, Delete:{2}", int_updatelog, int_insertlog, int_dellog));

                sqlConnection.Close();
                sqlConnection.Dispose();
            }

            LogAdd("[Daily] Expand to Dobs -- Completion.");
        }

        public void InsertAddr(string str_UPDDate, string wc_import_crime)
        {
            // update sdn data
            var dt = new DateTime();
            dt = DateTime.Now;
            int iaddrnum = 0;
            string query = "";
            SqlDataAdapter da;
            DataSet ds;

            if (string.IsNullOrEmpty(str_UPDDate))
            {
                str_UPDDate = "30";
            }

            LogAdd("[Daily] Expand to Addr -- Start.");

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                //清除暫存addr
                using (var sqlCommand = new SqlCommand("truncate table OFAC..TmpAddrTable", sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }

                //get 目前最大流水號
                using (var sqlCommand = new SqlCommand("select max(AddrNum) AddrNum from OFAC..SDNAddrTable", sqlConnection))
                {
                    da = new SqlDataAdapter(sqlCommand);
                    ds = new DataSet();

                    da.Fill(ds);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            if (r["AddrNum"].ToString().Trim().Length > 0)
                            {
                                iaddrnum = int.Parse(r["AddrNum"].ToString().Trim());
                            }
                        }
                    }
                }

                //2018.07 fix ent must exist in sdntable
                //2019.01 change to table config
                //Feb.6 2023 add both customer and worldcheck country into addr table
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    query = @"
                          select distinct CASE ISNULL(WC.DelFlag, '0') 
                           WHEN '0' THEN '1' 
                           WHEN '1' THEN '4' END AS delstatus,
                           (select Customer_country from OFAC..WorldCheckCountry where worldcheck_country = wc.country) customer_country,
                           (select CreateDate from SDNTable (nolock) where SDNTable.EntNum = wc.Entnum) as CreateDate,
                           (select LastModifDate from SDNTable (nolock) where SDNTable.EntNum = wc.Entnum) as LastModifDate,
                          wc.*
                            from worldcheck wc
                            left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                            left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                          where wc.Country != ''
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and exists (select * from OFAC.dbo.SDNTABLE(nolock) where wc.entnum = OFAC.dbo.SDNTABLE.entnum )
                            AND (isnull(SubCategory,'') != '') 
                    union
                          select distinct CASE ISNULL(WC.DelFlag, '0') 
                           WHEN '0' THEN '1' 
                           WHEN '1' THEN '4' END AS delstatus,
                           (select Customer_country from OFAC..WorldCheckCountry where worldcheck_country = wc.country) customer_country,
                           (select CreateDate from SDNTable (nolock) where SDNTable.EntNum = wc.Entnum) as CreateDate,
                           (select LastModifDate from SDNTable (nolock) where SDNTable.EntNum = wc.Entnum) as LastModifDate,
                          wc.*
                            from worldcheck wc
                            left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                            left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                          where wc.Country != ''
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and exists (select * from OFAC.dbo.SDNTABLE(nolock) where wc.entnum = OFAC.dbo.SDNTABLE.entnum )
                          and (isnull(SubCategory,'') = '')
                                         AND (wcl.Used = 1 ";
                    if (wc_import_crime == "Y")
                    {
                        query += @"          or Category like '%CRIME%'";
                    }
                    query += ")";

                    sqlCommand.CommandText = query;
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                    sqlCommand.CommandTimeout = 300;

                    da = new SqlDataAdapter(sqlCommand);
                    ds = new DataSet();

                    int rownum = 1;
                    da.Fill(ds);

                    LogAdd(string.Format("[Daily] SDNAddrTable - number count:{0}", ds.Tables[0].Rows.Count));

                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            string entnum = r["entnum"].ToString().Trim();
                            string country = r["customer_country"].ToString().Trim();
                            //Feb.6, 2023 add worldcheck country
                            string worldcheckcountry = r["country"].ToString().Trim();
                            //Feb.7, 2023 add to avoid dupilicate country
                            string tempcountry = worldcheckcountry;
                            string Entered = r["Entered"].ToString().Trim();
                            string Updated = r["Updated"].ToString().Trim();
                            string delstatus = r["delstatus"].ToString().Trim();
                            string location = r["location"].ToString().Trim();
                            string createdate = r["CreateDate"].ToString().Trim();
                            string modifdate = r["LastModifDate"].ToString().Trim();
                            string address = String.Empty;
                            string address2 = String.Empty;
                            string address3 = String.Empty;
                            string address4 = String.Empty;
                            string city = String.Empty;
                            string state = String.Empty;

                            //modify because tempsdnaddrtable schema different from sdnaddrtable
                            sqlCommand.CommandText = @"
                             insert into OFAC..TmpAddrTable(AddrNum,EntNum,Address,Address2,Address3,Address4,City,State,Country,deleted,status,ListCreatedate,ListModifDate,CreateDate,LastModifDate,LastOper)
                             select top 1 @AddrNum,@Entnum,@Address,@Address2,@Address3,@Address4,@City,@State,@Country,
                             0,@delstatus, @Entered,@Updated,@createdate,@modifdate,'Prime'
                             from OFAC..SDNAddrTable;
                            ";

                            //Feb.2, 2023 Polly request to add the country data into the sdnaddr table
                            if (!string.IsNullOrEmpty(country) && country.ToUpper().Trim() != "UNKNOWN")
                            {
                                sqlCommand.Parameters.Clear();
                                sqlCommand.Parameters.AddWithValue("@AddrNum", fortifyService.PathManipulation((iaddrnum + rownum).ToString()));
                                sqlCommand.Parameters.AddWithValue("@Entnum", fortifyService.PathManipulation(entnum));
                                sqlCommand.Parameters.AddWithValue("@Address", fortifyService.PathManipulation(address));
                                sqlCommand.Parameters.AddWithValue("@Address2", fortifyService.PathManipulation(address2));
                                sqlCommand.Parameters.AddWithValue("@Address3", fortifyService.PathManipulation(address3));
                                sqlCommand.Parameters.AddWithValue("@Address4", fortifyService.PathManipulation(address4));
                                sqlCommand.Parameters.AddWithValue("@City", fortifyService.PathManipulation(city));
                                sqlCommand.Parameters.AddWithValue("@State", fortifyService.PathManipulation(state));
                                sqlCommand.Parameters.AddWithValue("@Country", fortifyService.PathManipulation(country));
                                sqlCommand.Parameters.AddWithValue("@delstatus", fortifyService.PathManipulation(delstatus));
                                sqlCommand.Parameters.AddWithValue("@Entered", fortifyService.PathManipulation(Entered));
                                sqlCommand.Parameters.AddWithValue("@Updated", fortifyService.PathManipulation(Updated));
                                sqlCommand.Parameters.AddWithValue("@createdate", fortifyService.PathManipulation(createdate));
                                sqlCommand.Parameters.AddWithValue("@modifdate", fortifyService.PathManipulation(modifdate));
                                sqlCommand.Parameters.AddWithValue("@date", fortifyService.PathManipulation(dt.ToString()));

                                sqlCommand.ExecuteNonQuery();

                                rownum = rownum + 1;
                            }

                            //Feb.6, 2023 add worldcheck country
                            if (!string.IsNullOrEmpty(worldcheckcountry) && worldcheckcountry.ToUpper().Trim() != "UNKNOWN" && worldcheckcountry.ToUpper().Trim() != country.ToUpper().Trim())
                            {
                                sqlCommand.Parameters.Clear();
                                sqlCommand.Parameters.AddWithValue("@AddrNum", fortifyService.PathManipulation((iaddrnum + rownum).ToString()));
                                sqlCommand.Parameters.AddWithValue("@Entnum", fortifyService.PathManipulation(entnum));
                                sqlCommand.Parameters.AddWithValue("@Address", fortifyService.PathManipulation(address));
                                sqlCommand.Parameters.AddWithValue("@Address2", fortifyService.PathManipulation(address2));
                                sqlCommand.Parameters.AddWithValue("@Address3", fortifyService.PathManipulation(address3));
                                sqlCommand.Parameters.AddWithValue("@Address4", fortifyService.PathManipulation(address4));
                                sqlCommand.Parameters.AddWithValue("@City", fortifyService.PathManipulation(city));
                                sqlCommand.Parameters.AddWithValue("@State", fortifyService.PathManipulation(state));
                                sqlCommand.Parameters.AddWithValue("@Country", fortifyService.PathManipulation(worldcheckcountry));
                                sqlCommand.Parameters.AddWithValue("@delstatus", fortifyService.PathManipulation(delstatus));
                                sqlCommand.Parameters.AddWithValue("@Entered", fortifyService.PathManipulation(Entered));
                                sqlCommand.Parameters.AddWithValue("@Updated", fortifyService.PathManipulation(Updated));
                                sqlCommand.Parameters.AddWithValue("@createdate", fortifyService.PathManipulation(createdate));
                                sqlCommand.Parameters.AddWithValue("@modifdate", fortifyService.PathManipulation(modifdate));
                                sqlCommand.Parameters.AddWithValue("@date", fortifyService.PathManipulation(dt.ToString()));

                                sqlCommand.ExecuteNonQuery();

                                rownum = rownum + 1;
                            }

                            //splite by ; for number count
                            String[] A_location_count = location.Split(';');
                            for (int i = 0; i < A_location_count.Count(); i++)
                            {
                                //splite by ~ for address city,state country
                                String[] A_location_type = A_location_count[i].Split('~');
                                String[] A_location_city_state = new String[2];
                                for (int j = 0; j < A_location_type.Count(); j++)
                                {
                                    switch (j)
                                    {
                                        case 0:
                                            address4 = A_location_type[j].Trim().Length > 200 ? A_location_type[j].Trim().Substring(200, A_location_type[j].Trim().Length > 250 ? 50 : A_location_type[j].Trim().Length - 200) : "";
                                            address3 = A_location_type[j].Trim().Length > 150 ? A_location_type[j].Trim().Substring(150, A_location_type[j].Trim().Length > 200 ? 50 : A_location_type[j].Trim().Length - 150) : "";
                                            address2 = A_location_type[j].Trim().Length > 100 ? A_location_type[j].Trim().Substring(100, A_location_type[j].Trim().Length > 150 ? 50 : A_location_type[j].Trim().Length - 100) : "";
                                            address = A_location_type[j].Trim().Length > 100 ? A_location_type[j].Trim().Substring(0, 100) : A_location_type[j].Trim();
                                            break;

                                        case 1:

                                            //splite by , for city state
                                            A_location_city_state = A_location_type[j].Split(',');
                                            city = A_location_city_state[0].Trim().Length > 50 ? A_location_city_state[0].Trim().Substring(0, 50) : A_location_city_state[0].Trim();
                                            state = A_location_city_state[1].Trim().Length > 50 ? A_location_city_state[1].Trim().Substring(0, 50) : A_location_city_state[1].Trim();

                                            break;
                                        case 2:
                                            country = A_location_type[j].Trim().Length > 100 ? A_location_type[j].Trim().Substring(0, 100) : A_location_type[j].Trim();
                                            break;
                                        default:

                                            break;
                                    }

                                }

                                //Feb.7, 2023 add to avoid dupilicate country
                                if ((address.Trim() == "") && (city.Trim() == "") && (state.Trim() == "") && (country.Trim() == "") || ((tempcountry == country) && (address.Trim() == "") && (city.Trim() == "") && (state.Trim() == "")))
                                {
                                    continue;
                                }

                                //modify because tempsdnaddrtable schema different from sdnaddrtable
                                sqlCommand.CommandText = @"
                                 insert into OFAC..TmpAddrTable(AddrNum,EntNum,Address,Address2,Address3,Address4,City,State,Country,deleted,status,ListCreatedate,ListModifDate,CreateDate,LastModifDate,LastOper)
                                 select top 1 @AddrNum,@Entnum,@Address,@Address2,@Address3,@Address4,@City,@State,(select Customer_Country from WorldCheckCountry where WorldCheck_Country = @Country and @Country != ''),
                                 0,@delstatus, @Entered,@Updated,@createdate,@modifdate,'Prime'
                                 from OFAC..SDNAddrTable;
                                ";

                                sqlCommand.Parameters.Clear();
                                sqlCommand.Parameters.AddWithValue("@AddrNum", fortifyService.PathManipulation((iaddrnum + rownum).ToString()));
                                sqlCommand.Parameters.AddWithValue("@Entnum", fortifyService.PathManipulation(entnum));
                                sqlCommand.Parameters.AddWithValue("@Address", fortifyService.PathManipulation(address));
                                sqlCommand.Parameters.AddWithValue("@Address2", fortifyService.PathManipulation(address2));
                                sqlCommand.Parameters.AddWithValue("@Address3", fortifyService.PathManipulation(address3));
                                sqlCommand.Parameters.AddWithValue("@Address4", fortifyService.PathManipulation(address4));
                                sqlCommand.Parameters.AddWithValue("@City", fortifyService.PathManipulation(city));
                                sqlCommand.Parameters.AddWithValue("@State", fortifyService.PathManipulation(state));
                                sqlCommand.Parameters.AddWithValue("@Country", fortifyService.PathManipulation(country));
                                sqlCommand.Parameters.AddWithValue("@delstatus", fortifyService.PathManipulation(delstatus));
                                sqlCommand.Parameters.AddWithValue("@Entered", fortifyService.PathManipulation(Entered));
                                sqlCommand.Parameters.AddWithValue("@Updated", fortifyService.PathManipulation(Updated));
                                sqlCommand.Parameters.AddWithValue("@createdate", fortifyService.PathManipulation(createdate));
                                sqlCommand.Parameters.AddWithValue("@modifdate", fortifyService.PathManipulation(modifdate));
                                sqlCommand.Parameters.AddWithValue("@date", fortifyService.PathManipulation(dt.ToString()));

                                sqlCommand.ExecuteNonQuery();

                                rownum = rownum + 1;
                            }
                        }
                    }
                }

                int int_updatelog = 0;
                int int_insertlog = 0;
                int int_dellog = 0;

                //存在Addr 更新狀態=1
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    update OFAC..SDNAddrTable set status = '3' where entnum in (select entnum from OFAC..TmpAddrTable where Status = 1) and status != 1;";
                    sqlCommand.CommandTimeout = 300;
                    int_updatelog = sqlCommand.ExecuteNonQuery();
                }

                //存在Addr 更新狀態=4
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    update OFAC..SDNAddrTable set status = '4' where entnum in (select entnum from OFAC..TmpAddrTable where Status = 4) and status != 4;";
                    sqlCommand.CommandTimeout = 300;
                    int_dellog = sqlCommand.ExecuteNonQuery();
                }

                //不存在Addr insert
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    insert into OFAC..SDNAddrTable(AddrNum,EntNum,Address,Address2,Address3,Address4,City,State,Country,Deleted,Status,ListCreateDate,ListModifDate,CreateDate,LastModifDate,LastOper)
                    select AddrNum,EntNum,Address,Address2,Address3,Address4,City,State,Country,'0','2',ListCreatedate,ListModifDate,CreateDate,LastModifDate,LastOper
                      from OFAC..TmpAddrTable where not exists 
                      (select * from OFAC..SDNAddrTable 
                        where OFAC..SDNAddrTable.EntNum = OFAC..TmpAddrTable.EntNum
                        and isnull(OFAC.dbo.SDNAddrTable.Address,'') = isnull(OFAC.dbo.TmpAddrTable.Address, '')
                        and isnull(OFAC.dbo.SDNAddrTable.City,'') = isnull(OFAC.dbo.TmpAddrTable.City,'')
                        and isnull(OFAC.dbo.SDNAddrTable.State,'') = isnull(OFAC.dbo.TmpAddrTable.State,'')
                        and isnull(OFAC.dbo.SDNAddrTable.Country,'') = isnull(OFAC.dbo.TmpAddrTable.Country,'')
                      )
                    ;";
                    sqlCommand.CommandTimeout = 300;
                    sqlCommand.Parameters.AddWithValue("@date", dt);

                    int_insertlog = sqlCommand.ExecuteNonQuery();

                    LogAdd(string.Format("[Daily] SDNAddrTable - Insert:{0}", int_insertlog));
                }

                //201904 update status where not exists
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    string sqlDel = @" update OFAC..SDNAddrTable set Status = 4 , LastModifDate=SYSDATETIME()
                from OFAC..SDNAddrTable (nolock) sdnaddr
                where EntNum in (select EntNum from OFAC..TmpAddrTable)
                and not exists 
                (select * from OFAC..TmpAddrTable (nolock) tsdnaddr where sdnaddr.EntNum = tsdnaddr.EntNum and isnull(sdnaddr.Address,'') = isnull(tsdnaddr.Address,'')
                    and isnull(sdnaddr.Address,'') = isnull(tsdnaddr.Address,'')
                    and isnull(sdnaddr.City,'') = isnull(tsdnaddr.City,'')
                    and isnull(sdnaddr.State,'') = isnull(tsdnaddr.State,'')
                    and isnull(sdnaddr.Country,'') = isnull(tsdnaddr.Country,'')
                ) ";

                    sqlCommand.CommandText = sqlDel;
                    sqlCommand.CommandTimeout = 300;

                    int InsertCount = sqlCommand.ExecuteNonQuery();
                    LogAdd("[Daily] update SDNAddrTable set status = 4 where not exists:" + InsertCount);
                }

                OFACEventLog(string.Format("SDNAddr daily update:{0}, insert:{1}, delete:{2}", int_updatelog, int_insertlog, int_dellog));

                sqlConnection.Close();
                sqlConnection.Dispose();
            }

            LogAdd("[Daily] Expand to Addr -- Completion.");
        }

        public void UpdateAttribute(string str_UPDDate, string wc_import_crime, string wc_import_crime_SQL, string NoteTypeList)
        {
            // update sdn Attribute
            var dt = new DateTime();
            dt = DateTime.Now;
            string NotesID = string.Empty;
            string EntNum = string.Empty;

            string Status = string.Empty;
            string ListCreateDate = string.Empty;
            string ListModifDate = string.Empty;
            string CreateDate = string.Empty;
            string LastOper = string.Empty;
            string LastModify = string.Empty;
            string Note = string.Empty;
            string NoteType = string.Empty;
            string IDnumber = string.Empty;
            int NotesIdMax = 0;
            string query = "";
            SqlDataAdapter da;
            DataSet ds;

            if (string.IsNullOrEmpty(str_UPDDate))
            {
                str_UPDDate = "30";
            }

            LogAdd("[Daily] Expand to Notes -- Start.");

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                //清除暫存Notes
                using (var sqlCommand = new SqlCommand("truncate table OFAC..TempLoadNotes", sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }

                //get 目前最大流水號
                using (var sqlCommand = new SqlCommand("SELECT MAX(NotesID)+1 AS NotesIdMax FROM OFAC.dbo.Notes;", sqlConnection))
                {
                    da = new SqlDataAdapter(sqlCommand);
                    ds = new DataSet();

                    da.Fill(ds);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            if (r["NotesIdMax"].ToString().Trim().Length > 0)
                            {
                                NotesIdMax = int.Parse(r["NotesIdMax"].ToString().Trim());
                            }
                        }
                    }
                }

                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    query = @"
                          select distinct ISNULL(wc.Company,'') Company,ISNULL(wc.Category,'') Category,ISNULL(wc.Country,'') Country,
                                     ISNULL(wc.Age,'') Age,ISNULL(wc.Passports,'') Passports,ISNULL(wc.SSN,'') SSN,ISNULL(wc.PlaceOfBirth,'') PlaceOfBirth,
                                     ISNULL(wc.Deceased,'') Deceased, 
                                     isnull(wc.SubCategory,'') SubCategory,
                                     isnull(wc.Keywords,'') Keywords,
                                     isnull(wc.Title,'') Title,
                                     isnull(wc.Position,'') Position,
                                     CASE WHEN isnull(wc.UpdCategory,'') = 'C1' then 'C1:changes to names alt, dob, locations and country' 
                                     WHEN isnull(wc.UpdCategory,'') = 'C2' then 'C2:changes to keywords, subcategory, category' 
                                     WHEN isnull(wc.UpdCategory,'') = 'C3' then 'C3:changes to title, position, pob,SSN, passport' 
                                     WHEN isnull(wc.UpdCategory,'') = 'C4' then 'C4:changes to ex sources, link to , companies, furtherinfo etc'
                                     else '' end UpdCategory,
                                     isnull(wc.IDENTIFICATION_NUMBERS,'') IDENTIFICATION_NUMBERS,
                                     isnull(AgeDate,'') agedate,
                                     isnull(wc.citizenship,'') citizenship,
                                     CASE ISNULL(WC.DelFlag, '0') 
                                     WHEN '0' THEN '1' 
                                     WHEN '1' THEN '4' END AS Status,
                                     wc.*
                            from worldcheck wc
                            left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                            left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                          where 1 = 1
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and exists (select * from OFAC.dbo.SDNTABLE(nolock) where wc.entnum = OFAC.dbo.SDNTABLE.entnum )
                            AND (isnull(SubCategory,'') != '') 
                    union
                          select distinct ISNULL(wc.Company,'') Company,ISNULL(wc.Category,'') Category,ISNULL(wc.Country,'') Country,
                                     ISNULL(wc.Age,'') Age,ISNULL(wc.Passports,'') Passports,ISNULL(wc.SSN,'') SSN,ISNULL(wc.PlaceOfBirth,'') PlaceOfBirth,
                                     ISNULL(wc.Deceased,'') Deceased, 
                                     isnull(wc.SubCategory,'') SubCategory,
                                     isnull(wc.Keywords,'') Keywords,
                                     isnull(wc.Title,'') Title,
                                     isnull(wc.Position,'') Position,
                                     CASE WHEN isnull(wc.UpdCategory,'') = 'C1' then 'C1:changes to names alt, dob, locations and country' 
                                     WHEN isnull(wc.UpdCategory,'') = 'C2' then 'C2:changes to keywords, subcategory, category' 
                                     WHEN isnull(wc.UpdCategory,'') = 'C3' then 'C3:changes to title, position, pob,SSN, passport' 
                                     WHEN isnull(wc.UpdCategory,'') = 'C4' then 'C4:changes to ex sources, link to , companies, furtherinfo etc'
                                     else '' end UpdCategory,
                                     isnull(wc.IDENTIFICATION_NUMBERS,'') IDENTIFICATION_NUMBERS,
                                     isnull(AgeDate,'') agedate,
                                     isnull(wc.citizenship,'') citizenship,
                                     CASE ISNULL(WC.DelFlag, '0') 
                                     WHEN '0' THEN '1' 
                                     WHEN '1' THEN '4' END AS Status,
                                     wc.*
                            from worldcheck wc
                            left join OFAC.dbo.WCKeywords wck on wck.UID = wc.UID
                            left join OFAC.dbo.WCKeywordslistTable wcl on wck.Word = wcl.Abbreviation
                          where 1 = 1
                          and wc.UPDDate >= dateadd(MINUTE,@min * -1,GETDATE())
                          and exists (select * from OFAC.dbo.SDNTABLE(nolock) where wc.entnum = OFAC.dbo.SDNTABLE.entnum )
                          and (isnull(SubCategory,'') = '')
                                         AND (wcl.Used = 1 ";
                    if (wc_import_crime == "Y")
                    {
                        query += @"          or Category like '%CRIME%'";
                    }
                    query += ")";

                    sqlCommand.CommandText = query;
                    sqlCommand.Parameters.AddWithValue("@order", wc_import_crime_SQL);
                    sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);

                    sqlCommand.CommandTimeout = 300;

                    da = new SqlDataAdapter(sqlCommand);
                    ds = new DataSet();

                    da.Fill(ds);

                    LogAdd(string.Format("[Daily] Notes - number count:{0}", ds.Tables[0].Rows.Count));
                    
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            EntNum = r["Entnum"].ToString().Trim();
                            Status = r["Status"].ToString().Trim();
                            ListCreateDate = r["Entered"].ToString().Trim();
                            ListModifDate = r["Updated"].ToString().Trim();
                            CreateDate = dt.ToString("yyyy-MM-dd HH:mm:ss");
                            LastOper = "PRIMEADMIN";
                            LastModify = dt.ToString("yyyy-MM-dd HH:mm:ss");
                            IDnumber = r["IDENTIFICATION_NUMBERS"].ToString().Trim();
                            int i;

                            for (i = 0; i < r.ItemArray.Length; i++)
                            {
                                
                                if (r[i].ToString().Trim() != "" && i <= 15)
                                {
                                    //LogAdd("Entnum=" + EntNum + ",Notes No.=" + i.ToString() + ",Note=" + r[i].ToString().Trim());
                                    switch (i)
                                    {
                                        case 0:
                                            Note = r[i].ToString().Trim().Length > 1000 ? r[i].ToString().Trim().Substring(0, 1000) : r[i].ToString().Trim();
                                            NoteType = "50";
                                            break;
                                        case 1:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "48";

                                            break;
                                        case 2:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "8";

                                            break;

                                        case 3:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "47";

                                            break;

                                        case 4:
                                            if ((!NoteTypeList.IsNullOrWhiteSpace()) && (NoteTypeList.Split(',').Any(s => s == "PAS")))
                                            {
                                                Note = r[i].ToString().Trim();
                                                NoteType = "6";
                                            }
                                            else
                                            {
                                                Note = "";
                                            }
                                            
 
                                            break;

                                        case 5:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "3";

                                            break;

                                        case 6:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "1";

                                            break;

                                        case 7:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "49";

                                            break;

                                        case 8:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "100";

                                            break;

                                        case 9:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "101";

                                            break;
                                        case 10:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "103";

                                            break;
                                        case 11:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "104";

                                            break;

                                        case 12:
                                            Note = r[i].ToString().Trim();
                                            NoteType = "105";

                                            break;

                                        case 13:
                                            Note = r[i].ToString().Trim();

                                            break;

                                        case 14:
                                            //Dec.14, 2022 SG request add age of date
                                            Note = r[i].ToString().Trim();
                                            NoteType = "110";
                                            break;
                                        case 15:
                                            //Jan.17, 2023 SG request add citizenship
                                            Note = r[i].ToString().Trim();
                                            NoteType = "7";
                                            break;
                                    }

                                    string SQL = @"
                                insert into OFAC..TempLoadNotes (NotesID,EntNum,Note,NoteType,Status,ListCreateDate,ListModifDate,CreateOper,CreateDate,LastOper,LastModify)
                                select top 1 @NotesID,@EntNum,@Note,@NoteType,@Status,@ListCreateDate,@ListModifDate,@CreateOper,@CreateDate,@LastOper,@LastModify
                                from OFAC..Notes;";
                                    
                                    if ((i <= 15 && i != 13) && Note.Trim().Length > 0)
                                    {
                                        sqlCommand.Parameters.Clear();
                                        sqlCommand.CommandTimeout = 500;
                                        sqlCommand.CommandText = SQL;
                                        sqlCommand.Parameters.AddWithValue("@NotesID", fortifyService.PathManipulation(NotesIdMax.ToString()));
                                        sqlCommand.Parameters.AddWithValue("@Entnum", fortifyService.PathManipulation(EntNum));
                                        sqlCommand.Parameters.AddWithValue("@Note", fortifyService.PathManipulation(Note));
                                        sqlCommand.Parameters.AddWithValue("@NoteType", fortifyService.PathManipulation(NoteType));
                                        sqlCommand.Parameters.AddWithValue("@Status", fortifyService.PathManipulation(Status));
                                        sqlCommand.Parameters.AddWithValue("@ListCreateDate", fortifyService.PathManipulation(ListCreateDate));
                                        sqlCommand.Parameters.AddWithValue("@ListModifDate", fortifyService.PathManipulation(ListModifDate));
                                        sqlCommand.Parameters.AddWithValue("@CreateOper", fortifyService.PathManipulation(LastOper));
                                        sqlCommand.Parameters.AddWithValue("@CreateDate", fortifyService.PathManipulation(CreateDate));
                                        sqlCommand.Parameters.AddWithValue("@LastOper", fortifyService.PathManipulation(LastOper));
                                        sqlCommand.Parameters.AddWithValue("@LastModify", fortifyService.PathManipulation(LastModify));
                                        sqlCommand.ExecuteNonQuery();

                                        NotesIdMax = NotesIdMax + 1;
                                    }
                                    else if (i == 13 && Note.Trim().Length > 0 && !NoteTypeList.IsNullOrWhiteSpace())
                                    {
                                        string notevalue = string.Empty;
                                        foreach (string typelist in NoteTypeList.Split(','))
                                        {
                                            foreach (string id in Note.Split(';'))
                                            {
                                                if (id.Contains(typelist))
                                                {
                                                    switch (typelist)
                                                    {
                                                        case "BIC":
                                                            if (!id.Contains("{INT-BIC}") || !id.Contains("{INT-BIC6}"))
                                                            {
                                                                continue;
                                                            }
                                                            NoteType = "106";
                                                            notevalue = id.Replace("{INT-BIC}", "").Replace("{INT-BIC6}", "");
                                                            break;
                                                        case "IMO":
                                                            if (!id.Contains("{INT-IMO}"))
                                                            {
                                                                continue;
                                                            }
                                                            NoteType = "107";
                                                            notevalue = id.Replace("{INT-IMO}", "");
                                                            break;
                                                        case "ARN":
                                                            if (!id.Contains("{INT-ARN}"))
                                                            {
                                                                continue;
                                                            }
                                                            NoteType = "108";
                                                            notevalue = id.Replace("{INT-ARN}", "");
                                                            break;
                                                        case "MSN":
                                                            if (!id.Contains("{INT-MSN}"))
                                                            {
                                                                continue;
                                                            }
                                                            NoteType = "109";
                                                            notevalue = id.Replace("{INT-MSN}", "");
                                                            break;
                                                    }

                                                    sqlCommand.Parameters.Clear();
                                                    sqlCommand.CommandTimeout = 500;
                                                    sqlCommand.CommandText = SQL;
                                                    sqlCommand.Parameters.AddWithValue("@NotesID", fortifyService.PathManipulation(NotesIdMax.ToString()));
                                                    sqlCommand.Parameters.AddWithValue("@Entnum", fortifyService.PathManipulation(EntNum));
                                                    sqlCommand.Parameters.AddWithValue("@Note", fortifyService.PathManipulation(notevalue));
                                                    sqlCommand.Parameters.AddWithValue("@NoteType", fortifyService.PathManipulation(NoteType));
                                                    sqlCommand.Parameters.AddWithValue("@Status", fortifyService.PathManipulation(Status));
                                                    sqlCommand.Parameters.AddWithValue("@ListCreateDate", fortifyService.PathManipulation(ListCreateDate));
                                                    sqlCommand.Parameters.AddWithValue("@ListModifDate", fortifyService.PathManipulation(ListModifDate));
                                                    sqlCommand.Parameters.AddWithValue("@CreateOper", fortifyService.PathManipulation(LastOper));
                                                    sqlCommand.Parameters.AddWithValue("@CreateDate", fortifyService.PathManipulation(CreateDate));
                                                    sqlCommand.Parameters.AddWithValue("@LastOper", fortifyService.PathManipulation(LastOper));
                                                    sqlCommand.Parameters.AddWithValue("@LastModify", fortifyService.PathManipulation(LastModify));
                                                    sqlCommand.ExecuteNonQuery();

                                                    NotesIdMax = NotesIdMax + 1;
                                                }
                                            }
                                        }

                                        //Jan.17,2023 SG request add IdentificationNo
                                        NoteType = "111";
                                        notevalue = Note;
                                        sqlCommand.Parameters.Clear();
                                        sqlCommand.CommandTimeout = 500;
                                        sqlCommand.CommandText = SQL;
                                        sqlCommand.Parameters.AddWithValue("@NotesID", fortifyService.PathManipulation(NotesIdMax.ToString()));
                                        sqlCommand.Parameters.AddWithValue("@Entnum", fortifyService.PathManipulation(EntNum));
                                        sqlCommand.Parameters.AddWithValue("@Note", fortifyService.PathManipulation(notevalue.Trim().Substring(0, notevalue.Trim().Length > 1000 ? 1000 : notevalue.Trim().Length)));
                                        sqlCommand.Parameters.AddWithValue("@NoteType", fortifyService.PathManipulation(NoteType));
                                        sqlCommand.Parameters.AddWithValue("@Status", fortifyService.PathManipulation(Status));
                                        sqlCommand.Parameters.AddWithValue("@ListCreateDate", fortifyService.PathManipulation(ListCreateDate));
                                        sqlCommand.Parameters.AddWithValue("@ListModifDate", fortifyService.PathManipulation(ListModifDate));
                                        sqlCommand.Parameters.AddWithValue("@CreateOper", fortifyService.PathManipulation(LastOper));
                                        sqlCommand.Parameters.AddWithValue("@CreateDate", fortifyService.PathManipulation(CreateDate));
                                        sqlCommand.Parameters.AddWithValue("@LastOper", fortifyService.PathManipulation(LastOper));
                                        sqlCommand.Parameters.AddWithValue("@LastModify", fortifyService.PathManipulation(LastModify));
                                        sqlCommand.ExecuteNonQuery();

                                        NotesIdMax = NotesIdMax + 1;
                                    }
                                }
                            }
                        }
                    }
                }

                int int_updatelog = 0;
                int int_insertlog = 0;
                int int_dellog = 0;

                //存在Notes 更新狀態=1
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    update OFAC..Notes set status = '3' where entnum in (select entnum from OFAC..TempLoadNotes where Status = 1) and status != 1;";
                    sqlCommand.CommandTimeout = 300;
                    int_updatelog = sqlCommand.ExecuteNonQuery();
                }

                //存在Notes 更新狀態=4
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    update OFAC..Notes set status = '4' where entnum in (select entnum from OFAC..TempLoadNotes where Status = 4) and status != 4;";
                    sqlCommand.CommandTimeout = 300;
                    int_dellog = sqlCommand.ExecuteNonQuery();
                }

                //不存在Notes insert
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = @"
                    insert into OFAC..Notes(NotesID,EntNum,Note,NoteType,Status,ListCreateDate,ListModifDate,CreateOper,CreateDate,LastOper,LastModify)
                    select NotesID,EntNum,Note,NoteType,'2',ListCreateDate,ListModifDate,CreateOper,CreateDate,LastOper,LastModify
                      from OFAC..TempLoadNotes where not exists 
                      (select * from OFAC..Notes 
                        where OFAC..Notes.EntNum = OFAC..TempLoadNotes.EntNum
                        and isnull(OFAC.dbo.Notes.Note,'') = isnull(OFAC.dbo.TempLoadNotes.Note, '')
                        and isnull(OFAC.dbo.Notes.NoteType,'') = isnull(OFAC.dbo.TempLoadNotes.NoteType,'')
                      )
                    ;";
                    sqlCommand.CommandTimeout = 300;

                    int_insertlog = sqlCommand.ExecuteNonQuery();

                    OFACEventLog(string.Format("Notes daily Update:{0}, Insert:{1}, Delete:{2}", int_updatelog, int_insertlog, int_dellog));
                    LogAdd(string.Format("[Daily] Notes - Insert:{0}", int_insertlog));
                }

                //201904 update status where not exists
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    string sqlDel = @" update OFAC..Notes set Status = 4 , LastModify= getdate()
                from OFAC..Notes (nolock) note
                where EntNum in (select EntNum from OFAC..TempLoadNotes)
                and not exists 
                (select * from OFAC..TempLoadNotes (nolock) tnote where note.EntNum = tnote.EntNum and note.Note = tnote.Note
                    and note.NoteType = tnote.NoteType
                ) ";

                    sqlCommand.CommandText = sqlDel;
                    sqlCommand.CommandTimeout = 300;

                    int InsertCount = sqlCommand.ExecuteNonQuery();
                    LogAdd("[Daily] update Notes set status = 4 where not exists:" + InsertCount);
                }

                //20200330 delete where status equals to not exists
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    string sqlDel = @" delete OFAC..Notes
                where EntNum in (select EntNum from OFAC..TempLoadNotes)
                and status = 4";

                    sqlCommand.CommandText = sqlDel;
                    sqlCommand.CommandTimeout = 300;

                    int InsertCount = sqlCommand.ExecuteNonQuery();
                    LogAdd("[Daily] delete Notes where not exists:" + InsertCount);
                }

                sqlConnection.Close();
                sqlConnection.Dispose();
            }

            LogAdd("[Daily] Expand to Notes -- Completion.");
        }

        public void UpdateNative(string str_UPDDate, string ALT_Enable,string ALTCode, string NOTE_Enable, string NOTECode, string str_BaseEntNum)
        {
            try
            {
                LogAdd("[Daily] Native Update -- Start.");
                
                Database db;
                
                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
                //StreamWriter fileDWriter = new StreamWriter(BCPFile_SDNAltTable, false, Encoding.GetEncoding(65001));

                string str_datetime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                string sqlALTins = "";
                string sqlNoteins = "";
                string strAltlist = "";
                string strNotelist = "";
                if (string.IsNullOrEmpty(str_UPDDate))
                {
                    str_UPDDate = "30";
                }

                DataTable dt = new DataTable();
                

                if (ALT_Enable == "Y")
                {
                    for (int i = 0; i < ALTCode.Split(',').Count(); i++)
                    {
                        strAltlist = strAltlist + "'" + ALTCode.Split(',')[i] + "',";
                    }

                    strAltlist = strAltlist.TrimEnd(',');

                    //insert to ALT Table
                    //20200804 Lesley recommand use aka as the alttype.
                    sqlALTins = @"SELECT DISTINCT wcnv.EntNum,
                        ROW_NUMBER() OVER (ORDER BY wcnv.entnum, AliasesText,CreateDate asc) + (select max(AltNum) from SDNAltTable) AS AltNum,
                        'a.k.a.' as AltType,AliasesText as AltName, 0 Deleted,'2' Status, getdate() ListCreateDate,getdate() ListModifDate, getdate() CreateDate, getdate() LastModifDate, 'Prime' LastOper,
                        wcnv.Language
                        from WCNativeVIEW wcnv
                        join SDNTable sdn on wcnv.Entnum = sdn.EntNum
                        where sdn.Status != 4
                        and ListType != 'WPEP'
                        and Program != 'OTHER' 
                        and NOT EXISTS (SELECT * FROM OFAC.dbo.SDNAltTable WHERE wcnv.EntNum = EntNum AND wcnv.AliasesText = AltName)
                        ";

                    sqlALTins += "and Language in (" + strAltlist + ") ";
                    sqlALTins += "order by wcnv.EntNum, AltNum";

                    LogAdd("[Daily][Insert WCNative Alt]Select SQL:" + sqlALTins);

                    DbCommand dbCommandIns = db.GetSqlStringCommand(sqlALTins);
                    dbCommandIns.CommandTimeout = 1000;
                    //dbCommandIns.Parameters.AddWithValue("@min", str_datetime);

                    IDataReader dataReaderIns = db.ExecuteReader(dbCommandIns);

                    
                    dt.Load(dataReaderIns);
                    dbCommandIns.Dispose();

                    LogAdd("[Daily][Insert WCNative Alt]Data count Insert:" + dt.Rows.Count);

                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        sqlConnection.Open();
                        var sqlBulkCopy = new SqlBulkCopy(sqlConnection)
                        {
                            DestinationTableName = "SDNAltTable"
                        };

                        sqlBulkCopy.ColumnMappings.Add("EntNum", "EntNum");
                        sqlBulkCopy.ColumnMappings.Add("AltNum", "AltNum");
                        sqlBulkCopy.ColumnMappings.Add("AltType", "AltType");
                        sqlBulkCopy.ColumnMappings.Add("AltName", "AltName");
                        sqlBulkCopy.ColumnMappings.Add("Deleted", "Deleted");
                        sqlBulkCopy.ColumnMappings.Add("Status", "Status");
                        sqlBulkCopy.ColumnMappings.Add("ListCreateDate", "ListCreateDate");
                        sqlBulkCopy.ColumnMappings.Add("ListModifDate", "ListModifDate");
                        sqlBulkCopy.ColumnMappings.Add("CreateDate", "CreateDate");
                        sqlBulkCopy.ColumnMappings.Add("LastModifDate", "LastModifDate");
                        sqlBulkCopy.ColumnMappings.Add("LastOper", "LastOper");
                        sqlBulkCopy.ColumnMappings.Add("LANGUAGE", "ListID");

                        //Insert CCC code
                        InsertDataTable(sqlBulkCopy, sqlConnection, dt);

                        //update ALT Table which WC Native not exist, set status = 4
                        using (var sqlCommand = new SqlCommand("", sqlConnection))
                        {
                            string sqlDel = @" update OFAC.dbo.SDNAltTable set Status = 4 , LastModifDate= getdate()
                        from OFAC.dbo.SDNAltTable (nolock) sdnalt
                        where not exists 
                        (select * from OFAC..WCNativeVIEW (nolock) wcnv 
                        where sdnalt.EntNum = wcnv.EntNum and sdnalt.AltName = wcnv.AliasesText
                        ) and sdnalt.status != 4 ";

                            sqlDel += "and sdnalt.EntNum > " + str_BaseEntNum;
                            sqlDel += "and sdnalt.AltType in (" + strAltlist + ") ";

                            sqlCommand.CommandText = sqlDel;
                            sqlCommand.CommandTimeout = 300;

                            int delcount = sqlCommand.ExecuteNonQuery();
                            sqlCommand.Dispose();
                            LogAdd("[Daily] update WC Native in ALTtable set status = 4 where not exists:" + delcount);
                            OFACEventLog("WorldCheck Native data Insert SDNAlTtable count:" + dt.Rows.Count + ", delete count:" + delcount);
                        }

                        sqlConnection.Dispose();
                    }
                }

                if(NOTE_Enable == "Y")
                {
                    for (int i = 0; i < NOTECode.Split(',').Count(); i++)
                    {
                        strNotelist = strNotelist + "'" + NOTECode.Split(',')[i] + "',";
                    }

                    strNotelist = strNotelist.TrimEnd(',');

                    //insert to Notes Table
                    sqlNoteins = @"select ROW_NUMBER() OVER (ORDER BY wcnv.entnum, AliasesText,CreateDate asc) + (select max(NotesID) from Notes) AS NotesID,
                wcnv.EntNum, wcnv.AliasesText as Note,'102' NoteType, wcnv.Language ListID,'2' Status,getdate() ListCreateDate,getdate() ListModifDate, 'PrimeAdmin' CreateOper,getdate() CreateDate, 'PrimeAdmin' LastOper,getdate() LastModify 
                from WCNativeVIEW wcnv
                join SDNTable sdn on wcnv.Entnum = sdn.EntNum
                where sdn.Status != 4
                and ListType != 'WPEP'
                and Program != 'OTHER' 
                and NOT EXISTS (SELECT * FROM OFAC.dbo.Notes WHERE wcnv.EntNum = EntNum AND wcnv.AliasesText = Note and wcnv.Language = ListID) ";

                    sqlNoteins += "and Language in (" + strNotelist + ") ";
                    sqlNoteins += "order by wcnv.EntNum,NotesID";

                    LogAdd("[Daily][Insert WCNative Notes]Select SQL:" + sqlNoteins);

                    DbCommand dbCommandIns = db.GetSqlStringCommand(sqlNoteins);

                    dbCommandIns = db.GetSqlStringCommand(sqlNoteins);
                    dbCommandIns.CommandTimeout = 1000;
                    //dbCommandIns.Parameters.AddWithValue("@min", str_datetime);

                    IDataReader dataReaderIns = db.ExecuteReader(dbCommandIns);

                    dt = new DataTable();
                    dt.Load(dataReaderIns);
                    dbCommandIns.Dispose();

                    LogAdd("[Daily][Insert WCNative Notes]Data count Insert:" + dt.Rows.Count);
                    
                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        sqlConnection.Open();
                        var sqlBulkCopy = new SqlBulkCopy(sqlConnection)
                        {
                            DestinationTableName = "Notes"
                        };

                        sqlBulkCopy.ColumnMappings.Add("EntNum", "EntNum");
                        sqlBulkCopy.ColumnMappings.Add("NotesID", "NotesID");
                        sqlBulkCopy.ColumnMappings.Add("NoteType", "NoteType");
                        sqlBulkCopy.ColumnMappings.Add("Note", "Note");
                        sqlBulkCopy.ColumnMappings.Add("Status", "Status");
                        sqlBulkCopy.ColumnMappings.Add("ListID", "ListID");
                        sqlBulkCopy.ColumnMappings.Add("ListCreateDate", "ListCreateDate");
                        sqlBulkCopy.ColumnMappings.Add("ListModifDate", "ListModifDate");
                        sqlBulkCopy.ColumnMappings.Add("CreateOper", "CreateOper");
                        sqlBulkCopy.ColumnMappings.Add("CreateDate", "CreateDate");
                        sqlBulkCopy.ColumnMappings.Add("LastOper", "LastOper");
                        sqlBulkCopy.ColumnMappings.Add("LastModify", "LastModify");

                        //Insert Foreign Language names
                        InsertDataTable(sqlBulkCopy, sqlConnection, dt);

                        //update Notes Table which WC Native not exist, set status = 4
                        using (var sqlCommand = new SqlCommand("", sqlConnection))
                        {
                            string sqlDel = @" update OFAC.dbo.Notes set Status = 4 , LastModify= getdate()
                        from OFAC.dbo.Notes (nolock) n
                        where not exists 
                        (select * from OFAC..WCNativeVIEW (nolock) wcnv 
                        where n.EntNum = wcnv.EntNum and n.Note = wcnv.AliasesText and wcnv.Language = n.ListID
                        ) and NoteType = 102 and n.status != 4 ";

                            sqlDel += "and n.EntNum > " + str_BaseEntNum;
                            sqlDel += "and n.NoteType = 102";

                            sqlCommand.CommandText = sqlDel;
                            sqlCommand.CommandTimeout = 300;

                            int delcount = sqlCommand.ExecuteNonQuery();
                            LogAdd("[Daily] update WC Native in ALTtable set status = 4 where not exists:" + delcount);
                            OFACEventLog("WorldCheck Native data Insert Notes count:" + dt.Rows.Count + ", delete count:" + delcount);
                            sqlCommand.Dispose();
                        }

                        sqlConnection.Dispose();
                    }
                }

                LogAdd("[Daily] Native Update -- Completion.");
                dt.Dispose();

            }
            catch (Exception ex)
            {
                LogException(ex, "UpdateNative");
            }
        }

        /// <summary>
        /// 更新WCKeyword 資料
        /// </summary>
        /// <param name="str_UPDDate"></param>
        /// <param name="strBaseNum"></param>
        public void UpdateKeyWord(string str_UPDDate, int strBaseNum)
        {
            try
            {
                LogAdd("[Daily] update WCK -- Start.");

                Database db;
                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
                string str_datetime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string sqldel = "";
                string sqlins = "";

                if (string.IsNullOrEmpty(str_UPDDate))
                {
                    str_UPDDate = "30";
                }

                sqlins = "select UID, Keywords from WorldCheck(nolock) ";
                sqlins += " where UPDDate >= dateadd(MINUTE,@min * -1,GETDATE()) ";
                sqlins += "and isnull(DelFlag,0) = 0 ";
                sqlins += "and Keywords is not null and Keywords != '' ";

                LogAdd("Select SQL:" + sqlins);

                DbCommand dbCommandIns = db.GetSqlStringCommand(sqlins);
                dbCommandIns.CommandTimeout = 0;
                //dbCommandIns.Parameters.AddWithValue("@min", str_datetime);
                var parameter = dbCommandIns.CreateParameter();
                parameter.ParameterName = "@min";
                parameter.Value = str_UPDDate;
                dbCommandIns.Parameters.Add(parameter);

                IDataReader dataReaderDT = db.ExecuteReader(dbCommandIns);
                DataTable dt = new DataTable();
                dt.Load(dataReaderDT);

                DbCommand dbCommandSelect = db.GetSqlStringCommand("select * from WCKeywords where 0 =1;");
                dbCommandSelect.CommandTimeout = 0;
                IDataReader dataReaderDTAdd = db.ExecuteReader(dbCommandSelect);
                DataTable dtadd = new DataTable();
                dtadd.Load(dataReaderDTAdd);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int j = 0; j < dt.Rows[i]["Keywords"].ToString().Split('~').Count(); j++)
                    {
                        DataRow row = dtadd.NewRow();
                        row["UID"] = dt.Rows[i]["UID"].ToString();
                        row["Entnum"] = int.Parse(dt.Rows[i]["UID"].ToString()) + strBaseNum;
                        row["Word"] = dt.Rows[i]["Keywords"].ToString().Split('~')[j].ToString().Trim();
                        row["Deleted"] = false;
                        row["Status"] = 0;
                        row["CreateOper"] = "PrimeAdmin";
                        row["CreateDate"] = str_datetime;
                        row["LastModifDate"] = str_datetime;
                        row["LastOper"] = "PrimeAdmin";

                        dtadd.Rows.Add(row);
                        dtadd.AcceptChanges();
                    }
                }

                dbCommandSelect.Dispose();

                dataReaderDT.Close();
                var sqlConnection = new SqlConnection(connectionString);

                var sqlBulkCopy = new SqlBulkCopy(sqlConnection)
                {
                    DestinationTableName = "WCKeywords"
                };

                sqldel = "delete WCKeywords where UID in ( ";
                sqldel += "select UID from WorldCheck(nolock) ";
                sqldel += "where UPDDate >= dateadd(MINUTE,@min * -1,GETDATE()) ";
                sqldel += "and isnull(DelFlag,0) = 0) ";

                LogAdd("del SQL:" + sqldel);

                LogAdd("WCK data count:"+ dtadd.Rows.Count);
                //LogAdd("WCK show data: UID=" + dtadd.Rows[0]["UID"].ToString() + ",Entnum=" + dtadd.Rows[0]["Entnum"].ToString() + ",Word=" + dtadd.Rows[0]["Word"].ToString() + ",createdate=" + dtadd.Rows[0]["CreateDate"].ToString() + ".");
                //LogAdd("WCK show data: Delete=" + dtadd.Rows[0]["Deleted"].ToString() + ",Status=" + dtadd.Rows[0]["Status"].ToString() + ".");
                if (dtadd.Rows.Count > 0)
                {
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        sqlConnection.Open();
                        sqlCommand.CommandText = sqldel;
                        sqlCommand.Parameters.AddWithValue("@min", str_UPDDate);
                        int DelCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        //OFACEventLog("WorldCheck Keywords delete:" + DelCount);
                        LogAdd("WorldCheck Keywords delete:" + DelCount);
                    }

                    InsertDataTable(sqlBulkCopy, sqlConnection, dtadd);
                }

                sqlConnection.Close();
                sqlConnection.Dispose();

                LogAdd("[Daily] update WCK -- Completion.");
            }
            catch (Exception ex)
            {
                LogException(ex, "UpdateKeyWord");
            }
        }

        /// <summary>
        /// 更新WorldCheck Keyword-list table資料
        /// </summary>
        /// <param name="fileName"></param>
        public void UpdateWCKeywordList(string fileName)
        {
            try
            {
                LogAdd("[Daily] update WCK list -- start!");
                //2020.10.13 forren asked to remove the kw event log.
                //OFACEventLog("WCK list update start!");
                var dt = new DateTime();
                dt = DateTime.Now;

                ExcelReader excelreader = new ExcelReader();

                DataSet ds = new DataSet();
                SqlDataAdapter da;

                string Abbreviation = "";

                ds = excelreader.ReadExcel(fileName);

                ds.Tables[0].Rows[0].Delete();
                ds.Tables[0].Rows[0].AcceptChanges();
                ds.Tables[0].Rows[0].Delete();
                ds.Tables[0].Rows[0].AcceptChanges();
                ds.Tables[0].Columns[0].ColumnName = "Abbreviation";
                ds.Tables[0].Columns[1].ColumnName = "SOURCENAME";
                ds.Tables[0].Columns[2].ColumnName = "CountryAuthority";
                ds.Tables[0].Columns[3].ColumnName = "KWType";
                ds.Tables[0].Columns[4].ColumnName = "Explanation";
                ds.Tables[0].Columns.Add("USED", typeof(int));
                ds.Tables[0].Columns.Add("SDNPROGRAM", typeof(String));
                ds.Tables[0].Columns.Add("LastModify", typeof(DateTime));
                ds.Tables[0].Columns["SDNPROGRAM"].MaxLength = 50;
                ds.Tables[0].Columns["SDNPROGRAM"].AllowDBNull = true;
                ds.Tables[0].Columns["USED"].AllowDBNull = true;
                ds.Tables[0].Columns["LastModify"].AllowDBNull = true;


                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    string query = "";
                    query = "select Abbreviation from WCKeywordslistTable;";
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        sqlCommand.CommandText = query;
                        sqlCommand.CommandTimeout = 300;

                        da = new SqlDataAdapter(sqlCommand);
                        DataSet ds_wck = new DataSet();

                        da.Fill(ds_wck);

                        LogAdd("WCK list Count:" + ds_wck.Tables[0].Rows.Count);
                        //OFACEventLog("WCK list Count:" + ds_wck.Tables[0].Rows.Count);
                        foreach (DataRow r in ds.Tables[0].Select(""))
                        {
                            Abbreviation = r[0].ToString().Trim();

                            DataRow[] dr_result = ds_wck.Tables[0].Select("Abbreviation = '" + Abbreviation + "'");

                            //LogAdd("Match Count:" + dr_result.Count());

                            if (dr_result.Count() > 0)
                            {
                                r.Delete();
                                r.AcceptChanges();
                            }
                            else
                            {
                                r["LastModify"] = DateTime.Now.ToString("yyyy-MM-dd");

                                r.AcceptChanges();

                                LogAdd("WCK list new added:" + Abbreviation);

                                //OFACEventLog("WCK list new added:" + Abbreviation);
                            }


                        }
                    }

                    LogAdd("WCK list need modify count:" + ds.Tables[0].Rows.Count);

                    //OFACEventLog("WCK list need modify count:" + ds.Tables[0].Rows.Count);

                    var sqlBulkCopy = new SqlBulkCopy(sqlConnection)
                    {
                        DestinationTableName = "WCKeywordslistTable"
                    };

                    if (ds.Tables[0].Rows.Count > 0)
                        InsertDataTable(sqlBulkCopy, sqlConnection, ds.Tables[0]);

                    string str_keywords = string.Empty;
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        sqlCommand.CommandTimeout = 300;

                        string sql = "select Abbreviation from WCKeywordslistTable where used = 1 order by KWType desc, Abbreviation asc;";

                        sqlCommand.CommandText = sql;

                        da = new SqlDataAdapter(sqlCommand);
                        DataSet ds_wckl = new DataSet();

                        da.Fill(ds_wckl);

                        foreach (DataRow r in ds_wckl.Tables[0].Select(""))
                        {
                            str_keywords += "" + r["Abbreviation"].ToString().Trim() + ",";
                        }
                    }

                    sqlConnection.Close();
                    sqlConnection.Dispose();

                    LogAdd("[Daily] update WCK list -- Completion!");
                    //OFACEventLog("WCK list update End!");
                    //OFACEventLog("WorldCheck Sanction Keywords List :" + str_keywords.TrimEnd(','));
                }
            }
            catch (Exception ex)
            {
                LogException(ex, "UpdateWCKeywordList");
            }
            
        }

        /// <summary>
        /// Refresh the WCimport data status to 1
        /// </summary>
        public void RefreshWCStatus()
        {
            LogAdd("WC SDN Status refresh Start");
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                string query = "";
                LogAdd("WC SDN Status refresh SDN");
                query = @"update ofac..SDNTable
                            set Status = 1
                            where EntNum > 20000000
                            and Status in (2, 3);

                            update ofac..SDNAltTable
                            set Status = 1 
                            where EntNum > 20000000
                            and Status in (2,3);

                            update ofac..Relationship
                            set Status = 1 
                            where EntNum > 20000000
                            and Status in (2,3);

                            update ofac..URLs
                            set Status = 1 
                            where EntNum > 20000000
                            and Status in (2,3);

                            update ofac..DOBs
                            set Status = 1 
                            where EntNum > 20000000
                            and Status in (2,3);

                            update ofac..SDNAddrTable
                            set Status = 1 
                            where EntNum > 20000000
                            and Status in (2,3);

                            update ofac..Notes
                            set Status = 1 
                            where EntNum > 20000000
                            and Status in (2,3);";
                using (var sqlCommand = new SqlCommand("", sqlConnection))
                {
                    sqlCommand.CommandText = query;
                    sqlCommand.CommandTimeout = 300;
                    int UpdateCount = sqlCommand.ExecuteNonQuery();
                }

                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            LogAdd("WC SDN Status refresh End");
        }

        public void ImportNative(string filePathName)
        {
            try
            {
                LogAdd("[Daily] WCK Native Import -- Start!");
                //新建XML文件類別
                XmlDocument Xmldoc = new XmlDocument();
                //從指定的字串載入XML文件
                //Xmldoc.Load(filePathName);
                Xmldoc.XmlResolver = null;
                Xmldoc.Load(fortifyService.PathManipulation(filePathName));

                //建立此物件，並輸入透過StringReader讀取Xmldoc中的Xmldoc字串輸出
                //XmlReader Xmlreader = XmlReader.Create(new System.IO.StringReader(fortifyService.PathManipulation(Xmldoc.OuterXml)));

                XmlReaderSettings settings = new XmlReaderSettings();
                //settings.Schemas.Add(schema);
                settings.ValidationType = ValidationType.Schema;
                StringReader sr = new StringReader(Xmldoc.OuterXml);
                XmlReader Xmlreader = XmlReader.Create(sr, settings);

                //建立DataSet
                DataSet ds = new DataSet();
                //透過DataSet的ReadXml方法來讀取Xmlreader資料
                ds.ReadXml(Xmlreader);
                //建立DataTable並將DataSet中的Table資料給DataTable
                DataTable dt_record = ds.Tables[0];
                DataTable dt_aliases = ds.Tables[1];
                DataTable dt_alias = ds.Tables[2];
                //回傳DataTable

                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    //WCRecords import
                    using (var cmd = new SqlCommand("truncate table WCForeignRecords;", sqlConnection))
                    {
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                    LogAdd("[Daily] WCForeignRecords truncate!");
                    // Create the bulk copy object
                    var sqlBulkCopyRecord = new SqlBulkCopy(sqlConnection)
                    {
                        DestinationTableName = "WCForeignRecords"
                    };

                    // bulk column mapping.
                    for (int i = 0; i < dt_record.Columns.Count; i++)
                    {
                        string strHeader = dt_record.Columns[i].ColumnName;

                        if (strHeader.ToUpper().Equals("RECORD_ID"))
                        {
                            sqlBulkCopyRecord.ColumnMappings.Add("RECORD_ID", "RecordId");

                        }
                        else if (strHeader.ToUpper().Equals("UID"))
                        {
                            sqlBulkCopyRecord.ColumnMappings.Add("UID", "UID");

                        }
                        else
                        {
                            //sqlBulkCopy.ColumnMappings.Add(strHeader, strHeader);
                            LogAdd("[Daily_NativeImport] Records No Match Header:" + strHeader);
                        }

                    }

                    if (dt_record.Rows.Count > 0)
                    {
                        InsertDataTable(sqlBulkCopyRecord, sqlConnection, dt_record);
                    }

                    LogAdd("[Daily] WCForeignRecords insert :" + dt_record.Rows.Count);
                    //WCForeignAliases import
                    using (var cmd = new SqlCommand("truncate table WCForeign;", sqlConnection))
                    {
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                    LogAdd("[Daily] WCForeign truncate!");

                    // Create the bulk copy object
                    var sqlBulkCopyForeignAliases = new SqlBulkCopy(sqlConnection)
                    {
                        DestinationTableName = "WCForeign"
                    };

                    // bulk column mapping.
                    for (int i = 0; i < dt_aliases.Columns.Count; i++)
                    {
                        string strHeader = dt_aliases.Columns[i].ColumnName;

                        if (strHeader.ToUpper().Equals("FOREIGN_ALIASES_ID"))
                        {
                            sqlBulkCopyForeignAliases.ColumnMappings.Add("FOREIGN_ALIASES_ID", "AliasesID");

                        }
                        else if (strHeader.ToUpper().Equals("RECORD_ID"))
                        {
                            sqlBulkCopyForeignAliases.ColumnMappings.Add("RECORD_ID", "RecordId");

                        }
                        else
                        {
                            //sqlBulkCopy.ColumnMappings.Add(strHeader, strHeader);
                            LogAdd("[Daily_NativeImport] WCForeign No Match Header:" + strHeader);
                        }

                    }

                    if (dt_aliases.Rows.Count > 0)
                        InsertDataTable(sqlBulkCopyForeignAliases, sqlConnection, dt_aliases);

                    LogAdd("[Daily] WCForeign insert :" + dt_aliases.Rows.Count);

                    //WCForeignAlias import
                    using (var cmd = new SqlCommand("truncate table WCForeignAliases;", sqlConnection))
                    {
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                    LogAdd("[Daily] WCForeignAliases truncate!");

                    // Create the bulk copy object
                    var sqlBulkCopyForeignAlias = new SqlBulkCopy(sqlConnection)
                    {
                        DestinationTableName = "WCForeignAliases"
                    };

                    // bulk column mapping.
                    for (int i = 0; i < dt_alias.Columns.Count; i++)
                    {
                        string strHeader = dt_alias.Columns[i].ColumnName;

                        if (strHeader.ToUpper().Equals("LANGUAGE"))
                        {
                            sqlBulkCopyForeignAlias.ColumnMappings.Add("LANGUAGE", "Language");

                        }
                        else if (strHeader.ToUpper().Equals("FOREIGN_ALIAS_TEXT"))
                        {
                            sqlBulkCopyForeignAlias.ColumnMappings.Add("FOREIGN_ALIAS_TEXT", "AliasesText");

                        }
                        else if (strHeader.ToUpper().Equals("FOREIGN_ALIASES_ID"))
                        {
                            sqlBulkCopyForeignAlias.ColumnMappings.Add("FOREIGN_ALIASES_ID", "AliasesID");

                        }
                        else
                        {
                            //sqlBulkCopy.ColumnMappings.Add(strHeader, strHeader);
                            LogAdd("[Daily_NativeImport] WCForeignAliases No Match Header:" + strHeader);
                        }

                    }

                    if (dt_alias.Rows.Count > 0)
                        InsertDataTable(sqlBulkCopyForeignAlias, sqlConnection, dt_alias);

                    LogAdd("[Daily] WCForeignAliases insert :" + dt_alias.Rows.Count);

                    sqlConnection.Close();
                    sqlConnection.Dispose();
                }

                ds.Dispose();

                LogAdd("[Daily] WCK Native Import -- Completion!");
            }
            catch (Exception ex)
            {
                LogException(ex, "ImportNative");
            }
        }

        /// <summary>
        /// automatically alter table add column
        /// </summary>
        /// <param name="strTableName"></param>
        /// <param name="strColumnName"></param>
        public void AlterWC(string strTableName, string strColumnName)
        {
            try
            {
                LogAdd("[" + strTableName + "] Detect new column from WorldCheckList file!");

                string SQL = "";
                SqlDataAdapter da;
                DataSet ds;

                SQL = "ALTER TABLE " + strTableName + " ADD " + strColumnName + " VARCHAR(3000);";
                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        sqlConnection.Open();
                        
                        sqlCommand.CommandTimeout = 300;

                        sqlCommand.CommandText = @"
                        SELECT ORDINAL_POSITION,COLUMN_NAME
                        FROM INFORMATION_SCHEMA.COLUMNS
                            WHERE
                                TABLE_CATALOG = 'OFAC'
                                AND TABLE_SCHEMA = 'dbo'
                                AND TABLE_NAME = '" + strTableName + "' order by ORDINAL_POSITION";
                        da = new SqlDataAdapter(sqlCommand);
                        ds = new DataSet();

                        da.Fill(ds);

                        if (ds.Tables[0].Select("COLUMN_NAME = '" + strColumnName + "'").Count() == 0)
                        {
                            sqlCommand.CommandText = SQL;
                            sqlCommand.ExecuteNonQuery();
                            LogAdd("[" + strTableName + "] New Column " + strColumnName + " ALTER Finished!");
                        }
                        else
                        {
                            LogAdd("[" + strTableName + "] Column " + strColumnName + " already exist!");
                        }

                        sqlCommand.Dispose();
                        sqlConnection.Dispose();
                    }
                }
                LogAdd("[" + strTableName + "] ALTER Table Finished!");
            }
            catch (Exception ex)
            {
                LogException(ex, string.Format("AlterWC Table {0}", strTableName));
            }
        }

        public string GetWorldCheckSchema(string strTableName)
        {
            string returnSQL = "";
            SqlDataAdapter da;
            DataSet ds;
            try
            {
                //Get columns
                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        sqlCommand.CommandText = @"
                        SELECT ORDINAL_POSITION,COLUMN_NAME
                        FROM INFORMATION_SCHEMA.COLUMNS
                            WHERE
                                TABLE_CATALOG = 'OFAC'
                                AND TABLE_SCHEMA = 'dbo'
                                AND TABLE_NAME = '" + strTableName + "' order by ORDINAL_POSITION";
                        da = new SqlDataAdapter(sqlCommand);
                        ds = new DataSet();

                        da.Fill(ds);

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            returnSQL += "CAST(isnull(" + dr.ItemArray[1].ToString().ToUpper() + ",'') AS nvarchar(20)) + '|' +";
                        }

                        sqlCommand.Dispose();
                    }
                    sqlConnection.Close();
                    sqlConnection.Dispose();
                }

            }
            catch (Exception ex)
            {
                LogException(ex, "GetWorldCheckSchema");

            }

            return returnSQL;
        }

        public void ClearConnection()
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connectionString))
                {

                    // 開啟連線
                    sqlConnection.Open();

                    sqlConnection.Close();
                    sqlConnection.Dispose();
                    SqlConnection.ClearAllPools();

                    LogAdd("[Daily][Clean] Cleared Connection Pool");
                }
            }
            catch (Exception ex)
            {
                LogException(ex, "ClearConnection");
            }
        }

        /// <summary>
        /// SQL Bulk Insert
        /// </summary>
        /// <param name="sqlBulkCopy"></param>
        /// <param name="sqlConnection"></param>
        /// <param name="dataTable"></param>
        protected void InsertDataTable(SqlBulkCopy sqlBulkCopy, SqlConnection sqlConnection, DataTable dataTable)
        {
            LogAdd("[DB]WriteToServer " + dataTable.Rows.Count.ToString() + " Records.");
            //APR.21 ,2023 add retry
            bool bol_status = false;
            
            for(int i = 0; i < retries; i++)
            {
                try 
                {
                    if (!bol_status)
                    {
                        sqlBulkCopy.BatchSize = _batchSize;
                        sqlBulkCopy.BulkCopyTimeout = 0;
                        sqlBulkCopy.WriteToServer(dataTable);
                        sqlBulkCopy.Close();
                        dataTable.Dispose();

                        bol_status = true;
                    }
                }
                catch (Exception ex)
                {
                    LogException(ex, string.Format("[DB] Try {0} times", (i + 1).ToString()));
                    System.Threading.Thread.Sleep(10000);
                }
            }
        }

        /// <summary>
        /// 寫入OFAC Event
        /// </summary>
        public void OFACEventLog(string log)
        {
            try
            {
                string sql = @"
insert into SDNChangeLog (logdate, oper, logtext, type, objecttype, objectid) 
values 
(getdate(), 'Prime', @Log, 'Ann', 'Event', 'WORLDCHECK')
";

                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    using (var cmd = new SqlCommand(sql, sqlConnection))
                    {
                        cmd.Parameters.Add(new SqlParameter("@Log", fortifyService.PathManipulation(log.Replace(@"\n", Environment.NewLine))));
                        //cmd.Parameters.Add(new SqlParameter("@Log", fortifyService.PathManipulation(log)));
                        cmd.CommandTimeout = 50;
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }

                    sqlConnection.Dispose();

                    SqlConnection.ClearAllPools();


                }
            }
            catch (Exception ex)
            {
                LogException(ex, "OFACEventLog");
            }
            
        }

        private void LogAdd(string log)
        {
            logs.AppendLine(log);
            Log.WriteToLog("Info", log);
        }

        private void Logerr(string log)
        {
            Log.WriteToLog("Error", log);
        }

        public bool LogException(Exception e, string str_FunctionName)
        {
            Logerr(string.Format("Error at Function {0}", str_FunctionName));
            Logerr(string.Format("In the log routine. Caught {0}", e.GetType()));
            Logerr(string.Format("Message: {0}", e.Message));
            return true;
        }

        private string BuildFileImg()
        {
            StringBuilder s = new StringBuilder();
            Process p = new Process();
            ProcessStartInfo pInfo = new ProcessStartInfo("D:\\Prime\\Exe\\BuildFileImg.exe");

            s.AppendLine(string.Format("{0} | ========== Start Build File Image ==========",
                                       DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")))
             .AppendLine();

            pInfo.UseShellExecute = false;
            pInfo.RedirectStandardOutput = true;
            pInfo.CreateNoWindow = true;

            p.StartInfo = pInfo;
            p.Start();

            using (StreamReader myStreamReader = p.StandardOutput)
            {
                string result = "";
                string line = "";
                while ((line = ReadLineSafe(myStreamReader,int.MaxValue)) != null)
                {
                    result += line + "\r\n";
                }
                p.WaitForExit();
                p.Close();

                s.AppendLine(result);
            }

            s.AppendLine(string.Format("{0} | ========== End Build File Image ==========",
                                        DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));
            return s.ToString();
        }

        private void StopService()
        {
            try
            {
                //stop OFAC service
                Process p = new Process();
                //ProcessStartInfo pInfo = new ProcessStartInfo();

                LogAdd(string.Format("{0} | ========== Stop OFAC Server BOTOSB service begin ==========",
                                           DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                string m_ServiceName = "OFAC Server BOTOSB";
                System.ServiceProcess.ServiceController service = new System.ServiceProcess.ServiceController(m_ServiceName);

                // 設定一個 Timeout 時間，若超過 60 秒啟動不成功就宣告失敗!
                TimeSpan timeout = TimeSpan.FromMilliseconds(1000 * 60);

                // 若該服務不是「停用」的狀態，才將其停止運作，否則會引發 Exception
                if (service.Status != System.ServiceProcess.ServiceControllerStatus.Stopped &&
                    service.Status != System.ServiceProcess.ServiceControllerStatus.StopPending)
                {
                    service.Stop();
                    service.WaitForStatus(System.ServiceProcess.ServiceControllerStatus.Stopped, timeout);
                }

                service.Dispose();

                LogAdd(string.Format("{0} | ========== Stop OFAC Server BOTOSB service finished ==========",
                                            DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));
            }
            catch (Exception ex)
            {
                LogException(ex, "StopService");
            }
        }

        private void StartService()
        {
            try
            {
                //stop OFAC service
                Process p = new Process();
                //ProcessStartInfo pInfo = new ProcessStartInfo();

                LogAdd(string.Format("{0} | ========== Start OFAC Server BOTOSB service begin ==========",
                                           DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                string m_ServiceName = "OFAC Server BOTOSB";
                System.ServiceProcess.ServiceController service = new System.ServiceProcess.ServiceController(m_ServiceName);

                // 設定一個 Timeout 時間，若超過 60 秒啟動不成功就宣告失敗!
                TimeSpan timeout = TimeSpan.FromMilliseconds(1000 * 60);

                service.Start();
                service.WaitForStatus(System.ServiceProcess.ServiceControllerStatus.Running, timeout);

                service.Dispose();

                LogAdd(string.Format("{0} | ========== Start OFAC Server BOTOSB service finished ==========",
                                            DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));
            }
            catch (Exception ex)
            {
                LogException(ex, "StartService");
            }
        }

        private void UpdDBStatus()
        {
            //2018.05
            try
            {
                string sql = @"update OFAC.dbo.SDNDbStatus set MemImgBuildReq = 0;";

                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    using (var cmd = new SqlCommand(sql, sqlConnection))
                    {
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }

                    sqlConnection.Close();
                    sqlConnection.Dispose();
                }

                OFACEventLog("Update DBstatus to 0 ");

                Log.WriteToLog("Info", "Update DBstatus to 0 ");
            }
            catch (Exception ex)
            {
                LogException(ex, "UpdDBStatus");
            }
        }

        /// <summary>
        /// 202009 added Run batch after schedule.
        /// </summary>
        /// <param name="FilePath"></param>
        public void Run_command(string FilePath)
        {
            Process p = new Process();
            p.StartInfo.FileName = fortifyService.PathManipulation(FilePath);
            p.StartInfo.WorkingDirectory = @"D:\";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = false;
            p.Start();

            using (StreamReader myStreamReader = p.StandardOutput)
            {
                string result = "";
                string line = "";
                while ((line = ReadLineSafe(myStreamReader, int.MaxValue)) != null)
                {
                    result += line + "\r\n";
                }

                p.WaitForExit();
                p.Close();
                LogAdd("[Daily]Batch Result:" + result.ToString());
            }
        }

        public class GetWCPara
        {
            public string WCSQL { get; set; }
            public string WCListtype { get; set; }
            public string WCProgram { get; set; }
        }

        /// <summary>
        /// add Jul.11,2024 readline for fortify
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public string ReadLineSafe(TextReader reader, int maxLength)
        {
            var sb = new StringBuilder();
            bool bol_end = false;
            while (true)
            {
                int ch = reader.Read();
                if (ch == -1) break;
                if (ch == '\r' || ch == '\n')
                {
                    if (ch == '\r' && reader.Peek() == '\n')
                    {
                        bol_end = true;
                    }
                    else if (ch == '\r')
                    {
                        return sb.ToString();
                    }
                    if (ch == '\n')
                    {
                        return sb.ToString();
                    }
                }

                if (!bol_end)
                {
                    sb.Append((char)ch);
                }

                // Safety net here
                if (sb.Length > maxLength)
                {
                    return null;
                }
            }
            if (sb.Length > 0) return sb.ToString();
            return null;
        }
    }
}
