﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic.FileIO;
using System.Data;
using System.Data.SqlClient;
using Unisys.AML.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Data.Common;

namespace Unisys.AML.ImportService
{
    [DisallowConcurrentExecution]
    public class WorldCheckAddAllJob : IJob
    {
        private StringBuilder logs;

        public WorldCheckAddAllJob()
        {
        }

        public string connectionString = ConfigurationManager.ConnectionStrings["OFAC"].ConnectionString;

        public virtual void Execute(IJobExecutionContext context)
        {
            try
            {
                logs = new StringBuilder();
                LogAdd(string.Format("***** Start WorldCheckAdd All Job [{0}] *****", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                JobDataMap jobDataMap = context.MergedJobDataMap;

                //relation
                string BCPFile_TempRelationship =  jobDataMap.GetString("BCPFile_TempRelationship");
                string BCPFile_Relationship =  jobDataMap.GetString("BCPFile_Relationship");
                string BCPFmt_TempRelationship =  jobDataMap.GetString("BCPFmt_TempRelationship");
                string BCPFmt_Relationship =  jobDataMap.GetString("BCPFmt_Relationship");
                string str_relatedcolumn = jobDataMap.GetString("RelatedColumn");

                //Alternative
                string BCP_SDNAltTable_Enable = jobDataMap.GetString("BCPEnb_SDNAltTable");
                string BCPFile_TmpSDNAltTable =  jobDataMap.GetString("BCPFile_TmpSDNAltTable");
                string BCPFile_SDNAltTable =  jobDataMap.GetString("BCPFile_SDNAltTable");
                string BCPFmt_TmpSDNAltTable =  jobDataMap.GetString("BCPFmt_TmpSDNAltTable");
                string BCPFmt_SDNAltTable =  jobDataMap.GetString("BCPFmt_SDNAltTable");
                string str_weakALT = jobDataMap.GetString("wc_lowaliases");
                string str_weakALT_listtype = jobDataMap.GetString("wc_lowaliases_listtype");
                string str_ALTSPELL = jobDataMap.GetString("wc_AlterSpell");
                string str_ALTPASS = jobDataMap.GetString("wc_AltPass");
                string str_ALTBIC = jobDataMap.GetString("wc_AltBic");
                string str_ALTIMO = jobDataMap.GetString("wc_AltImo");
                string str_ALTARN = jobDataMap.GetString("wc_AltARN");
                string str_ALTMSN = jobDataMap.GetString("wc_AltMSN");
                
                //Notes
                string NoteTypeList = jobDataMap.GetString("Notes_TypeList");

                string BCPFile_TempURLs =  jobDataMap.GetString("BCPFile_TempURLs");
                string BCPFile_URLs =  jobDataMap.GetString("BCPFile_URLs");
                string BCPFmt_TempURLs =  jobDataMap.GetString("BCPFmt_TempURLs");
                string BCPFmt_URLs =  jobDataMap.GetString("BCPFmt_URLs");

                string BCPFile_TempDOBs = jobDataMap.GetString("BCPFile_TempDOBs");
                string BCPFile_DOBs = jobDataMap.GetString("BCPFile_DOBs");
                string BCPFmt_TempDOBs = jobDataMap.GetString("BCPFmt_TempDOBs");
                string BCPFmt_DOBs = jobDataMap.GetString("BCPFmt_DOBs");

                string BCPFile_TempAddr = jobDataMap.GetString("BCPFile_TempAddr");
                string BCPFile_Addr = jobDataMap.GetString("BCPFile_Addr");
                string BCPFmt_TempAddr = jobDataMap.GetString("BCPFmt_TempAddr");
                string BCPFmt_Addr = jobDataMap.GetString("BCPFmt_Addr");

                string BCPFile_TempNotes = jobDataMap.GetString("BCPFile_TempNotes");
                string BCPFile_Notes = jobDataMap.GetString("BCPFile_Notes");
                string BCPFmt_TempNotes = jobDataMap.GetString("BCPFmt_TempNotes");
                string BCPFmt_Notes = jobDataMap.GetString("BCPFmt_Notes");

                string BCPFile_Error =  jobDataMap.GetString("BCPFile_Error");
                string BCPServer = jobDataMap.GetString("BCPServer");
                string BaseNum = jobDataMap.GetString("BaseNum");

                //string wc_subcate_space = jobDataMap.GetString("wc_subcate_space");
                //string str_wc_import_un = jobDataMap.GetString("wc_import_UN");
                string wc_table_select = jobDataMap.GetString("wc_table_select");
                
                string str_wc_import_SQL = jobDataMap.GetString("wc_import_SQL");
                string wc_import_crime_SQL = jobDataMap.GetString("wc_import_CRIME_SQL");

                //201907 user can choose what table they want import. 
                if (wc_table_select == null || wc_table_select == "" || wc_table_select.ToUpper() == "ALL")
                {
                    for (int i = 0; i < str_relatedcolumn.Split('|').Length; i++)
                    {
                        UpdRelationshipAllDataIntoSqlServer(str_relatedcolumn.Split('|')[i], BCPFile_TempRelationship, BCPFile_Relationship, BCPFmt_TempRelationship, BCPFile_Error,
                    BCPFmt_Relationship, BCPServer, BaseNum, str_wc_import_SQL);
                    }
                        

                    if (BCP_SDNAltTable_Enable == "Y")
                    {
                        UpdSDNAltTableAllDataIntoSqlServer("ALT", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                    }

                    if (str_weakALT == "Y")
                    {
                        UpdSDNAltTableAllDataIntoSqlServer("ALTWEAK", str_weakALT_listtype, BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                    }

                    if (str_ALTSPELL == "Y")
                    {
                        UpdSDNAltTableAllDataIntoSqlServer("ALTSPELL", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                    }

                    if (str_ALTPASS == "Y")
                    {
                        UpdSDNAltTableAllDataIntoSqlServer("ALTPASS", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                    }

                    if (str_ALTBIC == "Y")
                    {
                        UpdSDNAltTableAllDataIntoSqlServer("ALTBIC", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                    }

                    if (str_ALTIMO == "Y")
                    {
                        UpdSDNAltTableAllDataIntoSqlServer("ALTIMO", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                    }

                    if (str_ALTARN == "Y")
                    {
                        UpdSDNAltTableAllDataIntoSqlServer("ALTARN", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                    }

                    if (str_ALTMSN == "Y")
                    {
                        UpdSDNAltTableAllDataIntoSqlServer("ALTMSN", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                    }

                    UpdURLsAllDataIntoSqlServer(BCPFile_TempURLs, BCPFile_URLs, BCPFmt_TempURLs, BCPFile_Error, BCPFmt_URLs, BCPServer, str_wc_import_SQL);

                    UpdDOBsAllDataIntoSqlServer(BCPFile_TempDOBs, BCPFile_DOBs, BCPFmt_TempDOBs, BCPFile_Error, BCPFmt_DOBs, BCPServer, str_wc_import_SQL);

                    UpdAddrAllDataIntoSqlServer(BCPFile_TempAddr, BCPFile_Addr, BCPFmt_TempAddr, BCPFile_Error, BCPFmt_Addr, BCPServer, str_wc_import_SQL);

                    UpdAttributeAllDataIntoSqlServer(BCPFile_TempNotes, BCPFile_Notes, BCPFmt_TempNotes, BCPFile_Error, BCPFmt_Notes, BCPServer, str_wc_import_SQL, wc_import_crime_SQL, NoteTypeList);
                }
                else
                {
                    string[] wc_selects = wc_table_select.Split(',');
                    foreach (string strType in wc_selects)
                    {
                        LogAdd("import Type:" + strType);
                        if (strType != "")
                        {
                            switch (strType.ToUpper())
                            {
                                case "ALT":
                                    if (BCP_SDNAltTable_Enable == "Y")
                                    {
                                        UpdSDNAltTableAllDataIntoSqlServer("ALT", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                                    }

                                    if (str_weakALT == "Y")
                                    {
                                        UpdSDNAltTableAllDataIntoSqlServer("ALTWEAK", str_weakALT_listtype, BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                                    }

                                    if (str_ALTSPELL == "Y")
                                    {
                                        UpdSDNAltTableAllDataIntoSqlServer("ALTSPELL", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                                    }

                                    if (str_ALTPASS == "Y")
                                    {
                                        UpdSDNAltTableAllDataIntoSqlServer("ALTPASS", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                                    }

                                    if (str_ALTBIC == "Y")
                                    {
                                        UpdSDNAltTableAllDataIntoSqlServer("ALTBIC", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                                    }

                                    if (str_ALTIMO == "Y")
                                    {
                                        UpdSDNAltTableAllDataIntoSqlServer("ALTIMO", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                                    }

                                    if (str_ALTARN == "Y")
                                    {
                                        UpdSDNAltTableAllDataIntoSqlServer("ALTARN", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                                    }

                                    if (str_ALTMSN == "Y")
                                    {
                                        UpdSDNAltTableAllDataIntoSqlServer("ALTMSN", "", BCPFile_TmpSDNAltTable, BCPFile_SDNAltTable, BCPFmt_TmpSDNAltTable, BCPFile_Error,
                                            BCPFmt_SDNAltTable, BCPServer, str_wc_import_SQL);
                                    }

                                    break;
                                case "RELATION":
                                    for (int i = 0; i < str_relatedcolumn.Split('|').Length; i++)
                                    {
                                        UpdRelationshipAllDataIntoSqlServer(str_relatedcolumn.Split('|')[i], BCPFile_TempRelationship, BCPFile_Relationship, BCPFmt_TempRelationship,
                                            BCPFile_Error, BCPFmt_Relationship, BCPServer, BaseNum, str_wc_import_SQL);
                                    }
                    
                                    break;
                                case "DOB":
                                    UpdDOBsAllDataIntoSqlServer(BCPFile_TempDOBs, BCPFile_DOBs, BCPFmt_TempDOBs, BCPFile_Error, BCPFmt_DOBs, BCPServer, str_wc_import_SQL);
                                    break;
                                case "URL":
                                    UpdURLsAllDataIntoSqlServer(BCPFile_TempURLs, BCPFile_URLs, BCPFmt_TempURLs, BCPFile_Error, BCPFmt_URLs, BCPServer, str_wc_import_SQL);
                                    break;
                                case "ADDR":
                                    UpdAddrAllDataIntoSqlServer(BCPFile_TempAddr, BCPFile_Addr, BCPFmt_TempAddr, BCPFile_Error, BCPFmt_Addr, BCPServer, str_wc_import_SQL);
                                    break;
                                case "NOTES":
                                    UpdAttributeAllDataIntoSqlServer(BCPFile_TempNotes, BCPFile_Notes, BCPFmt_TempNotes, BCPFile_Error, BCPFmt_Notes, BCPServer, str_wc_import_SQL, wc_import_crime_SQL, NoteTypeList);
                                    break;
                            }
                        }
                        
                    }
                }

                LogAdd(string.Format("***** Complete WorldCheckAdd All Job [{0}] *****", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                //string b = BuildFileImg();

                //LogAdd(b);
            }
            catch (Exception ex)
            {
                LogException(ex, "WorldCheckAddAllJob Execute");
            }
        }

        public void UpdRelationshipAllDataIntoSqlServer(string str_column_name, string BCPFile_TempRelationship, string BCPFile_Relationship, string BCPFmt_TempRelationship,
            string BCPFile_Error, string BCPFmt_Relationship, string BCPServer, string BaseNum, string wc_import_SQL)
        {
            try
            {
                LogAdd("[All] Expand to [" + str_column_name + "] -- Start");
                string query = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string LinkedTo = string.Empty;
                string RelationShipId = string.Empty;
                string EntNum = string.Empty;
                string RelatedID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                int RelationShipIdMax = 0;
                int File_TotalNum = 0;
                int File_FinalAddNum = 0;
                int finalLinkedToSplit = 0;

                DateTime NowDateTime = DateTime.Now;

                if (File.Exists(BCPFile_TempRelationship)) File.Delete(BCPFile_TempRelationship);

                if (File.Exists(BCPFile_Relationship)) File.Delete(BCPFile_Relationship);

                queryMax = string.Format(@" SELECT MAX(RelationshipId)+1 AS RelationshipIdMax FROM OFAC.dbo.Relationship ");

                //query = string.Format(@" SELECT Entnum,LinkedTo FROM OFAC.dbo.WorldCheck WHERE LinkedTo IS NOT NULL ");

                query = string.Format(" SELECT WC.Entnum, ");
                query += "WC." + str_column_name;
                query += @", CASE ISNULL(WC.DelFlag, '0') 
                                             WHEN '0' THEN '1' 
                                             WHEN '1' THEN '4' END AS Status 
                                         FROM OFAC.dbo.WorldCheck WC
                                         join SDNTable (nolock) sdn on wc.Entnum = sdn.EntNum ";
                query += "where WC." + str_column_name + " != '' ";
                query += " and sdn.Status != 4 ";

                if (!string.IsNullOrEmpty(wc_import_SQL) && wc_import_SQL.Trim().Length > 0)
                {
                    query += wc_import_SQL;
                }

                LogAdd("[All] Select " + str_column_name + " SQL=" + query);

                Database db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 500;

                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("RelationshipIdMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            RelationShipIdMax = Convert.ToInt32(dataReaderMax["RelationshipIdMax"]);
                            //LogAdd("RelationShipIdMax: " + RelationShipIdMax.ToString());
                        }
                    }
                }

                dbCommandMax.Dispose();
                int intBaseNum = Convert.ToInt32(BaseNum);

                using (StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TempRelationship, false, Encoding.GetEncoding(1252))) 
                {
                    using (IDataReader dataReader = db.ExecuteReader(dbCommand))
                    {
                        while (dataReader.Read())
                        {
                            EntNum = dataReader["Entnum"].ToString().Trim();
                            LinkedTo = dataReader[str_column_name].ToString().Trim();
                            string[] LinkedToSplit = dataReader[str_column_name].ToString().Trim().Split(';');
                            Status = dataReader["Status"].ToString().Trim();
                            int i;
                            for (i = 0; i < LinkedToSplit.Length; checked(i)++)
                            {
                                int linkedNumber = 0;
                                if (LinkedToSplit[i].Trim() != "" && int.TryParse(LinkedToSplit[i].Trim(), out linkedNumber))
                                {

                                    finalLinkedToSplit = linkedNumber + intBaseNum;
                                    StringBuilder builder = new StringBuilder();
                                    builder.AppendFormat("{0}\t", RelationShipIdMax.ToString());
                                    builder.AppendFormat("{0}\t", EntNum);
                                    builder.AppendFormat("{0}\t", finalLinkedToSplit.ToString());
                                    builder.AppendFormat("{0}", Status);
                                    TempfileDWriter.WriteLine(builder.ToString());
                                    RelationShipIdMax++;
                                    File_TotalNum++;
                                }
                            }
                        }
                        dataReader.Close();
                    }

                    dbCommand.Dispose();
                    TempfileDWriter.Close();
                }

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TempRelationship ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                dbCommandDel.Dispose();
                Process p = new Process();
                p.StartInfo.FileName = "bcp.exe";

                p.StartInfo.Arguments = "OFAC.dbo.TempRelationship in " + BCPFile_TempRelationship + "  -f " + BCPFmt_TempRelationship + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                LogAdd("[All] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();

                
                using (StreamWriter fileDWriter = new StreamWriter(BCPFile_Relationship, false, Encoding.GetEncoding(1252)))
                {
                    string queryAdd =
                    string.Format(@" SELECT DISTINCT RelationshipId, EntNum, RelatedID, Status FROM OFAC.dbo.TempRelationship
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.Relationship 
                                        WHERE TempRelationship.EntNum = EntNum AND TempRelationship.RelatedID = RelatedID
                                         ) and entnum in (select entnum from sdntable(nolock))
                                     ORDER BY RelationshipId, EntNum, RelatedID ");

                    DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                    dbCommandAdd.CommandTimeout = 500;
                    using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                    {
                        while (Tempdr.Read())
                        {
                            RelationShipId = Tempdr["RelationShipId"].ToString().Trim();
                            EntNum = Tempdr["EntNum"].ToString().Trim();
                            RelatedID = Tempdr["RelatedID"].ToString().Trim();
                            Status = Tempdr["Status"].ToString().Trim();
                            StringBuilder builder = new StringBuilder();
                            builder.AppendFormat("{0}\t", RelationShipId);
                            builder.AppendFormat("{0}\t", EntNum);
                            builder.AppendFormat("{0}\t", RelatedID);
                            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}", str_column_name, str_column_name, Status, NowDateTime.ToString(), NowDateTime.ToString(), "Prime", NowDateTime.ToString(), "Prime", NowDateTime.ToString());

                            fileDWriter.WriteLine(builder.ToString());
                            File_FinalAddNum++;
                        }
                        Tempdr.Close();
                    }

                    dbCommandAdd.Dispose();

                    fileDWriter.Close();
                }

                p = new Process();
                p.StartInfo.FileName = "bcp.exe";

                p.StartInfo.Arguments = "OFAC.dbo.RelationShip in " + BCPFile_Relationship + "  -f " + BCPFmt_Relationship + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                LogAdd("[All] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();

                LogAdd("[All] Temp" + str_column_name + " - Import: [" + File_TotalNum + "]; " + str_column_name + " - Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[All] Expand to " + str_column_name + " -- Completion");

                OFACEventLog(str_column_name + " total insert : " + File_FinalAddNum.ToString());
            }
            catch (Exception ex)
            {
                LogException(ex, "UpdRelationshipAllDataIntoSqlServer");
            }
        }

        public void UpdSDNAltTableAllDataIntoSqlServer(string strtype, string str_sanctionlist, string BCPFile_TmpSDNAltTable, string BCPFile_SDNAltTable, string BCPFmt_TmpSDNAltTable,
            string BCPFile_Error, string BCPFmt_SDNAltTable, string BCPServer, string wc_import_SQL)
        {
            try
            {
                LogAdd("[All] Expand to " + strtype + " -- Start");

                string query = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                string Aliases = string.Empty;
                string AltNum = string.Empty;
                string EntNum = string.Empty;
                string AltType = string.Empty;
                string AltName = string.Empty;
                string UID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");
                string str_type = string.Empty;
                string FirstName = string.Empty;
                string LastName = string.Empty;

                int AltNumMax = 0;
                int File_TotalNum = 0;
                int File_FinalAddNum = 0;
                int rowCount = 0;

                DateTime NowDateTime = DateTime.Now;

                if (File.Exists(BCPFile_TmpSDNAltTable)) File.Delete(BCPFile_TmpSDNAltTable);

                if (File.Exists(BCPFile_SDNAltTable)) File.Delete(BCPFile_SDNAltTable);

                queryMax = string.Format(@" SELECT MAX(AltNum)+1 AS AltNumMax FROM OFAC.dbo.SDNAltTable ");

                //query = string.Format(@" SELECT Entnum, Aliases FROM OFAC.dbo.WorldCheck WHERE Aliases IS NOT NULL ");

                query = @" SELECT WC.Entnum, ";
                if (strtype == "ALT")
                {
                    query += "WC.Aliases, ";
                }
                else if (strtype == "ALTWEAK")
                {
                    query += "WC.LowQAliases, ";
                }
                else if (strtype == "ALTSPELL")
                {
                    query += "WC.AlterSpell, ";
                }
                else if (strtype == "ALTPASS")
                {
                    query += "WC.Passports, ";
                }
                else if (strtype == "ALTBIC" || strtype == "ALTIMO" || strtype == "ALTARN" || strtype == "ALTMSN")
                {
                    query += "WC.IDENTIFICATION_NUMBERS, ";
                }

                query += @"CASE ISNULL(WC.DelFlag, '0') 
                                            WHEN '0' THEN '1' 
                                            WHEN '1' THEN '4' END AS Status ,isnull(wc.[type],'') as type
                                          FROM OFAC.dbo.WorldCheck WC
                                          join SDNTable (nolock) sdn on wc.Entnum = sdn.EntNum ";
                if (strtype == "ALT")
                {
                    query += "WHERE WC.Aliases != '' ";
                }
                else if (strtype == "ALTWEAK")
                {
                    query += @"WHERE WC.LowQAliases != '' ";
                    if (str_sanctionlist != "")
                    {
                        query += "  AND sdn.Program in (" + str_sanctionlist + ")";
                    }
                }
                else if (strtype == "ALTSPELL")
                {
                    query += "WHERE WC.AlterSpell != '' ";
                }
                else if (strtype == "ALTPASS")
                {
                    query += "WHERE WC.Passports != '' ";
                }
                else if (strtype == "ALTBIC" || strtype == "ALTIMO" || strtype == "ALTARN" || strtype == "ALTMSN")
                {
                    query += "WHERE WC.IDENTIFICATION_NUMBERS != '' ";
                    if (strtype == "ALTBIC")
                    {
                        query += "and WC.Category = 'BANK' ";
                    }
                }
                query += "and sdn.Status != 4 ";

                if (!string.IsNullOrEmpty(wc_import_SQL) && wc_import_SQL.Trim().Length > 0)
                {
                    query += wc_import_SQL;
                }

                LogAdd("[All] ALT Select SQL = " + query);

                Database db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 500;
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("AltNumMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            AltNumMax = Convert.ToInt32(dataReaderMax["AltNumMax"]);
                        }
                    }
                }
                dbCommandMax.Dispose();

                IDataReader dataReaderDT = db.ExecuteReader(dbCommand);
                DataTable dt = new DataTable();
                dt.Load(dataReaderDT);
                rowCount = dt.Rows.Count;
                dt.Dispose();
                dataReaderDT.Close();

                using (StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TmpSDNAltTable, false, Encoding.GetEncoding(1252))) 
                {
                    using (IDataReader dr = db.ExecuteReader(dbCommand))
                    {
                        while (dr.Read())
                        {
                            EntNum = dr["Entnum"].ToString().Trim();
                            Status = dr["Status"].ToString().Trim();
                            str_type = dr["type"].ToString().Trim();

                            try
                            {
                                string[] AliasesSplit = null;
                                if (strtype == "ALT")
                                {
                                    AliasesSplit = dr["Aliases"].ToString().Trim().Split(';');
                                }
                                else if (strtype == "ALTWEAK")
                                {
                                    AliasesSplit = dr["LowQAliases"].ToString().Trim().Split(';');
                                }
                                else if (strtype == "ALTSPELL")
                                {
                                    AliasesSplit = dr["AlterSpell"].ToString().Trim().Split(';');
                                }
                                else if (strtype == "ALTPASS")
                                {
                                    AliasesSplit = dr["Passports"].ToString().Trim().Split(';');
                                }
                                else if (strtype == "ALTBIC" || strtype == "ALTIMO" || strtype == "ALTARN" || strtype == "ALTMSN")
                                {
                                    AliasesSplit = dr["IDENTIFICATION_NUMBERS"].ToString().Trim().Split(';');
                                }

                                int i;

                                if (AliasesSplit != null)
                                {
                                    for (i = 0; i < AliasesSplit.Length; i++)
                                    {
                                        if (AliasesSplit[i] != "")
                                        {
                                            if ((strtype == "ALTBIC" && !AliasesSplit[i].Contains("{INT-BIC}") && !AliasesSplit[i].Contains("{INT-BIC6}")) || (strtype == "ALTIMO" && !AliasesSplit[i].Contains("INT-IMO")) || (strtype == "ALTARN" && !AliasesSplit[i].Contains("INT-ARN")) || (strtype == "ALTMSN" && !AliasesSplit[i].Contains("INT-MSN")))
                                                continue;
                                            StringBuilder builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", "");
                                            builder.AppendFormat("{0}\t", AliasesSplit[i].Replace("{INT-BIC}", "").Replace("{INT-BIC6}", "").Replace("{INT-IMO}", "").Replace("{INT-ARN}", "").Replace("{INT-MSN}", ""));
                                            if ((str_type == "M" || str_type == "F" || str_type == "I" || str_type == "U") && strtype != "ALTPASS")
                                            {
                                                //LogAdd("Process name=" + AliasesSplit[i]);
                                                //individual
                                                //1. 逗號前面是last name
                                                if (AliasesSplit[i].Split(',').Count() > 1)
                                                {
                                                    //LogAdd("comma count=" + (AliasesSplit[i].Split(',').Count()));
                                                    //firstname
                                                    builder.AppendFormat("{0}\t", AliasesSplit[i].Split(',')[1]);
                                                    //lastname
                                                    builder.AppendFormat("{0}\t", AliasesSplit[i].Split(',')[0]);
                                                }
                                                //2. 沒逗號>空白後面是last name
                                                else if (AliasesSplit[i].Split(' ').Count() > 1)
                                                {
                                                    //LogAdd("space count=" + (AliasesSplit[i].Split(' ').Count()));
                                                    //firstname
                                                    builder.AppendFormat("{0}\t", AliasesSplit[i].Split(' ')[0]);
                                                    //lastname
                                                    builder.AppendFormat("{0}\t", AliasesSplit[i].Split(' ')[1]);
                                                }
                                                else
                                                {
                                                    builder.AppendFormat("{0}\t", "");
                                                    builder.AppendFormat("{0}\t", AliasesSplit[i]);
                                                }

                                            }
                                            else if (str_type == "E")
                                            {
                                                //entity
                                                //全部放first name
                                                // 8/5 modify to last name
                                                builder.AppendFormat("{0}\t", "");
                                                builder.AppendFormat("{0}\t", "");
                                            }
                                            else if (strtype == "ALTPASS")
                                            {
                                                builder.AppendFormat("{0}\t", "");
                                                builder.AppendFormat("{0}\t", "");
                                            }
                                            builder.AppendFormat("{0}", Status);
                                            TempfileDWriter.WriteLine(builder.ToString());

                                            File_TotalNum++;
                                            //LogAdd("[Daily] Log for AltName:" + builder.ToString());
                                        }
                                    }

                                }
                            }
                            catch (Exception ex)
                            {
                                LogException(ex, string.Format("[All] Expand to {0}", strtype));
                            }
                        }
                        dr.Close();
                    }
                    TempfileDWriter.Close();
                    dbCommand.Dispose();
                }

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TmpSDNAltTable ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                dbCommandDel.Dispose();
                Process p = new Process();
                p.StartInfo.FileName = "bcp.exe";

                p.StartInfo.Arguments = "OFAC.dbo.TmpSDNAltTable in " + BCPFile_TmpSDNAltTable + "  -f " + BCPFmt_TmpSDNAltTable + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                LogAdd("[All] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();

                //2021 APR. add name parts data
                if (strtype == "ALTWEAK" || strtype == "ALT" || strtype == "ALTSPELL")
                {
                    string SQL = @"update SDNAltTable set FirstName = c.FirstName, LastName = c.LastName
                    from SDNAltTable a
                    join SDNTable b on a.EntNum = b.EntNum
                    join TmpSDNAltTable c on b.EntNum = c.EntNum and a.AltName = c.AltName
                    where b.Type = 'individual'
                    and b.EntNum > 20000000
                    and b.Status != 4
                    and(isnull(a.LastName, '') = '')
                    and isnull(a.ListID,'') in ('ALTSPELL', 'ALT', 'LOWQALIASES')
                    and isnull(a.LastName,'') != isnull(c.LastName,'')";

                    DbCommand dbCommandaddNameParts = db.GetSqlStringCommand(SQL);
                    dbCommandaddNameParts.CommandTimeout = 0;
                    db.ExecuteReader(dbCommandaddNameParts);
                    dbCommandaddNameParts.Dispose();

                }

                string queryAdd =
                        string.Format(@" SELECT DISTINCT EntNum, AltName, FirstName, LastName, '2' Status FROM OFAC.dbo.TmpSDNAltTable 
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.SDNAltTable WHERE TmpSDNAltTable.EntNum = EntNum AND TmpSDNAltTable.AltName = AltName 
                                                       and (status != 4 or deleted = 1)
                                     ");
                if (strtype == "ALTWEAK")
                {
                    queryAdd += "and listid = 'LOWQALIASES' ";
                }
                else if (strtype == "ALT")
                {
                    queryAdd += "and isnull(listid,'') in ('ALT','') ";
                }
                else if (strtype == "ALTSPELL")
                {
                    queryAdd += "and listid = 'ALTSPELL' ";
                }
                else if (strtype == "ALTPASS")
                {
                    queryAdd += "and listid = 'PASSPORT' ";
                }
                else if (strtype == "ALTBIC")
                {
                    queryAdd += "and listid = 'BIC' ";
                }
                else if (strtype == "ALTIMO")
                {
                    queryAdd += "and listid = 'IMO' ";
                }
                else if (strtype == "ALTARN")
                {
                    queryAdd += "and listid = 'ARN' ";
                }
                else if (strtype == "ALTMSN")
                {
                    queryAdd += "and listid = 'MSN' ";
                }

                queryAdd += ") ORDER BY EntNum, AltName ";

                using (StreamWriter fileDWriter = new StreamWriter(BCPFile_SDNAltTable, false, Encoding.GetEncoding(1252))) 
                {
                    DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                    dbCommandAdd.CommandTimeout = 0;
                    using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                    {
                        while (Tempdr.Read())
                        {
                            EntNum = Tempdr["EntNum"].ToString().Trim();
                            AltNum = AltNumMax.ToString();
                            AltName = Tempdr["AltName"].ToString().Trim();
                            FirstName = Tempdr["FirstName"].ToString().Trim();
                            LastName = Tempdr["LastName"].ToString().Trim();
                            Status = Tempdr["Status"].ToString().Trim();
                            StringBuilder builder = new StringBuilder();
                            builder.AppendFormat("{0}\t", EntNum);
                            builder.AppendFormat("{0}\t", AltNum);
                            if (strtype == "ALTSPELL")
                            {
                                builder.AppendFormat("{0}\t", "f.k.a.");
                            }
                            else
                            {
                                builder.AppendFormat("{0}\t", "a.k.a.");
                            }
                            builder.AppendFormat("{0}\t", AltName);
                            builder.AppendFormat("{0}\t", FirstName);
                            builder.AppendFormat("{0}\t", "");
                            builder.AppendFormat("{0}\t", LastName);

                            if (strtype == "ALTSPELL")
                            {
                                builder.AppendFormat("{0}\t", "ALTSPELL");
                            }
                            else if (strtype == "ALTPASS")
                            {
                                builder.AppendFormat("{0}\t", "PASSPORT");
                            }
                            else if (strtype == "ALTBIC")
                            {
                                builder.AppendFormat("{0}\t", "BIC");
                            }
                            else if (strtype == "ALTIMO")
                            {
                                builder.AppendFormat("{0}\t", "IMO");
                            }
                            else if (strtype == "ALTARN")
                            {
                                builder.AppendFormat("{0}\t", "ARN");
                            }
                            else if (strtype == "ALTMSN")
                            {
                                builder.AppendFormat("{0}\t", "MSN");
                            }
                            else if (strtype == "ALT")
                            {
                                builder.AppendFormat("{0}\t", "ALT");
                            }
                            else if (strtype == "ALTWEAK")
                            {
                                builder.AppendFormat("{0}\t", "LOWQALIASES");
                            }
                            else
                            {
                                builder.AppendFormat("{0}\t", "");
                            }

                            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}", "", "0", Status, NowDateTime.ToString(), NowDateTime.ToString(), NowDateTime.ToString(), NowDateTime.ToString(), "Prime", strtype == "ALTWEAK" ? "weak" : "");
                            fileDWriter.WriteLine(builder.ToString());
                            AltNumMax++;
                            File_FinalAddNum++;
                        }
                        Tempdr.Close();
                    }

                    fileDWriter.Close();
                    dbCommandAdd.Dispose();
                }

                p = new Process();
                p.StartInfo.FileName = "bcp.exe";

                p.StartInfo.Arguments = "OFAC.dbo.SDNAltTable in " + BCPFile_SDNAltTable + "  -f " + BCPFmt_SDNAltTable + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                LogAdd("[All] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();

                LogAdd("[All] TmpSDNAltTable - Import: [" + File_TotalNum + "]; " + strtype + " - Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[All] Expand to " + strtype + " -- Completion");

                OFACEventLog(strtype + " total insert : " + File_FinalAddNum.ToString());
                
                System.Threading.Thread.Sleep(5000);

                //更新SDNAltTable AltType
                /*
                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    using (var sqlCommand = new SqlCommand("", sqlConnection))
                    {
                        LogAdd("[All] Update SDNAltTable AltType --  Start.");

                        sqlCommand.CommandText = @"
                          update OFAC.dbo.SDNAltTable set AltType = 'a.k.a.' from OFAC.dbo.WorldCheck(nolock) wc
                            inner join OFAC.dbo.SDNAltTable(nolock) b on wc.Entnum = b.EntNum
                            inner join OFAC.dbo.SDNTable(nolock) c on wc.Entnum = c.EntNum
                            where isnull(wc.DelFlag,0) = 0 
                            and c.Status != 4
                            and b.AltType != 'a.k.a.';
                          ";
                        sqlCommand.CommandTimeout = 500;
                        int UpdateCount = sqlCommand.ExecuteNonQuery();
                        //顯示更新筆數
                        Console.WriteLine("Update:{0}", UpdateCount);
                        LogAdd("[All] Update SDNAltTable AltType --  Completed. Count : " + UpdateCount);
                        OFACEventLog("ALT_AltType totoal update:" + UpdateCount);
                    }
                    sqlConnection.Dispose();
                }
                */
            }
            catch (Exception ex)
            {
                LogException(ex, "UpdSDNAltTableAllDataIntoSqlServer");
            }
        }

        public void UpdURLsAllDataIntoSqlServer(string BCPFile_TempURLs, string BCPFile_URLs, string BCPFmt_TempURLs, string BCPFile_Error, 
            string BCPFmt_URLs, string BCPServer, string wc_import_SQL)
        {
            try
            {
                LogAdd("[All] Expand to URLs -- Start");

                string query = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                string Aliases = string.Empty;
                string URLsID = string.Empty;
                string EntNum = string.Empty;
                string AltType = string.Empty;
                string URL = string.Empty;
                string Description = string.Empty;
                string UID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                int URLsIDMax = 0;
                int File_TotalNum = 0;
                int File_FinalAddNum = 0;

                DateTime NowDateTime = DateTime.Now;

                Database db;

                if (File.Exists(BCPFile_TempURLs)) File.Delete(BCPFile_TempURLs);

                if (File.Exists(BCPFile_URLs)) File.Delete(BCPFile_URLs);

                queryMax = string.Format(@" SELECT MAX(URLsID)+1 AS URLsIDMax FROM OFAC.dbo.URLs ");

                //query = string.Format(@" SELECT Entnum, ExSources FROM OFAC.dbo.WorldCheck WHERE ExSources IS NOT NULL ");

                query = string.Format(@" SELECT WC.Entnum, WC.ExSources,  CASE ISNULL(WC.DelFlag, '0') 
                                        WHEN '0' THEN '1' 
                                        WHEN '1' THEN '4' END AS Status 
                                        FROM OFAC.dbo.WorldCheck WC
                                        join SDNTable (nolock) sdn on wc.Entnum = sdn.EntNum
                                        WHERE WC.ExSources != ''  
                                        and sdn.Status != 4 
                                    ");

                if (!string.IsNullOrEmpty(wc_import_SQL) && wc_import_SQL.Trim().Length > 0)
                {
                    query += wc_import_SQL;
                }

                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("URLsIDMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            URLsIDMax = Convert.ToInt32(dataReaderMax["URLsIDMax"]);
                        }
                    }
                }
                dbCommandMax.Dispose();
                
                using(StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TempURLs, false, Encoding.GetEncoding(28591)))
                {
                    using (IDataReader dr = db.ExecuteReader(dbCommand))
                    {
                        while (dr.Read())
                        {
                            EntNum = dr["Entnum"].ToString().Trim();
                            string[] ExSourcesSplit = dr["ExSources"].ToString().Trim().Split(' ');
                            Status = dr["Status"].ToString().Trim();
                            int i;
                            for (i = 0; i < ExSourcesSplit.Length; i++)
                            {
                                if (ExSourcesSplit[i] != "")
                                {
                                    StringBuilder builder = new StringBuilder();
                                    builder.AppendFormat("{0}\t", URLsIDMax.ToString());
                                    builder.AppendFormat("{0}\t", EntNum);
                                    builder.AppendFormat("{0}\t", ExSourcesSplit[i].Length > 1000 ? ExSourcesSplit[i].Substring(0, 1000) : ExSourcesSplit[i]);
                                    builder.AppendFormat("{0}", Status);
                                    TempfileDWriter.WriteLine(builder.ToString());
                                    URLsIDMax++;
                                    File_TotalNum++;
                                }
                            }
                        }
                        dr.Close();
                    }

                    dbCommand.Dispose();
                    TempfileDWriter.Close();
                }
                
                queryDel = @" TRUNCATE TABLE OFAC.dbo.TempURLs ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                dbCommandDel.Dispose();
                Process p = new Process();
                p.StartInfo.FileName = "bcp.exe";

                p.StartInfo.Arguments = "OFAC.dbo.TempURLs in " + BCPFile_TempURLs + "  -f " + BCPFmt_TempURLs + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                LogAdd("[All] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();

                using (StreamWriter fileDWriter = new StreamWriter(BCPFile_URLs, false, Encoding.GetEncoding(28591)))
                {
                    string queryAdd =
                        string.Format(@" SELECT DISTINCT URLsID, EntNum, URL, Status FROM OFAC.dbo.TempURLs 
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.URLs WHERE TempURLs.EntNum = EntNum AND len(TempURLs.URL) = len(URL)) 
                                     ORDER BY URLsID, EntNum, URL ");

                    DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                    dbCommandAdd.CommandTimeout = 0;
                    using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                    {
                        while (Tempdr.Read())
                        {
                            URLsID = Tempdr["URLsID"].ToString().Trim();
                            EntNum = Tempdr["EntNum"].ToString().Trim();
                            URL = Tempdr["URL"].ToString().Trim();
                            Description = Tempdr["URL"].ToString().Trim();
                            Status = Tempdr["Status"].ToString().Trim();
                            StringBuilder builder = new StringBuilder();
                            builder.AppendFormat("{0}\t", URLsID);
                            builder.AppendFormat("{0}\t", EntNum);
                            builder.AppendFormat("{0}\t", URL);
                            builder.AppendFormat("{0}\t", Description);
                            builder.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}", Status, NowDateTime.ToString(), NowDateTime.ToString(), "Prime", NowDateTime.ToString(), "Prime", NowDateTime.ToString());
                            fileDWriter.WriteLine(builder.ToString());
                            File_FinalAddNum++;
                        }
                        Tempdr.Close();
                    }
                    dbCommandAdd.Dispose();
                    fileDWriter.Close();
                }    

                p = new Process();
                p.StartInfo.FileName = "bcp.exe";

                p.StartInfo.Arguments = "OFAC.dbo.URLs in " + BCPFile_URLs + "  -f " + BCPFmt_URLs + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                LogAdd("[All] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();

                LogAdd("[All] TempURLs - Import: [" + File_TotalNum + "]; URLs - Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[All] Expand to URLs -- Completion");

                OFACEventLog("URLs total insert : " + File_FinalAddNum.ToString());
            }
            catch (Exception ex)
            {
                LogException(ex, "UpdURLsAllDataIntoSqlServer");
            }
        }

        public void UpdDOBsAllDataIntoSqlServer(string BCPFile_TempDOBs, string BCPFile_DOBs, string BCPFmt_TempDOBs, string BCPFile_Error,
            string BCPFmt_DOBs, string BCPServer, string wc_import_SQL)
        {
            try
            {
                LogAdd("[All] Expand to DOBs -- Start");

                string query = string.Empty;
                string queryLog1 = string.Empty;
                string queryLog2 = string.Empty;
                string LogContent1 = string.Empty;
                string LogContent2 = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;

                string URLsID = string.Empty;
                string EntNum = string.Empty;

                string DOB = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                string Entered = string.Empty;
                string Updated = string.Empty;

                int DOBsIdMax = 0;
                int File_TotalNum = 0;
                int File_FinalAddNum = 0;

                DateTime NowDateTime = DateTime.Now;

                Database db;

                if (File.Exists(BCPFile_TempDOBs)) File.Delete(BCPFile_TempDOBs);

                if (File.Exists(BCPFile_DOBs)) File.Delete(BCPFile_DOBs);

                queryMax = string.Format(@" SELECT MAX(DOBsID)+1 AS DOBsIdMax FROM OFAC.dbo.DOBs;");

                query = string.Format(@" 
                                    select wc.EntNum, isnull(wc.DOB,'') as DOB,
                                     CASE ISNULL(WC.DelFlag, '0') 
                                     WHEN '0' THEN '1' 
                                     WHEN '1' THEN '4' END AS Status,wc.Entered ,wc.Updated,
                                     isnull(wc.DOBs,'') as DOBs
                                    from worldcheck wc
                                    join SDNTable (nolock) sdn on wc.Entnum = sdn.EntNum
                                    where isnull(wc.DOBs,'') != '' 
                                    and sdn.Status != 4 
                                    ");

                if (!string.IsNullOrEmpty(wc_import_SQL) && wc_import_SQL.Trim().Length > 0)
                {
                    query += wc_import_SQL;
                }

                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("DOBsIdMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            DOBsIdMax = Convert.ToInt32(dataReaderMax["DOBsIdMax"]);
                        }
                    }
                }
                dbCommandMax.Dispose();

                using(StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TempDOBs, false, Encoding.GetEncoding(28591)))
                {
                    using (IDataReader dr = db.ExecuteReader(dbCommand))
                    {
                        while (dr.Read())
                        {
                            EntNum = dr["Entnum"].ToString().Trim();
                            string[] dob = dr["dob"].ToString().Trim().Split(';');
                            Status = dr["Status"].ToString().Trim();
                            Entered = dr["Entered"].ToString().Trim();
                            Updated = dr["Updated"].ToString().Trim();
                            int i;

                            //202008 add for new column DOBS
                            try
                            {
                                if (dr["dobs"].ToString().Trim() != "")
                                {
                                    dob = dr["dobs"].ToString().Trim().Split(';');
                                }
                            }
                            catch (Exception ex)
                            {
                                LogException(ex, "dobs Split");
                                dob = dr["dob"].ToString().Trim().Split(';');
                            }

                            for (i = 0; i < dob.Length; i++)
                            {
                                if (dob[i] != "")
                                {
                                    StringBuilder builder = new StringBuilder();
                                    builder.AppendFormat("{0}\t", DOBsIdMax.ToString());
                                    builder.AppendFormat("{0}\t", EntNum);
                                    builder.AppendFormat("{0}\t", dob[i].Substring(5, 2) + dob[i].Substring(8, 2) + dob[i].Substring(0, 4));
                                    builder.AppendFormat("{0}\t", Status);
                                    builder.AppendFormat("{0}\t", Entered);
                                    builder.AppendFormat("{0}\t", Updated);
                                    builder.AppendFormat("{0}\t", "Prime");
                                    builder.AppendFormat("{0}\t", NowTime);
                                    builder.AppendFormat("{0}\t", "Prime");
                                    builder.AppendFormat("{0}", NowTime);
                                    TempfileDWriter.WriteLine(builder.ToString());
                                    DOBsIdMax++;
                                    File_TotalNum++;
                                }
                            }
                        }
                        dr.Close();
                    }
                    dbCommand.Dispose();
                    TempfileDWriter.Close();
                }

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TempLoadDOBs ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                dbCommandDel.Dispose();

                Process p = new Process();
                p.StartInfo.FileName = "bcp.exe";

                p.StartInfo.Arguments = "OFAC.dbo.TempLoadDOBs in " + BCPFile_TempDOBs + "  -f " + BCPFmt_TempDOBs + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                LogAdd("[All] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();

                using(StreamWriter fileDWriter = new StreamWriter(BCPFile_DOBs, false, Encoding.GetEncoding(28591)))
                {
                    string queryAdd =
                    string.Format(@" SELECT DISTINCT DOBsID, EntNum, DOB,Status, ListCreateDate,ListModifDate FROM OFAC.dbo.TempLoadDOBs 
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.DOBs WHERE TempLoadDOBs.EntNum = EntNum AND TempLoadDOBs.DOB = DOB) 
                                     ORDER BY DOBsID, EntNum ");

                    DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                    dbCommandAdd.CommandTimeout = 0;
                    using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                    {
                        while (Tempdr.Read())
                        {
                            URLsID = Tempdr["DOBsID"].ToString().Trim();
                            EntNum = Tempdr["EntNum"].ToString().Trim();
                            DOB = Tempdr["DOB"].ToString().Trim();
                            Status = Tempdr["Status"].ToString().Trim();
                            Entered = Tempdr["ListCreateDate"].ToString().Trim();
                            Updated = Tempdr["ListModifDate"].ToString().Trim();
                            StringBuilder builder = new StringBuilder();
                            builder.AppendFormat("{0}\t", URLsID);
                            builder.AppendFormat("{0}\t", EntNum);
                            builder.AppendFormat("{0}\t", DOB);
                            builder.AppendFormat("{0}\t", Status);
                            builder.AppendFormat("{0}\t", Entered);
                            builder.AppendFormat("{0}\t", Updated);
                            builder.AppendFormat("{0}\t{1}\t{2}\t{3}", "Prime", NowDateTime.ToString(), "Prime", NowDateTime.ToString());
                            fileDWriter.WriteLine(builder.ToString());
                            File_FinalAddNum++;
                        }
                        Tempdr.Close();
                    }
                    dbCommandAdd.Dispose();
                    fileDWriter.Close();
                }

                p = new Process();
                p.StartInfo.FileName = "bcp.exe";

                p.StartInfo.Arguments = "OFAC.dbo.DOBs in " + BCPFile_DOBs + "  -f " + BCPFmt_DOBs + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                LogAdd("[All] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();

                LogAdd("[All] TempDOBs - Import: [" + File_TotalNum + "]; DOBs - Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[All] Expand to DOBs -- Completion");

                OFACEventLog("DOBs total insert : " + File_FinalAddNum.ToString());

                //2020.11 update dobs data in SDNTable dob column.
                string query_dobs =
                    @"select EntNum,count(DOB) from OFAC.dbo.DOBs where EntNum > 20000000 and Status != 4
                group by EntNum
                having count(DOB) > 1;";
                DbCommand dbCommanddobs = db.GetSqlStringCommand(query_dobs);
                dbCommanddobs.CommandTimeout = 0;
                using (IDataReader Tempdobsdr = db.ExecuteReader(dbCommanddobs))
                {
                    while (Tempdobsdr.Read())
                    {
                        string DOBS = string.Empty;
                        EntNum = Tempdobsdr["EntNum"].ToString().Trim();
                        query_dobs = @"select dob from OFAC.dbo.DOBs where entnum = @ent and Status != 4;";
                        
                        DbCommand dbCommanddobs_detail = db.GetSqlStringCommand(query_dobs);
                        dbCommanddobs_detail.Parameters.Add(new SqlParameter("@ent", EntNum));
                        dbCommanddobs_detail.CommandTimeout = 0;
                        using (IDataReader Tempdobsdetaildr = db.ExecuteReader(dbCommanddobs_detail))
                        {
                            
                            while (Tempdobsdetaildr.Read())
                            {
                                DOB = Tempdobsdetaildr["dob"].ToString().Trim();
                                DOBS += DOB + ",";
                            }
                            DOBS = DOBS.TrimEnd(',');
                        }
                        //LogAdd("Debug: ent = " + EntNum + ", Dobs = " + DOBS);
                        //Mar.09 2023 modify the sperate character to comma
                        if (DOBS.Replace(",", "").Length > 0)
                        {
                            string upd_sdn = "update OFAC.dbo.SDNTable set dob = @dobs, LastModifDate = getdate() where entnum = @ent and DOB != @dobs;";
                            
                            using (var sqlConnection = new SqlConnection(connectionString))
                            {
                                sqlConnection.Open();

                                using (var cmd = new SqlCommand(upd_sdn, sqlConnection))
                                {
                                    cmd.Parameters.Add(new SqlParameter("@dobs", DOBS.Length > 143 ? DOBS.Substring(0, 43) : DOBS));
                                    cmd.Parameters.Add(new SqlParameter("@ent", EntNum));
                                    cmd.ExecuteNonQuery();
                                    cmd.Dispose();
                                }

                                sqlConnection.Close();
                                SqlConnection.ClearAllPools();
                            }

                        }
                        dbCommanddobs_detail.Dispose();

                    }
                }
                dbCommanddobs.Dispose();

            }
            catch (Exception ex)
            {
                LogException(ex, "UpdDOBsAllDataIntoSqlServer");
            }
        }

        public void UpdAddrAllDataIntoSqlServer(string BCPFile_TempAddr, string BCPFile_Addr, string BCPFmt_TempAddr, string BCPFile_Error,
            string BCPFmt_Addr, string BCPServer, string wc_import_SQL)
        {
            try
            {
                LogAdd("[All] Expand to Addr -- Start");

                string query = string.Empty;
                
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;
                
                string EntNum = string.Empty;
                
                
                string UID = string.Empty;
                string Status = string.Empty;
                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                string address = String.Empty;
                string address2 = String.Empty;
                string address3 = String.Empty;
                string address4 = String.Empty;
                string city = String.Empty;
                string state = String.Empty;
                string country = String.Empty;

                string Entered = String.Empty;
                string Updated = String.Empty;
                string createdate = String.Empty;
                string modifdate = String.Empty;

                int AddrIDMax = 0;
                int File_TotalNum = 0;
                int File_FinalAddNum = 0;

                DateTime NowDateTime = DateTime.Now;

                Database db;

                if (File.Exists(BCPFile_TempAddr)) File.Delete(BCPFile_TempAddr);

                
                if (File.Exists(BCPFile_Addr)) File.Delete(BCPFile_Addr);

                queryMax = string.Format(@" SELECT MAX(AddrNum)+1 AS addrIDMax FROM OFAC.dbo.SDNAddrTable ");

                //query = string.Format(@" SELECT Entnum, ExSources FROM OFAC.dbo.WorldCheck WHERE ExSources IS NOT NULL ");
                //Feb.2, 2023 add the country field sdnaddr table
                query = string.Format(@" SELECT WC.Entnum, WC.Location,  CASE ISNULL(WC.DelFlag, '0') 
                                        WHEN '0' THEN '1' 
                                        WHEN '1' THEN '4' END AS Status, wc.Entered, wc.Updated, sdn.CreateDate, sdn.LastModifDate,
                                        wc.country, 
                                        (select Customer_country from OFAC..WorldCheckCountry where worldcheck_country = wc.country) customer_country
                                        FROM OFAC.dbo.WorldCheck WC
                                        join SDNTable (nolock) sdn on wc.Entnum = sdn.EntNum
                                        WHERE WC.Location != ''  
                                        and sdn.Status != 4 
                                    ");

                if (!string.IsNullOrEmpty(wc_import_SQL) && wc_import_SQL.Trim().Length > 0)
                {
                    query += wc_import_SQL;
                }

                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("addrIDMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            AddrIDMax = Convert.ToInt32(dataReaderMax["addrIDMax"]);
                        }
                    }
                }
                dbCommandMax.Dispose();

                using (StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TempAddr, false, Encoding.GetEncoding(28591)))
                {
                    using (IDataReader dr = db.ExecuteReader(dbCommand))
                    {
                        while (dr.Read())
                        {
                            EntNum = dr["Entnum"].ToString().Trim();
                            string[] A_location_count = dr["Location"].ToString().Trim().Split(';');
                            Status = dr["Status"].ToString().Trim();
                            Entered = dr["Entered"].ToString().Trim();
                            Updated = dr["Updated"].ToString().Trim();
                            createdate = dr["CreateDate"].ToString().Trim();
                            modifdate = dr["LastModifDate"].ToString().Trim();
                            string primecountry = dr["customer_country"].ToString().Trim();
                            country = dr["country"].ToString().Trim();
                            //Feb.7, 2023 avoid dupilicate country
                            string tempcountry = country;

                            StringBuilder builder = new StringBuilder();
                            //Feb.6, 2023 Polly request to add both country data into the sdnaddr table
                            //customer country
                            if (!string.IsNullOrEmpty(primecountry) && primecountry.ToUpper().Trim() != "UNKNOWN")
                            {
                                builder.AppendFormat("{0}\t", EntNum);
                                builder.AppendFormat("{0}\t", AddrIDMax.ToString());
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", primecountry);
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "0");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", Status);
                                builder.AppendFormat("{0}\t", Entered);
                                builder.AppendFormat("{0}\t", Updated);
                                builder.AppendFormat("{0}\t", createdate);
                                builder.AppendFormat("{0}\t", modifdate);
                                builder.AppendFormat("{0}", "Primeadmin");
                                TempfileDWriter.WriteLine(builder.ToString());
                                AddrIDMax++;
                                File_TotalNum++;
                            }

                            //Feb.6, 2023 Polly request to add both country data into the sdnaddr table
                            //worldcheck country
                            if (!string.IsNullOrEmpty(country) && country.ToUpper().Trim() != "UNKNOWN" && primecountry.ToUpper().Trim() != country.ToUpper().Trim())
                            {
                                builder = new StringBuilder();
                                builder.AppendFormat("{0}\t", EntNum);
                                builder.AppendFormat("{0}\t", AddrIDMax.ToString());
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", country);
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "0");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", Status);
                                builder.AppendFormat("{0}\t", Entered);
                                builder.AppendFormat("{0}\t", Updated);
                                builder.AppendFormat("{0}\t", createdate);
                                builder.AppendFormat("{0}\t", modifdate);
                                builder.AppendFormat("{0}", "Primeadmin");
                                TempfileDWriter.WriteLine(builder.ToString());
                                AddrIDMax++;
                                File_TotalNum++;
                            }

                            int i;
                            for (i = 0; i < A_location_count.Length; i++)
                            {
                                //splite by ~ for address city,state country
                                string[] A_location_type = A_location_count[i].Split('~');
                                string[] A_location_city_state = new string[2];
                                for (int j = 0; j < A_location_type.Length; j++)
                                {
                                    switch (j)
                                    {
                                        case 0:

                                            address4 = A_location_type[j].Trim().Length > 200 ? A_location_type[j].Trim().Substring(200, A_location_type[j].Trim().Length > 250 ? 50 : A_location_type[j].Trim().Length - 200) : "";
                                            address3 = A_location_type[j].Trim().Length > 150 ? A_location_type[j].Trim().Substring(150, A_location_type[j].Trim().Length > 200 ? 50 : A_location_type[j].Trim().Length - 150) : "";
                                            address2 = A_location_type[j].Trim().Length > 100 ? A_location_type[j].Trim().Substring(100, A_location_type[j].Trim().Length > 150 ? 50 : A_location_type[j].Trim().Length - 100) : "";
                                            address = A_location_type[j].Trim().Length > 100 ? A_location_type[j].Trim().Substring(0, 100) : A_location_type[j].Trim();
                                            break;

                                        case 1:

                                            //splite by , for city state
                                            A_location_city_state = A_location_type[j].Split(',');
                                            city = A_location_city_state[0].Trim().Length > 50 ? A_location_city_state[0].Trim().Substring(0, 50) : A_location_city_state[0].Trim();
                                            state = A_location_city_state[1].Trim().Length > 50 ? A_location_city_state[1].Trim().Substring(0, 50) : A_location_city_state[1].Trim();

                                            break;
                                        case 2:
                                            country = A_location_type[j].Trim().Length > 100 ? A_location_type[j].Trim().Substring(0, 100) : A_location_type[j].Trim();
                                            primecountry = country;
                                            break;
                                        default:

                                            break;
                                    }
                                }

                                //Feb.7, 2023 add to avoid dupilicate country
                                if ((address.Trim() == "") && (city.Trim() == "") && (state.Trim() == "") && (country.Trim() == "") || ((tempcountry == country) && (address.Trim() == "") && (city.Trim() == "") && (state.Trim() == "")))
                                {
                                    continue;
                                }
                                //AddrNum,EntNum,Address,City,State,Country,deleted,status,ListCreatedate,ListModifDate,CreateDate,LastModifDate,LastOper

                                builder = new StringBuilder();
                                builder.AppendFormat("{0}\t", EntNum);
                                builder.AppendFormat("{0}\t", AddrIDMax.ToString());
                                builder.AppendFormat("{0}\t", address);
                                builder.AppendFormat("{0}\t", city);
                                builder.AppendFormat("{0}\t", country);
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "0");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", address2);
                                builder.AppendFormat("{0}\t", address3);
                                builder.AppendFormat("{0}\t", address4);
                                builder.AppendFormat("{0}\t", state);
                                builder.AppendFormat("{0}\t", "");
                                builder.AppendFormat("{0}\t", Status);
                                builder.AppendFormat("{0}\t", Entered);
                                builder.AppendFormat("{0}\t", Updated);
                                builder.AppendFormat("{0}\t", createdate);
                                builder.AppendFormat("{0}\t", modifdate);
                                builder.AppendFormat("{0}", "Primeadmin");
                                TempfileDWriter.WriteLine(builder.ToString());
                                AddrIDMax++;
                                File_TotalNum++;

                            }
                        }
                        dr.Close();
                    }
                    dbCommand.Dispose();
                    TempfileDWriter.Close();
                }
                

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TmpAddrTable ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                dbCommandDel.Dispose();
                Process p = new Process();
                p.StartInfo.FileName = "bcp.exe";

                p.StartInfo.Arguments = "OFAC.dbo.TmpAddrTable in " + BCPFile_TempAddr + "  -f " + BCPFmt_TempAddr + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                LogAdd("[All] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();

                string queryAdd =
                    string.Format(@"select distinct * from (
                      select AddrNum,EntNum,Address,Address2,Address3,Address4,City,State,(select Customer_Country from WorldCheckCountry where WorldCheck_Country = TmpAddrTable.country) Country,Status,ListCreatedate,ListModifDate,CreateDate,LastModifDate,LastOper
                      from OFAC.dbo.TmpAddrTable where not exists 
                      (select * from OFAC.dbo.SDNAddrTable 
                        where OFAC.dbo.SDNAddrTable.EntNum = OFAC.dbo.TmpAddrTable.EntNum
                        and isnull(OFAC.dbo.SDNAddrTable.Address,'') = isnull(OFAC.dbo.TmpAddrTable.Address, '')
                        and isnull(OFAC.dbo.SDNAddrTable.City,'') = isnull(OFAC.dbo.TmpAddrTable.City,'')
                        and isnull(OFAC.dbo.SDNAddrTable.State,'') = isnull(OFAC.dbo.TmpAddrTable.State,'')
                        and isnull(OFAC.dbo.SDNAddrTable.Country,'') = isnull((select Customer_Country from WorldCheckCountry where WorldCheck_Country = TmpAddrTable.country),'')
                      ) 
                      and (isnull(Address,'') != '' or isnull(City,'') != '' or isnull(State,'') != '' )
                    union
                        select AddrNum,EntNum,Address,Address2,Address3,Address4,City,State,Country,Status,ListCreatedate,ListModifDate,CreateDate,LastModifDate,LastOper
                        from OFAC.dbo.TmpAddrTable where not exists 
                        (select * from OFAC.dbo.SDNAddrTable 
                        where OFAC.dbo.SDNAddrTable.EntNum = OFAC.dbo.TmpAddrTable.EntNum
                        and isnull(OFAC.dbo.SDNAddrTable.Address,'') = isnull(OFAC.dbo.TmpAddrTable.Address, '')
                        and isnull(OFAC.dbo.SDNAddrTable.City,'') = isnull(OFAC.dbo.TmpAddrTable.City,'')
                        and isnull(OFAC.dbo.SDNAddrTable.State,'') = isnull(OFAC.dbo.TmpAddrTable.State,'')
                        and isnull(OFAC.dbo.SDNAddrTable.Country,'') = isnull((OFAC.dbo.TmpAddrTable.country),'')
                        ) 
                        and country not in ('UNKNOWN','')
                        and isnull(country,'') != '' and isnull(Address,'') = '' and isnull(City,'') = '' and isnull(State,'') = '' 
                        ) X
");

                using (StreamWriter fileDWriter = new StreamWriter(BCPFile_Addr, false, Encoding.GetEncoding(28591)))
                {
                    DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                    dbCommandAdd.CommandTimeout = 0;
                    using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                    {
                        while (Tempdr.Read())
                        {
                            AddrIDMax = int.Parse(Tempdr["AddrNum"].ToString().Trim());
                            EntNum = Tempdr["EntNum"].ToString().Trim();
                            address = Tempdr["Address"].ToString().Trim();
                            address2 = Tempdr["Address2"].ToString().Trim();
                            address3 = Tempdr["Address3"].ToString().Trim();
                            address4 = Tempdr["Address4"].ToString().Trim();
                            city = Tempdr["City"].ToString().Trim();
                            state = Tempdr["State"].ToString().Trim();
                            country = Tempdr["Country"].ToString().Trim();
                            Status = Tempdr["Status"].ToString().Trim();
                            Entered = Tempdr["ListCreatedate"].ToString().Trim();
                            Updated = Tempdr["ListModifDate"].ToString().Trim();
                            createdate = Tempdr["CreateDate"].ToString().Trim();
                            modifdate = Tempdr["LastModifDate"].ToString().Trim();

                            StringBuilder builder = new StringBuilder();
                            builder.AppendFormat("{0}\t", EntNum);
                            builder.AppendFormat("{0}\t", AddrIDMax);
                            builder.AppendFormat("{0}\t", "");
                            builder.AppendFormat("{0}\t", "");
                            builder.AppendFormat("{0}\t", address);
                            builder.AppendFormat("{0}\t", address2);
                            builder.AppendFormat("{0}\t", address3);
                            builder.AppendFormat("{0}\t", address4);
                            builder.AppendFormat("{0}\t", city);
                            builder.AppendFormat("{0}\t", state);
                            builder.AppendFormat("{0}\t", "");
                            builder.AppendFormat("{0}\t", country);
                            builder.AppendFormat("{0}\t", "");
                            builder.AppendFormat("{0}\t", "0");
                            builder.AppendFormat("{0}\t", Status);
                            builder.AppendFormat("{0}\t", Entered);
                            builder.AppendFormat("{0}\t", Updated);
                            builder.AppendFormat("{0}\t", createdate);
                            builder.AppendFormat("{0}\t", modifdate);
                            builder.AppendFormat("{0}", "Primeadmin");

                            fileDWriter.WriteLine(builder.ToString());
                            File_FinalAddNum++;
                        }
                        Tempdr.Close();
                    }
                    dbCommandAdd.Dispose();

                    fileDWriter.Close();
                }

                p = new Process();
                p.StartInfo.FileName = "bcp.exe";

                p.StartInfo.Arguments = "OFAC.dbo.SDNAddrTable in " + BCPFile_Addr + "  -f " + BCPFmt_Addr + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                LogAdd("[All] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();

                LogAdd("[All] TempAddr - Import: [" + File_TotalNum + "]; sdnaddrTable - Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[All] Expand to sdnaddrTable -- Completion");

                OFACEventLog("sdnaddrTable total insert : " + File_FinalAddNum.ToString());
            }
            catch (Exception ex)
            {
                LogException(ex, "UpdAddrAllDataIntoSqlServer");
            }
        }

        public void UpdAttributeAllDataIntoSqlServer(string BCPFile_TempNotes, string BCPFile_Notes, string BCPFmt_TempNotes,
            string BCPFile_Error, string BCPFmt_Notes, string BCPServer, string wc_import_SQL, string wc_import_crime_SQL, string NoteTypeList)
        {
            try
            {
                LogAdd("[All] Expand to Notes -- Start");

                string query = string.Empty;
                string queryMax = string.Empty;
                string queryDel = string.Empty;
                string queryShrink = string.Empty;

                string NotesID = string.Empty;
                string EntNum = string.Empty;

                string Status = string.Empty;
                string ListCreateDate = string.Empty;
                string ListModifDate = string.Empty;
                string CreateDate = string.Empty;
                string LastOper = string.Empty;
                string LastModify = string.Empty;
                string Note = string.Empty;
                string NoteType = string.Empty;


                string NowTime = DateTime.Now.ToString("yyyyMMdd");

                int NotesIdMax = 0;
                int File_TotalNum = 0;
                int File_FinalAddNum = 0;

                DateTime NowDateTime = DateTime.Now;

                Database db;

                if (File.Exists(BCPFile_TempNotes)) File.Delete(BCPFile_TempNotes);
                
                if (File.Exists(BCPFile_Notes)) File.Delete(BCPFile_Notes);
                
                queryMax = string.Format(@" SELECT MAX(NotesID)+1 AS NotesIdMax FROM OFAC.dbo.Notes;");

                query = string.Format(@" 
                                    select ISNULL(wc.Company,'') Company,ISNULL(wc.Category,'') Category,ISNULL(wc.Country,'') Country,
                                     ISNULL(wc.Age,'') Age,ISNULL(wc.Passports,'') Passports,ISNULL(wc.SSN,'') SSN,ISNULL(wc.PlaceOfBirth,'') PlaceOfBirth,
                                     ISNULL(wc.Deceased,'') Deceased,
                                     isnull(wc.SubCategory,'') Subcategory,
	                                 isnull(wc.Keywords,'') Keywords,
                                     isnull(wc.Title,'') Title,
                                     isnull(wc.Position,'') Position,
                                     CASE WHEN isnull(wc.UpdCategory,'') = 'C1' then 'C1:changes to names alt, dob, locations and country' 
                                     WHEN isnull(wc.UpdCategory,'') = 'C2' then 'C2:changes to keywords, subcategory, category' 
                                     WHEN isnull(wc.UpdCategory,'') = 'C3' then 'C3:changes to title, position, pob,SSN, passport' 
                                     WHEN isnull(wc.UpdCategory,'') = 'C4' then 'C4:changes to ex sources, link to , companies, furtherinfo etc'
                                     else '' end UpdCategory,
                                     isnull(wc.IDENTIFICATION_NUMBERS,'') IDENTIFICATION_NUMBERS,
                                     isnull(AgeDate,'') agedate,
                                     isnull(wc.citizenship,'') citizenship,
                                     sdn.Entnum, 
                                     CASE ISNULL(WC.DelFlag, '0') 
                                     WHEN '0' THEN '1' 
                                     WHEN '1' THEN '4' END AS Status,
                                     sdn.ListCreateDate, sdn.ListModifDate, sdn.CreateDate,
                                     sdn.LastOper,sdn.LastModifDate
                                    from worldcheck wc
                                    join SDNTable (nolock) sdn on wc.Entnum = sdn.EntNum
                                    where sdn.Status != 4 
                                    ");

                if (!string.IsNullOrEmpty(wc_import_SQL) && wc_import_SQL.Trim().Length > 0)
                {
                    query += wc_import_SQL;
                }

                db = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");

                DbCommand dbCommand = db.GetSqlStringCommand(query);
                dbCommand.CommandTimeout = 0;
                dbCommand.Parameters.Add(new SqlParameter("@negnews", wc_import_crime_SQL));
                
                DbCommand dbCommandMax = db.GetSqlStringCommand(queryMax);
                dbCommandMax.CommandTimeout = 0;
                using (IDataReader dataReaderMax = db.ExecuteReader(dbCommandMax))
                {
                    if (dataReaderMax.GetName(0).Equals("NotesIdMax", StringComparison.InvariantCultureIgnoreCase))
                    {
                        while (dataReaderMax.Read())
                        {
                            NotesIdMax = Convert.ToInt32(dataReaderMax["NotesIdMax"]);
                        }
                    }
                }
                dbCommandMax.Dispose();

                using(StreamWriter TempfileDWriter = new StreamWriter(BCPFile_TempNotes, false, Encoding.GetEncoding(28591)))
                {
                    using (IDataReader dr = db.ExecuteReader(dbCommand))
                    {
                        while (dr.Read())
                        {
                            EntNum = dr["Entnum"].ToString().Trim();
                            Status = dr["Status"].ToString().Trim();
                            ListCreateDate = dr["ListCreateDate"].ToString().Trim();
                            ListModifDate = dr["ListModifDate"].ToString().Trim();
                            CreateDate = dr["CreateDate"].ToString().Trim();
                            LastOper = dr["LastOper"].ToString().Trim();
                            LastModify = dr["LastModifDate"].ToString().Trim();

                            int i;
                            for (i = 0; i < dr.FieldCount; i++)
                            {
                                //Dec.14, 2022 SG request add age of date
                                if (dr[i].ToString().Trim() != "" && i <= 15)
                                {
                                    StringBuilder builder = null;
                                    switch (i)
                                    {
                                        case 0:
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim().Length > 1000 ? dr[i].ToString().Trim().Substring(0, 1000) : dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "50");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;
                                        case 1:
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "48");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;
                                        case 2:
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "8");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;

                                        case 3:
                                            if (dr[i].ToString().Trim() == "0")
                                                break;
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "47");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;

                                        case 4:
                                            if (!string.IsNullOrEmpty(NoteTypeList) && NoteTypeList.Split(',').Any(s => s == "PAS"))
                                            {
                                                //Feb.29 2024 aviod NoteTypeList empty
                                                builder = new StringBuilder();
                                                builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                                builder.AppendFormat("{0}\t", EntNum);
                                                builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                                builder.AppendFormat("{0}\t", "6");
                                                builder.AppendFormat("{0}\t", Status);
                                                builder.AppendFormat("{0}\t", ListCreateDate);
                                                builder.AppendFormat("{0}\t", ListModifDate);
                                                builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                                builder.AppendFormat("{0}\t", CreateDate);
                                                builder.AppendFormat("{0}\t", LastOper);
                                                builder.AppendFormat("{0}", LastModify);
                                                TempfileDWriter.WriteLine(builder.ToString());
                                                NotesIdMax++;
                                                File_TotalNum++;
                                            }

                                            break;

                                        case 5:
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "3");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;

                                        case 6:
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "1");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;

                                        case 7:
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "49");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;

                                        case 8:
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "100");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;

                                        case 9:
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "101");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;

                                        case 10:
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "103");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;

                                        case 11:
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "104");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;

                                        case 12:
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "105");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;

                                        case 13:
                                            string notevalue = string.Empty;
                                            //Feb.29 2024 aviod NoteTypeList empty
                                            if (!string.IsNullOrEmpty(NoteTypeList))
                                            {
                                                foreach (string typelist in NoteTypeList.Split(','))
                                                {
                                                    foreach (string id in dr[i].ToString().Trim().Split(';'))
                                                    {

                                                        if (id.Contains(typelist))
                                                        {
                                                            switch (typelist)
                                                            {
                                                                case "BIC":
                                                                    if (!id.Contains("{INT-BIC}") || !id.Contains("{INT-BIC6}"))
                                                                    {
                                                                        continue;
                                                                    }
                                                                    NoteType = "106";
                                                                    notevalue = id.Replace("{INT-BIC}", "").Replace("{INT-BIC6}", "");
                                                                    break;
                                                                case "IMO":
                                                                    if (!id.Contains("{INT-IMO}"))
                                                                    {
                                                                        continue;
                                                                    }
                                                                    NoteType = "107";
                                                                    notevalue = id.Replace("{INT-IMO}", "");
                                                                    break;
                                                                case "ARN":
                                                                    if (!id.Contains("{INT-ARN}"))
                                                                    {
                                                                        continue;
                                                                    }
                                                                    NoteType = "108";
                                                                    notevalue = id.Replace("{INT-ARN}", "");
                                                                    break;
                                                                case "MSN":
                                                                    if (!id.Contains("{INT-MSN}"))
                                                                    {
                                                                        continue;
                                                                    }
                                                                    NoteType = "109";
                                                                    notevalue = id.Replace("{INT-MSN}", "");
                                                                    break;
                                                            }
                                                            if (!string.IsNullOrEmpty(notevalue))
                                                            {
                                                                builder = new StringBuilder();
                                                                builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                                                builder.AppendFormat("{0}\t", EntNum);
                                                                builder.AppendFormat("{0}\t", notevalue);
                                                                builder.AppendFormat("{0}\t", NoteType);
                                                                builder.AppendFormat("{0}\t", Status);
                                                                builder.AppendFormat("{0}\t", ListCreateDate);
                                                                builder.AppendFormat("{0}\t", ListModifDate);
                                                                builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                                                builder.AppendFormat("{0}\t", CreateDate);
                                                                builder.AppendFormat("{0}\t", LastOper);
                                                                builder.AppendFormat("{0}", LastModify);
                                                                TempfileDWriter.WriteLine(builder.ToString());
                                                                NotesIdMax++;
                                                                File_TotalNum++;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            

                                            //Jan.17, 2023 SG request add Identification No
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim().Substring(0, dr[i].ToString().Trim().Length > 1000 ? 1000 : dr[i].ToString().Trim().Length));
                                            builder.AppendFormat("{0}\t", "111");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;

                                        case 14:
                                            //Dec.14, 2022 SG request add age of date
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "110");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;

                                        case 15:
                                            //Jan.17, 2023 SG request add citizenship
                                            builder = new StringBuilder();
                                            builder.AppendFormat("{0}\t", NotesIdMax.ToString());
                                            builder.AppendFormat("{0}\t", EntNum);
                                            builder.AppendFormat("{0}\t", dr[i].ToString().Trim());
                                            builder.AppendFormat("{0}\t", "7");
                                            builder.AppendFormat("{0}\t", Status);
                                            builder.AppendFormat("{0}\t", ListCreateDate);
                                            builder.AppendFormat("{0}\t", ListModifDate);
                                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                                            builder.AppendFormat("{0}\t", CreateDate);
                                            builder.AppendFormat("{0}\t", LastOper);
                                            builder.AppendFormat("{0}", LastModify);
                                            TempfileDWriter.WriteLine(builder.ToString());
                                            NotesIdMax++;
                                            File_TotalNum++;
                                            break;
                                    }
                                }
                            }
                        }
                        dr.Close();
                    }
                    dbCommand.Dispose();
                    TempfileDWriter.Close();
                }

                queryDel = @" TRUNCATE TABLE OFAC.dbo.TempLoadNotes ";
                DbCommand dbCommandDel = db.GetSqlStringCommand(queryDel);
                dbCommandDel.CommandTimeout = 0;
                db.ExecuteReader(dbCommandDel);
                dbCommandDel.Dispose();
                Process p = new Process();
                p.StartInfo.FileName = "bcp.exe";

                p.StartInfo.Arguments = "OFAC.dbo.TempLoadNotes in " + BCPFile_TempNotes + "  -f " + BCPFmt_TempNotes + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                LogAdd("[All] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();

                using(StreamWriter fileDWriter = new StreamWriter(BCPFile_Notes, false, Encoding.GetEncoding(28591)))
                {
                    string queryAdd =
                    string.Format(@" SELECT DISTINCT [NotesID],[EntNum],[Note],[NoteType],[Status],
                                     [ListCreateDate],[ListModifDate],[CreateOper],[CreateDate],[LastOper],[LastModify] 
                                    FROM OFAC.dbo.TempLoadNotes 
                                     WHERE NOT EXISTS (SELECT * FROM OFAC.dbo.Notes WHERE TempLoadNotes.EntNum = EntNum AND TempLoadNotes.NoteType = NoteType AND TempLoadNotes.Note = Note) 
                                     AND Status = 1
                                     ORDER BY NotesID, EntNum ");

                    DbCommand dbCommandAdd = db.GetSqlStringCommand(queryAdd);
                    dbCommandAdd.CommandTimeout = 0;
                    using (IDataReader Tempdr = db.ExecuteReader(dbCommandAdd))
                    {
                        while (Tempdr.Read())
                        {
                            NotesID = Tempdr["NotesID"].ToString().Trim();
                            EntNum = Tempdr["EntNum"].ToString().Trim();
                            Note = Tempdr["Note"].ToString().Trim();
                            NoteType = Tempdr["NoteType"].ToString().Trim();
                            Status = Tempdr["Status"].ToString().Trim();
                            ListCreateDate = Tempdr["ListCreateDate"].ToString().Trim();
                            ListModifDate = Tempdr["ListModifDate"].ToString().Trim();

                            CreateDate = Tempdr["CreateDate"].ToString().Trim();
                            LastOper = Tempdr["LastOper"].ToString().Trim();
                            LastModify = Tempdr["LastModify"].ToString().Trim();
                            StringBuilder builder = new StringBuilder();
                            builder.AppendFormat("{0}\t", NotesID);
                            builder.AppendFormat("{0}\t", EntNum);
                            builder.AppendFormat("{0}\t", Note);
                            builder.AppendFormat("{0}\t", NoteType);
                            builder.AppendFormat("{0}\t", Status);
                            builder.AppendFormat("{0}\t", ListCreateDate);
                            builder.AppendFormat("{0}\t", ListModifDate);
                            builder.AppendFormat("{0}\t", "PRIMEADMIN");
                            builder.AppendFormat("{0}\t", CreateDate);
                            builder.AppendFormat("{0}\t", LastOper);
                            builder.AppendFormat("{0}", LastModify);
                            fileDWriter.WriteLine(builder.ToString());
                            File_FinalAddNum++;
                        }
                        Tempdr.Close();
                    }
                    dbCommandAdd.Dispose();
                    fileDWriter.Close();
                }

                p = new Process();
                p.StartInfo.FileName = "bcp.exe";

                p.StartInfo.Arguments = "OFAC.dbo.Notes in " + BCPFile_Notes + "  -f " + BCPFmt_Notes + " -S " + BCPServer + " -T -e " + BCPFile_Error + " -m 1000";
                LogAdd("[All] " + p.StartInfo.FileName + " " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();

                LogAdd("[All] TempLoadNotes - Import: [" + File_TotalNum + "]; Notes - Final Import: [" + File_FinalAddNum + "]");
                LogAdd("[All] Expand to Notes -- Completion");

                OFACEventLog("Notes total insert : " + File_FinalAddNum.ToString());
            }
            catch (Exception ex)
            {
                LogException(ex, "UpdAttributeAllDataIntoSqlServer");
            }
        }

        /// <summary>
        /// 寫入OFAC Event
        /// </summary>
        public void OFACEventLog(string log)
        {
            string sql = @"
insert into SDNChangeLog (logdate, oper, logtext, type, objecttype, objectid) 
values 
(getdate(), 'Prime', @Log, 'Ann', 'Event', 'WORLDCHECK')
";

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                using (var cmd = new SqlCommand(sql, sqlConnection))
                {
                    cmd.Parameters.Add(new SqlParameter("@Log", log.Replace(@"\n", Environment.NewLine)));
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

                sqlConnection.Close();
                SqlConnection.ClearAllPools();
            }
        }

        private void LogAdd(string log)
        {
            logs.AppendLine(log);
            Log.WriteToLog("Info", log);
        }

        public bool LogException(Exception e, string str_FunctionName)
        {
            Logerr(string.Format("Error at Function {0}", str_FunctionName));
            Logerr(string.Format("In the log routine. Caught {0}", e.GetType()));
            Logerr(string.Format("Message: {0}", e.Message));
            return true;
        }

        private void Logerr(string log)
        {
            Log.WriteToLog("Error", log);
        }
    }
}
