﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Unisys.AML.DirectoryWatcher
{
    public class FileSetToWatch : ConfigurationElement
    {
        [ConfigurationProperty("matchExpression", DefaultValue = "*")]
        public string MatchExpression
        {
            get
            {
                return (string)base["matchExpression"];
            }
        }

        [ConfigurationProperty("fileToWatch", IsRequired = true)]
        public FileToWatch FileToWatch
        {
            get
            {
                return (FileToWatch)base["fileToWatch"];
            }
        }

        [ConfigurationProperty("programToExecute")]
        public ProgramToExecute ProgramToExecute
        {
            get
            {
                return (ProgramToExecute)base["programToExecute"];
            }
        }
    }
}
