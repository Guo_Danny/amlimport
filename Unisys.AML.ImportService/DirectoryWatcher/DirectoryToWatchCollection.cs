﻿using System;
using System.Collections;
using System.Configuration;

namespace Unisys.AML.DirectoryWatcher
{
    public class DirectoryToWatchCollection : ConfigurationElementCollection
    {
        public DirectoryToWatch this[int index]
        {
            get
            {
                return (DirectoryToWatch)base.BaseGet(index);
            }
        }

        public new DirectoryToWatch this[string key]
        {
            get
            {
                return (DirectoryToWatch)base.BaseGet(key);
            }
        }

        protected override bool IsElementName(string elementName)
        {
            if ((string.IsNullOrEmpty(elementName)) || (elementName != "directoryToWatch"))
                return false;

            return true;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMapAlternate;
            }
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((DirectoryToWatch)element).Path;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new DirectoryToWatch();
        }
    }
}
