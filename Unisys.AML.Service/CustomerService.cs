﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Unisys.AML.Domain;
using Unisys.AML.DataAccess;
using System.Data;

namespace Unisys.AML.Service
{
    public class CustomerService
    {
        private ICustomerRepository customerRepository;

        public CustomerService()
        {
            customerRepository = new CustomerRepository();
        }

        public Customer GetByID(string id)
        {
            return customerRepository.GetByID(id);
        }

        public List<Customer> GetByName(string name, string filter, string tin)
        {
            return customerRepository.GetByName(name, filter, tin);
        }

        public List<Customer> GetByNameLike(string name)
        {
            return customerRepository.GetByNameLike(name);
        }

        public string GetSequence(string objectType)
        {
            return customerRepository.GetSequence(objectType);
        }

        public void Insert(string id, string parent, string name, string ownerBranch, string ownerDept, string riskClass, 
                           string createOper, string type, int status, string indOrBusType, string address, string country, string unification)
        {
            customerRepository.Insert(id, parent, name, ownerBranch, ownerDept, riskClass, createOper, 
                                      type, status, indOrBusType, address, country, unification);
        }

        public void InsertLog(string objectID,string strlogtxt)
        {
            try
            {
                customerRepository.insertlog(objectID, strlogtxt);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void InsertLog2(string str_type, string str_objType, string str_id, string str_logtext)
        {
            try
            {
                customerRepository.Insertlog2(str_type, str_objType, str_id, str_logtext);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RefreshMode(string strMode, string strType)
        {
            customerRepository.RefreshMode(strMode, strType);
        }

        public int CheckRelation(string strPartyId, string strRelation, string strRelatedParty, string strRelatedSource)
        {
            DataSet ds = customerRepository.CheckRelation(strPartyId, strRelation, strRelatedParty, strRelatedSource);

            return ds.Tables[0].Rows.Count;
        }

        public string GetCustIDByUnificationNo(string strUnificationNo, string strName)
        {
            string Custid = customerRepository.GetCustIDByUnificationNo(strUnificationNo, strName);

            return Custid;
        }

        public void InsertRelation(string strPartyId, string strRelation, string strRelatedParty, string strRelatedSource)
        {
            customerRepository.InsertRelation(strPartyId, strRelation, strRelatedParty, strRelatedSource);
        }

        public int CheckPartyID(string strCustomerId, string strIDType)
        {
            return customerRepository.CheckPartyID(strCustomerId, strIDType);
        }

        public void InsertPartyID(string strCustomerId, string strIDType, string strIDNumber, string strIssueAgency, string strIssuePlace, string strIssueCountry, string strIssueDate, string strExpiryDate)
        {
            customerRepository.InsertPartyID(strCustomerId, strIDType, strIDNumber, strIssueAgency, strIssuePlace, strIssueCountry, strIssueDate, strExpiryDate);
        }

        public DataSet CheckCustomerID(string strstrCustomerId)
        {
            DataSet ds = customerRepository.CheckCustID(strstrCustomerId);
            return ds;
        }

        public string GetAccount(string custid, string str_Ref)
        {
            return customerRepository.GetAccount(custid, str_Ref);
        }
    }
}
