﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Threading;
using Quartz;
using Quartz.Impl;
using log4net;
using Unisys.AML.Common;
using Unisys.AML.DirectoryWatcher;

namespace Unisys.AML.ImportService
{
    [DisallowConcurrentExecution]
    public class DirectoryScanJob : IJob
    {
        private const string _DIRECTORY_NAME = "DIRECTORY_NAME";
        private const string _CommandLine = "CommandLine";
        private const string _FilePath = "FilePath";
        private const string _BackupFolder = "BackupFolder";

        public string DIRECTORY_NAME { get; set; }
        public string CommandLine { get; set; }
        public string FilePath { get; set; }
        public string BackupFolder { get; set; }
        public DateTime ScheduleTime { get; set; }

        public DirectoryScanJob()
        {
        }

        protected FileInfo[] ProcessFolder(string pathName)
        {
            try
            {
                DirectoryInfo theFolder = new DirectoryInfo(pathName);
                if (theFolder.Exists)
                {
                    FileInfo[] files = theFolder.GetFiles();
                    return files;
                }
            }
            catch (FileNotFoundException ex)
            {
                LogException(ex, string.Format("This folder does not exist"));
            }
            catch (Exception ex)
            {
                LogException(ex, "ProcessFolder");
            }
            return null;
        }

        protected void ProcessFile(FileInfo fileInfo)
        {
            string programPath = string.Empty;
            string programArguments = string.Empty;
            string targetFile = string.Empty;
            string backupFile = string.Empty;

            try
            {
                Log.WriteToLog("Info", "Process {0}", fileInfo.Name);
                if (fileInfo.Name.ToLower().Contains("cif"))
                {
                    programPath = CommandLine.Substring(0, CommandLine.IndexOf(' '));
                    programArguments = string.Format(CommandLine.Substring(CommandLine.IndexOf(' ')), fileInfo.Name.Substring(0, 4));
                    targetFile = string.Format(FilePath, fileInfo.Name.Substring(0, 4));
                    backupFile = string.Format("{0}{1}", BackupFolder, fileInfo.Name);
                }
                else if (fileInfo.Name.ToLower().Contains("sancparty"))
                {
                    programPath = CommandLine.Substring(0, CommandLine.IndexOf(' '));
                    programArguments = CommandLine.Substring(CommandLine.IndexOf(' '));

                    foreach(string new_path in FilePath.Split(';'))
                    {
                        if (fileInfo.Name.ToLower().Contains(".txt") && new_path.ToLower().Contains(".txt"))
                        {
                            targetFile = new_path;
                        }
                        else if (fileInfo.Name.ToLower().Contains(".xml") && new_path.ToLower().Contains(".xml"))
                        {
                            targetFile = new_path;
                        }
                    }

                    Log.WriteToLog("Info", "Log File name: {0}", targetFile);
                    backupFile = string.Format("{0}{1}", BackupFolder, fileInfo.Name);
                }
                else
                {
                    Log.WriteToLog("Info", "Wrong File Name {0}", fileInfo.Name);
                    return;
                }
                if (!string.IsNullOrEmpty(BackupFolder))
                {
                    if (!CopyFile(fileInfo, targetFile))
                    {
                        Log.WriteToLog("Error", "Copy {0} To {1}", fileInfo.Name, targetFile);
                        return;
                    }
                    if (!MoveFile(fileInfo, backupFile))
                    {
                        Log.WriteToLog("Error", "Move {0} To {1}", fileInfo.Name, backupFile);
                        return;
                    }
                }
                else
                {
                    if (!MoveFile(fileInfo, targetFile))
                    {
                        Log.WriteToLog("Error", "Move {0} To {1}", fileInfo.Name, targetFile);
                        return;
                    }
                }
                if (ScheduleTime > DateTime.Now)
                    ScheduleTime = ScheduleTime.AddMinutes(2);
                else
                    ScheduleTime = DateTime.Now;

                //2018.03 modify for multi sanction import
                //string new_command = "";
                if (CommandLine.Split(';').Count() > 1)
                {
                    
                    for(int j = 0; j < CommandLine.Split(';').Count();j++)
                    //foreach (string new_command in CommandLine.Split(';'))
                    {
                        string new_command = CommandLine.Split(';')[j];
                        string new_path = FilePath.Split(';')[j];
                        if (fileInfo.Name.ToLower().Contains(".txt") && new_command.ToLower().Contains(".xml"))
                        {
                            continue;
                        }
                        else if (fileInfo.Name.ToLower().Contains(".xml") && new_command.ToLower().Contains("importsancparty "))
                        {
                            continue;
                        }

                        Log.WriteToLog("Info", "file:{0},command:{1}", fileInfo.Name, new_command);

                        programPath = new_command.Substring(0, new_command.IndexOf(' '));
                        programArguments = new_command.Substring(new_command.IndexOf(' '));
                        targetFile = new_path;
                        backupFile = string.Format("{0}{1}", BackupFolder, fileInfo.Name);

                        IScheduler scheduler = new StdSchedulerFactory().GetScheduler();
                        CommandLineJob commandLineJob = new CommandLineJob();
                        commandLineJob.ProgramPath = programPath;
                        commandLineJob.ProgramArguments = programArguments;
                        commandLineJob.FilePath = targetFile;
                        scheduler.ScheduleJob(
                            JobBuilder.Create<CommandLineJob>()
                            .UsingJobData(commandLineJob.BuildJobDataMap())
                            .Build(),
                            TriggerBuilder.Create()
                            .StartAt(DateTime.UtcNow.AddSeconds(1))
                            .Build());
                    }
                }
                else
                {
                    IScheduler scheduler = new StdSchedulerFactory().GetScheduler();
                    CommandLineJob commandLineJob = new CommandLineJob();
                    commandLineJob.ProgramPath = programPath;
                    commandLineJob.ProgramArguments = programArguments;
                    commandLineJob.FilePath = targetFile;
                    scheduler.ScheduleJob(
                        JobBuilder.Create<CommandLineJob>()
                        .UsingJobData(commandLineJob.BuildJobDataMap())
                        .Build(),
                        TriggerBuilder.Create()
                        .StartAt(DateTime.UtcNow.AddSeconds(1))
                        .Build());
                }
                
            }
            catch (Exception ex)
            {
                LogException(ex, "ProcessFile");
            }
        }

        private bool CopyFile(FileInfo fileInfo, string fileName)
        {
            bool success = false;
            int retries = 100;
            try
            {
                while (!success)
                {
                    try
                    {
                        Thread.Sleep(1000);
                        fileInfo.CopyTo(fileName, true);
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        if (--retries == 0)
                        {
                            LogException(ex, string.Format("Exception occurred while copying the file {0}", fileInfo.FullName));
                            break;
                        }
                    }

                    if (success)
                    {
                        Log.WriteToLog("Info", "Copy {0} To {1}", fileInfo.FullName, fileName);
                    }
                }
            }
            catch (Exception ex)
            {
                LogException(ex, "CopyFile");
            }
            return success;
        }

        private bool MoveFile(FileInfo fileInfo, string fileName)
        {
            bool success = false;
            int retries = 100;
            try
            {
                if (File.Exists(fileName))
                {
                    try
                    {
                        File.Move(fileName, string.Format("{0}.{1}", fileName, DateTime.Now.ToString("HHmmss")));
                    }
                    catch (Exception ex)
                    {
                        LogException(ex, string.Format("Exception occurred while moving the file {0}", fileName));
                    }
                }

                while (!success)
                {
                    try
                    {
                        Thread.Sleep(1000);
                        fileInfo.MoveTo(fileName);
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        if (--retries == 0)
                        {
                            LogException(ex, string.Format("Exception occurred while moving the file. File {0}", fileInfo.FullName));
                            break;
                        }
                    }

                    if (success)
                    {
                        Log.WriteToLog("Info", "Move {0} To {1}", fileInfo.FullName, fileName);
                    }
                }
            }
            catch (Exception ex)
            {
                LogException(ex, "MoveFile");
            }
            return success;
        }

        public virtual void Execute(IJobExecutionContext context)
        {
            JobDataMap jobDataMap = context.MergedJobDataMap;
            DIRECTORY_NAME = jobDataMap.GetString(_DIRECTORY_NAME);
            CommandLine = jobDataMap.GetString(_CommandLine);
            FilePath = jobDataMap.GetString(_FilePath);
            BackupFolder = jobDataMap.GetString(_BackupFolder);
            Log.WriteToLog("Info", "DirectoryScanJob DIRECTORY_NAME:{0} CommandLine:{1} FilePath:{2} BackupFolder:{3}", DIRECTORY_NAME, CommandLine, FilePath, BackupFolder);

            FileInfo[] files = ProcessFolder(DIRECTORY_NAME);
            if (files == null || files.Length == 0)
                return;
            Log.WriteToLog("Info", "Found {0} files", files.Length);
            ProcessFile(files[0]);
        }

        public bool LogException(Exception e, string str_FunctionName)
        {
            Logerr(string.Format("Error at Function {0}", str_FunctionName));
            Logerr(string.Format("In the log routine. Caught {0}", e.GetType()));
            Logerr(string.Format("Message: {0}", e.Message));
            return true;
        }

        private void Logerr(string log)
        {
            Log.WriteToLog("Error", log);
        }
    }
}
