﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Unisys.AML.Common;

namespace Unisys.AML.DataAccess
{
    public class SDNTableRepository
    {
        private Database database;

        Fortify fortifyService = new Fortify();

        /// <summary>
        /// 更新時間
        /// </summary>
        public DateTime UpdateDateTime { get; set; }
        /// <summary>
        /// 更新人員
        /// </summary>
        public string Updater { get; set; }
        /// <summary>
        /// SDNTable最小序號 (使用者自訂名單從10000001起)
        /// </summary>
        public int MinNo { get; set; }
        public SDNTableRepository()
        {
            UpdateDateTime = DateTime.Now;
            Updater = "UnisysAD";
            MinNo = 10000000;

            database = EnterpriseLibraryContainer.Current.GetInstance<Database>("OFAC");
        }

        #region 主檔 SDNTable
        /// <summary>
        /// 主檔新增
        /// </summary>
        /// <Remarks>
        /// 1.Insert SDNTable
        /// 2.use insert/select
        /// 3.use 【LastModifDate】 feild to save original 【EntNum】 temporary
        /// </Remarks>
        public int InsertSDNTable(string userListType, string userProgram, List<string> countries)
        {
            string sql = @"
insert into sdntable
(EntNum, Name, FirstName, MiddleName, LastName, ListID, ListType, Title, Program, Type, CallSign, VessType, Tonnage, 
GRT, VessFlag, VessOwner, Dob, PrimeAdded, Sex, Height, Weight, Build, Eyes, Hair, Complexion, Race, Country, Remarks, 
Remarks1, Remarks2, EffectiveDate, ExpiryDate, StandardOrder, SDNType, DataType, UserRec, Deleted, DuplicRec, 
IgnoreDerived, Status, ListCreatedate, ListModifDate, CreateDate, LastModifDate, LastOper)
select row_number() over (order by Name) + case when (select max(entnum) from sdntable) - @MinNo > 0 then (select max(entnum) from sdntable) else @MinNo end, 
       Name, FirstName, MiddleName, LastName, ListID, @UserListType, Title, @UserProgram, Type, CallSign, VessType, Tonnage, 
       GRT, VessFlag, VessOwner, Dob, PrimeAdded, Sex, Height, Weight, Build, Eyes, Hair, Complexion, Race, Country, Remarks, 
       Remarks1, Remarks2, EffectiveDate, ExpiryDate, StandardOrder, SDNType, DataType, 1, Deleted, DuplicRec, 
       IgnoreDerived, Status, ListCreatedate, ListModifDate, @CreateDate,
       DateAdd(second, EntNum,'1753-01-01'), 'UnisysRun'
  from sdntable as a
 where listtype = 'PEP'
   and country in ({0})
   and not exists (select 1
		             from SDNCustKey
		            where TableName = 'SDNTable'
	                  and RefKey = a.entnum);

insert into SDNCustKey
(TableName, PriKey, RefKey, CreateDate)
select 'SDNTable', EntNum, Datediff(second,'1753-01-01',LastModifDate), @CreateDate
  from SDNTable
 where LastOper = 'UnisysRun';

update SDNTable
   set LastOper = @LastOper,
       LastModifDate = null
 where LastOper = 'UnisysRun';
";
            string paramName = "country";
            using (DbCommand cmd = database.GetSqlStringCommand(string.Format(sql, InClauseSql(paramName, countries))))
            {
                cmd.Parameters.Add(new SqlParameter("@UserListType", userListType));
                cmd.Parameters.Add(new SqlParameter("@UserProgram", userProgram));
                cmd.Parameters.Add(new SqlParameter("@CreateDate", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));
                cmd.Parameters.Add(new SqlParameter("@MinNo", MinNo));

                for (int i = 0; i < countries.Count(); i++)
                    cmd.Parameters.Add(new SqlParameter(string.Format("@{0}{1}", paramName, i.ToString()), countries[i]));

                return ExecuteSQL(cmd);
            }
        }

        /// <summary>
        /// 主檔待更新筆數
        /// </summary>
        public int GetSDNTableUpdCnt()
        {
            string sql = @"
select count(1)
  from sdncustkey as a
  join sdntable as b on a.prikey = b.entnum
  join sdntable as c on a.refkey = c.entnum
 where a.tablename = 'SDNTable'
   and (b.Name != c.Name
   or isnull(b.FirstName,'') != isnull(c.FirstName,'')
   or isnull(b.MiddleName,'') != isnull(c.MiddleName,'')
   or isnull(b.LastName,'') != isnull(c.LastName,'')
   or isnull(b.ListID,'') != isnull(c.ListID,'')
   or isnull(b.Title,'') != isnull(c.Title,'')
   or isnull(b.Type,'') != isnull(c.Type,'')
   or isnull(b.CallSign,'') != isnull(c.CallSign,'')
   or isnull(b.VessType,'') != isnull(c.VessType,'')
   or isnull(b.Tonnage,'') != isnull(c.Tonnage,'')
   or isnull(b.GRT,'') != isnull(c.GRT,'')
   or isnull(b.VessFlag,'') != isnull(c.VessFlag,'')
   or isnull(b.VessOwner,'') != isnull(c.VessOwner,'')
   or isnull(b.Dob,'') != isnull(c.Dob,'')
   or isnull(b.Sex,'') != isnull(c.Sex,'')
   or isnull(b.Height,0) != isnull(c.Height,0)
   or isnull(b.Weight,0) != isnull(c.Weight,0)
   or isnull(b.Build,'') != isnull(c.Build,'')
   or isnull(b.Eyes,'') != isnull(c.Eyes,'')
   or isnull(b.Hair,'') != isnull(c.Hair,'')
   or isnull(b.Complexion,'') != isnull(c.Complexion,'')
   or isnull(b.Race,'') != isnull(c.Race,'')
   or isnull(b.Country,'') != isnull(c.Country,'')
   or isnull(cast(b.Remarks as nvarchar(max)), '') != isnull(cast(c.Remarks as nvarchar(max)), '')
   or isnull(b.Remarks1,'') != isnull(c.Remarks1,'')
   or isnull(b.Remarks2,'') != isnull(c.Remarks2,'')
   or isnull(b.EffectiveDate,'') != isnull(c.EffectiveDate,'')
   or isnull(b.ExpiryDate,'') != isnull(c.ExpiryDate,'')
   or isnull(b.StandardOrder,0) != isnull(c.StandardOrder,0)
   or isnull(b.SDNType,0) != isnull(c.SDNType,0)
   or isnull(b.DataType,0) != isnull(c.DataType,0)
   or isnull(b.Deleted,0) != isnull(c.Deleted,0)
   or isnull(b.DuplicRec,0) != isnull(c.DuplicRec,0)
   or isnull(b.IgnoreDerived,0) != isnull(c.IgnoreDerived,0)
   or isnull(b.Status,0) != isnull(c.Status,0)
   or isnull(b.ListCreateDate, '') != isnull(c.ListCreateDate, '')
   or isnull(b.ListModifDate, '') != isnull(c.ListModifDate, '')
)
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                return (int)database.ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// 主檔更新
        /// </summary>
        public int UpdateSDNTable()
        {
            string sql = @"
update b
   set b.Name = c.Name,
       b.FirstName = c.FirstName,
       b.MiddleName = c.MiddleName,
       b.LastName = c.LastName,
       b.ListID = c.ListID,
       b.Title = c.Title,
       b.Type = c.Type,
       b.CallSign = c.CallSign,
       b.VessType = c.VessType,
       b.Tonnage = c.Tonnage,
       b.GRT = c.GRT,
       b.VessFlag = c.VessFlag,
       b.VessOwner = c.VessOwner,
       b.Dob = c.Dob,
       b.Sex = c.Sex,
       b.Height = c.Height,
       b.Weight = c.Weight,
       b.Build = c.Build,
       b.Eyes = c.Eyes,
       b.Hair = c.Hair,
       b.Complexion = c.Complexion,
       b.Race = c.Race,
       b.Country = c.Country,
       b.Remarks = cast(c.Remarks as nvarchar(max)),
       b.Remarks1 = c.Remarks1,
       b.Remarks2 = c.Remarks2,
       b.EffectiveDate = c.EffectiveDate,
       b.ExpiryDate = c.ExpiryDate,
       b.StandardOrder = c.StandardOrder,
       b.SDNType = c.SDNType,
       b.DataType = c.DataType,
       b.UserRec = 1,
       b.Deleted = c.Deleted,
       b.DuplicRec = c.DuplicRec,
       b.IgnoreDerived = c.IgnoreDerived,
       b.Status = c.Status,
       b.ListCreateDate = c.ListCreateDate,
       b.ListModifDate = c.ListModifDate,
       b.LastModifDate = @UpdateDateTime,
       b.LastOper = @LastOper
  from sdncustkey as a
  join sdntable as b on a.prikey = b.entnum
  join sdntable as c on a.refkey = c.entnum
 where a.tablename = 'SDNTable'
   and (b.Name != c.Name
    or isnull(b.FirstName,'') != isnull(c.FirstName,'')
    or isnull(b.MiddleName,'') != isnull(c.MiddleName,'')
    or isnull(b.LastName,'') != isnull(c.LastName,'')
    or isnull(b.ListID,'') != isnull(c.ListID,'')
    or isnull(b.Title,'') != isnull(c.Title,'')
    or isnull(b.Type,'') != isnull(c.Type,'')
    or isnull(b.CallSign,'') != isnull(c.CallSign,'')
    or isnull(b.VessType,'') != isnull(c.VessType,'')
    or isnull(b.Tonnage,'') != isnull(c.Tonnage,'')
    or isnull(b.GRT,'') != isnull(c.GRT,'')
    or isnull(b.VessFlag,'') != isnull(c.VessFlag,'')
    or isnull(b.VessOwner,'') != isnull(c.VessOwner,'')
    or isnull(b.Dob,'') != isnull(c.Dob,'')
    or isnull(b.Sex,'') != isnull(c.Sex,'')
    or isnull(b.Height,0) != isnull(c.Height,0)
    or isnull(b.Weight,0) != isnull(c.Weight,0)
    or isnull(b.Build,'') != isnull(c.Build,'')
    or isnull(b.Eyes,'') != isnull(c.Eyes,'')
    or isnull(b.Hair,'') != isnull(c.Hair,'')
    or isnull(b.Complexion,'') != isnull(c.Complexion,'')
    or isnull(b.Race,'') != isnull(c.Race,'')
    or isnull(b.Country,'') != isnull(c.Country,'')
    or isnull(cast(b.Remarks as nvarchar(max)), '') != isnull(cast(c.Remarks as nvarchar(max)), '')
    or isnull(b.Remarks1,'') != isnull(c.Remarks1,'')
    or isnull(b.Remarks2,'') != isnull(c.Remarks2,'')
    or isnull(b.EffectiveDate,'') != isnull(c.EffectiveDate,'')
    or isnull(b.ExpiryDate,'') != isnull(c.ExpiryDate,'')
    or isnull(b.StandardOrder,0) != isnull(c.StandardOrder,0)
    or isnull(b.SDNType,0) != isnull(c.SDNType,0)
    or isnull(b.DataType,0) != isnull(c.DataType,0)
    or isnull(b.Deleted,0) != isnull(c.Deleted,0)
    or isnull(b.DuplicRec,0) != isnull(c.DuplicRec,0)
    or isnull(b.IgnoreDerived,0) != isnull(c.IgnoreDerived,0)
    or isnull(b.Status,0) != isnull(c.Status,0)
    or isnull(b.ListCreateDate, '') != isnull(c.ListCreateDate, '')
    or isnull(b.ListModifDate, '') != isnull(c.ListModifDate, '')
)
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@UpdateDateTime", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));

                return ExecuteSQL(cmd);
            }
        }

        #endregion

        #region 地址檔 SDNAddrTable

        public int InsertAddr()
        {
            string sql = @"
insert into sdnaddrtable
(EntNum, AddrNum, ListID, FreeFormat, Address, Address2, Address3, Address4, City, 
 State, PostalCode, Country, Remarks, Deleted, Status, ListCreateDate, ListModifDate, 
 CreateDate, LastModifDate, LastOper)
select a.prikey, 
       row_number() over (order by entnum) + 
	   case when (select max(AddrNum) from sdnaddrtable) - @MinNo > 0 
	        then (select max(AddrNum) from sdnaddrtable) 
	   else @MinNo end,
	   b.ListID, b.FreeFormat, b.Address, b.Address2, b.Address3, b.Address4, b.City, 
       b.State, b.PostalCode, b.Country, b.Remarks, b.Deleted, b.Status, b.ListCreateDate, b.ListModifDate, 
	   @CreateDate, DateAdd(second, b.AddrNum,'1753-01-01'), 'UnisysRun'
  from SDNCustKey as a
  join SDNAddrTable as b on a.refkey = b.entnum
 where a.TableName = 'SDNTable'
   and not exists (select 1
                     from sdnaddrtable
				    where entnum = a.prikey)

insert into SDNCustKey
(TableName, PriKey, RefKey, CreateDate)
select 'SDNAddrTable', AddrNum, Datediff(second,'1753-01-01',LastModifDate), @CreateDate
  from SDNAddrTable
 where LastOper = 'UnisysRun'

update SDNAddrTable
   set LastOper = @LastOper,
       LastModifDate = null
 where LastOper = 'UnisysRun';
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@CreateDate", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));
                cmd.Parameters.Add(new SqlParameter("@MinNo", MinNo));

                return ExecuteSQL(cmd);
            }
        }

        public int GetAddrUpdCnt()
        {
            string sql = @"
select count(1)
  from sdncustkey as a
  join SDNAddrTable as b on a.prikey = b.addrnum
  join SDNAddrTable as c on a.refkey = c.addrnum
 where a.tablename = 'SDNAddrTable'
   and (isnull(b.ListID, '') != isnull(c.ListID, '')
    or isnull(b.FreeFormat, '') != isnull(c.FreeFormat, '')
    or isnull(b.Address, '') != isnull(c.Address, '')
    or isnull(b.Address2, '') != isnull(c.Address2, '')
    or isnull(b.Address3, '') != isnull(c.Address3, '')
    or isnull(b.Address4, '') != isnull(c.Address4, '')
    or isnull(b.City, '') != isnull(c.City, '')
    or isnull(b.State, '') != isnull(c.State, '')
    or isnull(b.PostalCode, '') != isnull(c.PostalCode, '')
    or isnull(b.Country, '') != isnull(c.Country, '')
    or isnull(b.Remarks, '') != isnull(c.Remarks, '')
    or isnull(b.Deleted, '') != isnull(c.Deleted, '')
    or isnull(b.Status, '') != isnull(c.Status, '')
    or isnull(b.ListCreateDate, '') != isnull(c.ListCreateDate, '')
    or isnull(b.ListModifDate, '') != isnull(c.ListModifDate, '')
)
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                return (int)database.ExecuteScalar(cmd);
            }
        }

        public int UpdateAddr()
        {
            string sql = @"
update b set
       b.ListID = c.ListID,
       b.FreeFormat = c.FreeFormat,
       b.Address = c.Address,
       b.Address2 = c.Address2,
       b.Address3 = c.Address3,
       b.Address4 = c.Address4,
       b.City = c.City,
       b.State = c.State,
       b.PostalCode = c.PostalCode,
       b.Country = c.Country,
       b.Remarks = c.Remarks,
       b.Deleted = c.Deleted,
       b.Status = c.Status,
       b.ListCreateDate = c.ListCreateDate,
       b.ListModifDate = c.ListModifDate,
       b.LastModifDate = @UpdateDateTime,
       b.LastOper = @LastOper
  from sdncustkey as a
  join SDNAddrTable as b on a.prikey = b.addrnum
  join SDNAddrTable as c on a.refkey = c.addrnum
 where a.tablename = 'SDNAddrTable'
   and (isnull(b.ListID, '') != isnull(c.ListID, '')
    or isnull(b.FreeFormat, '') != isnull(c.FreeFormat, '')
    or isnull(b.Address, '') != isnull(c.Address, '')
    or isnull(b.Address2, '') != isnull(c.Address2, '')
    or isnull(b.Address3, '') != isnull(c.Address3, '')
    or isnull(b.Address4, '') != isnull(c.Address4, '')
    or isnull(b.City, '') != isnull(c.City, '')
    or isnull(b.State, '') != isnull(c.State, '')
    or isnull(b.PostalCode, '') != isnull(c.PostalCode, '')
    or isnull(b.Country, '') != isnull(c.Country, '')
    or isnull(b.Remarks, '') != isnull(c.Remarks, '')
    or isnull(b.Deleted, '') != isnull(c.Deleted, '')
    or isnull(b.Status, '') != isnull(c.Status, '')
    or isnull(b.ListCreateDate, '') != isnull(c.ListCreateDate, '')
    or isnull(b.ListModifDate, '') != isnull(c.ListModifDate, '')
)
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@UpdateDateTime", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));

                return ExecuteSQL(cmd);
            }
        }

        #endregion

        #region 別名檔

        public int InsertAlt()
        {
            string sql = @"
insert into SDNAltTable
(EntNum, AltNum, AltType, AltName, FirstName, MiddleName, LastName, ListID, 
 Remarks, Deleted, Status, ListCreateDate, ListModifDate, CreateDate, LastModifDate, LastOper)
select a.prikey, 
       row_number() over (order by entnum) + 
	   case when (select max(AltNum) from SDNAltTable) - @MinNo > 0 
	        then (select max(AltNum) from SDNAltTable) 
	   else @MinNo end, 
	   b.AltType, b.AltName, b.FirstName, b.MiddleName, b.LastName, 
       b.ListID, b.Remarks, b.Deleted, b.Status, b.ListCreateDate, b.ListModifDate, 
       @CreateDate, DateAdd(second, b.AltNum,'1753-01-01'), 'UnisysRun'
  from SDNCustKey as a
  join SDNAltTable as b on a.refkey = b.entnum
 where a.TableName = 'SDNTable'
   and not exists (select 1
                     from SDNAltTable
				    where entnum = a.prikey);

insert into SDNCustKey
(TableName, PriKey, RefKey, CreateDate)
select 'SDNAltTable', AltNum, Datediff(second,'1753-01-01',LastModifDate), @CreateDate
  from SDNAltTable
 where LastOper = 'UnisysRun'

update SDNAltTable
   set LastOper = @LastOper,
       LastModifDate = null
 where LastOper = 'UnisysRun';
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@CreateDate", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));
                cmd.Parameters.Add(new SqlParameter("@MinNo", MinNo));

                return ExecuteSQL(cmd);
            }
        }

        public int GetAltUpdCnt()
        {
            string sql = @"
select count(1)
  from sdncustkey as a
  join SDNAltTable as b on a.prikey = b.altnum
  join SDNAltTable as c on a.refkey = c.altnum
 where a.tablename = 'SDNAltTable'
   and (isnull(b.AltType,'') != isnull(c.AltType,'')
    or isnull(b.AltName,'') != isnull(c.AltName,'')
    or isnull(b.FirstName,'') != isnull(c.FirstName,'')
    or isnull(b.MiddleName,'') != isnull(c.MiddleName,'')
    or isnull(b.LastName,'') != isnull(c.LastName,'')
    or isnull(b.ListID,'') != isnull(c.ListID,'')
    or isnull(b.Remarks,'') != isnull(c.Remarks,'')
    or isnull(b.Deleted,'') != isnull(c.Deleted,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                return (int)database.ExecuteScalar(cmd);
            }
        }

        public int UpdateAlt()
        {
            string sql = @"
update b
   set b.AltType = c.AltType,
       b.AltName = c.AltName,
       b.FirstName = c.FirstName,
       b.MiddleName = c.MiddleName,
       b.LastName = c.LastName,
       b.ListID = c.ListID,
       b.Remarks = c.Remarks,
       b.Deleted = c.Deleted,
       b.Status = c.Status,
       b.ListCreateDate = c.ListCreateDate,
       b.ListModifDate = c.ListModifDate,
       b.LastModifDate = @UpdateDateTime,
       b.LastOper = @LastOper
  from sdncustkey as a
  join SDNAltTable as b on a.prikey = b.altnum
  join SDNAltTable as c on a.refkey = c.altnum
 where a.tablename = 'SDNAltTable'
   and (isnull(b.AltType,'') != isnull(c.AltType,'')
    or isnull(b.AltName,'') != isnull(c.AltName,'')
    or isnull(b.FirstName,'') != isnull(c.FirstName,'')
    or isnull(b.MiddleName,'') != isnull(c.MiddleName,'')
    or isnull(b.LastName,'') != isnull(c.LastName,'')
    or isnull(b.ListID,'') != isnull(c.ListID,'')
    or isnull(b.Remarks,'') != isnull(c.Remarks,'')
    or isnull(b.Deleted,'') != isnull(c.Deleted,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@UpdateDateTime", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));

                return ExecuteSQL(cmd);
            }
        }

        #endregion

        #region 親友檔 Relationship

        public int InsertRelationship()
        {
            string sql = @"
insert into Relationship
(RelationshipId, EntNum, RelatedID, RelationshipPToR, RelationshipRToP, Status, ListCreateDate, ListModifDate, 
CreateOper, CreateDate, LastOper, LastModify)
select row_number() over (order by entnum) + 
	   case when (select max(RelationshipID) from Relationship) - @MinNo > 0 
	        then (select max(RelationshipID) from Relationship) 
	   else @MinNo end, 
	   a.prikey, b.RelatedID, b.RelationshipPToR, b.RelationshipRToP, 
	   b.Status, b.ListCreateDate, b.ListModifDate, 
       'UnisysRun', @CreateDate, 'UnisysRun', DateAdd(second, b.RelationshipID,'1950-01-01')
  from SDNCustKey as a
  join Relationship as b on a.refkey = b.entnum
 where a.TableName = 'SDNTable'
   and not exists (select 1
                     from Relationship
				    where entnum = a.prikey);

insert into SDNCustKey
(TableName, PriKey, RefKey, CreateDate)
select 'Relationship', RelationshipID, Datediff(second,'1950-01-01',LastModify), @CreateDate
  from Relationship
 where LastOper = 'UnisysRun'

update Relationship
   set LastOper = @LastOper,
       CreateOper = @LastOper,
       LastModify = @CreateDate
 where LastOper = 'UnisysRun';
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@CreateDate", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));
                cmd.Parameters.Add(new SqlParameter("@MinNo", MinNo));

                return ExecuteSQL(cmd);
            }
        }

        public int GetRelationshipUpdCnt()
        {
            string sql = @"
select count(1)
  from sdncustkey as a
  join Relationship as b on a.prikey = b.RelationshipId
  join Relationship as c on a.refkey = c.RelationshipId
 where a.tablename = 'Relationship'
   and (isnull(b.RelatedID,'') != isnull(c.RelatedID,'')
    or isnull(b.RelationshipPToR,'') != isnull(c.RelationshipPToR,'')
    or isnull(b.RelationshipRToP,'') != isnull(c.RelationshipRToP,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                return (int)database.ExecuteScalar(cmd);
            }
        }

        public int UpdateRelationship()
        {
            string sql = @"
update b
   set b.RelatedID = c.RelatedID,
       b.RelationshipPToR = c.RelationshipPToR,
       b.RelationshipRToP = c.RelationshipRToP,
       b.Status = c.Status,
       b.ListCreateDate = c.ListCreateDate,
       b.ListModifDate = c.ListModifDate,
       b.LastModify = @UpdateDateTime,
       b.LastOper = @LastOper
  from sdncustkey as a
  join Relationship as b on a.prikey = b.RelationshipId
  join Relationship as c on a.refkey = c.RelationshipId
 where a.tablename = 'Relationship'
   and (isnull(b.RelatedID,'') != isnull(c.RelatedID,'')
    or isnull(b.RelationshipPToR,'') != isnull(c.RelationshipPToR,'')
    or isnull(b.RelationshipRToP,'') != isnull(c.RelationshipRToP,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@UpdateDateTime", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));

                return ExecuteSQL(cmd);
            }
        }

        #endregion

        #region 生日檔 DOBS

        public int InsertDOBs()
        {
            string sql = @"
insert into dobs
(DOBsId, EntNum, DOB, Status, ListCreateDate, ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
select row_number() over (order by entnum) + 
	   case when (select max(DOBsId) from DOBs) - @MinNo > 0 
	        then (select max(DOBsId) from DOBs) 
	   else @MinNo end, 
	   a.prikey, b.DOB, b.Status, b.ListCreateDate, b.ListModifDate, 
       'UnisysRun', @CreateDate, 'UnisysRun', DateAdd(second, b.DOBsId,'1950-01-01')
  from SDNCustKey as a
  join DOBs as b on a.refkey = b.entnum
 where a.TableName = 'SDNTable'
   and not exists (select 1
                     from dobs
				    where entnum = a.prikey);

insert into SDNCustKey
(TableName, PriKey, RefKey, CreateDate)
select 'DOBs', DOBsId, Datediff(second,'1950-01-01',LastModify), @CreateDate
  from DOBs
 where LastOper = 'UnisysRun'

update DOBs
   set LastOper = @LastOper,
       CreateOper = @LastOper,
       LastModify = @CreateDate
 where LastOper = 'UnisysRun';
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@CreateDate", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));
                cmd.Parameters.Add(new SqlParameter("@MinNo", MinNo));

                return ExecuteSQL(cmd);
            }
        }

        public int GetDOBsUpdCnt()
        {
            string sql = @"
select count(1)
  from sdncustkey as a
  join DOBs as b on a.prikey = b.DOBsId
  join DOBs as c on a.refkey = c.DOBsId
 where a.tablename = 'DOBs'
   and (isnull(b.DOB,'') != isnull(c.DOB,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                return (int)database.ExecuteScalar(cmd);
            }
        }

        public int UpdateDOBs()
        {
            string sql = @"
update b
   set b.DOB = c.DOB,
       b.Status = c.Status,
       b.ListCreateDate = c.ListCreateDate,
       b.ListModifDate = c.ListModifDate
  from sdncustkey as a
  join DOBs as b on a.prikey = b.DOBsId
  join DOBs as c on a.refkey = c.DOBsId
 where a.tablename = 'DOBs'
   and (isnull(b.DOB,'') != isnull(c.DOB,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@UpdateDateTime", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));

                return ExecuteSQL(cmd);
            }
        }

        #endregion

        #region URL

        public int InsertUrls()
        {
            string sql = @"
insert into Urls
(URLsID, EntNum, URL, Description, Status, ListCreateDate, ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
select row_number() over (order by entnum) + 
	   case when (select max(URLsID) from Urls) - @MinNo > 0 
	        then (select max(URLsID) from Urls) 
	   else @MinNo end, 
	   a.prikey, b.URL, b.Description, b.Status, b.ListCreateDate, b.ListModifDate,
       'UnisysRun', @CreateDate, 'UnisysRun', DateAdd(second, b.URLsID,'1950-01-01')
  from SDNCustKey as a
  join Urls as b on a.refkey = b.entnum
 where a.TableName = 'SDNTable'
   and not exists (select 1
                     from Urls
				    where entnum = a.prikey);

insert into SDNCustKey
(TableName, PriKey, RefKey, CreateDate)
select 'Urls', URLsID, Datediff(second,'1950-01-01',LastModify), @CreateDate
  from Urls
 where LastOper = 'UnisysRun'

update Urls
   set LastOper = @LastOper,
       CreateOper = @LastOper,
       LastModify = @CreateDate
 where LastOper = 'UnisysRun';
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@CreateDate", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));
                cmd.Parameters.Add(new SqlParameter("@MinNo", MinNo));

                return ExecuteSQL(cmd);
            }
        }

        public int GetUrlsUpdCnt()
        {
            string sql = @"
select count(1)
  from sdncustkey as a
  join Urls as b on a.prikey = b.UrlsId
  join Urls as c on a.refkey = c.UrlsId
 where a.tablename = 'Urls'
   and (isnull(b.URL,'') != isnull(c.URL,'')
    or isnull(b.Description,'') != isnull(c.Description,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                return (int)database.ExecuteScalar(cmd);
            }
        }

        public int UpdateUrls()
        {
            string sql = @"
update b
   set b.URL = c.URL,
       b.Description = c.Description,
       b.Status = c.Status,
       b.ListCreateDate = c.ListCreateDate,
       b.ListModifDate = c.ListModifDate,
       b.LastOper = @LastOper,
       b.LastModify = @UpdateDateTime
  from sdncustkey as a
  join Urls as b on a.prikey = b.UrlsId
  join Urls as c on a.refkey = c.UrlsId
 where a.tablename = 'Urls'
   and (isnull(b.URL,'') != isnull(c.URL,'')
    or isnull(b.Description,'') != isnull(c.Description,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@UpdateDateTime", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));

                return ExecuteSQL(cmd);
            }
        }

        #endregion

        #region Notes

        public int InsertNotes()
        {
            string sql = @"
insert into Notes
(NotesID, EntNum, Note, NoteType, Status, ListCreateDate, ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
select row_number() over (order by entnum) + 
	   case when (select max(NotesID) from Notes) - @MinNo > 0 
	        then (select max(NotesID) from Notes) 
	   else @MinNo end, 
	   a.prikey, b.Note, b.NoteType, b.Status, b.ListCreateDate, b.ListModifDate,
       'UnisysRun', @CreateDate, 'UnisysRun', DateAdd(second, b.NotesID,'1950-01-01')
  from SDNCustKey as a
  join Notes as b on a.refkey = b.entnum
 where a.TableName = 'SDNTable'
   and not exists (select 1
                     from Notes
				    where entnum = a.prikey);

insert into SDNCustKey
(TableName, PriKey, RefKey, CreateDate)
select 'Notes', NotesID, Datediff(second,'1950-01-01',LastModify), @CreateDate
  from Notes
 where LastOper = 'UnisysRun'

update Notes
   set LastOper = @LastOper,
       CreateOper = @LastOper,
       LastModify = @CreateDate
 where LastOper = 'UnisysRun';
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@CreateDate", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));
                cmd.Parameters.Add(new SqlParameter("@MinNo", MinNo));

                return ExecuteSQL(cmd);
            }
        }

        public int GetNotesUpdCnt()
        {
            string sql = @"
select count(1)
  from sdncustkey as a
  join Notes as b on a.prikey = b.NotesID
  join Notes as c on a.refkey = c.NotesID
 where a.tablename = 'Notes'
   and (isnull(b.Note,'') != isnull(c.Note,'')
    or isnull(b.NoteType,'') != isnull(c.NoteType,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                return (int)database.ExecuteScalar(cmd);
            }
        }

        public int UpdateNotes()
        {
            string sql = @"
update b
   set b.Note = c.Note,
       b.NoteType = c.NoteType,
       b.Status = c.Status,
       b.ListCreateDate = c.ListCreateDate,
       b.ListModifDate = c.ListModifDate,
       b.LastOper = @LastOper,
       b.LastModify = @UpdateDateTime
  from sdncustkey as a
  join Notes as b on a.prikey = b.NotesID
  join Notes as c on a.refkey = c.NotesID
 where a.tablename = 'Notes'
   and (isnull(b.Note,'') != isnull(c.Note,'')
    or isnull(b.NoteType,'') != isnull(c.NoteType,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@UpdateDateTime", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));

                return ExecuteSQL(cmd);
            }
        }

        #endregion

        #region 關鍵字主檔

        public int InsertKeywords()
        {
            string sql = @"
insert into Keywords
(KeywordsId, EntNum, Word, Deleted, Status, ListCreateDate, ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
select row_number() over (order by entnum) + 
	   case when (select max(KeywordsId) from Keywords) - @MinNo > 0 
	        then (select max(KeywordsId) from Keywords) 
	   else @MinNo end, 
	   a.prikey, b.Word, b.Deleted, b.Status, b.ListCreateDate, b.ListModifDate,
       'UnisysRun', @CreateDate, 'UnisysRun', DateAdd(second, b.KeywordsId,'1950-01-01')
  from SDNCustKey as a
  join Keywords as b on a.refkey = b.entnum
 where a.TableName = 'SDNTable'
   and not exists (select 1
                     from Keywords
				    where entnum = a.prikey);

insert into SDNCustKey
(TableName, PriKey, RefKey, CreateDate)
select 'Keywords', KeywordsId, Datediff(second,'1950-01-01',LastModify), @CreateDate
  from Keywords
 where LastOper = 'UnisysRun'

update Keywords
   set LastOper = @LastOper,
       CreateOper = @LastOper,
       LastModify = @CreateDate
 where LastOper = 'UnisysRun';
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@CreateDate", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));
                cmd.Parameters.Add(new SqlParameter("@MinNo", MinNo));

                return ExecuteSQL(cmd);
            }
        }

        public int GetKeywordsUpdCnt()
        {
            string sql = @"
select count(1)
  from sdncustkey as a
  join Keywords as b on a.prikey = b.KeywordsId
  join Keywords as c on a.refkey = c.KeywordsId
 where a.tablename = 'Keywords'
   and (isnull(b.Word,'') != isnull(c.Word,'')
    or isnull(b.Deleted,'') != isnull(c.Deleted,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                return (int)database.ExecuteScalar(cmd);
            }
        }

        public int UpdateKeywords()
        {
            string sql = @"
update b
   set b.Word = c.Word,
       b.Deleted = c.Deleted,
       b.Status = c.Status,
       b.ListCreateDate = c.ListCreateDate,
       b.ListModifDate = c.ListModifDate,
       b.LastOper = @LastOper,
       b.LastModify = @UpdateDateTime
  from sdncustkey as a
  join Keywords as b on a.prikey = b.KeywordsId
  join Keywords as c on a.refkey = c.KeywordsId
 where a.tablename = 'Keywords'
   and (isnull(b.Word,'') != isnull(c.Word,'')
    or isnull(b.Deleted,'') != isnull(c.Deleted,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@UpdateDateTime", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));

                return ExecuteSQL(cmd);
            }
        }

        #endregion

        #region 關鍵字別名

        public int InsertKeywordsAlt()
        {
            string sql = @"
insert into KeywordsAlt
(KeywordsAltId, AltNum, Word, Deleted, Status, ListCreateDate, ListModifDate, CreateOper, CreateDate, LastOper, LastModify)
select row_number() over (order by AltNum) + 
	   case when (select max(KeywordsAltId) from KeywordsAlt) - @MinNo > 0 
	        then (select max(KeywordsAltId) from KeywordsAlt) 
	   else @MinNo end, 
	   a.prikey, b.Word, b.Deleted, b.Status, b.ListCreateDate, b.ListModifDate,
       'UnisysRun', @CreateDate, 'UnisysRun', DateAdd(second, b.KeywordsAltId,'1950-01-01')
  from SDNCustKey as a
  join KeywordsAlt as b on a.refkey = b.AltNum
 where a.TableName = 'SDNAltTable'
   and not exists (select 1
                     from KeywordsAlt
				    where AltNum = a.prikey);

insert into SDNCustKey
(TableName, PriKey, RefKey, CreateDate)
select 'KeywordsAlt', KeywordsAltId, Datediff(second,'1950-01-01',LastModify), @CreateDate
  from KeywordsAlt
 where LastOper = 'UnisysRun'

update KeywordsAlt
   set LastOper = @LastOper,
       CreateOper = @LastOper,
       LastModify = @CreateDate
 where LastOper = 'UnisysRun';
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@CreateDate", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));
                cmd.Parameters.Add(new SqlParameter("@MinNo", MinNo));

                return ExecuteSQL(cmd);
            }
        }

        public int GetKeywordsAltUpdCnt()
        {
            string sql = @"
select count(1)
  from sdncustkey as a
  join KeywordsAlt as b on a.prikey = b.KeywordsAltId
  join KeywordsAlt as c on a.refkey = c.KeywordsAltId
 where a.tablename = 'KeywordsAlt'
   and (isnull(b.Word,'') != isnull(c.Word,'')
    or isnull(b.Deleted,'') != isnull(c.Deleted,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                return (int)database.ExecuteScalar(cmd);
            }
        }

        public int UpdateKeywordsAlt()
        {
            string sql = @"
update b
   set b.Word = c.Word,
       b.Deleted = c.Deleted,
       b.Status = c.Status,
       b.ListCreateDate = c.ListCreateDate,
       b.ListModifDate = c.ListModifDate,
       b.LastOper = @LastOper,
       b.LastModify = @UpdateDateTime
  from sdncustkey as a
  join KeywordsAlt as b on a.prikey = b.KeywordsAltId
  join KeywordsAlt as c on a.refkey = c.KeywordsAltId
 where a.tablename = 'KeywordsAlt'
   and (isnull(b.Word,'') != isnull(c.Word,'')
    or isnull(b.Deleted,'') != isnull(c.Deleted,'')
    or isnull(b.Status,'') != isnull(c.Status,'')
    or isnull(b.ListCreateDate,'') != isnull(c.ListCreateDate,'')
    or isnull(b.ListModifDate,'') != isnull(c.ListModifDate,'')
)";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@UpdateDateTime", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));

                return ExecuteSQL(cmd);
            }
        }

        #endregion

        public StringBuilder GetUpdateLog()
        {
            string sql = @"
select '1. SDNTable Create ' + cast(count(1) as varchar) + ' Record(s).'
  from sdntable
 where createdate = @Date and LastOper = @Oper
 union
select '1. SDNTable Update ' + cast(count(1) as varchar) + ' Record(s).'
  from sdntable
 where LastModifDate = @Date and LastOper = @Oper
 union
select '2. SDNAddrTable Create ' + cast(count(1) as varchar) + ' Record(s).'
  from SDNAddrTable
 where createdate = @Date and LastOper = @Oper
 union
select '2. SDNAddrTable Update ' + cast(count(1) as varchar) + ' Record(s).'
  from SDNAddrTable
 where LastModifDate = @Date and LastOper = @Oper
 union
select '3. SDNAltTable Create ' + cast(count(1) as varchar) + ' Record(s).'
  from SDNAltTable
 where createdate = @Date and LastOper = @Oper
 union
select '3. SDNAltTable Update ' + cast(count(1) as varchar) + ' Record(s).'
  from SDNAltTable
 where LastModifDate = @Date and LastOper = @Oper
  union
select '4. Relationship Create ' + cast(count(1) as varchar) + ' Record(s).'
  from Relationship
 where createdate = @Date and LastOper = @Oper and createdate != LastModify
 union
select '4. Relationship Update ' + cast(count(1) as varchar) + ' Record(s).'
  from Relationship
 where LastModify = @Date and LastOper = @Oper
 union
select '5. DOBs Create ' + cast(count(1) as varchar) + ' Record(s).'
  from Relationship
 where createdate = @Date and LastOper = @Oper
 union
select '5. DOBs Update ' + cast(count(1) as varchar) + ' Record(s).'
  from Relationship
 where LastModify = @Date and LastOper = @Oper and createdate != LastModify
 union
select '6. Urls Create ' + cast(count(1) as varchar) + ' Record(s).'
  from Urls
 where createdate = @Date and LastOper = @Oper
 union
select '6. Urls Update ' + cast(count(1) as varchar) + ' Record(s).'
  from Urls
 where LastModify = @Date and LastOper = @Oper and createdate != LastModify
  union
select '7. Notes Create ' + cast(count(1) as varchar) + ' Record(s).'
  from Notes
 where createdate = @Date and LastOper = @Oper
 union
select '7. Notes Update ' + cast(count(1) as varchar) + ' Record(s).'
  from Notes
 where LastModify = @Date and LastOper = @Oper and createdate != LastModify
  union
select '8. KeyWords Create ' + cast(count(1) as varchar) + ' Record(s).'
  from KeyWords
 where createdate = @Date and LastOper = @Oper
 union
select '8. KeyWords Update ' + cast(count(1) as varchar) + ' Record(s).'
  from KeyWords
 where LastModify = @Date and LastOper = @Oper and createdate != LastModify
 union
select '9. KeyWordsAlt Create ' + cast(count(1) as varchar) + ' Record(s).'
  from KeyWordsAlt
 where createdate = @Date and LastOper = @Oper
 union
select '9. KeyWordsAlt Update ' + cast(count(1) as varchar) + ' Record(s).'
  from KeyWordsAlt
 where LastModify = @Date and LastOper = @Oper and createdate != LastModify
";
            StringBuilder result = new StringBuilder().AppendLine();

            result.AppendLine(string.Format("TimeStamp : {0}", UpdateDateTime));

            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@Date", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@Oper", Updater));

                DataSet ds = database.ExecuteDataSet(cmd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow r in ds.Tables[0].Rows)
                    {
                        result.AppendLine(r[0].ToString());
                    }
                }
            }

            return result;
        }

        public DataSet GetSummary(string userListType)
        {
            string sql = @"
select Country, count(1) as Cnt
  from sdntable 
 where lastoper = @LastOper
   and (createdate = @UpdateDateTime or lastmodifdate = @UpdateDateTime)
 group by country
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@UpdateDateTime", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));

                return database.ExecuteDataSet(cmd);
            }
        }

        /// <summary>
        /// 寫入OFAC Event
        /// </summary>
        public void OFACEventLog(string log)
        {
            string sql = @"
insert into SDNChangeLog (logdate, oper, logtext, type, objecttype, objectid, evtDetail) 
values 
(getdate(), 'PrimeAdmin', 'PEP Customization List Update Summary', 'Mod', 'Event', '', @Log)
";
            using (DbCommand cmd = database.GetSqlStringCommand(sql))
            {
                cmd.Parameters.Add(new SqlParameter("@Log", fortifyService.PathManipulation(log.Replace(@"\n", Environment.NewLine))));
                database.ExecuteDataSet(cmd);
            }
        }

        /// <summary>
        /// Program 欄位取代
        /// </summary>
        public void UpdateProgram(List<string> programCountry, string programName)
        {
            string sql = @"
update sdntable
   set program = @program
 where lastoper = @LastOper
   and (createdate = @UpdateDateTime or lastmodifdate = @UpdateDateTime)
   and country in ({0})
";
            string paramName = "country";
            using (DbCommand cmd = database.GetSqlStringCommand(string.Format(sql, InClauseSql(paramName, programCountry))))
            {
                cmd.Parameters.Add(new SqlParameter("@UpdateDateTime", UpdateDateTime));
                cmd.Parameters.Add(new SqlParameter("@LastOper", Updater));
                cmd.Parameters.Add(new SqlParameter("@program", programName));

                for (int i = 0; i < programCountry.Count(); i++)
                    cmd.Parameters.Add(new SqlParameter(string.Format("@{0}{1}", paramName, i.ToString()), programCountry[i]));

                database.ExecuteDataSet(cmd);
            }
        }

        /// <summary>
        /// 產生 In SQL Clause
        /// </summary>
        private string InClauseSql(string name, List<string> param)
        {
            StringBuilder sql = new StringBuilder();

            for (int i = 0; i < param.Count(); i++)
                sql.Append(string.Format("@{0}{1},", name, i.ToString()));

            return sql.ToString().TrimEnd(',');
        }

        private int ExecuteSQL(DbCommand cmd)
        {
            using (DbConnection conn = database.CreateConnection())
            {
                conn.Open();
                DbTransaction trans = conn.BeginTransaction();

                try
                {
                    int rows = database.ExecuteNonQuery(cmd, trans);
                    trans.Commit();
                    return rows;
                }
                catch(Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
            }
        }
    }
}