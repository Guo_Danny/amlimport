﻿using System;
using System.Collections;
using System.Configuration;

namespace Unisys.AML.DirectoryWatcher
{
    public class FileSetToWatchCollection : ConfigurationElementCollection
    {
        public FileSetToWatch this[int index]
        {
            get
            {
                return (FileSetToWatch)base.BaseGet(index);
            }
        }

        public new FileSetToWatch this[string key]
        {
            get
            {
                return (FileSetToWatch)base.BaseGet(key);
            }
        }

        protected override bool IsElementName(string elementName)
        {
            if ((string.IsNullOrEmpty(elementName)) || (elementName != "fileSetToWatch"))
                return false;

            return true;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMapAlternate;
            }
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FileSetToWatch)element).MatchExpression;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new FileSetToWatch();
        }
    }
}
