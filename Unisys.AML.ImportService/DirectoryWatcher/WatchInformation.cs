﻿using System;
using System.Configuration;
using System.Xml;

namespace Unisys.AML.DirectoryWatcher
{
    public class WatchInformation : ConfigurationSection
    {
        [ConfigurationProperty("directoriesToWatch", IsRequired = true)]
        public DirectoryToWatchCollection DirectoriesToWatch
        {
            get
            {
                return (DirectoryToWatchCollection)base["directoriesToWatch"];
            }
        }
    }
}
