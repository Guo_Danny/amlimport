﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using ExcelDataReader;

namespace Unisys.AML.ImportService
{
    class ExcelReader
    {
        public DataSet ReadExcel(string excelPath)
        {
            DataSet ds = new DataSet();

            var extension = Path.GetExtension(excelPath).ToLower();

            using (var stream = new FileStream(excelPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                //判斷格式套用讀取方法
                IExcelDataReader reader = null;
                if (extension == ".xls")
                {
                    reader = ExcelReaderFactory.CreateBinaryReader(stream, new ExcelReaderConfiguration()
                    {
                        FallbackEncoding = Encoding.GetEncoding("big5")
                    });
                }
                else if (extension == ".xlsx")
                {
                    reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else if (extension == ".csv")
                {
                    reader = ExcelReaderFactory.CreateCsvReader(stream, new ExcelReaderConfiguration()
                    {
                        FallbackEncoding = Encoding.GetEncoding("big5")
                    });
                }
                else if (extension == ".txt")
                {
                    reader = ExcelReaderFactory.CreateCsvReader(stream, new ExcelReaderConfiguration()
                    {
                        FallbackEncoding = Encoding.GetEncoding("big5"),
                        AutodetectSeparators = new char[] { '\t' }
                    });
                }

                //沒有對應產生任何格式
                if (reader == null)
                {
                    return ds;
                }

                using (reader)
                {
                    ds = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        UseColumnDataType = false,
                        ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
                        {
                            //設定讀取資料時是否忽略標題
                            UseHeaderRow = false
                            
                        }
                    });
                }
            }

            return ds;
        }
    }
}
