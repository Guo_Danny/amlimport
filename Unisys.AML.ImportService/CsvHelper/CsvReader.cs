﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using Unisys.AML.Common;

namespace Unisys.AML.ImportService
{
    public sealed class CsvReader : IDisposable
    {

        #region Members

        private FileStream _fileStream;
        private Stream _stream;
        private StreamReader _streamReader;
        private StreamWriter _streamWriter;
        private Stream _memoryStream;
        private Encoding _encoding;
        private readonly StringBuilder _columnBuilder = new StringBuilder(100);
        private readonly Type _type = Type.File;
        private const char _delimiter = '~';

        #endregion Members

        #region Properties

        public bool TrimColumns { get; set; }

        public bool HasHeaderRow { get; set; }

        public List<string> Fields { get; private set; }

        public int? FieldCount
        {
            get
            {
                return (Fields != null ? Fields.Count : (int?)null);
            }
        }

        #endregion Properties

        #region Enums

        private enum Type
        {
            File,
            Stream,
            String
        }

        #endregion Enums

        #region Constructors

        public CsvReader(string filePath)
        {
            _type = Type.File;
            Initialise(filePath, Encoding.Default);
        }

        public CsvReader(string filePath, Encoding encoding)
        {
            _type = Type.File;
            Initialise(filePath, encoding);
        }

        public CsvReader(Stream stream)
        {
            _type = Type.Stream;
            Initialise(stream, Encoding.Default);
        }

        public CsvReader(Stream stream, Encoding encoding)
        {
            _type = Type.Stream;
            Initialise(stream, encoding);
        }

        public CsvReader(Encoding encoding, string csvContent)
        {
            _type = Type.String;
            Initialise(encoding, csvContent);
        }

        #endregion Constructors

        #region Methods

        private void Initialise(string filePath, Encoding encoding)
        {
            if (!File.Exists(filePath))
                throw new FileNotFoundException(string.Format("The file '{0}' does not exist.", filePath));

            _fileStream = File.OpenRead(filePath);
            Initialise(_fileStream, encoding);
        }

        private void Initialise(Stream stream, Encoding encoding)
        {
            if (stream == null)
                throw new ArgumentNullException("The supplied stream is null.");

            _stream = stream;
            _stream.Position = 0;
            _encoding = (encoding ?? Encoding.Default);
            _streamReader = new StreamReader(_stream, _encoding);
        }

        private void Initialise(Encoding encoding, string csvContent)
        {
            if (csvContent == null)
                throw new ArgumentNullException("The supplied csvContent is null.");

            _encoding = (encoding ?? Encoding.Default);

            _memoryStream = new MemoryStream(csvContent.Length);
            _streamWriter = new StreamWriter(_memoryStream);
            _streamWriter.Write(csvContent);
            _streamWriter.Flush();
            Initialise(_memoryStream, encoding);
        }

        public bool ReadNextRecord()
        {
            Fields = null;
            string line = ReadLineSafe(_streamReader, int.MaxValue);

            if (line == null)
                return false;

            ParseLine(line);
            return true;
        }

        public DataTable ReadIntoDataTable()
        {
            return ReadIntoDataTable(new System.Type[] { });
        }

        public DataTable ReadIntoDataTable(System.Type[] columnTypes)
        {
            DataTable dataTable = new DataTable();
            bool addedHeader = false;
            _stream.Position = 0;

            while (ReadNextRecord())
            {
                if (!addedHeader)
                {
                    for (int i = 0; i < Fields.Count; i++)
                        dataTable.Columns.Add(Fields[i], (columnTypes.Length > 0 ? columnTypes[i] : typeof(string)));

                    addedHeader = true;
                    continue;
                }

                DataRow row = dataTable.NewRow();

                for (int i = 0; i < Fields.Count; i++)
                    row[i] = Fields[i];

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        private void ParseLine(string line)
        {
            Fields = new List<string>();
            bool inColumn = false;
            _columnBuilder.Remove(0, _columnBuilder.Length);

            for (int i = 0; i < line.Length; i++)
            {
                char character = line[i];

                if (character == _delimiter)
                    inColumn = false;
                else
                {
                    _columnBuilder.Append(character);

                    inColumn = true;
                    continue;
                }

                if (!inColumn)
                {
                    Fields.Add(TrimColumns ? _columnBuilder.ToString().Trim() : _columnBuilder.ToString());
                    //Log.WriteToLog("Debug", "Field{0}={1}", Fields.Count, _columnBuilder.ToString());
                    _columnBuilder.Remove(0, _columnBuilder.Length);
                }
                else
                    _columnBuilder.Append(character);
            }

            Fields.Add(TrimColumns ? _columnBuilder.ToString().Trim() : _columnBuilder.ToString());
            //Log.WriteToLog("Debug", "Field{0}={1}", Fields.Count, _columnBuilder.ToString());
        }

        public void Dispose()
        {
            if (_streamReader != null)
            {
                _streamReader.Close();
                _streamReader.Dispose();
            }

            if (_streamWriter != null)
            {
                _streamWriter.Close();
                _streamWriter.Dispose();
            }

            if (_memoryStream != null)
            {
                _memoryStream.Close();
                _memoryStream.Dispose();
            }

            if (_fileStream != null)
            {
                _fileStream.Close();
                _fileStream.Dispose();
            }

            if ((_type == Type.String || _type == Type.File) && _stream != null)
            {
                _stream.Close();
                _stream.Dispose();
            }
        }

        #endregion Methods

        /// <summary>
        /// add Jul.11,2024 readline for fortify
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public string ReadLineSafe(TextReader reader, int maxLength)
        {
            var sb = new StringBuilder();
            bool bol_end = false;
            while (true)
            {
                int ch = reader.Read();
                if (ch == -1) break;
                if (ch == '\r' || ch == '\n')
                {
                    if (ch == '\r' && reader.Peek() == '\n')
                    {
                        bol_end = true;
                    }
                    else if (ch == '\r')
                    {
                        return sb.ToString();
                    }
                    if (ch == '\n')
                    {
                        return sb.ToString();
                    }
                }

                if (!bol_end)
                {
                    sb.Append((char)ch);
                }

                // Safety net here
                if (sb.Length > maxLength)
                {
                    return null;
                }
            }
            if (sb.Length > 0) return sb.ToString();
            return null;
        }
    }
}
