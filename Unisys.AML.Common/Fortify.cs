﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unisys.AML.Common
{
    public class Fortify
    {
        /// <summary>對路徑進行解析防止 Path Manipulation 安全性問題</summary>
        /// <param name="src">路徑</param>
        /// <returns>解析後的路徑</returns>
        public string PathManipulation(string src)
        {
            Encoding _encoding;
            Dictionary<char, char> _dictionary;
            StringBuilder _builder;

            Byte[] _base64Encode;
            Byte[] _base64Decode;
            string _base64String;
            _builder = new StringBuilder();

            _encoding = Encoding.GetEncoding("utf-8");
            _dictionary = new Dictionary<char, char>();

            for (int i = 32; i <= 126; i++)
            {
                var _char = Convert.ToChar(i);
                _dictionary.Add(_char, _char);
            }


            //替換有安全疑慮的路徑字串內容
            src = src.Replace("..\\", string.Empty);
            src = src.Replace("\\\\\\", string.Empty);
            src = src.Replace("\\\\", string.Empty);

            src = src.Replace("../", string.Empty);
            src = src.Replace("///", string.Empty);
            src = src.Replace("//", string.Empty);


            //將檔案路徑進行 base64 編碼
            _base64Encode = _encoding.GetBytes(src);
            _base64String = Convert.ToBase64String(_base64Encode);

            var _items = _base64String.ToCharArray();

            foreach (var _item in _items)
            {
                _builder.Append(_dictionary[_item]);
            }
            _base64String = _builder.ToString();


            //進行 Base64 解碼取得加工後的檔案路徑
            _base64Decode = Convert.FromBase64String(_base64String);

            src = _encoding.GetString(_base64Decode);

            return src;
        }
    }
}
