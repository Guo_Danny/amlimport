﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Unisys.AML.Common;
using Unisys.AML.Service;
using System.Data.SqlClient;

namespace Unisys.AML.ImportService
{
    [DisallowConcurrentExecution]
    public class SDNTableJob : IJob
    {
        private SDNTableService service;
        private StringBuilder logs;

        public SDNTableJob()
        {
        }

        public string connectionString = ConfigurationManager.ConnectionStrings["OFAC"].ConnectionString;

        public virtual void Execute(IJobExecutionContext context)
        {
            try
            {
                logs = new StringBuilder();
                LogAdd(string.Format("***** Start SDNTable Job [{0}] *****", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                JobDataMap jobDataMap = context.MergedJobDataMap;

                string Country = jobDataMap.GetString("Country");
                string UserListType = jobDataMap.GetString("UserListType");
                string UserProgram = jobDataMap.GetString("UserProgram");
                string SummaryLog = jobDataMap.GetString("SummaryLog");
                string ProgramUpdate = jobDataMap.GetString("ProgramUpdate");
                string Buildimg = jobDataMap.GetString("Buildimg");
                string str_updateDB = jobDataMap.GetString("UpdateDBStatus");

                List<string> Countries = Country.Split(',').ToList();

                service = new SDNTableService(UserListType, UserProgram, Countries);
                
                LogAdd(string.Format("ListType : {0} *****", UserListType));
                LogAdd(string.Format("Country : {0}", Country));

                int rows = 0;

                #region 0.SDNTable (Master)
                rows = service.InsertSDNTable();

                //*** 先取得更新筆數 (避免直接Update觸發trigger) ***
                rows = service.GetSDNTableUpdCnt();
                if (rows > 0)
                    rows = service.UpdateSDNTable();
                #endregion

                #region 1.Address (Detail)
                rows = service.InsertAddr();

                rows = service.GetAddrUpdCnt();
                if (rows > 0)
                    rows = service.UpdateAddr();
                #endregion

                #region 2.Alterative Name (Detail)
                rows = service.InsertAlt();

                rows = service.GetAltUpdCnt();
                if (rows > 0)
                    rows = service.UpdateAlt();
                #endregion

                #region 3.Relationship (Detail)
                rows = service.InsertRelationship();

                rows = service.GetRelationshipUpdCnt();
                if (rows > 0)
                    rows = service.UpdateRelationship();
                #endregion

                #region 4.DOBs
                rows = service.InsertDOBs();

                rows = service.GetDOBsUpdCnt();
                if (rows > 0)
                    rows = service.UpdateDOBs();
                #endregion

                #region 5.Url
                rows = service.InsertUrls();

                rows = service.GetUrlsUpdCnt();
                if (rows > 0)
                    rows = service.UpdateUrls();
                #endregion

                #region 6.Notes
                rows = service.InsertNotes();

                rows = service.GetNotesUpdCnt();
                if (rows > 0)
                    rows = service.UpdateNotes();
                #endregion

                #region 7.Keywords
                rows = service.InsertKeywords();

                rows = service.GetKeywordsUpdCnt();
                if (rows > 0)
                    rows = service.UpdateKeywords();
                #endregion

                #region 8.KeywordsAlt
                rows = service.InsertKeywordsAlt();

                rows = service.GetKeywordsAltUpdCnt();
                if (rows > 0)
                    rows = service.UpdateKeywordsAlt();
                #endregion

                //Program Update
                if (!string.IsNullOrEmpty(ProgramUpdate))
                {
                    LogAdd(string.Format("Update Program : {0}", ProgramUpdate));

                    List<string> program = ProgramUpdate.Split(';').ToList();
                    foreach (string p in program)
                    {
                        string _country = p.Split('=')[0];
                        string _programName = p.Split('=')[1];

                        service.UpdateProgram(_country.Split(',').ToList(), _programName);
                    }
                }

                LogAdd(service.GetUpdateLog());
                LogAdd(string.Format("***** Complete SDNTable Job [{0}] *****", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

                //201801 add config
                string b = "";

                //2018.05 config DB sdn status
                if (str_updateDB == "Y")
                {
                    //set MemImgBuildReq = 0
                    UpdDBStatus();
                }

                if (Buildimg == "Y")
                {
                    b = BuildFileImg();

                    //產生OFAC Log
                    if (!string.IsNullOrEmpty(SummaryLog))
                    {
                        StringBuilder summary = service.GetSummary();
                        summary.AppendLine().AppendLine();
                        summary.AppendLine("========== Database Info ==========");
                        summary.AppendLine();
                        summary.AppendLine(logs.ToString());

                        File.WriteAllText(SummaryLog, summary.AppendLine(b).ToString());

                        //寫入OFAC Event
                        service.OFACEventLog(summary.ToString());
                    }

                    LogAdd(b);
                }

                //產生Summary Log
                Log.WriteToLog("Info", logs.Insert(0, Environment.NewLine)
                                           .Insert(0, "========== SDNTableJob Summary ==========")
                                           .ToString());
            }
            catch (Exception ex)
            {
                LogException(ex, "Exception occurred");
            }
        }

        private void LogAdd(string log)
        {
            logs.AppendLine(log);
            Log.WriteToLog("Info", log);
        }

        private string BuildFileImg()
        {
            StringBuilder s = new StringBuilder();
            Process p = new Process();
            ProcessStartInfo pInfo = new ProcessStartInfo("D:\\Prime\\Exe\\BuildFileImg.exe");

            s.AppendLine(string.Format("{0} | ========== Start Build File Image ==========", 
                                       DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")))
             .AppendLine();
             
            pInfo.UseShellExecute = false;
            pInfo.RedirectStandardOutput = true;
            pInfo.CreateNoWindow = true;

            p.StartInfo = pInfo;
            p.Start();

            using (StreamReader myStreamReader = p.StandardOutput)
            {
                string result = "";
                while((result = ReadLineSafe(myStreamReader, int.MaxValue)) != null)
                {
                    s.AppendLine(result);
                }
                p.WaitForExit();
                p.Close();

                
            }

            s.AppendLine(string.Format("{0} | ========== End Build File Image ==========", 
                                        DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));
            return s.ToString();
        }

        private void UpdDBStatus()
        {
            //2018.05
            try
            {
                string sql = @"update OFAC.dbo.SDNDbStatus set MemImgBuildReq = 0;";

                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    using (var cmd = new SqlCommand(sql, sqlConnection))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    sqlConnection.Close();
                }

                OFACEventLog("Update DBstatus to 0 ");

                Log.WriteToLog("Info", "Update DBstatus to 0 ");
            }
            catch (Exception ex)
            {
                LogException(ex, "Update DBstatus");
            }
        }

        /// <summary>
        /// 寫入OFAC Event
        /// </summary>
        public void OFACEventLog(string log)
        {
            string sql = @"
insert into OFAC.dbo.SDNChangeLog (logdate, oper, logtext, type, objecttype, objectid) 
values 
(getdate(), 'Prime', @Log, 'Ann', 'Event', 'CUSTPEP')
";

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                using (var cmd = new SqlCommand(sql, sqlConnection))
                {
                    cmd.Parameters.Add(new SqlParameter("@Log", log.Replace(@"\n", Environment.NewLine)));
                    cmd.ExecuteNonQuery();
                }

                sqlConnection.Close();
            }
        }

        /// <summary>
        /// add Jul.11,2024 readline for fortify
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public string ReadLineSafe(TextReader reader, int maxLength)
        {
            var sb = new StringBuilder();
            bool bol_end = false;
            while (true)
            {
                int ch = reader.Read();
                if (ch == -1) break;
                if (ch == '\r' || ch == '\n')
                {
                    if (ch == '\r' && reader.Peek() == '\n')
                    {
                        bol_end = true;
                    }
                    else if (ch == '\r')
                    {
                        return sb.ToString();
                    }
                    if (ch == '\n')
                    {
                        return sb.ToString();
                    }
                }

                if (!bol_end)
                {
                    sb.Append((char)ch);
                }

                // Safety net here
                if (sb.Length > maxLength)
                {
                    return null;
                }
            }
            if (sb.Length > 0) return sb.ToString();
            return null;
        }

        public bool LogException(Exception e, string str_FunctionName)
        {
            Logerr(string.Format("Error at Function {0}", str_FunctionName));
            Logerr(string.Format("In the log routine. Caught {0}", e.GetType()));
            Logerr(string.Format("Message: {0}", e.Message));
            return true;
        }

        private void Logerr(string log)
        {
            Log.WriteToLog("Error", log);
        }
    }
}
