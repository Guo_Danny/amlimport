﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using Unisys.AML.Common;

namespace Unisys.AML.ImportService
{
    public sealed class CsvWriter : IDisposable
    {

        #region Members

        private StreamWriter _streamWriter;
        private const string _delimiter = "~";
        
        #endregion Members

        #region Properties

        #endregion Properties

        #region Methods

        #region CsvFile write methods

        public void WriteCsv(CsvFile csvFile, string filePath)
        {
            WriteCsv(csvFile, filePath, null);
        }

        public void WriteCsv(CsvFile csvFile, string filePath, Encoding encoding)
        {
            Fortify fortifyService = new Fortify();

            string path_for_fortify = fortifyService.PathManipulation(filePath);

            if (File.Exists(path_for_fortify))
                File.Delete(path_for_fortify);

            using (StreamWriter writer = new StreamWriter(fortifyService.PathManipulation(filePath), false, encoding ?? Encoding.Default))
            {
                WriteToStream(csvFile, writer);
                writer.Flush();
                writer.Close();
            }
        }

        public void WriteCsv(CsvFile csvFile, Stream stream)
        {
            WriteCsv(csvFile, stream, null);
        }

        public void WriteCsv(CsvFile csvFile, Stream stream, Encoding encoding)
        {
            stream.Position = 0;
            _streamWriter = new StreamWriter(stream, encoding ?? Encoding.Default);
            WriteToStream(csvFile, _streamWriter);
            _streamWriter.Flush();
            stream.Position = 0;
        }

        public string WriteCsv(CsvFile csvFile, Encoding encoding)
        {
            string content = string.Empty;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(memoryStream, encoding ?? Encoding.Default))
                {
                    WriteToStream(csvFile, writer);
                    writer.Flush();
                    memoryStream.Position = 0;

                    using (StreamReader reader = new StreamReader(memoryStream, encoding ?? Encoding.Default))
                    {
                        string line = "";
                        while ((line = ReadLineSafe(reader, int.MaxValue)) != null)
                        {
                            content += line;
                        }
                        writer.Close();
                        reader.Close();
                        memoryStream.Close();
                    }
                }
            }

            return content;
        }

        #endregion CsvFile write methods

        #region DataTable write methods

        public void WriteCsv(DataTable dataTable, string filePath)
        {
            WriteCsv(dataTable, filePath, null);
        }

        public void WriteCsv(DataTable dataTable, string filePath, Encoding encoding)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            using (StreamWriter writer = new StreamWriter(filePath, false, encoding ?? Encoding.Default))
            {
                WriteToStream(dataTable, writer);
                writer.Flush();
                writer.Close();
            }
        }

        public void WriteCsv(DataTable dataTable, Stream stream)
        {
            WriteCsv(dataTable, stream, null);
        }

        public void WriteCsv(DataTable dataTable, Stream stream, Encoding encoding)
        {
            stream.Position = 0;
            _streamWriter = new StreamWriter(stream, encoding ?? Encoding.Default);
            WriteToStream(dataTable, _streamWriter);
            _streamWriter.Flush();
            stream.Position = 0;
        }

        public string WriteCsv(DataTable dataTable, Encoding encoding)
        {
            string content = string.Empty;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(memoryStream, encoding ?? Encoding.Default))
                {
                    WriteToStream(dataTable, writer);
                    writer.Flush();
                    memoryStream.Position = 0;

                    using (StreamReader reader = new StreamReader(memoryStream, encoding ?? Encoding.Default))
                    {
                        string line = "";
                        while((line = ReadLineSafe(reader, int.MaxValue)) != null)
                        {
                            content += line;
                        }
                        writer.Close();
                        reader.Close();
                        memoryStream.Close();
                    }
                }
            }

            return content;
        }

        #endregion DataTable write methods

        private void WriteToStream(CsvFile csvFile, TextWriter writer)
        {
            if (csvFile.Headers.Count > 0)
                WriteRecord(csvFile.Headers, writer);

            csvFile.Records.ForEach(record => WriteRecord(record.Fields, writer));
        }

        private void WriteToStream(DataTable dataTable, TextWriter writer)
        {
            List<string> fields = (from DataColumn column in dataTable.Columns select column.ColumnName).ToList();
            WriteRecord(fields, writer);

            foreach (DataRow row in dataTable.Rows)
            {
                fields.Clear();
                fields.AddRange(row.ItemArray.Select(o => o.ToString()));
                WriteRecord(fields, writer);
            }
        }

        private void WriteRecord(IList<string> fields, TextWriter writer)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                string fieldValue = fields[i];
                writer.Write(string.Format("{0}{1}",
                    fieldValue,
                    (i < (fields.Count - 1) ? _delimiter : string.Empty)));
            }

            writer.WriteLine();
        }

        public void Dispose()
        {
            if (_streamWriter == null)
                return;

            _streamWriter.Close();
            _streamWriter.Dispose();
        }

        #endregion Methods

        /// <summary>
        /// add Jul.11,2024 readline for fortify
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public string ReadLineSafe(TextReader reader, int maxLength)
        {
            var sb = new StringBuilder();
            bool bol_end = false;
            while (true)
            {
                int ch = reader.Read();
                if (ch == -1) break;
                if (ch == '\r' || ch == '\n')
                {
                    if (ch == '\r' && reader.Peek() == '\n')
                    {
                        bol_end = true;
                    }
                    else if (ch == '\r')
                    {
                        return sb.ToString();
                    }
                    if (ch == '\n')
                    {
                        return sb.ToString();
                    }
                }

                if (!bol_end)
                {
                    sb.Append((char)ch);
                }

                // Safety net here
                if (sb.Length > maxLength)
                {
                    return null;
                }
            }
            if (sb.Length > 0) return sb.ToString();
            return null;
        }
    }
}
