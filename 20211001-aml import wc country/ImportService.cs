﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using Unisys.AML.Common;
using Unisys.AML.DirectoryWatcher;
using Unisys.AML.Domain;
using Unisys.AML.Service;
using System.Text;
using System.Data;
using Microsoft.VisualBasic.FileIO;


namespace Unisys.AML.ImportService
{
    public partial class ImportService : ServiceBase
    {
        private WatchInformation watchInformation = null;
        private Dictionary<string, Dictionary<string, FileSystemWatcher>> watchers = new Dictionary<string, Dictionary<string, FileSystemWatcher>>();
        Fortify fortifyService = new Fortify();
        private AutoMode Mode;
        private string CustomerFilter;
        private string import_duplicate;

        private string ActivFilter;

        enum AutoMode
        {
            SG,
            US
        }

        internal void Start(object p)
        {
            OnStart(null);
        }

        public static IScheduler Scheduler { get; private set; }

        public ImportService()
        {
            InitializeComponent();

            switch (ConfigurationManager.AppSettings["AutoCustomer"])
            {
                case "0":
                    Mode = AutoMode.US;
                    break;
                case "1":
                    Mode = AutoMode.SG;
                    break;
                default:
                    Mode = AutoMode.SG;
                    break;
            }
            CustomerFilter = ConfigurationManager.AppSettings["AutoCustomerFilter"];

            import_duplicate = ConfigurationManager.AppSettings["duplicate_allow"];

            ActivFilter = ConfigurationManager.AppSettings["ActivityFilter"];
        }

        protected override void OnStart(string[] args)
        {
            Log.WriteToLog("Info", "Starting up the Import service.");
            try
            {
                StartScheduler();

                watchInformation = (WatchInformation)ConfigurationManager.GetSection("watchInformation");
                foreach (DirectoryToWatch directoryToWatch in watchInformation.DirectoriesToWatch)
                {
                    foreach (FileSetToWatch fileSetToWatch in directoryToWatch.FileSetsToWatch)
                    {
                        FileSystemWatcher watcher = new FileSystemWatcher(directoryToWatch.Path);
                        watcher.Filter = fileSetToWatch.MatchExpression;
                        watcher.Created += new FileSystemEventHandler(watcher_OnChanged);
                        if (!watchers.ContainsKey(directoryToWatch.Path))
                            watchers[directoryToWatch.Path] = new Dictionary<string, FileSystemWatcher>();
                        watchers[directoryToWatch.Path][watcher.Filter] = watcher;
                        watcher.EnableRaisingEvents = true;
                        Log.WriteToLog("Info", "Added watcher for the path \"{0}\" and \"{1}\".", directoryToWatch.Path, fileSetToWatch.MatchExpression);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred while starting the service. Type: {0} Message: {1}", ex.GetType().FullName, ex.Message);
                throw;
            }
        }

        protected override void OnStop()
        {
            Log.WriteToLog("Info", "Stopping the Directory Watcher service.");
            try
            {
                foreach (string directory in watchers.Keys)
                {
                    foreach (string filter in watchers[directory].Keys)
                        watchers[directory][filter].EnableRaisingEvents = false;
                }

                StopScheduler();
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "Exception occurred while stoping the service. Type: {0}\nMessage: {1}", ex.GetType().FullName, ex.Message);
                throw;
            }
        }

        private void StartScheduler()
        {
            try
            {
                Scheduler = new StdSchedulerFactory().GetScheduler();

                if (!Scheduler.IsStarted)
                {
                    Scheduler.Start();
                    while (!Scheduler.IsStarted)
                    {
                        Log.WriteToLog("Info", "Waiting for scheduler to start.");
                        Thread.Sleep(1000);
                    }
                    Log.WriteToLog("Info", "IsStarted={0}", Scheduler.IsStarted);
                    Log.WriteToLog("Info", "SchedulerInstanceId={0}", Scheduler.SchedulerInstanceId);
                    Log.WriteToLog("Info", "SchedulerName={0}", Scheduler.SchedulerName);
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "StartScheduler: Message: {0}", ex.Message);
            }
        }

        private void StopScheduler()
        {
            try
            {
                if (Scheduler != null)
                {
                    Log.WriteToLog("Info", "Shutting down scheduler");
                    Scheduler.Shutdown(false);
                    while (!Scheduler.IsShutdown)
                    {
                        Log.WriteToLog("Info", "Waiting for scheduler to shutdown.");
                        Thread.Sleep(1000);
                    }
                    Log.WriteToLog("Info", "IsShutdown={0}", Scheduler.IsShutdown);
                    Log.WriteToLog("Info", "The scheduler has been shutdown.");
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "StopScheduler: Message: {0}", ex.Message);
            }
        }

        private void AddCommandLineJob(string programPath, string programArguments, string filePath, string programDelay)
        {
            int delay = int.Parse(programDelay);
            Log.WriteToLog("Debug", "ProgramPath={0} ProgramArguments={1} ProgramDelay={2}", programPath, programArguments, delay);


            //201912 for data import 
            if (filePath.ToUpper().Contains(programPath.ToUpper()))
            {
                Thread.Sleep(delay * 1000);

                string str_logpath = string.Empty;

                if (programPath.ToUpper() == "PARTY")
                {
                    str_logpath = ConfigurationManager.AppSettings["RelationParty_log"];

                    InsertRelationParty(ConfigurationManager.AppSettings["ImportRelationParty_Mode"], filePath, str_logpath);
                }
                else if (programPath.ToUpper() == "ID")
                {
                    str_logpath = ConfigurationManager.AppSettings["PartyID_log"];

                    InsertPartyID(ConfigurationManager.AppSettings["ImportPartyID_Mode"], filePath, str_logpath);
                }
                else if (programPath.ToUpper() == "COUNTRY")
                {
                    //SEP.23.2021
                    //SEP.28.2021 BOT think one time batch is OK,
                    //no need to design the import country function so far.
                    
                    //str_logpath = ConfigurationManager.AppSettings["Country_log"];

                    //InsertWCCountry(filePath, str_logpath);
                }
            }
            else
            {
                CommandLineJob commandLineJob = new CommandLineJob();
                commandLineJob.ProgramPath = programPath;
                commandLineJob.ProgramArguments = programArguments;
                commandLineJob.FilePath = filePath;
                Scheduler.ScheduleJob(
                    JobBuilder.Create<CommandLineJob>()
                    .UsingJobData(commandLineJob.BuildJobDataMap())
                    .Build(),
                    TriggerBuilder.Create()
                    .StartAt(DateTime.UtcNow.AddSeconds(delay + 1))
                    .Build());
            }
        }

        protected void watcher_OnChanged(object source, FileSystemEventArgs e)
        {
            bool success = false;
            int retries = 100;
            int recordCount = 0;

            try
            {
                FileSystemWatcher watcher = (FileSystemWatcher)source;

                Fortify fortifyService = new Fortify();

                string targetFilePath = Path.Combine(fortifyService.PathManipulation(watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].FileToWatch.Path), fortifyService.PathManipulation(e.Name));

                string path_for_fortify = fortifyService.PathManipulation(targetFilePath);

                if (watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].FileToWatch.Type == "Move")
                {
                    Log.WriteToLog("Debug", "DirectoryToWatch.Type={0}", watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].FileToWatch.Type);
                    if (File.Exists(targetFilePath))
                    {
                        try
                        {
                            //2018/06 modify check duplicate file name and decide abort or not?
                            if (import_duplicate == "N" && (fortifyService.PathManipulation(e.FullPath).ToUpper().Contains("ACTIVITY") || fortifyService.PathManipulation(e.FullPath).ToUpper().Contains("ACCOUNT") || fortifyService.PathManipulation(e.FullPath).ToUpper().Contains("CUSTOMER")))
                            {
                                bool bol_success = false;
                                for (int i = 0; i < 5; i++)
                                {
                                    if (!bol_success)
                                        try
                                        {
                                            Log.WriteToLog("Info", "Duplicate file detected try={0}", i + 1);
                                            FileStream fs = null;
                                            bool inUse = true;
                                            try
                                            {
                                                fs = new FileStream(fortifyService.PathManipulation(e.FullPath), FileMode.Open, FileAccess.Read, FileShare.None);
                                                inUse = false;
                                            }
                                            catch
                                            {

                                            }
                                            finally
                                            {
                                                if (fs != null)
                                                    fs.Close();
                                                Log.WriteToLog("Info", "file:{0}, inuse={1};", fortifyService.PathManipulation(e.FullPath), inUse.ToString());
                                            }

                                            if (!inUse)
                                            {
                                                File.Copy(fortifyService.PathManipulation(e.FullPath), string.Format("{0}.{1}", path_for_fortify, DateTime.Now.ToString("HHmmss")), true);
                                                Log.WriteToLog("Info", "import file duplicate,{0}; Stop Import Process!! ", targetFilePath);
                                                File.Delete(fortifyService.PathManipulation(e.FullPath));
                                                Log.WriteToLog("Info", "import file duplicate {0}; file deleted!", fortifyService.PathManipulation(e.FullPath));
                                                return;
                                            }
                                            else
                                            {
                                                File.Copy(fortifyService.PathManipulation(e.FullPath), string.Format("{0}.{1}", path_for_fortify, DateTime.Now.ToString("HHmmss")), true);
                                                Log.WriteToLog("Info", "import file duplicate,{0}; Stop Import Process!! ", targetFilePath);
                                                File.Delete(fortifyService.PathManipulation(e.FullPath));
                                                Log.WriteToLog("Info", "import file duplicate {0}; file deleted!", fortifyService.PathManipulation(e.FullPath));
                                                return;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Log.WriteToLog("Error", "File Copy or Delete Error:{0}, Retry!", ex.ToString());
                                        }
                                }
                            }

                            File.Move(path_for_fortify, string.Format("{0}.{1}", path_for_fortify, DateTime.Now.ToString("HHmmss")));
                        }
                        catch (Exception ex)
                        {
                            Log.WriteToLog("Error", "Exception occurred while moving the file. File: {0} Message: {1}", targetFilePath, ex.Message);
                        }
                    }

                    while (!success)
                    {
                        try
                        {
                            Thread.Sleep(1000);
                            if (File.Exists(e.FullPath))
                                File.Move(fortifyService.PathManipulation(e.FullPath), path_for_fortify);
                            else
                                Log.WriteToLog("Info", "File {0} not found", e.FullPath);
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            if (--retries == 0)
                            {
                                Log.WriteToLog("Error", "Exception occurred while moving the file. File: {0} Message: {1}", e.FullPath, ex.Message);
                                break;
                            }
                        }

                        if (success)
                        {
                            Log.WriteToLog("Info", "Move {0} To {1}", e.FullPath, targetFilePath);
                            Log.WriteToLog("Info", "Move file {0} delay...Start.", e.FullPath);
                            Thread.Sleep(1000 * 30);
                            
                            Log.WriteToLog("Info", "Move file {0} delay...End.", e.FullPath);
                        }
                    }
                }
                else if (watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].FileToWatch.Type.Contains("Write"))
                {
                    int i_dely = int.Parse(ConfigurationManager.AppSettings["customer_dely"]);
                    Thread.Sleep(1000 * i_dely);

                    Log.WriteToLog("Debug", "DirectoryToWatch.Type={0}", watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].FileToWatch.Type);
                    if (Path.GetExtension(targetFilePath) != ConfigurationManager.AppSettings["FileExtension"])
                        targetFilePath = targetFilePath + ConfigurationManager.AppSettings["FileExtension"];
                    recordCount = WriteCsvFileToFile(e.FullPath, targetFilePath, watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].FileToWatch.Type);
                    success = true;
                }

                if (!success)
                    return;

                ProgramToExecute programToExecute = watchInformation.DirectoriesToWatch[watcher.Path].FileSetsToWatch[watcher.Filter].ProgramToExecute;
                if (programToExecute != null && programToExecute.Path != string.Empty)
                {
                    AddCommandLineJob(programToExecute.Path, programToExecute.Arguments, targetFilePath, programToExecute.Delay);
                    if (programToExecute.Audit == "true")
                        AuditLog.WriteToLog(e.FullPath, programToExecute.Arguments, "Success", recordCount.ToString());
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "watcher_OnChanged: Message: {0}", ex.Message);
            }
        }

        public int WriteCsvFileToFile(string sourceFilePath, string targetFilePath, string type)
        {
            int recordCount = 0;
            string custID = string.Empty;
            CsvFile csvFile = new CsvFile();
            CustomerService customerService = new CustomerService();
            Fortify fortifyService = new Fortify();

            //20170905 edit by danny 
            //fix SG activity import deadlock problem.
            int idely = int.Parse(ConfigurationManager.AppSettings["activity_dely"]);
            // for import retry dely
            int idely2 = int.Parse(ConfigurationManager.AppSettings["activity_dely2"]);
            int int_try = int.Parse(ConfigurationManager.AppSettings["import_try"]);
            int int_try_limit = int.Parse(ConfigurationManager.AppSettings["import_try_limit"]);

            string str_activity_for_i = ConfigurationManager.AppSettings["act_I"].ToString();
            string str_activity_for_i_account = ConfigurationManager.AppSettings["act_I_account"].ToString();

            //2021,Jan. HK request check and replace specific customer id.
            string str_fix_id = ConfigurationManager.AppSettings["fix_id"].ToString().ToUpper();

            try
            {
                csvFile.Populate(sourceFilePath, false, true);
                recordCount = csvFile.RecordCount;

                string watcherPath = Path.GetDirectoryName(sourceFilePath) + "\\";

                if (type.Contains("Replace"))
                {
                    Log.WriteToLog("Info", "Wait modification for " + idely + " sec");
                    Thread.Sleep(idely * 1000);

                    for (int i = 0; i < csvFile.RecordCount; i++)
                    {
                        bool bol_try = false;

                        for (int j = 0; j < int_try; j++)
                        {
                            try
                            {
                                if (i * j >= int_try_limit) //try 超過一定次數就跳出不要再try
                                    break;
                                if (bol_try == true)
                                    break;
                                if (j > 0)
                                    Thread.Sleep(idely2 * 1000);

                                //2020.09 add account
                                string RecvPay = csvFile[i, 5];                 // 1: Recive, 2:Pay
                                string CustomerID = csvFile[i, 0].Trim();       // Customer ID
                                string Account = csvFile[i, 1].Trim();          // Account ID
                                string Ref = csvFile[i, 8].Trim();          // Account ID

                                string ByOrderCustId = csvFile[i, 37].Trim();   // Originator Customer ID
                                string ByOrder = csvFile[i, 38].Trim();         // Originator Customer Name
                                string ByOrderAddress = csvFile[i, 40].Trim();  // Originator Address
                                string ByOrderCountry = csvFile[i, 44].Trim();  // Originator Country

                                string BeneCustId = csvFile[i, 11].Trim();      // Benbeneficiary Customer ID
                                string Bene = csvFile[i, 12].Trim();            // Benbeneficiary Customer Name
                                string BeneAddress = csvFile[i, 14].Trim();     // Benbeneficiary Address
                                string BeneCountry = csvFile[i, 27].Trim();     // Benbeneficiary Country

                                string Branch = csvFile[i, 55];
                                string Department = csvFile[i, 56];

                                Log.WriteToLog("Debug", "Ori({0}):RecvPay={1}, CustomerID={2}, ByOrderCustId={3}, ByOrder={4}, BeneCustId={5}, Bene={6}",
                                                        (i + 1).ToString().PadLeft(4, '0'), RecvPay, CustomerID, ByOrderCustId, ByOrder, BeneCustId, Bene);

                                /*
                                //JUN-28-2021 Mark
                                if (string.IsNullOrEmpty(CustomerID) || RecvPay == "1")
                                {
                                    if (string.IsNullOrEmpty(BeneCustId) && !string.IsNullOrEmpty(Bene))
                                        csvFile[i, 11] = GetCustID(Bene, Branch, Department, BeneAddress, BeneCountry);
                                }

                                if (string.IsNullOrEmpty(CustomerID) || RecvPay == "2")
                                {
                                    if (string.IsNullOrEmpty(ByOrderCustId) && !string.IsNullOrEmpty(ByOrder))
                                        csvFile[i, 37] = GetCustID(ByOrder, Branch, Department, ByOrderAddress, ByOrderCountry);
                                }
                                */
                                
                                //JUN-28-2021 remove statement
                                if (string.IsNullOrEmpty(BeneCustId) && !string.IsNullOrEmpty(Bene))
                                {
                                    csvFile[i, 11] = GetCustID(Bene, Branch, Department, BeneAddress, BeneCountry, "");
                                }

                                if (string.IsNullOrEmpty(ByOrderCustId) && !string.IsNullOrEmpty(ByOrder))
                                {
                                    csvFile[i, 37] = GetCustID(ByOrder, Branch, Department, ByOrderAddress, ByOrderCountry, "");
                                }
                                
                                if (string.IsNullOrEmpty(CustomerID))
                                {
                                    //JUN-25-2021

                                    if (RecvPay == "1")                 //RecvPay=receive
                                        csvFile[i, 0] = csvFile[i, 11]; //ByOrderCustId
                                    else if (RecvPay == "2")            //RecvPay=pay
                                        csvFile[i, 0] = csvFile[i, 37]; //BeneCustId

                                    /* JUN-25-2021 mark for up side down
                                        if (RecvPay == "1")                 //RecvPay=receive
                                            csvFile[i, 0] = csvFile[i, 37]; //ByOrderCustId
                                        else if (RecvPay == "2")            //RecvPay=pay
                                            csvFile[i, 0] = csvFile[i, 11]; //BeneCustId
                                    */
                                }
                                /* JUN-25-2021 mark for up side down
                                else
                                {
                                    if (Mode == AutoMode.SG)
                                    {
                                        if (RecvPay == "1")                 //RecvPay=receive
                                            csvFile[i, 37] = csvFile[i, 0]; //ByOrderCustId
                                        else if (RecvPay == "2")            //RecvPay=pay
                                            csvFile[i, 11] = csvFile[i, 0]; //BeneCustId
                                    }
                                }
                                */

                                //202004 HK ask to check the empty bene.
                                BeneCustId = csvFile[i, 11];
                                Bene = csvFile[i, 12].Trim();
                                ByOrderCustId = csvFile[i, 37].Trim();
                                ByOrder = csvFile[i, 38].Trim();

                                if (!string.IsNullOrEmpty(BeneCustId) && string.IsNullOrEmpty(Bene))
                                {
                                    csvFile[i, 12] = GetCustName(BeneCustId);
                                }

                                if (!string.IsNullOrEmpty(ByOrderCustId) && string.IsNullOrEmpty(ByOrder))
                                {
                                    csvFile[i, 38] = GetCustName(ByOrderCustId);
                                }

                                //2020.09 get the account from accountowner.
                                if (str_activity_for_i_account == "Y" && !string.IsNullOrEmpty(csvFile[i, 0]) && string.IsNullOrEmpty(Account))
                                {
                                    csvFile[i, 1] = GetAcctID(csvFile[i, 0], Ref);
                                }
                                
                                //2021, Jan.
                                if (!string.IsNullOrEmpty(str_fix_id))
                                {
                                    foreach(string FixID in str_fix_id.Split('|'))
                                    {
                                        //customer id
                                        if (csvFile[i, 0].ToString().ToUpper().Trim().Contains(FixID))
                                        {
                                            csvFile[i, 0] = FixID;
                                        }
                                        /*2021, Jan. after the 5th Monthly meeting mark below
                                         * they want to keep the original activity data
                                        //byorder id
                                        if (csvFile[i, 37].ToString().ToUpper().Trim().Contains(str_fix_id))
                                        {
                                            csvFile[i, 37] = str_fix_id;
                                        }
                                        //bene id
                                        if (csvFile[i, 11].ToString().ToUpper().Trim().Contains(str_fix_id))
                                        {
                                            csvFile[i, 11] = str_fix_id;
                                        }
                                        */
                                    }
                                }

                                Log.WriteToLog("Debug", "Mod({0}):RecvPay={1}, CustomerID={2}, ByOrderCustId={3}, ByOrder={4}, BeneCustId={5}, Bene={6}",
                                                        (i + 1).ToString().PadLeft(4, '0'), RecvPay, csvFile[i, 0], csvFile[i, 37], ByOrder, csvFile[i, 11], Bene);
                                bol_try = true;
                            }
                            catch (Exception e)
                            {
                                Log.WriteToLog("Error", "Activity try {0}, Modify Error:{1}", (j + 1), e.ToString());
                                customerService.InsertLog("", string.Format("BSA Activity import fail, retry {0}, please check the amlimport log for detail message.", (j + 1)));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                recordCount = 0;
                Log.WriteToLog("Error", "WriteCsvFileToFile: {0} To: {1} Message: {2}", sourceFilePath, targetFilePath, ex.Message);
            }
            finally
            {
                using (CsvWriter writer = new CsvWriter())
                {
                    writer.WriteCsv(csvFile, targetFilePath);
                }

                if (type.Contains("Replace"))
                {
                    if(File.Exists(fortifyService.PathManipulation(sourceFilePath + ".mod")))
                    {
                        File.Move(fortifyService.PathManipulation(sourceFilePath + ".mod"), fortifyService.PathManipulation(sourceFilePath + ".mod." + DateTime.Now.ToString("HHmmss")));
                    }

                    File.Copy(fortifyService.PathManipulation(targetFilePath), fortifyService.PathManipulation(sourceFilePath) + ".mod");

                }

                Log.WriteToLog("Info", "Write {0} To {1}", sourceFilePath, targetFilePath);

            }
            return recordCount;
        }

        public string GetCustomerByNameLike(string name)
        {
            string custID = string.Empty;
            try
            {
                string custName = name.Length >= int.Parse(ConfigurationManager.AppSettings["CustNameLen"]) ? name.Substring(0, int.Parse(ConfigurationManager.AppSettings["CustNameLen"])) : name;
                Log.WriteToLog("Debug", "custName={0}", custName);

                CustomerService customerService = new CustomerService();
                List<Customer> customers = customerService.GetByNameLike(custName);
                if (customers != null && customers.Count > 0)
                {
                    Log.WriteToLog("Info", "customers.Count={0}", customers.Count);
                    if (customers.Count == 1)
                    {
                        custID = customers[0].Id;
                        Log.WriteToLog("Debug", "customer.Id={0} customer.Name={1}", custID, customers[0].Name);
                        return custID;
                    }
                    else if (customers.Count > 1)
                    {
                        var foundCustomer = customers.FirstOrDefault(x => x.Name == name);
                        if (foundCustomer != null)
                        {
                            custID = foundCustomer.Id;
                            Log.WriteToLog("Debug", "foundCustomer.Id={0} foundCustomer.Name={1}", custID, foundCustomer.Name);
                            return custID;
                        }
                        //Customer noncustCustomer = customers.SingleOrDefault(x => x.Id.Contains("NONCUST"));
                        //if (noncustCustomer != null)
                        //{
                        //    custID = noncustCustomer.Id;
                        //    Log.WriteToLog("Debug", "noncustCustomer.Id={0} noncustCustomer.Name={1}", custID, noncustCustomer.Name);
                        //    return custID;
                        //}
                        //custID = customers[0].Id;
                        //Log.WriteToLog("Debug", "customer[0].Id={0} customer[0].Name={1}", custID, customers[0].Name);
                        return custID;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "GetCustomerByName: Message: {0}", ex.Message);
                throw ex;
            }
        }

        public string GetCustomerByName(string name, string ownerBranch, string ownerDept, string address, string country, string tin)
        {
            string custID = string.Empty;
            //JUL-26-2021 add parameter TIN for updating the noncust tin field.
            try
            {
                CustomerService customerService = new CustomerService();
                List<Customer> customers = customerService.GetByName(name, CustomerFilter, tin);

                if (customers != null && customers.Count > 0)
                {
                    if (customers.Count == 1)
                    {
                        custID = customers[0].Id;
                        Log.WriteToLog("Debug", "customer.Id={0} customer.Name={1} search from 1 Record.", custID, customers[0].Name);
                        return custID;
                    }
                    else
                    {
                        Customer noncustCustomer = customers.FirstOrDefault(x => x.Id.Contains("NONCUST"));
                        if (noncustCustomer == null)
                        {
                            custID = customers[0].Id;
                            Log.WriteToLog("Debug", "customer[0].Id={0} customer[0].Name={1}", custID, customers[0].Name);
                        }
                        else
                        {
                            custID = noncustCustomer.Id;
                            Log.WriteToLog("Debug", "noncustCustomer.Id={0} noncustCustomer.Name={1}", custID, noncustCustomer.Name);
                        }
                        return custID;
                    }
                }
                else
                {
                    string type = ConfigurationManager.AppSettings["AutoCustomerType"];
                    string indOrBusType = ConfigurationManager.AppSettings["AutoCustomerIndOrBusType"];
                    string unification = tin;
                    return AddCustomer(name, ownerBranch, ownerDept, "DefClass", "PRIMEADMIN",
                                       type, 1,
                                       indOrBusType,
                                       address,
                                       country, 
                                       unification);
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "GetCustomerByName: Message: {0}", ex.Message);
                return null;
            }
        }

        public string AddCustomer(string name, string ownerBranch, string ownerDept, string riskClass, string createOper,
                                  string type, int status, string indOrBusType, string address, string country, string unification)
        {
            try
            {
                CustomerService customerService = new CustomerService();
                string id = customerService.GetSequence("CUST");
                string EntityCustomer = ConfigurationManager.AppSettings["EntityCustomer"];

                if (!string.IsNullOrEmpty(EntityCustomer) && EntityCustomer.Split('|').ToList().Any(s => name.ToUpper().Contains(s)))
                {
                    type = "E";
                    indOrBusType = "B";
                }

                customerService.Insert(id, id, name, ownerBranch, ownerDept, riskClass, createOper, type,
                                       status, indOrBusType, address, country, unification);
                return id;
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "InsertCustomer: Message: {0}", ex.Message);
                return null;
            }
        }

        private string GetCustID(string name, string branch, string department, string address, string country, string tin)
        {
            string CustID = string.Empty;

            if (!string.IsNullOrEmpty(name))
            {
                switch (Mode)
                {
                    case AutoMode.SG:
                        CustID = GetCustomerByName(name, branch, department, address, country, tin);
                        break;
                    case AutoMode.US:
                        CustID = GetCustomerByNameLike(name);
                        break;
                    default:
                        break;
                }
            }

            return CustID;
        }

        private string GetCustID(string strUnificationNo, string strCustName, string strBranch)
        {
            string CustID = string.Empty;
            CustomerService customerService = new CustomerService();

            if (!string.IsNullOrEmpty(strUnificationNo))
            {
                switch (Mode)
                {
                    case AutoMode.SG:
                        CustID = customerService.GetCustIDByUnificationNo(strUnificationNo);
                        /*if (string.IsNullOrEmpty(CustID))
                        {
                            string type = ConfigurationManager.AppSettings["AutoCustomerType"];
                            string indOrBusType = ConfigurationManager.AppSettings["AutoCustomerIndOrBusType"];

                            CustID = AddCustomer(strCustName, strBranch, "AML", "DefClass", "PRIMEADMIN",
                                               type, 1,
                                               indOrBusType,
                                               "",
                                               "",
                                               strUnificationNo);
                        }*/
                        break;
                    case AutoMode.US:
                        CustID = customerService.GetCustIDByUnificationNo(strUnificationNo);
                        break;
                    default:
                        break;
                }
            }

            return CustID;
        }

        private string GetCustName(string str_custid)
        {
            string CustName = "";
            Customer GetCustomer = new Customer();
            CustomerService customerService = new CustomerService();
            GetCustomer = customerService.GetByID(str_custid);
            CustName = GetCustomer.Name;
            return CustName;
        }

        private string GetAcctID(string str_custid, string str_ref)
        {
            string str_acctid = "";
            CustomerService customerService = new CustomerService();
            str_acctid = customerService.GetAccount(str_custid, str_ref);

            return str_acctid;
        }

        private int CheckCustID(string str_custid)
        {
            int icnt = 0;
            CustomerService customerService = new CustomerService();
            DataSet ds = customerService.CheckCustomerID(str_custid);
            icnt =  ds.Tables[0].Rows.Count;

            return icnt;
        }

        private void InsertRelationParty(string strMode, string sourceFilePath, string Log_Path)
        {
            try
            {
                Log.WriteToLog("Info", "RelationParty Import Start.");

                if (File.Exists(Log_Path))
                {
                    if (File.Exists(Log_Path.Replace("log", DateTime.Now.DayOfWeek + ".log")))
                    {
                        File.AppendAllText(fortifyService.PathManipulation(Log_Path.Replace("log", DateTime.Now.DayOfWeek + ".log")), File.ReadAllText(fortifyService.PathManipulation(Log_Path)));
                    }
                    else
                    {
                        File.Move(fortifyService.PathManipulation(Log_Path), fortifyService.PathManipulation(Log_Path.Replace("log", DateTime.Now.DayOfWeek + ".log")));
                    }
                }

                int i_countduplicate = 0;
                int i_countinsert = 0;
                int i_countError = 0;
                int iexists = 0;
                int i_totalcount = 0;
                string strPartyId = "";
                string strpartyname = "";
                string strRelation = "";
                string strRelatedParty = "";
                //string strRelatedParty_Name = "";
                string strRelatedSource = "";
                string strUnificationNo = "";
                CustomerService customerService = new CustomerService();

                customerService.RefreshMode(strMode, "RELATION");
                Log.WriteToLog("Info", "Import Relation refresh Mode={0}", strMode);

                CsvFile csvFile = new CsvFile();
                csvFile.Populate(sourceFilePath, false, true);

                StreamWriter sw = new System.IO.StreamWriter(fortifyService.PathManipulation(Log_Path));
                StringBuilder sb = new StringBuilder();

                sb.AppendLine(string.Format("[RelationParty] RelationParty Import Processed File {0} Start at {1}", Log_Path, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))).AppendLine();

                for (int i = 0; i < csvFile.RecordCount; i++)
                {
                    //202009 INC000000397290 BOT request No. 
                    //Change PartyID and RelatedParty, Remove RelatedParty_Name.
                    //The new RelatedPartyID could be ID or Name.
                    i_totalcount = i_totalcount + 1;
                    strPartyId = csvFile[i, 0];
                    strpartyname = csvFile[i, 0];
                    strRelation = csvFile[i, 1];
                    strRelatedParty = csvFile[i, 2];
                    //strRelatedParty_Name = csvFile[i, 3];
                    strRelatedSource = csvFile[i, 3];
                    //2021.01 add unification number. Priority = partyid > unification no > name.
                    strUnificationNo = csvFile[i, 4];
                    try
                    {
                        if (strRelatedParty != "" && strRelation != "")
                        {
                            //if no relatedpartyid apply noncust algorithm and get customer id.
                            Log.WriteToLog("Info", "GetID count:" + CheckCustID(strPartyId).ToString());
                            if (CheckCustID(strPartyId) == 0)
                            {
                                Log.WriteToLog("Info", "branch no = " + Path.GetFileName(sourceFilePath).Substring(0, 4));
                                
                                //search cust by unification number
                                if (strUnificationNo.Trim() != "")
                                {
                                    Log.WriteToLog("Info", "Could not find the relatedPartyID! Look for unification=" + strUnificationNo.Trim() + ".");
                                    strPartyId = GetCustID(strUnificationNo, strPartyId, Path.GetFileName(sourceFilePath).Substring(0, 4));

                                }
                                else
                                {
                                    Log.WriteToLog("Info", "Could not find the relatedPartyID! Look for Name=" + strPartyId.Trim() + ".");
                                    strPartyId = GetCustID(strPartyId.Trim(), Path.GetFileName(sourceFilePath).Substring(0, 4), "AML", "", "", strUnificationNo);
                                }

                                if (string.IsNullOrEmpty(strPartyId))
                                {
                                    Log.WriteToLog("Info", "Could not find the relatedPartyID! Look for Name=" + strpartyname.Trim());
                                    strPartyId = GetCustID(strpartyname.Trim(), Path.GetFileName(sourceFilePath).Substring(0, 4), "AML", "", "", strUnificationNo);
                                    
                                }
                                Log.WriteToLog("Info", "Found Customer ID=" + strPartyId + ".");
                            }
                            iexists = customerService.CheckRelation(strPartyId, strRelation, strRelatedParty, strRelatedSource);

                            if (iexists > 0)
                            {
                                i_countduplicate = i_countduplicate + 1;
                                Log.WriteToLog("Info", "Import Relation Party duplicate PartyId={0},RelatedParty={1}", strPartyId, strRelatedParty);
                                sb.AppendLine(string.Format("[RelationParty][Duplicates] {0}~{1}~{2}~{3}~{4}", strPartyId, strRelation, strRelatedParty, strRelatedSource, strUnificationNo));
                            }
                            else
                            {
                                i_countinsert = i_countinsert + 1;
                                customerService.InsertRelation(strPartyId, strRelation, strRelatedParty, strRelatedSource);
                                sb.AppendLine(string.Format("[RelationParty][Inserts]    {0}~{1}~{2}~{3}~{4}", strPartyId, strRelation, strRelatedParty, strRelatedSource, strUnificationNo));
                            }
                        }
                        else
                        {
                            i_countError = i_countError + 1;
                            Log.WriteToLog("Error", "Import Relation Lost Data:PartyId={0}, Relation:{1}, RelatedParty:{2}", strPartyId, strRelation, strRelatedParty);
                            sb.AppendLine(string.Format("[RelationParty][Errors]     {0}~{1}~{2}~{3}~{4}", strPartyId, strRelation, strRelatedParty, strRelatedSource, strUnificationNo));
                        }
                    }
                    catch (Exception e)
                    {
                        i_countError = i_countError + 1;
                        i_countinsert = i_countinsert - 1;
                        Log.WriteToLog("Error", "Import Relation PartyId={0} Error:{1}", strPartyId, e.ToString());
                        sb.AppendLine(string.Format("[RelationParty][Errors]     {0}~{1}~{2}~{3}~{4}", strPartyId, strRelation, strRelatedParty, strRelatedSource, strUnificationNo));

                    }
                }

                Log.WriteToLog("Info", "Import Relation counts, Insert={0}, Duplicates={1}, Errors={2}, Total={3}", i_countinsert, i_countduplicate, i_countError, i_totalcount);
                customerService.InsertLog2("Imp", "Event", "", string.Format("Import Relation counts, Insert={0}, Duplicates={1}, Errors={2}, Total={3}", i_countinsert, i_countduplicate, i_countError, i_totalcount));
                sb.AppendLine().AppendLine(string.Format("[RelationParty][Summary] Insert={0}, Duplicates={1}, Errors={2}, Total={3}", i_countinsert, i_countduplicate, i_countError, i_totalcount)).AppendLine();
                sb.AppendLine(string.Format("[RelationParty] RelationParty Import End at {0}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                Log.WriteToLog("Info", "RelationParty Import End.");

                sw.WriteLine(sb.ToString());
                sw.Close();
            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "ImportRelationParty: Message: {0}", ex.Message);

                Log.WriteToLog("Info", "RelationParty Import End.");
            }
        }

        private void InsertPartyID(string strMode, string sourceFilePath, string Log_Path)
        {
            try
            {
                Log.WriteToLog("Info", "PartyID Import Start.");
                
                if (File.Exists(Log_Path))
                {
                    if (File.Exists(Log_Path.Replace("log", DateTime.Now.DayOfWeek + ".log")))
                    {
                        File.AppendAllText(fortifyService.PathManipulation(Log_Path.Replace("log", DateTime.Now.DayOfWeek + ".log")), File.ReadAllText(fortifyService.PathManipulation(Log_Path)));
                    }
                    else
                    {
                        File.Move(fortifyService.PathManipulation(Log_Path), fortifyService.PathManipulation(Log_Path.Replace("log", DateTime.Now.DayOfWeek + ".log")));
                    }
                }

                CustomerService customerService = new CustomerService();
                customerService.RefreshMode(strMode, "PARTYID");

                CsvFile csvFile = new CsvFile();
                csvFile.Populate(sourceFilePath, false, true);

                StreamWriter sw = new System.IO.StreamWriter(fortifyService.PathManipulation(Log_Path));
                StringBuilder sb = new StringBuilder();

                sb.AppendLine(string.Format("[PartyID] PartyID Import Processed File {0} Start at {1}", Log_Path, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))).AppendLine();
                
                string str_custid = string.Empty;
                string str_idtype = string.Empty;
                string str_idnumber = string.Empty;
                string str_issueagcy = string.Empty;
                string str_issueplace = string.Empty;
                string str_issuecountry = string.Empty;
                string str_issuedate = string.Empty;
                string str_expiredate = string.Empty;
                int iexists = 0;
                int i_countduplicate = 0;
                int i_countinsert = 0;
                int i_countError = 0;
                int i_totalcount = 0;
                int i_countempty = 0;

                for (int i = 0; i < csvFile.RecordCount; i++)
                {
                    i_totalcount = i_totalcount + 1;

                    str_custid = csvFile[i, 0].Trim();
                    str_idtype = csvFile[i, 1].Trim();
                    str_idnumber = csvFile[i, 2].Trim();
                    str_issueagcy = csvFile[i, 3].Trim();
                    str_issueplace = csvFile[i, 4].Trim();
                    str_issuecountry = csvFile[i, 5].Trim();
                    str_issuedate = csvFile[i, 6].Trim();
                    str_expiredate = csvFile[i, 7].Trim();

                    try
                    {
                        if (str_custid != "" && str_idnumber != "" && str_expiredate != "" && str_expiredate != "00000000")
                        {
                            //check if ID exists
                            iexists = customerService.CheckPartyID(str_custid, str_idtype);
                            //insert data
                            if (iexists > 0)
                            {
                                i_countduplicate = i_countduplicate + 1;
                                sb.AppendLine(string.Format("[PartyID][Duplicates] {0}~{1}~{2}", str_custid, str_idtype, str_idnumber));

                            }
                            else
                            {
                                i_countinsert = i_countinsert + 1;
                                customerService.InsertPartyID(str_custid, str_idtype, str_idnumber, str_issueagcy, str_issueplace, str_issuecountry, str_issuedate, str_expiredate);
                                sb.AppendLine(string.Format("[PartyID][Insert] {0}~{1}~{2}", str_custid, str_idtype, str_idnumber));
                            }
                        }
                        else
                        {
                            i_countempty = i_countempty + 1;
                            sb.AppendLine(string.Format("[PartyID][Empty] {0}~{1}~{2}", str_custid, str_idtype, str_idnumber));
                        }
                    }
                    catch(Exception e)
                    {
                        i_countError = i_countError + 1;
                        i_countinsert = i_countinsert - 1;
                        sb.AppendLine(string.Format("[PartyID][ERROR] {0}~{1}~{2}", str_custid, str_idtype, str_idnumber));
                        Log.WriteToLog("Error", "Import Party ID={0} Error:{1}", str_custid, e.ToString());
                    }
                }

                sb.AppendLine().AppendLine(string.Format("[PartyID][Summary] Insert={0}, Duplicates={1}, Empty={2}, Errors={3}, Total={4}", i_countinsert, i_countduplicate, i_countempty, i_countError, i_totalcount)).AppendLine();
                customerService.InsertLog2("Imp", "Event", "", string.Format("Import PartyID counts, Insert={0}, Duplicates={1}, Empty={2}, Errors={3}, Total={4}", i_countinsert, i_countduplicate, i_countempty, i_countError, i_totalcount));

                sb.AppendLine(string.Format("[PartyID] PartyID Import Process End at {0}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))).AppendLine();
                
                Log.WriteToLog("Info", "PartyID Import End.");
                
                sw.WriteLine(sb.ToString());
                sw.Close();

            }
            catch(Exception ex)
            {
                Log.WriteToLog("Error", "PartyID: Message: {0}", ex.Message);
            }
        }

        private void InsertWCCountry(string sourceFilePath, string Log_Path)
        {
            //SEP.2021 Get WC country file and update.
            try
            {
                Log.WriteToLog("Info", "WorldCheck Country Import Start.");

                int createdCount = 0;
                DataTable dataTable_wcimp = new DataTable();

                if (File.Exists(Log_Path))
                {
                    if (File.Exists(Log_Path.Replace("log", DateTime.Now.DayOfWeek + ".log")))
                    {
                        File.AppendAllText(fortifyService.PathManipulation(Log_Path.Replace("log", DateTime.Now.DayOfWeek + ".log")), File.ReadAllText(fortifyService.PathManipulation(Log_Path)));
                    }
                    else
                    {
                        File.Move(fortifyService.PathManipulation(Log_Path), fortifyService.PathManipulation(Log_Path.Replace("log", DateTime.Now.DayOfWeek + ".log")));
                    }
                }

                CustomerService customerService = new CustomerService();

                using (var textFieldParser = new TextFieldParser(sourceFilePath, System.Text.Encoding.GetEncoding(28605)))
                {
                    textFieldParser.TextFieldType = FieldType.Delimited;
                    textFieldParser.Delimiters = new[] { "," };
                    while (!textFieldParser.EndOfData)
                    {
                        createdCount++;

                        if (createdCount == 1)
                        {
                            string[] List_header = textFieldParser.ReadFields();
                            foreach (string strHeader in List_header)
                            {
                                dataTable_wcimp.Columns.Add(strHeader);
                            }
                        }


                        dataTable_wcimp.Rows.Add(textFieldParser.ReadFields());
                        //dr = dataTable.Rows[createdCount - 1];


                        Log.WriteToLog("Info", "total row counts:" + dataTable_wcimp.Rows.Count);
                    }

                    //query wc_country table

                    DataTable dt_wcnow = new DataTable();

                    if (dt_wcnow != null && dt_wcnow.Rows.Count != 0)
                    {
                        foreach (DataRow dr_wcimport in dataTable_wcimp.Rows)
                        {
                            if (dt_wcnow.Select(" code = '" + dr_wcimport["ISO3166-1"] + "' ").Length == 0)
                            {
                                //wc_import country code not in wc_now country code
                                //insert

                            }
                            else
                            {
                                //check country name
                                if (dt_wcnow.Select(" WorldCheck_Country = '" + dr_wcimport["CountryName"] + "' ").Length == 0)
                                {
                                    //same country code, but country name no match
                                    //update country name

                                }
                            }
                        }

                    }
                }

                StreamWriter sw = new System.IO.StreamWriter(fortifyService.PathManipulation(Log_Path));
                StringBuilder sb = new StringBuilder();

                sb.AppendLine(string.Format("[WC Country] WorldCheck Country Import Processed File {0} Start at {1}", Log_Path, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))).AppendLine();

                string str_code = "";
                string str_name = "";

                sw.WriteLine(sb.ToString());
                sw.Close();

            }
            catch (Exception ex)
            {
                Log.WriteToLog("Error", "PartyID: Message: {0}", ex.Message);
            }
        }
    }
}
